/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.SwingUtilities;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.GenotypeFilter;
import fr.pasteur.sysbio.Gravity.API.Interaction;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.API.VariantsUtils;
import fr.pasteur.sysbio.Gravity.API.utils.Partitionner;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import fr.pasteur.sysbio.Gravity.impl.internal.component.NetworkExpansionPanel;

/**
 * @author Freddy Cliquet
 * @version 1.0
 *
 */
public class CompleteFamilyNetworkCreationTask extends AbstractTask {

	boolean _update;
	String family;
	List<Patient> affected, unaffected;
	List<GenotypeFilter> quality;
	boolean all, recessive, compound, dominant;
	int _nbVariants = 0;
	Set<String> _geneList = null;
	
	public CompleteFamilyNetworkCreationTask(boolean update, Set<String> geneList) {
		_update = update;
		_geneList = geneList;
		family = ServicesUtil.controlPanel.getSelectedFamily();
		affected = ServicesUtil.controlPanel.getSelectedAffected();
		unaffected = ServicesUtil.controlPanel.getSelectedUnaffected();
		quality = ServicesUtil.controlPanel.getQualityFilters();
		all = ServicesUtil.controlPanel.keepAllVariants4CompleteFamilies();
		recessive = ServicesUtil.controlPanel.considerHomAutosomalRecessive();
		dominant = ServicesUtil.controlPanel.considerDominant();
		compound = ServicesUtil.controlPanel.considerHetCompound();
		
	}
	
	@Override
	public void run(TaskMonitor arg0) throws Exception {
		arg0.setTitle("Updating view");
		arg0.setStatusMessage("retrieving genes");
		arg0.setProgress(0);
		
		Set<Interaction> interactions = ServicesUtil.ppi.getInteractions( ServicesUtil.controlPanel.getSelectedInteractionsTypes() );
		
		Set<String> genesNames;
		if( _geneList != null && _geneList.size()>0 ){
			genesNames = _geneList;
		}else{
			if( ServicesUtil.controlPanel.runOnAllGenes() ){
				genesNames = ServicesUtil.variantDatabase.getAllGenesNames();
				genesNames.addAll( ServicesUtil.ppi.getAllGenes() );
			}else{
				genesNames = ServicesUtil.pathways.getGenes( ServicesUtil.controlPanel.getSelectedPathways() );
				switch(ServicesUtil.controlPanel.getNetworkExpansionLevel()){
					case NetworkExpansionPanel.NO_EXPANSION:
						break;
					case NetworkExpansionPanel.SMART_EXPANSION:
						Set<String> genesNamesTmp = new HashSet<String>(genesNames);
						Partitionner p = new Partitionner(ServicesUtil.ppi, ServicesUtil.controlPanel.getSelectedInteractionsTypes() );
						p.buildPartitions( genesNames );
						p.sort();
						genesNamesTmp.addAll( p.connectAll() );
						genesNames = genesNamesTmp;
						break;
					case NetworkExpansionPanel.FIRST_NEIGHBOURS_EXPANSION:
					case NetworkExpansionPanel.SECOND_NEIGHBOURS_EXPANSION:
						for(int level=0; level<ServicesUtil.controlPanel.getNetworkExpansionLevel() ; level++){
							Set<String> genesNamesNew = new HashSet<String>(genesNames);
							for(Interaction i:interactions){
								if( genesNames.contains(i.symbol1) )
									genesNamesNew.add(i.symbol2);
								if( genesNames.contains(i.symbol2) )
									genesNamesNew.add(i.symbol1);
							}
							genesNames = genesNamesNew;
						}
						break;
				}
			}		
		}
		Map<String,CyNode> myNodes = new HashMap<String, CyNode>();
		
		CyNetwork myNet;
		if( _update && ServicesUtil.lastNetwork!=null){
			myNet = ServicesUtil.lastNetwork;
			myNet.getRow(myNet).set(CyNetwork.NAME, family+" network");
			myNet.removeEdges(myNet.getEdgeList());
			myNet.removeNodes(myNet.getNodeList());
			ServicesUtil.eventHelper.flushPayloadEvents();
		}else{
			myNet = ServicesUtil.cyNetworkFactory.createNetwork();
			ServicesUtil.lastNetwork = myNet;
			myNet.getRow(myNet).set(CyNetwork.NAME, family+" network");
			myNet.getDefaultNodeTable().createColumn("impact", String.class, false, "");
			myNet.getDefaultNodeTable().createColumn("severity", String.class, false, "");
			myNet.getDefaultNodeTable().createColumn("de novo", Boolean.class, false, false);
			myNet.getDefaultNodeTable().createColumn("cadd", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd father", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd mother", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd de novo", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd homalt", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd unknown", Double.class, false, 0d);
			
			myNet.getDefaultEdgeTable().createColumn("confidence", Integer.class, false, 0);
		}
		
		if( ServicesUtil.controlPanel.displayAllGenesOfInterest() || ( _geneList != null && _geneList.size()>0 ) ){
			arg0.setStatusMessage("create nodes");
			
			for(String s:genesNames){
				CyNode node = myNet.addNode();
				myNet.getRow(node).set("impact", "");
				myNet.getRow(node).set("severity", "");
				myNet.getRow(node).set("de novo", false);
				myNet.getRow(node).set("cadd", 0d);
				myNet.getRow(node).set("cadd father", 0d);
				myNet.getRow(node).set("cadd mother", 0d);
				myNet.getRow(node).set("cadd de novo", 0d);
				myNet.getRow(node).set("cadd homalt", 0d);
				myNet.getRow(node).set("cadd unknown", 0d);
				myNet.getRow(node).set(CyNetwork.NAME, s);
				myNodes.putIfAbsent(s, node);
			}
		}
		
		arg0.setStatusMessage("map variants");
		
		List<Gene> genes = ServicesUtil.variantDatabase.getGenesCarryingVariant(ServicesUtil.controlPanel.getGenericFilters());
		
//		System.out.println(allAffected+" = "+recessive+" = "+allRecessive);
		
		for(Gene g:genes){
			if( 	   (dominant && g.carryAFilteredVariant(affected, unaffected, quality, all)) 
					|| ( recessive && g.carryAFilteredRecessiveVariant(affected, unaffected, quality, all)) 
					|| ( compound && g.carryCompoundHomozygousVariants(affected, unaffected, quality, all) ) ){
				
				CyNode node = myNodes.get(g.getName());
				List<? extends Variant> vDom = null;
				List<? extends Variant> vRec = null;
				List<? extends Variant> vCH = null;
				Set<Variant> vAll = null;
				
				if( node != null ){
					if(dominant)
						vDom = g.getFilteredVariants(affected, unaffected, quality, all);
					if(recessive)
						vRec = g.getFilteredRecessiveVariants(affected, unaffected, quality, all);
					if(compound)
						vCH = g.getCompoundHomozygousVariants(affected, unaffected, quality, all);
					vAll = (Set<Variant>) VariantsUtils.mergeVariantsLists(vDom, vRec, vCH);
					_nbVariants += vAll.size();
					myNet.getRow(node).set("impact", VariantsUtils.getHighestImpact(vAll).impact());
					myNet.getRow(node).set("severity", VariantsUtils.getHighestImpact(vAll).impactSeverity());
					myNet.getRow(node).set(CyNetwork.NAME, g.getName());
				}else{
					if( genesNames.contains(g.getName()) ){
						if(dominant)
							vDom = g.getFilteredVariants(affected, unaffected, quality, all);
						if(recessive)
							vRec = g.getFilteredRecessiveVariants(affected, unaffected, quality, all);
						if(compound)
							vCH = g.getCompoundHomozygousVariants(affected, unaffected, quality, all);
						vAll = (Set<Variant>) VariantsUtils.mergeVariantsLists(vDom, vRec, vCH);
						_nbVariants += vAll.size();
						node = myNet.addNode();
						myNet.getRow(node).set("impact", VariantsUtils.getHighestImpact(vAll).impact());
						myNet.getRow(node).set("severity", VariantsUtils.getHighestImpact(vAll).impactSeverity());
						myNet.getRow(node).set(CyNetwork.NAME, g.getName());
						myNodes.putIfAbsent(g.getName(), node);
					}
				}
			}
		}
		
		arg0.setStatusMessage("create edges");
		
		for( Interaction i:interactions ){
			CyNode node1 = myNodes.get(i.symbol1);
			CyNode node2 = myNodes.get(i.symbol2);
			if(node1!=null && node2!=null){
				CyEdge edge = myNet.addEdge(node1, node2, false);
				myNet.getRow(edge).set("confidence", ServicesUtil.ppi.getInteractionWeight(i, ServicesUtil.controlPanel.getSelectedInteractionsTypes()));
				myNet.getRow(edge).set(CyNetwork.NAME, i.symbol1+" - "+i.symbol2);
			}
		}
		
		if( !_update ){
			arg0.setTitle("add network");
			ServicesUtil.cyNetworkManager.addNetwork(myNet);
		}
		
		String working_pathway = "all genes";
		if( !ServicesUtil.controlPanel.runOnAllGenes() ){
			List<String> pathways = ServicesUtil.controlPanel.getSelectedPathways();
			if( pathways.size() > 2 )
				working_pathway = "multiple pathways";
			else{
				if( pathways.size()==1 ){
					working_pathway = pathways.get(0);
				}else{
					working_pathway = pathways.get(0)+" and "+pathways.get(1);
				}
			}
		}
		myNet.getRow(myNet).set(CyNetwork.NAME, family+" ("+myNet.getNodeCount()+" genes; "+_nbVariants+" variants) on "+working_pathway);
		
		TaskIterator ti = new TaskIterator(new NetworkViewTask(myNet));
		this.insertTasksAfterCurrentTask(ti);
		
	}

}
