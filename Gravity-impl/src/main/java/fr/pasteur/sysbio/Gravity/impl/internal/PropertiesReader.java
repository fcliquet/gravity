/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal;

import org.cytoscape.property.AbstractConfigDirPropsReader;
import org.cytoscape.property.CyProperty;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
class PropertiesReader extends AbstractConfigDirPropsReader {

	public PropertiesReader(String name, String propFileName) {
		super(name, propFileName, CyProperty.SavePolicy.CONFIG_DIR);
	}

}
