package fr.pasteur.sysbio.Gravity.impl.internal;

import java.util.Properties;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.application.swing.CytoPanelState;
import org.cytoscape.event.CyEventHelper;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.events.RowsSetListener;
import org.cytoscape.property.CyProperty;
import org.cytoscape.service.util.AbstractCyActivator;
import org.cytoscape.task.read.LoadVizmapFileTaskFactory;
import org.cytoscape.util.swing.FileUtil;
import org.cytoscape.util.swing.IconManager;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.work.SynchronousTaskManager;
import org.cytoscape.work.swing.DialogTaskManager;
import org.cytoscape.work.swing.PanelTaskManager;
import org.osgi.framework.BundleContext;

import fr.pasteur.sysbio.Gravity.API.PPI;
import fr.pasteur.sysbio.Gravity.API.Pathways;
import fr.pasteur.sysbio.Gravity.impl.internal.task.GravityTaskFactory;

public class CyActivator extends AbstractCyActivator {

	public final static String APP_NAME = "Gravity";
	
	@SuppressWarnings("unchecked")
	@Override
	public void start(BundleContext context) throws Exception {
		
		ServicesUtil.dialogTaskManager = getService(context, DialogTaskManager.class);
		ServicesUtil.cyNetworkFactory = getService(context, CyNetworkFactory.class);
		ServicesUtil.cyNetworkManager = getService(context, CyNetworkManager.class);
		ServicesUtil.cyNetworkViewFactory = getService(context, CyNetworkViewFactory.class);
		ServicesUtil.cyNetworkViewManager = getService(context, CyNetworkViewManager.class);
		ServicesUtil.cyLayoutAlgorithmManager = getService(context, CyLayoutAlgorithmManager.class);
		ServicesUtil.panelTaskManager = getService(context, PanelTaskManager.class);
		ServicesUtil.loadVizmapFileTaskFactory = getService(context, LoadVizmapFileTaskFactory.class);
		ServicesUtil.visualMappingManager = getService(context, VisualMappingManager.class);
		ServicesUtil.synchronousTaskManager = getService(context, SynchronousTaskManager.class);
		ServicesUtil.eventHelper = getService(context, CyEventHelper.class);
		ServicesUtil.applicationManager = getService(context, CyApplicationManager.class);
		
		ServicesUtil.fileUtil = getService(context, FileUtil.class);
		ServicesUtil.swingApplication = getService(context, CySwingApplication.class);
		ServicesUtil.swingApplication.getCytoPanel(CytoPanelName.EAST).setState(CytoPanelState.DOCK);
		ServicesUtil.iconManager = getService(context, IconManager.class);
		
		PropertiesReader propsPathways = new PropertiesReader("Gravity.pathways", "Gravity.pathways.props");
		Properties propsPathwaysProps = new Properties();
		propsPathwaysProps.setProperty("cyPropertyName", "Gravity.pathways.props");
		registerAllServices(context, propsPathways, propsPathwaysProps);
		
		PropertiesReader propsInteractions = new PropertiesReader("Gravity.interactions", "Gravity.interactions.props");
		Properties propsInteractionsProps = new Properties();
		propsInteractionsProps.setProperty("cyPropertyName", "Gravity.interactions.props");
		registerAllServices(context, propsInteractions, propsInteractionsProps);
		
		PropertiesReader propsFilters = new PropertiesReader("Gravity.filters", "Gravity.filters.props");
		Properties propsFiltersProps = new Properties();
		propsFiltersProps.setProperty("cyPropertyName", "Gravity.filters.props");
		registerAllServices(context, propsFilters, propsFiltersProps);
		
		PropertiesReader propsQualities = new PropertiesReader("Gravity.qualities", "Gravity.qualities.props");
		Properties propsQualitiesProps = new Properties();
		propsQualitiesProps.setProperty("cyPropertyName", "Gravity.qualities.props");
		registerAllServices(context, propsQualities, propsQualitiesProps);
		
		PropertiesReader propsGravity = new PropertiesReader("Gravity.gravity", "Gravity.gravity.props");
		Properties propsGravityProps = new Properties();
		propsGravityProps.setProperty("cyPropertyName", "Gravity.gravity.props");
		registerAllServices(context, propsGravity, propsGravityProps);
		
		ServicesUtil.pathwaysProperties = getService(context, CyProperty.class, "(cyPropertyName=Gravity.pathways.props)");
		ServicesUtil.interactionsProperties = getService(context, CyProperty.class, "(cyPropertyName=Gravity.interactions.props)");
		ServicesUtil.filtersProperties = getService(context, CyProperty.class, "(cyPropertyName=Gravity.filters.props)");
		ServicesUtil.qualitiesProperties = getService(context, CyProperty.class, "(cyPropertyName=Gravity.qualities.props)");
		ServicesUtil.gravityProperties = getService(context, CyProperty.class, "(cyPropertyName=Gravity.gravity.props)");
		
		ServicesUtil.ppi = new PPI();
		ServicesUtil.ppi.load();
		ServicesUtil.pathways = new Pathways();
		
		ServicesUtil.variantDatabase = null;
		
		ServicesUtil.gravityTaskFactory = new GravityTaskFactory();
		
	    ServicesUtil.controlPanel = new ControlPanel();
	    ServicesUtil.controlPanel.setName(APP_NAME);
	    registerService(context, ServicesUtil.controlPanel, CytoPanelComponent.class, new Properties());
	    
	    ServicesUtil.legendPanel = new LegendPanel();
	    ServicesUtil.legendPanel.setName(APP_NAME+" legend");
	    registerService(context, ServicesUtil.legendPanel, CytoPanelComponent.class, new Properties());
	    
	    ServicesUtil.resultGeneDetailPanel = new ResultGeneDetailPanel();
	    ServicesUtil.resultGeneDetailPanel.setName("Gene details");
	    registerService(context, ServicesUtil.resultGeneDetailPanel, CytoPanelComponent.class, new Properties());
	    registerService(context, ServicesUtil.resultGeneDetailPanel, RowsSetListener.class, new Properties());
	    
	    ServicesUtil.resultCohortDetail = new ResultCohortDetail();
	    ServicesUtil.resultCohortDetail.setName("Cohort details");
	    registerService(context, ServicesUtil.resultCohortDetail, CytoPanelComponent.class, new Properties());
	    registerService(context, ServicesUtil.resultCohortDetail, RowsSetListener.class, new Properties());
	    
	    ServicesUtil.swingApplication.getCytoPanel(CytoPanelName.EAST).setSelectedIndex(0);
	}

}
