/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;

import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;

/**
 * @author Freddy Cliquet
 * @version 1.0
 *
 */
public class ApplyLayoutTask extends AbstractTask {

	private CyNetworkView _view;
	
	public ApplyLayoutTask(CyNetworkView view){
		_view = view;
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.work.AbstractTask#run(org.cytoscape.work.TaskMonitor)
	 */
	@Override
	public void run(TaskMonitor arg0) throws Exception {
		arg0.setStatusMessage("create layout task");

		// force-directed-cl
		// grid
		CyLayoutAlgorithm layout = ServicesUtil.cyLayoutAlgorithmManager.getDefaultLayout();//ServicesUtil.cyLayoutAlgorithmManager.getLayout("grid");
		if(layout == null)
			layout = ServicesUtil.cyLayoutAlgorithmManager.getDefaultLayout();
		
		
		
		TaskIterator ti = new TaskIterator(new LoadVisualPropertiesTask(_view));
		this.insertTasksAfterCurrentTask(ti);
		
		_view.updateView();
		
		TaskIterator itr = layout.createTaskIterator(_view, layout.createLayoutContext(), CyLayoutAlgorithm.ALL_NODE_VIEWS, null);
		insertTasksAfterCurrentTask(itr);
		
	}

}
