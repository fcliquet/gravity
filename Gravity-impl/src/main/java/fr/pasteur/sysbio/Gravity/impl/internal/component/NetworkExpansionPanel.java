/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import net.miginfocom.swing.MigLayout;

/**
 * Component for the selection of the type of network expansion to apply.
 * 
 * @author Freddy Cliquet
 * @version 1.0
 */
public class NetworkExpansionPanel extends JPanel {

	private static final long serialVersionUID = -5064946698587746452L;
	
	/**
	 * No expansion applied
	 */
	public final static int NO_EXPANSION = 0;
	
	/**
	 * Expand to the first neighbours of all original genes
	 */
	public final static int FIRST_NEIGHBOURS_EXPANSION = 1;
	
	/**
	 * Expand to the second neighbours of the original genes
	 */
	public final static int SECOND_NEIGHBOURS_EXPANSION = 2;
	
	/**
	 * Smart expansion of the network, this relate to the Partitioning of the network
	 */
	public final static int SMART_EXPANSION = 3;
	
	private String _name = "Network expansion";	
	private ButtonGroup _group;
	private JRadioButton _radioNone, _radioExpandOne, _radioExpandTwo, _radioSmartExpand;
	
	/**
	 * Create and initialize the networkExpansionPanel
	 */
	public NetworkExpansionPanel(){
		initComponents();
	}
	
	private void initComponents(){
		setBorder(BorderFactory.createTitledBorder(_name));
		setLayout(new MigLayout("fill, ins 1, gap 1 1", "", "[min!]"));
		
		_group = new ButtonGroup();
		_radioNone = new JRadioButton("None", true);
		add(_radioNone, "wrap");
		_group.add(_radioNone);
		_radioExpandOne = new JRadioButton("First neighbours");
		add(_radioExpandOne, "wrap");
		_group.add(_radioExpandOne);
		_radioExpandTwo = new JRadioButton("Second neighbours");
		add(_radioExpandTwo, "wrap");
		_group.add(_radioExpandTwo);
		_radioSmartExpand = new JRadioButton("Smart expand");
		add(_radioSmartExpand, "wrap");
		_group.add(_radioSmartExpand);
	}
	
	/**
	 * Access the expansion level of the network requested by the user.
	 * It is returned as a int code stored in constants variable within NetworkExpansionPanel.
	 * The options are NO_EXPANSION, FIRST_NEIGHBOURS_EXPANSION and SECOND_NEIGHBOURS_EXPANSION.
	 * 
	 * @return expansion level
	 */
	public int getExpansionLevel(){
		if( _radioNone.isSelected() )
			return NO_EXPANSION;
		if( _radioExpandOne.isSelected() )
			return FIRST_NEIGHBOURS_EXPANSION;
		if( _radioExpandTwo.isSelected() )
			return SECOND_NEIGHBOURS_EXPANSION;
		if( _radioSmartExpand.isSelected() )
			return SMART_EXPANSION;
		return NO_EXPANSION;
	}
	
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		JFrame f = new JFrame();
//		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		NetworkExpansionPanel nep = new NetworkExpansionPanel();
//		f.getContentPane().add( nep );
//		f.validate();
//		f.pack();
//		f.setVisible(true);
//	}

}
