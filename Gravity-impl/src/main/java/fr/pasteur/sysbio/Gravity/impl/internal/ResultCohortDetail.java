/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultRowSorter;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTableUtil;
import org.cytoscape.model.events.RowsSetEvent;
import org.cytoscape.model.events.RowsSetListener;
import org.cytoscape.util.swing.FileChooserFilter;
import org.cytoscape.util.swing.FileUtil;
import org.cytoscape.util.swing.IconManager;

import fr.pasteur.sysbio.Gravity.API.GenotypeUtils;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.IconTableCellRenderer;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.TextIcon;
import net.miginfocom.swing.MigLayout;

/**
 * @author Freddy Cliquet
 * @version 1.1
 */
public class ResultCohortDetail extends JPanel implements CytoPanelComponent, RowsSetListener, ActionListener {

	private static final long serialVersionUID = 8732232405665198711L;
	private CohortTableModel _model;
	private JTable _table;
	private JCheckBox _checkChildrenOnly;
	private JButton _buttonExport;
	private JComboBox<String> listIndis, listFormats;

	public ResultCohortDetail() {
		setPreferredSize(new Dimension(300, getMinimumSize().height));
		setLayout(new MigLayout("fill, ins 0, gap 0 5", "[]", "[min!][][min!]"));
		_checkChildrenOnly = new JCheckBox("only patients with parents");
		add(_checkChildrenOnly, "wrap");
		_checkChildrenOnly.setEnabled(false);
		_checkChildrenOnly.setActionCommand("check with child only");
		_checkChildrenOnly.addActionListener(this);
		
		_model = new CohortTableModel();
		_table = new JTable(_model);
		_table.setTableHeader( new JTableHeader(_table.getColumnModel()) {
			private static final long serialVersionUID = 1L;

			@Override public Dimension getPreferredSize() {
				    Dimension d = super.getPreferredSize();
				    d.height = 80;
				    return d;
				  }
				} );
		_table.getTableHeader().setDefaultRenderer(new IconTableCellRenderer());
		_table.setAutoCreateRowSorter(true);
		
		TableColumn column = null;
		for (int i = 0; i < _table.getColumnCount(); i++) {
		    column = _table.getColumnModel().getColumn(i);
		    if (i == 0) {
		        column.setPreferredWidth(135); //third column is bigger
		    } else {
		    	if( i == 4 )
		    		column.setPreferredWidth(40);
		    	else
		    		if( i == 5 )
		    			column.setPreferredWidth(24);
		    		else
		    			column.setPreferredWidth(20);
		    }
		}
		
		JScrollPane scrollPane = new JScrollPane(_table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.getVerticalScrollBar().setUnitIncrement(30);
		_table.setAutoscrolls(true);
		add(scrollPane, "grow, push, wrap");
		_table.setFillsViewportHeight(true);
		_table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//		_table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		_table.addMouseListener(new MouseAdapter(){
			public void mousePressed(MouseEvent me) {
		        JTable table =(JTable) me.getSource();
		        Point p = me.getPoint();
		        int row = table.convertRowIndexToModel(table.rowAtPoint(p));
		        
		        if (me.getClickCount() == 2 && row!=-1) {
		        	System.out.println("Switching to \t"+_model.getValueAt(row, 0));
		        	ServicesUtil.controlPanel.updateToAnotherIndividual((String)_model.getValueAt(row, 0));
		        }
		    }
		});
		
		JPanel pExport = new JPanel(new MigLayout("fill, ins 0, gap 0"));
		pExport.add(new JLabel("Export variants for the selected genes"), "span, wrap");
		pExport.add(new JLabel("Individuals: "));
		
		listIndis = new JComboBox<String>();
		listIndis.addItem("all");
		listIndis.addItem("selected family");
		listIndis.addItem("selected patient");
		pExport.add(listIndis, "growx, wrap");
		
		pExport.add(new JLabel("Format: "));
		
		listFormats = new JComboBox<String>();
		listFormats.addItem("compact");
		listFormats.addItem("full");
		listFormats.addItem("cnv");
		pExport.add(listFormats, "growx, wrap");
		
		add(pExport, "growx, pushx, wrap");
		
		_buttonExport = new JButton("Export CSV");
		_buttonExport.setActionCommand("export CSV");
		_buttonExport.addActionListener(this);
		TextIcon ti = new TextIcon(_buttonExport, IconManager.ICON_ARROW_RIGHT+""+IconManager.ICON_FILE_TEXT_O);
		ti.setFont(ServicesUtil.iconManager.getIconFont(16f));
		_buttonExport.setIcon(ti);
		add(_buttonExport, "growx, pushx");
	}
	
	public void enableChildrenOnlyOption(){
		_checkChildrenOnly.setEnabled(true);
	}
	
	public boolean isChildrenOnly(){
		return _checkChildrenOnly.isSelected();
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.model.events.RowsSetListener#handleEvent(org.cytoscape.model.events.RowsSetEvent)
	 */
	@Override
	public void handleEvent(RowsSetEvent event) {
		if (!event.containsColumn(CyNetwork.SELECTED))
			return;
		if (event.getSource() != ServicesUtil.lastNetwork.getDefaultNodeTable())
			return;
		updateTableContent();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void updateTableContent(){
		List<CyNode> nodes = CyTableUtil.getNodesInState(ServicesUtil.lastNetwork, CyNetwork.SELECTED, true);
		List<Patient> patients;
		if(_checkChildrenOnly.isSelected()){
			patients = ServicesUtil.variantDatabase.getPatientsWithParents();
		}else{
			patients = ServicesUtil.variantDatabase.getPatients();
		}
		_model.clear();
		DefaultRowSorter sorter = (DefaultRowSorter) _table.getRowSorter();
		
		if( nodes.size() == 0){
			revalidate();
			repaint();
			return;
		}
		
		if( nodes.size() >= 1 ){
			for(Patient p:patients){
				Double maxCadd = null; 
				int nbVariants = 0;
				String severity = "OTHER";
				for(CyNode node:nodes){
					String geneName = ServicesUtil.lastNetwork.getRow( node ).get(CyNetwork.NAME, String.class);
					if( maxCadd == null ){
						maxCadd = ServicesUtil.variantDatabase.getGene(geneName, ServicesUtil.controlPanel.getFilters(p)).maxcadd(p);
					}else{
						maxCadd = Math.max(maxCadd, ServicesUtil.variantDatabase.getGene(geneName, ServicesUtil.controlPanel.getFilters(p)).maxcadd(p));
					}
					if( ServicesUtil.variantDatabase.getGene(geneName, ServicesUtil.controlPanel.getFilters(p)).highestImpactVariant(p) != null ){
						String tmpSev = ServicesUtil.variantDatabase.getGene(geneName, ServicesUtil.controlPanel.getFilters(p)).highestImpactVariant(p).impactSeverity();
						switch( tmpSev ){
							case "HIGH":
								severity = tmpSev;
								break;
							case "MED":
								if( !severity.equals("HIGH") )
									severity = tmpSev;
							case "LOW":
								if( severity.equals("OTHER") )
									severity = tmpSev;
						}
					}
						
					for(Variant v:ServicesUtil.variantDatabase.getGene(geneName, ServicesUtil.controlPanel.getFilters(p)).getFilteredVariants()){
						if( GenotypeUtils.isCarryingVariant( v.getPatientGenotype( p ) ) ){
							nbVariants++;
						}
					}
				}
				_model.addRow(p.getName(), p.getPatientSex(), p.getPatientPhenotype(), maxCadd, severity, nbVariants);
			}	
		}

		if(sorter == null){
			sorter = new TableRowSorter<CohortTableModel>(_model);
			List<RowSorter.SortKey>sortKeys = new ArrayList<RowSorter.SortKey>();
			sortKeys.add(new RowSorter.SortKey(3, SortOrder.DESCENDING));
			sorter.setSortKeys(sortKeys); 
		}
		_table.setRowSorter(sorter);
		sorter.sort();
		
		revalidate();
		repaint();
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getComponent()
	 */
	@Override
	public Component getComponent() {
		return this;
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getCytoPanelName()
	 */
	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.EAST;
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getIcon()
	 */
	@Override
	public Icon getIcon() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getTitle()
	 */
	@Override
	public String getTitle() {
		return getName();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if( e.getActionCommand().equals("check with child only") ){
			updateTableContent();
		}
		
		if( e.getActionCommand().equals("export CSV") ){
			FileChooserFilter xlsFilter = new FileChooserFilter("Excel", "csv");
			ArrayList<FileChooserFilter> filters = new ArrayList<FileChooserFilter>();
			filters.add(xlsFilter);
			File file = ServicesUtil.fileUtil.getFile(ServicesUtil.swingApplication.getJFrame(), "export to CSV file...", FileUtil.SAVE, "/Users/fcliquet/Documents/Data/GHFC/", "Save", filters);
			if( !file.getAbsolutePath().endsWith(".csv") )
				file = new File( file.getAbsolutePath()+".csv" );
			
			ServicesUtil.dialogTaskManager.execute(ServicesUtil.gravityTaskFactory.createTaskCsvExport(file, (String)listIndis.getSelectedItem(), (String)listFormats.getSelectedItem()));
		}
	}

}

class CohortTableModel extends AbstractTableModel{

	private static final long serialVersionUID = -8184151005484244349L;
	private List<String> _patients;
	private List<Integer> _sexes;
	private List<Integer> _phenotypes;
	private List<String> _severities;
	private List<Double> _cadds;
	private List<Integer> _nbVariants;
	
	public CohortTableModel() {
		_patients = new ArrayList<String>();
		_sexes = new ArrayList<Integer>();
		_phenotypes = new ArrayList<Integer>();
		_cadds = new ArrayList<Double>();
		_nbVariants = new ArrayList<Integer>();
		_severities = new ArrayList<String>();
	}
	
	public synchronized void addRow(String patient, int sex, int phenotype, double cadd, String severity, int nbVariants){
		_patients.add(patient);
		_sexes.add(sex);
		_phenotypes.add(phenotype);
		_cadds.add(cadd);
		_nbVariants.add(nbVariants);
		_severities.add(severity);
	}
	
	public synchronized void clear(){
		_patients.clear();
		_sexes.clear();
		_phenotypes.clear();
		_cadds.clear();
		_nbVariants.clear();
		_severities.clear();
	}
	
	private Object getPatient(int idx){
		if( idx >= _patients.size() )
			return null;
		else
			return _patients.get(idx);
	}
	
	private Object getCadd(int idx){
		if( idx >= _cadds.size() )
			return null;
		else
			if( _cadds.get(idx) < -100000000 )
				return null;
			else
				return _cadds.get(idx);
	}
	
	private Object getNbVariants(int idx){
		if( idx >= _nbVariants.size() )
			return null;
		else
			return _nbVariants.get(idx);
	}
	
	private Object getSex(int idx){
		if( idx >= _nbVariants.size() )
			return null;
		else{
			if( _sexes.get(idx)==Patient.MALE ){
				TextIcon ti = new TextIcon(new JLabel(), IconManager.ICON_MARS);
				ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
				ti.setForeground(Color.BLUE);
				return ti;
			}
			if( _sexes.get(idx)==Patient.FEMALE ){
				TextIcon ti = new TextIcon(new JLabel(), IconManager.ICON_VENUS);
				ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
				ti.setForeground(new Color( 255, 0, 255 ));
				return ti;
			}
			return null;
		}
		
	}
	
	private Object getSeverity(int idx){
		if( idx >= _nbVariants.size() )
			return null;
		else{
			if( _severities.get(idx).equals("HIGH") ){
				TextIcon ti = new TextIcon(new JLabel(), IconManager.ICON_CIRCLE);
				ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
				ti.setForeground(Color.RED);
				return ti;
			}
			if( _severities.get(idx).equals("MED") ){
				TextIcon ti = new TextIcon(new JLabel(), IconManager.ICON_CIRCLE);
				ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
				ti.setForeground(new Color( 255, 153, 0 ));
				return ti;
			}
			if( _severities.get(idx).equals("LOW") ){
				TextIcon ti = new TextIcon(new JLabel(), IconManager.ICON_CIRCLE);
				ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
				ti.setForeground(new Color( 0, 255, 0 ));
				return ti;
			}
			return null;
		}
	}
	
	private Object getPhenotype(int idx){
		if( idx >= _nbVariants.size() )
			return null;
		else{
			if( _phenotypes.get(idx)==Patient.AFFECTED ){
				TextIcon ti = new TextIcon(new JLabel(), IconManager.ICON_STETHOSCOPE);
				ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
				return ti;
			}
			return null;
		}
	}
	
	@Override
	public int getRowCount() {
		return _patients.size();
	}

	@Override
	public int getColumnCount() {
		return 6;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		switch(columnIndex){
		case 0:
			return getPatient(rowIndex);
		case 1:
			return getSex(rowIndex);
		case 2:
			return getPhenotype(rowIndex);
		case 3:
			return getSeverity(rowIndex);
		case 4:
			return getCadd(rowIndex);
		case 5:
			return getNbVariants(rowIndex);
		default:
			return null;
		}
	}
	
	public String getColumnName(int col) {
        switch(col){
        case 0:
        	return "patient";
        case 1:
        	return "sex";
        case 2:
        	return "phenotype";
        case 3:
        	return "severity";
        case 4:
        	return "CADD";
        case 5:
        	return "nb variants";
        default:
        	return "";
        }
    }
	
	 public Class<?> getColumnClass(int c) {
		 if( _patients.isEmpty() )
			 return Object.class;
		 if( c==1 || c==2 || c==3 )
			 return Icon.class;
		 if( getValueAt(0, c)!=null )
			 return getValueAt(0, c).getClass();
		 return Object.class;
	 }
	 
	 public boolean isCellEditable(int row, int col) {
		 return false;
    }
}

