/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;

/**
 * @author Freddy Cliquet
 * @version 1.0
 *
 */
public class NetworkViewTask extends AbstractTask {

	private CyNetwork network;
	
	public NetworkViewTask(CyNetwork net){
		network = net;
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.work.AbstractTask#run(org.cytoscape.work.TaskMonitor)
	 */
	@Override
	public void run(TaskMonitor taskMonitor) throws Exception {
		taskMonitor.setStatusMessage("Creating network view ...");
		
//		for(CyNetworkView nv:ServicesUtil.cyNetworkViewManager.getNetworkViews(network)){
//			ServicesUtil.cyNetworkViewManager.destroyNetworkView(nv);
//		}
		
		CyNetworkView _view = ServicesUtil.cyNetworkViewFactory.createNetworkView(network);
		ServicesUtil.cyNetworkViewManager.addNetworkView(_view);
//		ServicesUtil.applicationManager.setCurrentNetwork(network);
//		ServicesUtil.applicationManager.setCurrentNetworkView(_view);
		
		TaskIterator ti = new TaskIterator(new ApplyLayoutTask(_view));
		this.insertTasksAfterCurrentTask(ti);
		
		
	}

}
