/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;

import org.cytoscape.util.swing.IconManager;

import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.GenotypeUtils;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.API.gemini.GeminiVariant;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.TextIcon;
import net.miginfocom.swing.MigLayout;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class VariantComponent extends JPanel {

	private static final long serialVersionUID = 7509261632709822142L;
	private Variant variant;
	
	private JLabel labelIcon;
	
	public VariantComponent(){
		
	}
	
	public VariantComponent(Variant var){
		variant = var;
		
		if( ServicesUtil.controlPanel.isFamilyTrio() )
			initComponentsFamilyTrio();
		if( ServicesUtil.controlPanel.isComparisonMode() )
			initComponentsComparisonMode();
			
		addMouseListener(new MouseAdapter() 
		{    
		    final int defaultInitialDelay = ToolTipManager.sharedInstance().getInitialDelay();
		    final int defaultTimeout = ToolTipManager.sharedInstance().getDismissDelay();

		    @Override
		    public void mouseEntered(MouseEvent e) {
		        ToolTipManager.sharedInstance().setInitialDelay(0);
		        ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);
		    }

		    @Override
		    public void mouseExited(MouseEvent e) {
		        ToolTipManager.sharedInstance().setInitialDelay(defaultInitialDelay);
		        ToolTipManager.sharedInstance().setDismissDelay(defaultTimeout);
		    }
		    
		    @Override
		    public void mouseClicked(MouseEvent e){
		    	if(e.getClickCount()==1){
		    		StringSelection stringSelection = new StringSelection(variant.getChromosome()+":"+variant.getPosition());
		    		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		    		clpbrd.setContents(stringSelection, null);
		        }
		    	if(e.getClickCount()==2){
		            try {
						String url = (String)ServicesUtil.gravityProperties.getProperties().get("Gravity.settings.BAMserver");
						
						String LOCUS = var.getChromosome()+":"+var.getPosition();
						String POSITION = var.getChromosome()+":"+(var.getPosition()-100)+"-"+(var.getPosition()+100+ Math.max(var.getReference().length(), var.getAlternative().length()) );
						String CHR = var.getChromosome();
						String START = ""+(var.getPosition()-100);
						String END = ""+(var.getPosition()+100+ Math.max(var.getReference().length(), var.getAlternative().length()) );
						String INDIVIDUALS = ServicesUtil.controlPanel.getSelectedPatient().getName()+";"+ServicesUtil.controlPanel.getSelectedFather()+";"+ServicesUtil.controlPanel.getSelectedMother();
						String FAMILY = ServicesUtil.controlPanel.getSelectedPatient().getFamily();
						
						String url2 = url.replaceAll("%POSITION%", POSITION);
						url2 = url2.replaceAll("%CHR%", CHR);
						url2 = url2.replaceAll("%START%", START);
						url2 = url2.replaceAll("%END%", END);
						url2 = url2.replaceAll("%LOCUS%", LOCUS);
						url2 = url2.replaceAll("%INDIVIDUALS%", INDIVIDUALS);
						url2 = url2.replaceAll("%FAMILY%", FAMILY);
						
						System.out.println(url2);
						
		            	Desktop.getDesktop().browse(new URI( url2 ));
					}catch (IOException | URISyntaxException e1){ }
		        }
		    }
		    
		    @Override
		    public void mousePressed(MouseEvent e) {
		    	setBackground( UIManager.getColor("Panel.background").darker() );
		    }
		    
		    @Override
		    public void mouseReleased(MouseEvent e) {
		    	setBackground( UIManager.getColor("Panel.background") );
		    }
		});
	}
	
	public Variant getVariant(){
		return variant;
	}
	
	private void initComponentsComparisonMode(){
		setLayout(new MigLayout("fill, ins 5, gap 0 0"));
		setBorder( BorderFactory.createEtchedBorder() );
		labelIcon = new JLabel();
		TextIcon ti;
		int fontStyle = Font.PLAIN;
		
		boolean carriedByA = GenotypeUtils.isCarryingVariant( variant.getPatientGenotype( ServicesUtil.controlPanel.getSelectedPatientA() ) );
		boolean carriedByB = GenotypeUtils.isCarryingVariant( variant.getPatientGenotype( ServicesUtil.controlPanel.getSelectedPatientB() ) );
		
		if( carriedByA && carriedByB ){
			ti = new TextIcon(labelIcon, "AB");
			fontStyle = Font.BOLD;
		}else{
			if( carriedByA ){
				ti = new TextIcon(labelIcon, "A-");
				fontStyle = Font.BOLD;
			}else{
				ti = new TextIcon(labelIcon, "-B");
				fontStyle = Font.BOLD;
			}
		}		
		
		switch( variant.impactSeverity() ){
		case "HIGH":
			ti.setForeground(new Color(255, 0, 0));
			break;
		case "MED":
			ti.setForeground(new Color(255, 153, 0));
			break;
		case "LOW":
			ti.setForeground(new Color(0, 255, 0));
			break;
		default:
			ti.setForeground(new Color(153, 153, 153));	
		}
		
		ti.setFont( ti.getFont().deriveFont(fontStyle, 32) );
		
		
		labelIcon.setIcon(ti);
		add(labelIcon, "dock west, gap left 5px, gap top 5px, gap bottom 5px, w 50px");
		
		if(variant.isCNV()){
			String len = Integer.parseInt(variant.getObject("sv_length").toString())/1000+" kb";
			add(new JLabel(variant.getObject("sub_type")+" of size "+len), "wrap");
			if( variant instanceof GeminiVariant ){
				GeminiVariant gv = (GeminiVariant)variant;
				try {
					add(new JLabel("p."+((String)gv.getObject("aa_change")).split("/")[0]+((String)gv.getObject("aa_length")).split("/")[0]+((String)gv.getObject("aa_change")).split("/")[1]), "wrap");
				} catch (Exception e) {
					add(new JLabel(variant.impact()), "wrap");
				}
			}else{
				add(new JLabel(variant.impact()), "wrap");
			}
			add( new JLabel( 
					"GTs: "+variant.getPatientGenotype( ServicesUtil.controlPanel.getSelectedPatient() )
					+" (F:"+variant.getPatientGenotypeEnlarged4CNVsFiltered( ServicesUtil.controlPanel.getSelectedFather() )
					+" ; M:"+variant.getPatientGenotypeEnlarged4CNVsFiltered( ServicesUtil.controlPanel.getSelectedMother() )+")"
					), "wrap" );
			add(new JLabel("Cohort frequency: "+String.format("%.2f", variant.cohortFrequency())), "wrap");
		}else{
			if( variant instanceof GeminiVariant ){
				GeminiVariant gv = (GeminiVariant)variant;
				try {
					add(new JLabel("p."+((String)gv.getObject("aa_change")).split("/")[0]+((String)gv.getObject("aa_length")).split("/")[0]+((String)gv.getObject("aa_change")).split("/")[1]), "wrap");
				} catch (Exception e) {
					add(new JLabel(variant.impact()), "wrap");
				}
			}else{
				add(new JLabel(variant.impact()), "wrap");
			}
			add( new JLabel( 
					"GTs A="+variant.getPatientGenotype( ServicesUtil.controlPanel.getSelectedPatientA() )
					+" B="+variant.getPatientGenotype( ServicesUtil.controlPanel.getSelectedPatientB() )), "wrap" );
			if( variant.cadd()==null )
				add(new JLabel("no cadd "), "wrap");
			else
				add(new JLabel("cadd: "+variant.cadd_scaled()), "wrap");
			add(new JLabel("Freq: max "+String.format("%.2f", variant.maxAlleleFrequency())+" ; cohort "+String.format("%.2f", variant.cohortFrequency())), "wrap");
		}
		
		setToolTipText(variant.getHTMLtooltip( 
				ServicesUtil.controlPanel.getSelectedPatientA(), 
				ServicesUtil.controlPanel.getSelectedPatientB()));
	}
	
	private void initComponentsFamilyTrio(){
		setLayout(new MigLayout("fill, ins 5, gap 0 0"));
		setBorder( BorderFactory.createEtchedBorder() );
		labelIcon = new JLabel();
		TextIcon ti;
		int fontStyle = Font.PLAIN;
		
		switch( GenotypeUtils.inheritance( 
					variant.getPatientGenotype( ServicesUtil.controlPanel.getSelectedPatient() ) ,
					ServicesUtil.controlPanel.getSelectedPatient().getPatientSex(),
					variant.getChromosome(),
					variant.getPatientGenotypeEnlarged4CNVsFiltered( ServicesUtil.controlPanel.getSelectedFather() ), 
					variant.getPatientGenotypeEnlarged4CNVsFiltered( ServicesUtil.controlPanel.getSelectedMother() )  ) ){
		case 2:
			ti = new TextIcon(labelIcon, IconManager.ICON_VENUS);
			fontStyle = Font.BOLD;
			break;
		case 1:
			ti = new TextIcon(labelIcon, IconManager.ICON_MARS);
			fontStyle = Font.BOLD;
			break;
		case 3:
			ti = new TextIcon(labelIcon, IconManager.ICON_VENUS_MARS);
			fontStyle = Font.BOLD;
			break;
		case 0:
			ti = new TextIcon(labelIcon, IconManager.ICON_STAR_O);
			fontStyle = Font.BOLD;
			break;
		default:
			ti = new TextIcon(labelIcon, IconManager.ICON_QUESTION);	
		}
		
		
		switch( variant.impactSeverity() ){
		case "HIGH":
			ti.setForeground(new Color(255, 0, 0));
			break;
		case "MED":
			ti.setForeground(new Color(255, 153, 0));
			break;
		case "LOW":
			ti.setForeground(new Color(0, 255, 0));
			break;
		default:
			ti.setForeground(new Color(153, 153, 153));	
		}
		
		ti.setFont( ServicesUtil.iconManager.getIconFont(32f).deriveFont(fontStyle) );
		
		
		labelIcon.setIcon(ti);
		add(labelIcon, "dock west, gap left 5px, gap top 5px, gap bottom 5px, w 40px");
		
		if(variant.isCNV()){
			String len = Integer.parseInt(variant.getObject("sv_length").toString())/1000+" kb";
			add(new JLabel(variant.getObject("sub_type")+" of size "+len), "wrap");
			if( variant instanceof GeminiVariant ){
				GeminiVariant gv = (GeminiVariant)variant;
				try {
					add(new JLabel("p."+((String)gv.getObject("aa_change")).split("/")[0]+((String)gv.getObject("aa_length")).split("/")[0]+((String)gv.getObject("aa_change")).split("/")[1]), "wrap");
				} catch (Exception e) {
					add(new JLabel(variant.impact()), "wrap");
				}
			}else{
				add(new JLabel(variant.impact()), "wrap");
			}
			add( new JLabel( 
					"GTs: "+variant.getPatientGenotype( ServicesUtil.controlPanel.getSelectedPatient() )
					+" (F:"+variant.getPatientGenotypeEnlarged4CNVsFiltered( ServicesUtil.controlPanel.getSelectedFather() )
					+" ; M:"+variant.getPatientGenotypeEnlarged4CNVsFiltered( ServicesUtil.controlPanel.getSelectedMother() )+")"
					), "wrap" );
			add(new JLabel("Cohort frequency: "+String.format("%.2f", variant.cohortFrequency())), "wrap");
		}else{
			if( variant instanceof GeminiVariant ){
				GeminiVariant gv = (GeminiVariant)variant;
				try {
					add(new JLabel("p."+((String)gv.getObject("aa_change")).split("/")[0]+((String)gv.getObject("aa_length")).split("/")[0]+((String)gv.getObject("aa_change")).split("/")[1]), "wrap");
				} catch (Exception e) {
					add(new JLabel(variant.impact()), "wrap");
				}
			}else{
				add(new JLabel(variant.impact()), "wrap");
			}
			add( new JLabel( 
					"GTs: "+variant.getPatientGenotype( ServicesUtil.controlPanel.getSelectedPatient() )
					+" (F:"+variant.getPatientGenotypeEnlarged4CNVsFiltered( ServicesUtil.controlPanel.getSelectedFather() )
					+" ; M:"+variant.getPatientGenotypeEnlarged4CNVsFiltered( ServicesUtil.controlPanel.getSelectedMother() )+")"
					), "wrap" );
			if( variant.cadd()==null )
				add(new JLabel("no cadd "), "wrap");
			else
				add(new JLabel("cadd: "+variant.cadd_scaled()), "wrap");
			add(new JLabel("Freq: max "+String.format("%.2f", variant.maxAlleleFrequency())+" ; cohort "+String.format("%.2f", variant.cohortFrequency())), "wrap");
		}
		
		setToolTipText(variant.getHTMLtooltip( 
				ServicesUtil.controlPanel.getSelectedPatient(), 
				ServicesUtil.controlPanel.getSelectedFather(), 
				ServicesUtil.controlPanel.getSelectedMother()));
	}
	
}
