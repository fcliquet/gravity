/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.collections.api.bimap.MutableBiMap;
import org.eclipse.collections.impl.bimap.mutable.HashBiMap;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class PathwayUpdater {

	private MutableBiMap<Integer, String> mapNames = new HashBiMap<Integer, String>() ;
	private MutableBiMap<String, Integer> mapNamesR = null;
	private Map<Integer, List<String>> mapPathways = new HashMap<Integer, List<String>>();
	
	public PathwayUpdater() throws URISyntaxException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(PathwayUpdater.class.getResourceAsStream("/Gravity.pathways.props")));
		try {
			String line;
			while( (line = br.readLine()) != null ){
				process(line);
			}
		}catch(Exception e){
			e.printStackTrace(System.out);
		}
	}
	
	private void process(String line){
		if( line.startsWith("#") )
			return;
		String[] s=line.split("=");
		String[] s2=s[0].split("\\.");
		if( s2.length <= 3 )
			return;
		if( s2[3].equals("name") )
			mapNames.put(Integer.parseInt( s2[2] ), s[1]);
		if( s2[3].equals("genes") )
			mapPathways.put(Integer.parseInt( s2[2] ), Arrays.asList( s[1].split("\\s*,\\s*") ));
	}
	
	public Collection<String> getPathwaysList(){
		return mapNames.values();
	}
	
	public List<String> getPathway(String name){
		if( mapNamesR == null )
			mapNamesR = mapNames.inverse();
		return mapPathways.get( mapNamesR.get(name) );
	}
	
	public boolean needUpdate(String pathway, Set<String> genes){
		if( getPathway(pathway).size() != genes.size() )
			return true;
		else
			for(String g:getPathway(pathway) )
				if( !genes.contains(g) )
					return true;
		return false;
	}
}
