/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.util.Arrays;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.cytoscape.util.swing.IconManager;

import fr.pasteur.sysbio.Gravity.API.Filter;
import fr.pasteur.sysbio.Gravity.impl.internal.ControlPanel;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.TextIcon;
import net.miginfocom.swing.MigLayout;

/**
 * @author Freddy Cliquet
 * @version 1.1
 */
public class FilterComponent<T extends Filter> extends JPanel {

	private static final long serialVersionUID = 4205637317962323360L;
	public static final int TYPE_TEXT = 1;
	public static final int TYPE_DOUBLE = 2;
	
	private JComboBox<String> listFilters;
	private JComboBox<String> listOperators;
	private JTextField fieldFilter;
	private JButton removeButton;
	private ControlPanel autismPanel;
	private T _filter;
	private String _name;
	
	public FilterComponent(String name, int id, ControlPanel al, List<String> properties, boolean sort, T filter){
		autismPanel = al;
		_name = name;
		_filter = filter;
		String aProps[] = properties.toArray(new String[0]);
		if(sort)
			Arrays.sort(aProps);
		listFilters = new JComboBox<String>( aProps );
		if( _filter.getProperty()!=null && !_filter.getProperty().equals("") )
			listFilters.setSelectedItem(_filter.getProperty());
		String[] operatorStrings = { "<=", ">=", "=", "!=", "contains"};
		listOperators = new JComboBox<String>(operatorStrings);
		if( _filter.getOperator()!=null && !_filter.getOperator().equals("") )
			listOperators.setSelectedItem(_filter.getOperator());
		fieldFilter = new JTextField();
		if( _filter.getValue()!=null && !_filter.getValue().equals("") )
			fieldFilter.setText(_filter.getValue());
		removeButton = new JButton();
		TextIcon ti = new TextIcon(removeButton, IconManager.ICON_TRASH_O);
		ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
		removeButton.setIcon(ti);
		removeButton.setActionCommand("remove filter");
		removeButton.addActionListener(autismPanel);
		
		setLayout(new MigLayout("fill, ins 1, gap 1 1", "", "[min!]"));
		add(listFilters, "span 3, grow, wrap");
		add(listOperators);
		add(fieldFilter, "growx, push");
		add(removeButton);
		
		validate();
		
		setBorder(BorderFactory.createTitledBorder(name+" #"+id));
	}
	
	public String getCategory(){
		return _name;
	}
	
	public T getVariantFilter(){
		_filter.setProperty(getProperty());
		_filter.setOperator(getOperator());
		_filter.setValue(getValue());
		return _filter;
	}
	
	public String getProperty(){
		return (String)listFilters.getSelectedItem();
	}
	
	public String getOperator(){
		return (String)listOperators.getSelectedItem();
	}
	
	public String getValue(){
		return fieldFilter.getText();
	}
}
