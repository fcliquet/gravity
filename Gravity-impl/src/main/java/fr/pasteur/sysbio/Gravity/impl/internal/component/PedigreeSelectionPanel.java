package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cytoscape.util.swing.IconManager;

import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.IconListCellRenderer;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.TextIcon;
import net.miginfocom.swing.MigLayout;

public class PedigreeSelectionPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 3662002152337962507L;
	private JComboBox<Patient> _listPatient, _listFather, _listMother;
	private JCheckBox checkUncoupling;
	private List<Patient> patients;
	
	public PedigreeSelectionPanel() {
		initComponents();
		enableUncoupling();
	}
	
	public PedigreeSelectionPanel(boolean alwaysUncoupled) {
		initComponents();
		_listFather.setEnabled(true);
		_listMother.setEnabled(false);
	}
	
	private void initComponents(){
		setBorder(BorderFactory.createEtchedBorder());
		setLayout(new MigLayout("fill, ins 1, gap 1 1", "[min!][]", "[min!]"));
		
		add(new JLabel("Patient:"), "");
		
		_listPatient = new JComboBox<Patient>();
		_listPatient.addActionListener(this);
		add(_listPatient,"growx,wrap");
		
		add(new JLabel("Father:"), "");

		_listFather = new JComboBox<Patient>();
		_listFather.addActionListener(this);
		_listFather.setEnabled(false);
		add(_listFather,"growx,wrap");
		
		add(new JLabel("Mother:"), "");

		_listMother = new JComboBox<Patient>();
		_listMother.addActionListener(this);
		_listMother.setEnabled(false);
		add(_listMother,"growx,wrap");
		
		checkUncoupling = new JCheckBox("patients with parents only");
		checkUncoupling.setActionCommand("uncoupling");
		checkUncoupling.addActionListener(this);
	}
	
	public void setPatientList(List<Patient> lp){
		patients = lp;
		reloadPatientLists();
	}
	
	private void reloadPatientLists(){
		_listPatient.removeAllItems();
		_listFather.removeAllItems();
		_listMother.removeAllItems();
		
		Patient pnull = new Patient("[none]", "[none]", "[none]", "[none]", Patient.UNKNOWN, Patient.UNKNOWN);
		
		Collections.sort(patients);
		
		Map<Object,Icon> icons = new HashMap<Object,Icon>();
		for(Patient p:ServicesUtil.variantDatabase.getPatients()){
			String icon = "";
			Color c = Color.BLACK;
			if( p.getPatientSex()==Patient.MALE ){
				icon += IconManager.ICON_MARS;
				c = Color.BLUE;
				if( p.getPatientPhenotype()==Patient.AFFECTED )
					icon += " "+IconManager.ICON_STETHOSCOPE;
			}else{
				if( p.getPatientSex()==Patient.FEMALE ){
					icon += IconManager.ICON_VENUS;
					c = new Color( 255, 0, 255 );
					if( p.getPatientPhenotype()==Patient.AFFECTED )
						icon += " "+IconManager.ICON_STETHOSCOPE;
				}else{
					if( p.getPatientPhenotype()==Patient.AFFECTED )
						icon += IconManager.ICON_STETHOSCOPE;
				}
			}
			TextIcon ti = new TextIcon(new JLabel(), icon);
			ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
			ti.setForeground(c);
			icons.put(p, ti);
		}
		_listPatient.setRenderer(new IconListCellRenderer(icons));
		_listFather.setRenderer(new IconListCellRenderer(icons));
		_listMother.setRenderer(new IconListCellRenderer(icons));
	
		if( !checkUncoupling.isSelected() ){
			_listFather.addItem(pnull);
			_listMother.addItem(pnull);
		}
		
		for(Patient s:patients){
			if( !checkUncoupling.isSelected() ){
				_listPatient.addItem(s);
				if(s.getPatientSex() != Patient.FEMALE)
					_listFather.addItem(s);
				if(s.getPatientSex() != Patient.MALE)
					_listMother.addItem(s);
			}else{
				if( s.hasParents() )
					_listPatient.addItem(s);
			}
		}
		revalidate();
		repaint();
		autoaffectParents();
	}
	
	public void enableUncoupling(){
		add(checkUncoupling, "spanx,growx,wrap");
		_listFather.setEnabled(true);
		_listMother.setEnabled(true);
	}
	
	public void setPatient(Patient patient){
		_listPatient.setSelectedItem(patient);
		autoaffectParents();
	}
	
	public Patient getPatient(){
		return (Patient)_listPatient.getSelectedItem();
	}
	
	public Patient getFather(){
		return (Patient)_listFather.getSelectedItem();
	}
	
	public Patient getMother(){
		return (Patient)_listMother.getSelectedItem();
	}
	
	private void autoaffectParents(){
		Patient p = (Patient)_listPatient.getSelectedItem();
		Patient pnull = new Patient("[none]", "[none]", "[none]", "[none]", Patient.UNKNOWN, Patient.UNKNOWN);
		
		if( p!= null ){
			Patient f = p.getFather();
			if( f != null ){
				if( checkUncoupling.isSelected() ){
					_listFather.removeAllItems();
					_listFather.addItem(f);
				}else{
					_listFather.setSelectedItem(f);
				}
			}else{
				_listFather.removeAllItems();
				_listFather.addItem(pnull);
				for(Patient fp:patients)
					if( fp.getPatientSex() != Patient.FEMALE )
						_listFather.addItem(fp);
			}
			
			Patient m = p.getMother();
			if( m != null ){
				if( checkUncoupling.isSelected() ){
					_listMother.removeAllItems();
					_listMother.addItem(m);
				}else{
					_listMother.setSelectedItem(m);
				}
			}else{
				_listMother.removeAllItems();
				_listMother.addItem( pnull );
				for(Patient fp:patients)
					if( fp.getPatientSex() != Patient.MALE )
						_listMother.addItem(fp);
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if( e.getSource() == _listPatient ){
			if(e.getActionCommand().equals("comboBoxChanged")){
				autoaffectParents();
			}
		}
		
		if( e.getSource() == checkUncoupling ){
			if( checkUncoupling.isSelected() ){
				_listFather.setEnabled(false);
				_listMother.setEnabled(false);
				reloadPatientLists();
			}else{
				_listFather.setEnabled(true);
				_listMother.setEnabled(true);
				reloadPatientLists();
			}
		}
	}

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		PedigreeSelectionPanel c = new PedigreeSelectionPanel();
		
		List<Patient> lp = new ArrayList<Patient>();
		lp.add(new Patient("01-patient", "01", "01-father", "01-mother", 1, 2));
		lp.add(new Patient("01-father", "01", "0", "0", 1, 1));
		lp.add(new Patient("01-mother", "01", "0", "0", 2, 1));
		lp.add(new Patient("02-patient", "02", "father", "mother", 2, 2));
		lp.add(new Patient("father", "02", "0", "0", 1, 1));
		lp.add(new Patient("mother", "02", "0", "0", 2, 1));
		c.setPatientList(lp);
		
		f.getContentPane().add( c );
		f.validate();
		f.pack();
		f.setVisible(true);
	}
	
}
