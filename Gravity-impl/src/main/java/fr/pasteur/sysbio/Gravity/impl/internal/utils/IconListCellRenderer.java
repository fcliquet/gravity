/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.utils;

import java.awt.Component;
import java.util.Map;

import javax.swing.DefaultListCellRenderer;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.SwingConstants;

/**
 * ListCellRenderer that will add Icons based on the mapping of the map given to the constructor
 * @author Freddy Cliquet
 * @version 1.0
 */
public class IconListCellRenderer extends DefaultListCellRenderer {

	private static final long serialVersionUID = 3413579643870277057L;
	private Map<Object, Icon> _icons;

	public IconListCellRenderer(Map<Object, Icon> icons) {
		_icons = icons;
	}
	
	/* (non-Javadoc)
	 * @see javax.swing.ListCellRenderer#getListCellRendererComponent(javax.swing.JList, java.lang.Object, int, boolean, boolean)
	 */
	@Override
	public Component getListCellRendererComponent(JList<? extends Object> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
		JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
		label.setIcon(_icons.get(value));
		label.setHorizontalTextPosition(SwingConstants.LEFT);
		return label;
	}

}
