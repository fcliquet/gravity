/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.cytoscape.util.swing.IconManager;

import fr.pasteur.sysbio.Gravity.API.Filter;
import fr.pasteur.sysbio.Gravity.API.GenotypeUtils;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import net.miginfocom.swing.MigLayout;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class PatientVariantComponent extends JPanel {

	private static final long serialVersionUID = 5328675262078384180L;
	private Patient patient;
	private JLabel labelName, labelWarning;
	private int sex, phenotype;
	private Variant variant;
	private int recessive;
	
	public PatientVariantComponent(Patient p, Variant v, int recessive) {
		patient = p;
		variant = v;
		sex = p.getPatientSex();
		phenotype = p.getPatientPhenotype();
		this.recessive = recessive;
		setToolTip();
		setLayout(new MigLayout("fill, ins 0, gap 0 0", "[][min!]", ""));
		labelName = new JLabel( patient.getShortName() );
		labelWarning = new JLabel( IconManager.ICON_WARNING );
		labelWarning.setFont( ServicesUtil.iconManager.getIconFont(20f) );
		
		int x_shift = labelName.getPreferredSize().width / 2 - 12;
	
		@SuppressWarnings("serial")
		JPanel pp = new JPanel(new MigLayout("fill, ins 0, gap 0 0")){
			@Override
			protected void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g.create();
				int x_shift2 = Math.max(x_shift, 0);
				g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON );
				switch(variant.impactSeverity()){
					case "HIGH":
					case "MED":
					case "LOW":
						g2.setColor(Color.LIGHT_GRAY);
					break;
					default:
						g2.setColor(Color.WHITE);
						break;
				}
				g2.setFont( ServicesUtil.iconManager.getIconFont(28f) );
//				g2.setFont( g2.getFont().deriveFont(28f) );
				if( phenotype == Patient.AFFECTED )
					g2.drawString(IconManager.ICON_STETHOSCOPE, x_shift2+2, 24);
//					g2.drawString("A", x_shift2+2, 24);				
			}
		};
		pp.setPreferredSize(new Dimension(25,28));
		add(pp, "alignx center, spanx, wrap");
		labelName.setOpaque(true);
		labelName.setBackground(new Color( 1f, 1f, 1f, .6f ));
		
		pp.add( labelName , "align center,spanx, gaptop 4, wrap" );	
		
		Dimension min = pp.getMinimumSize();
		Dimension pref = pp.getPreferredSize();
		
		setMinimumSize(new Dimension( Math.max(pp.getPreferredSize().width, labelName.getPreferredSize().width)+8 , getPreferredSize().height));
		setPreferredSize(getMinimumSize());
		
		if( !checkQuality() ){
			pp.remove(labelName);
			pp.add(labelWarning, "align center,spanx, gaptop 3, wrap");
			pp.setMinimumSize(min);
			pp.setPreferredSize(pref);
		}
	}
	
	private boolean checkQuality(){
		for(Filter f:ServicesUtil.controlPanel.getQualityFilters(patient)){
			if( !f.passFilter(variant) )
				return false;
		}
		return true;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g.create();
		g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON );
		g2.setColor( new Color(153, 153, 153) );
		Color c = new Color(153, 153, 153);
		switch (variant.impactSeverity()) {
		case "HIGH":
			c = new Color(255, 0, 0);
			break;
		case "MED":
			c = new Color(255, 153, 0);
			break;
		case "LOW":
			c = new Color(0, 255, 0);
			break;
		default:
			c = new Color(153, 153, 153);
			break;
		}
		
		if( sex == Patient.MALE ){
			if( GenotypeUtils.isHomAlt( variant.getPatientGenotype(patient) ) ){
				g2.setColor(c);
			}
			g2.fillRect(0, 0, getMinimumSize().width, getMinimumSize().height);
			
			if( GenotypeUtils.isHet( variant.getPatientGenotype(patient) ) ){
				g2.setColor(c);
				g2.fillRect(0, 0, getMinimumSize().width/2, getMinimumSize().height);
			}
		}else{
			if( GenotypeUtils.isHomAlt( variant.getPatientGenotype(patient) ) ){
				g2.setColor(c);
			}
			g2.fillOval(0, 0, getMinimumSize().width, getMinimumSize().height);
			if( GenotypeUtils.isHet( variant.getPatientGenotype(patient) ) ){
				g2.setColor(c);
				g2.fillArc(0, 0, getMinimumSize().width, getMinimumSize().height, 90, 180);
			}
		}
	}
	
	private void setToolTip(){
		String qs = "";
		if( !checkQuality() ){
			qs = "<br><br><b>FAILS QUALITY TEST</b><br>";
		}
		
		setToolTipText("<html>Patient: "+patient.getName()
		+qs
		+"<br>Sex: "+(sex==Patient.MALE ? "Male" : "Female")
		+"<br>Phenotype: "+(phenotype==Patient.AFFECTED ? "Affected" : "Unaffected")
		+"<br>Genotype: "+variant.getPatient(patient));
	}

	
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		Patient p = new Patient("C0733-011-105-001", "C0733-011-105", "C0733-011-105-002", "C0733-011-105-003", 1,	2);
//		Patient p2 = new Patient("387834-LEUVEN","LEUVEN","393737-LEUVEN","393738-LEUVEN",2,2);
//		Patient p3 = new Patient("C0733-011-105-004", "C0733-011-105", "C0733-011-105-002", "C0733-011-105-003", 2,	2);
//		Patient p4 = new Patient("387835-LEUVEN","LEUVEN","393737-LEUVEN","393738-LEUVEN",1,2);
//		
//		PatientVariantComponent pc = new PatientVariantComponent(p, "HIGH");
//		PatientVariantComponent pc2 = new PatientVariantComponent(p2, "MEDIUM");
//		PatientVariantComponent pc3 = new PatientVariantComponent(p3, "LOW");
//		PatientVariantComponent pc4 = new PatientVariantComponent(p4, "UNKNOWN");
//		
//		JPanel panel = new JPanel( new MigLayout() );
//		panel.add(pc);
//		panel.add(pc2);
//		panel.add(pc3);
//		panel.add(pc4);
//		
//		JFrame f = new JFrame("pedigree");
//		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		f.getContentPane().add( panel );
//		f.validate();
//		f.pack();
//		f.setVisible(true);
//	}

}
