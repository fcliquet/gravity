/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal;

import java.awt.Checkbox;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.BoundedRangeModel;
import javax.swing.ButtonGroup;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.util.swing.FileChooserFilter;
import org.cytoscape.util.swing.FileUtil;
import org.cytoscape.util.swing.IconManager;

import fr.pasteur.sysbio.Gravity.API.Filter;
import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.Genotype;
import fr.pasteur.sysbio.Gravity.API.GenotypeFilter;
import fr.pasteur.sysbio.Gravity.API.ImpactFilter;
import fr.pasteur.sysbio.Gravity.API.InheritanceFilter;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.VariantFilter;
import fr.pasteur.sysbio.Gravity.API.gemini.GeminiDatabase;
import fr.pasteur.sysbio.Gravity.impl.internal.component.BoxPlotComponent;
import fr.pasteur.sysbio.Gravity.impl.internal.component.ComparisonPanel;
import fr.pasteur.sysbio.Gravity.impl.internal.component.CustomizedList;
import fr.pasteur.sysbio.Gravity.impl.internal.component.FamilyPedigreePanel;
import fr.pasteur.sysbio.Gravity.impl.internal.component.FilterComponent;
import fr.pasteur.sysbio.Gravity.impl.internal.component.NetworkExpansionPanel;
import fr.pasteur.sysbio.Gravity.impl.internal.component.PedigreeSelectionPanel;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.JRangeSlider;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.TextIcon;
import net.miginfocom.swing.MigLayout;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class ControlPanel extends JPanel implements ActionListener, CytoPanelComponent {

	private static final long serialVersionUID = 4460572454906579293L;
	
	// components
	JButton buttonCreateNetwork;
	JButton buttonUpdateNetwork;
	JButton buttonOpenDataset;
	
	//filters components
	JCheckBox checkDisplayInterestGenes;
	JComboBox<String> listMendelianacity;
	JComboBox<String> listInheritance;
	CustomizedList listImpacts;
	JButton buttonAddFilter, buttonAddFilter2, buttonSaveFilters, buttonLoadFilters;
	List<FilterComponent<? extends Filter>> listPanelFilters;
	JPanel panelFilters;
	JPanel panelAnalysisModeFilters;
	JPanel panelCompleteFamily;
	JPanel panelInheritance;
	JPanel panelComparison;
	JCheckBox cbDominant,cbRecessive,cbCompHet,cbQuality;
	JRadioButton rbOr,rbAnd,rbOnlyA,rbOnlyB,rbBoth;
	//JComboBox<String> listCFmutatedLink;
	//JCheckBox checkHOMrecessive, checkHETcompound;
	
	//JPanel panelParamNetwork;
	JPanel panelNetwork, panelFiltering, panelTopAutism, panelQuality, panelInfo;
	JTabbedPane tabbedPane;
	
	//Quality
	JButton buttonSaveQualities, buttonLoadQualities;
	JTextField tfQualityMinDP, tfQualityMinGQ, tfMinAD;
	JRangeSlider rangeSlider;
	Checkbox checkQualityParents, checkOverrideGT;
	
	//NetworkContext networkContext;
	CustomizedList interactionChoice;
	CustomizedList pathwayChoice;
	PedigreeSelectionPanel pedigreePanel;
	FamilyPedigreePanel familyPedigreePanel;
	ComparisonPanel comparisonPanel;
	JComboBox<String> listAnalysisMode;

	JPanel pQuickSearch;
	JTextField tfQuickSearch;
	JButton bQuickSearch;
	JButton bDelQuickSearch;
	
	private NetworkExpansionPanel networkExpansionPanel;
	private static int _nbFilter = 0;
	
	public ControlPanel() {
		initializeComponents();
	}
	
	public void initializeComponents(){
		setLayout( new MigLayout("fill, ins 0, gap 0 0", "", "[min!][]") );
		
		panelTopAutism = new JPanel();
		panelTopAutism.setLayout( new MigLayout("fill, ins 0, gap 0 0", "[33%][34%][33%]", "") );
		
		panelTopAutism.add(new JLabel("Load variants from a Cohort VCF or a Gemini file."), "gap top 20px, span 3, align center, wrap");
		
		buttonOpenDataset = new JButton(new ImageIcon(getClass().getResource("/images/open-file-40.png")));
		buttonOpenDataset.setToolTipText("Open a new dataset");
		buttonOpenDataset.setActionCommand("open new dataset");
		buttonOpenDataset.addActionListener(this);
		
		panelTopAutism.add(buttonOpenDataset, "gapbottom 10px, gaptop 10px,span 3, align center");
		
		add(panelTopAutism, "span 2, growx, push, wrap");
		
		buttonCreateNetwork = new JButton(new ImageIcon(getClass().getResource("/images/plus2.png")));
		buttonCreateNetwork.setToolTipText("Create a new network");
		buttonCreateNetwork.setActionCommand("create network");
		buttonCreateNetwork.addActionListener(this);
		buttonUpdateNetwork = new JButton(new ImageIcon(getClass().getResource("/images/refresh2.png")));
		buttonUpdateNetwork.setToolTipText("Update the current network");
		buttonUpdateNetwork.setActionCommand("update network");
		buttonUpdateNetwork.addActionListener(this);
		
		pQuickSearch = new JPanel(new MigLayout("fill, ins 0, gap 0 0", "[][min!][min!]", ""));
		tfQuickSearch = new JTextField("");
		tfQuickSearch.addActionListener(this);
		tfQuickSearch.setActionCommand("quick search");
		tfQuickSearch.setToolTipText("Enter a list of genes to search, overriding the selected pathways/gene lists.");
		
		TextIcon tiSearch = new TextIcon(new JButton(), IconManager.ICON_SEARCH);
		tiSearch.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		bQuickSearch = new JButton(tiSearch);
		bQuickSearch.addActionListener(this);
		bQuickSearch.setActionCommand("quick search");
		bQuickSearch.setToolTipText("Quick search overriding the selected pathways/genes.");
		
		TextIcon tiDelSearch = new TextIcon(new JButton(), IconManager.ICON_CLOSE);
		tiDelSearch.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		bDelQuickSearch = new JButton(tiDelSearch);
		bDelQuickSearch.addActionListener(this);
		bDelQuickSearch.setActionCommand("delete quick search");
		bDelQuickSearch.setToolTipText("Clear the quick search field.");
		
		pQuickSearch.add(tfQuickSearch, "growx");
		pQuickSearch.add(bQuickSearch);
		pQuickSearch.add(bDelQuickSearch);
		
		tabbedPane = new JTabbedPane();
		
		JScrollPane scrollPane = new JScrollPane(tabbedPane);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		add(scrollPane, "w 400, growy, aligny top, span, push");
		
		validate();
	}
	
	private void initNetworkTab(){
		tabbedPane.remove(panelNetwork);
		
		panelNetwork = new JPanel();
		panelNetwork.setLayout(new MigLayout("fill, ins 0", "", "[pref!]"));
		
		interactionChoice = new CustomizedList("Interactions");
		interactionChoice.setList( ServicesUtil.ppi.getInteractionTypes(), true );
		interactionChoice.selectAll();
		interactionChoice.addSelectAllButton();
		interactionChoice.setPreferredSize(new Dimension(interactionChoice.getPreferredSize().width, 180));
		panelNetwork.add(interactionChoice, "growx, wrap");
		
		pathwayChoice = new CustomizedList("Pathways/Genes");
		pathwayChoice.addCircumventOption("Run on all genes instead");
		pathwayChoice.addSelectAllButton();
		pathwayChoice.addSpacer();
		
		ActionListener pathwaySaveListener = new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				savePathwaysProperties();
			}
		};
		TextIcon tiSave = new TextIcon(new JButton(), IconManager.ICON_ARROW_RIGHT+IconManager.ICON_SAVE);
		tiSave.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		pathwayChoice.addButton(tiSave, "save all pathways and the selection", pathwaySaveListener);
		
		ActionListener pathwayLoadListener = new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				loadPathwaysProperties();
			}
		};
		TextIcon tiLoad = new TextIcon(new JButton(), IconManager.ICON_SAVE+IconManager.ICON_ARROW_RIGHT);
		tiLoad.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		pathwayChoice.addButton(tiLoad, "load the saved pathways",pathwayLoadListener);
		pathwayChoice.addSpacer();
		
		ActionListener pathwayNewListener = new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				JPanel p = new JPanel(new MigLayout());
				p.add(new JLabel("Pathway Name:"), "wrap");
				JTextField tf_name = new JTextField(15);
				p.add(tf_name, "pushx, wrap");
				p.add(new JLabel("Gene list (one gene per line):"), "wrap");
				JTextArea ta_genes = new JTextArea(10, 15);
				JScrollPane ta_scroll = new JScrollPane(ta_genes);
				ta_scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
				p.add(ta_scroll, "pushx, wrap");
				
				JOptionPane.showConfirmDialog(ServicesUtil.swingApplication.getJFrame(), p, "Create pathway", JOptionPane.OK_CANCEL_OPTION);
				
				ServicesUtil.pathways.addPathway(tf_name.getText(), new HashSet<String>(Arrays.asList(ta_genes.getText().split("\n"))));
				updatePathwaysList();
			}
		};
		TextIcon tiPlus = new TextIcon(new JButton(), IconManager.ICON_PLUS);
		tiPlus.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		pathwayChoice.addButton(tiPlus, "add a new pathway", pathwayNewListener);
		
		ActionListener pathwayDelListener = new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				ServicesUtil.pathways.removePathways(getSelectedPathways());
				updatePathwaysList();
			}
		};
		TextIcon tiMinus = new TextIcon(new JButton(), IconManager.ICON_MINUS);
		tiMinus.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		pathwayChoice.addButton(tiMinus, "remove selected pathways", pathwayDelListener);
		
		ActionListener pathwayUpdateListener = new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if( getSelectedPathways().size() != 1 ){
					return;
				}else{
					String updated = getSelectedPathways().get(0);
					JPanel p = new JPanel(new MigLayout());
					p.add(new JLabel("Pathway Name:"), "wrap");
					JTextField tf_name = new JTextField(15);
					tf_name.setText( updated );
					p.add(tf_name, "pushx, wrap");
					p.add(new JLabel("Gene list (one gene per line):"), "wrap");
					JTextArea ta_genes = new JTextArea(10, 15);
					JScrollPane ta_scroll = new JScrollPane(ta_genes);
					ta_scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
					p.add(ta_scroll, "pushx, wrap");
					ta_genes.setText( String.join("\n", ServicesUtil.pathways.getGenes(updated)) );
					
					JOptionPane.showConfirmDialog(ServicesUtil.swingApplication.getJFrame(), p, "Edit pathway", JOptionPane.OK_CANCEL_OPTION);
					
					ServicesUtil.pathways.removePathways(getSelectedPathways());					
					ServicesUtil.pathways.addPathway(tf_name.getText(), new HashSet<String>(Arrays.asList(ta_genes.getText().split("\n"))));
					updatePathwaysList();
				}
			}
		};
		TextIcon tiEdit = new TextIcon(new JButton(), IconManager.ICON_PENCIL);
		tiEdit.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		pathwayChoice.addButton(tiEdit, "edit the selected pathway", pathwayUpdateListener);
		
		pathwayChoice.setPreferredSize(new Dimension(pathwayChoice.getPreferredSize().width, 300));
		loadPathwaysProperties();
		panelNetwork.add(pathwayChoice, "growx,wrap");
		
		networkExpansionPanel = new NetworkExpansionPanel();
//		panelNetwork.add(networkExpansionPanel, "growx, pushx, wrap");
		
		panelNetwork.add(new JPanel(), "grow, push, wrap");
		
		TextIcon ti = new TextIcon(new JLabel(), IconManager.ICON_SHARE_ALT);
		ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
		tabbedPane.addTab("Network", ti, panelNetwork);
	}
	
	private void saveFiltersProperties(){
		ServicesUtil.filtersProperties.getProperties().clear();
		if( displayAllGenesOfInterest() )
			ServicesUtil.filtersProperties.getProperties().put("Gravity.filters.displayall", "1" );
		else
			ServicesUtil.filtersProperties.getProperties().put("Gravity.filters.displayall", "0" );
		
		ServicesUtil.filtersProperties.getProperties().put("Gravity.filters.mendelianicity", listMendelianacity.getSelectedItem() );
		ServicesUtil.filtersProperties.getProperties().put("Gravity.filters.inheritance", listInheritance.getSelectedItem() );
		
//		if( checkDeNovoOnly.isSelected() )
//			ServicesUtil.filtersProperties.getProperties().put("Gravity.filters.denovo_only", "1" );
//		else
//			ServicesUtil.filtersProperties.getProperties().put("Gravity.filters.denovo_only", "0" );
		ServicesUtil.filtersProperties.getProperties().put("Gravity.filters.impacts", listImpacts.getSelection().stream().collect(Collectors.joining(",")) );
		
		int cpt=0;
		for(FilterComponent<? extends Filter> apf:listPanelFilters){
//			String cat = "genotype";
//			if( apf.getVariantFilter() instanceof VariantFilter )
//				cat = "variant";
			ServicesUtil.filtersProperties.getProperties().put("Gravity.filters."+cpt+"."+apf.getCategory()+".property", apf.getProperty() );
			ServicesUtil.filtersProperties.getProperties().put("Gravity.filters."+cpt+"."+apf.getCategory()+".operator", apf.getOperator() );
			ServicesUtil.filtersProperties.getProperties().put("Gravity.filters."+cpt+"."+apf.getCategory()+".value", apf.getValue() );
			cpt++;
		}
	}
	
	private void loadFiltersProperties(){
		HashMap<String,String> filtersProperties = new HashMap<String, String>();
		HashMap<String,String> filtersOperators = new HashMap<String, String>();
		HashMap<String,String> filtersValues = new HashMap<String, String>();
		HashMap<String,String> filtersCategories = new HashMap<String, String>();
		
		for( Object okey:ServicesUtil.filtersProperties.getProperties().keySet() ){
			String key = (String)okey;
			switch(key){
				case "Gravity.filters.displayall":
					if( ServicesUtil.filtersProperties.getProperties().getProperty(key).equals("1") )
						checkDisplayInterestGenes.setSelected( true );
					else
						checkDisplayInterestGenes.setSelected( false );
					break;
				case "Gravity.filters.mendelianicity":
					listMendelianacity.setSelectedItem( ServicesUtil.filtersProperties.getProperties().getProperty(key) );
					break;
				case "Gravity.filters.inheritance":
					listInheritance.setSelectedItem( ServicesUtil.filtersProperties.getProperties().getProperty(key) );
					break;
				case "Gravity.filters.impacts":
					listImpacts.select( Arrays.asList(ServicesUtil.filtersProperties.getProperties().getProperty(key).split(",")) );
					break;
				case "Gravity.filters.denovo_only":
					break;
				default:
					String s[] = key.split("\\.");
					if( s[4].equals("property") ){
						filtersProperties.put( s[2], ServicesUtil.filtersProperties.getProperties().getProperty(key) );
						filtersCategories.put( s[2], s[3]);
					}
					if( s[4].equals("operator") ){
						filtersOperators.put( s[2], ServicesUtil.filtersProperties.getProperties().getProperty(key) );
						filtersCategories.put( s[2], s[3]);
					}
					if( s[4].equals("value") ){
						filtersValues.put( s[2], ServicesUtil.filtersProperties.getProperties().getProperty(key) );
						filtersCategories.put( s[2], s[3]);
					}
			}
		}
		
		listPanelFilters.clear();
		for(String key:filtersProperties.keySet()){
			
			if( filtersCategories.get(key).equals("Filter frequency") ){
				listPanelFilters.add(
						new FilterComponent<VariantFilter>( 
								filtersCategories.get(key) , 
								_nbFilter++, 
								this, 
								ServicesUtil.variantDatabase.listFrequencyAttributes(),
								false,
								new VariantFilter( filtersProperties.get(key), filtersOperators.get(key), filtersValues.get(key) )
						));
			}
			if( filtersCategories.get(key).equals("Filter CNVs") ){
				listPanelFilters.add(
						new FilterComponent<VariantFilter>( 
								filtersCategories.get(key) , 
								_nbFilter++, 
								this, 
								ServicesUtil.variantDatabase.listCNVsAttributes(),
								true,
								new VariantFilter( filtersProperties.get(key), filtersOperators.get(key), filtersValues.get(key) )
						));
			}
			if( filtersCategories.get(key).equals("Filter pathogenecity") ){
				listPanelFilters.add(
						new FilterComponent<VariantFilter>( 
								filtersCategories.get(key) , 
								_nbFilter++, 
								this, 
								ServicesUtil.variantDatabase.listPathoAttributes(),
								true,
								new VariantFilter( filtersProperties.get(key), filtersOperators.get(key), filtersValues.get(key) )
						));
			}
			if( filtersCategories.get(key).equals("Filter") ){
				listPanelFilters.add(
						new FilterComponent<VariantFilter>( 
								filtersCategories.get(key) , 
								_nbFilter++, 
								this, 
								ServicesUtil.variantDatabase.listOtherAttributes(),
								true,
								new VariantFilter( filtersProperties.get(key), filtersOperators.get(key), filtersValues.get(key) )
						));
			}
		}
		updateFiltersList();
	}
	
	private void savePathwaysProperties(){
		ServicesUtil.pathwaysProperties.getProperties().clear();
		ServicesUtil.pathwaysProperties.getProperties().put("Gravity.pathways.default", getSelectedPathways().stream().collect(Collectors.joining(",")) );
		int cpt=0;
		for( String pathway:ServicesUtil.pathways.getPathwaysList() ){
			String genes = ServicesUtil.pathways.getGenes(pathway).stream().collect(Collectors.joining(","));
			ServicesUtil.pathwaysProperties.getProperties().put("Gravity.pathways."+cpt+".name", pathway);
			ServicesUtil.pathwaysProperties.getProperties().put("Gravity.pathways."+cpt+".genes", genes);
			cpt++;
		}
	}
	
	private void loadPathwaysProperties(){
		List<String> defaultPathway = new ArrayList<String>();
		defaultPathway.add("SFARI");
		pathwayChoice.setCircumvent(false);
		
		HashMap<String,String> pathwayNames = new HashMap<String, String>();
		HashMap<String, Set<String>> pathwayGenes = new HashMap<String, Set<String>>();
		
		for( Object okey:ServicesUtil.pathwaysProperties.getProperties().keySet() ){
			String key = (String)okey;
			switch(key){
				case "Gravity.pathways.default":
					defaultPathway = Arrays.asList(ServicesUtil.pathwaysProperties.getProperties().getProperty(key).split(","));
					break;
				default:
					String s[] = key.split("\\.");
					if( s[3].equals("name") ){
						pathwayNames.put( s[2], ServicesUtil.pathwaysProperties.getProperties().getProperty(key) );
					}
					if( s[3].equals("genes") ){
						pathwayGenes.put( s[2], new HashSet<String>( Arrays.asList(ServicesUtil.pathwaysProperties.getProperties().getProperty(key).split(",")) ) );
					}
			}
		}
		
		for(String key:pathwayNames.keySet()){
			ServicesUtil.pathways.addPathway(pathwayNames.get(key), pathwayGenes.get(key));
		}
		
		try {
			PathwayUpdater pu = new PathwayUpdater();
			for( String p:pu.getPathwaysList() ){
				if( ServicesUtil.pathways.getGenes(p) != null || ServicesUtil.pathways.getGenes(p).size()==0 ){
					if( pu.needUpdate(p, ServicesUtil.pathways.getGenes(p)) ){
						System.out.println("Updating pathway: "+p);
						String name = new String(p);
						while( ServicesUtil.pathways.getGenes(name).size()!=0 )
							name+="[new]";
						ServicesUtil.pathways.addPathway( name, new HashSet<String>( pu.getPathway(p) ) );
					}
				}else{
					System.out.println("New pathway: "+p);
					ServicesUtil.pathways.addPathway( p, new HashSet<String>( pu.getPathway(p) ) );
				}
			}
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}
		
		pathwayChoice.setList( new ArrayList<String>(ServicesUtil.pathways.getPathwaysList()), true );
		pathwayChoice.select(defaultPathway);
	}
	
	private String[] loadPathwaysPropertiesBis(){
		List<String> defaultPathway = new ArrayList<String>();
		defaultPathway.add("SFARI");
		
		HashMap<String,String> pathwayNames = new HashMap<String, String>();
		HashMap<String, Set<String>> pathwayGenes = new HashMap<String, Set<String>>();
		
		for( Object okey:ServicesUtil.pathwaysProperties.getProperties().keySet() ){
			String key = (String)okey;
			switch(key){
				case "Gravity.pathways.default":
					defaultPathway = Arrays.asList(ServicesUtil.pathwaysProperties.getProperties().getProperty(key).split(","));
					break;
				default:
					String s[] = key.split("\\.");
					if( s[3].equals("name") ){
						pathwayNames.put( s[2], ServicesUtil.pathwaysProperties.getProperties().getProperty(key) );
					}
					if( s[3].equals("genes") ){
						pathwayGenes.put( s[2], new HashSet<String>( Arrays.asList(ServicesUtil.pathwaysProperties.getProperties().getProperty(key).split(",")) ) );
					}
			}
		}
		
		for(String key:pathwayNames.keySet()){
			ServicesUtil.pathways.addPathway(pathwayNames.get(key), pathwayGenes.get(key));
		}
		
		try {
			PathwayUpdater pu = new PathwayUpdater();
			for( String p:pu.getPathwaysList() ){
				if( ServicesUtil.pathways.getGenes(p) != null || ServicesUtil.pathways.getGenes(p).size()==0 ){
					if( pu.needUpdate(p, ServicesUtil.pathways.getGenes(p)) ){
						System.out.println("Updating pathway: "+p);
						String name = new String(p);
						while( ServicesUtil.pathways.getGenes(name).size()!=0 )
							name+="[new]";
						ServicesUtil.pathways.addPathway( name, new HashSet<String>( pu.getPathway(p) ) );
					}
				}else{
					System.out.println("New pathway: "+p);
					ServicesUtil.pathways.addPathway( p, new HashSet<String>( pu.getPathway(p) ) );
				}
			}
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}
		
		return ServicesUtil.pathways.getPathwaysList().toArray(new String[ServicesUtil.pathways.getPathwaysList().size()]);
	}

	private void refreshFilterTab(){
		panelAnalysisModeFilters.removeAll();
		if( isCompleteFamily() )
			panelAnalysisModeFilters.add(panelCompleteFamily, "span, growx, wrap");
		if( isFamilyTrio() )
			panelAnalysisModeFilters.add(panelInheritance, "span, growx, wrap");
		if( isComparisonMode() )
			panelAnalysisModeFilters.add(panelComparison, "span, growx, wrap");
			
		panelFiltering.revalidate();
		panelFiltering.repaint();
	}
	
	private void initFilterTab(){
		tabbedPane.remove(panelFiltering);
		
		panelFiltering = new JPanel();
		panelFiltering.setLayout(new MigLayout("fill, ins 0, gap 0 5", "[]", "[min!][min!][min!][min!][]"));
		
		JPanel panelButtons= new JPanel();
		panelButtons.setLayout(new MigLayout("fill, ins 0, gap 0 0", "", ""));
		
		panelButtons.add(new JPanel(), "growx, push");
		buttonSaveFilters = new JButton("Save");
		TextIcon tiSave = new TextIcon(buttonSaveFilters, IconManager.ICON_ARROW_RIGHT+IconManager.ICON_SAVE);
		tiSave.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		buttonSaveFilters.setIcon(tiSave);
		buttonSaveFilters.setToolTipText("Save as default values");
		buttonSaveFilters.setAlignmentX(LEFT_ALIGNMENT);
		buttonSaveFilters.setActionCommand("save defaults filters");
		buttonSaveFilters.addActionListener(this);
		panelButtons.add(buttonSaveFilters);
		
		buttonLoadFilters = new JButton("Load");
		TextIcon tiLoad = new TextIcon(buttonLoadFilters, IconManager.ICON_SAVE+IconManager.ICON_ARROW_RIGHT);
		tiLoad.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		buttonLoadFilters.setIcon(tiLoad);
		buttonLoadFilters.setToolTipText("Load default values");
		buttonLoadFilters.setAlignmentX(LEFT_ALIGNMENT);
		buttonLoadFilters.setActionCommand("load defaults filters");
		buttonLoadFilters.addActionListener(this);
		panelButtons.add(buttonLoadFilters);
		
		panelButtons.add(new JPanel(), "growx, push");
		panelFiltering.add(panelButtons, "growx, span, wrap");
		
		checkDisplayInterestGenes = new JCheckBox("display all genes");
		checkDisplayInterestGenes.setToolTipText("Display all the genes, even the ones not carrying a variant. This is useful to display connected networks.");
		checkDisplayInterestGenes.setAlignmentX(LEFT_ALIGNMENT);
		panelFiltering.add(checkDisplayInterestGenes, "span,wrap");
				
		panelAnalysisModeFilters = new JPanel(new MigLayout("fill, ins 0, gap 0 0"));
		panelFiltering.add(panelAnalysisModeFilters, "span, growx, wrap");
		
		
		// OLD Complete family version
//		panelCompleteFamily = new JPanel(new MigLayout("fill, ins 0, gap 0 0", "[pref!][]", ""));
//		panelCompleteFamily.setBorder(BorderFactory.createTitledBorder("Complete families"));
//		listCFmutatedLink = new JComboBox<String>();
//		listCFmutatedLink.addItem("AND (all must carry it)");
//		listCFmutatedLink.addItem("OR (at least one must carry it)");
//		panelCompleteFamily.add(new JLabel("Variant: "), "");
//		panelCompleteFamily.add(listCFmutatedLink, "growx, wrap");
//		checkHOMrecessive = new JCheckBox("HOM autosomal recessive");
//		checkHETcompound = new JCheckBox("compound HET");
//		panelCompleteFamily.add(checkHOMrecessive, "gaptop 2px, span, growx, wrap");
//		panelCompleteFamily.add(checkHETcompound, "gaptop 2px, span, growx");
		
		// NEW Complete family version
		panelCompleteFamily = new JPanel(new MigLayout("fill, ins 0, gap 0 0", "[]", ""));
		panelCompleteFamily.setBorder(BorderFactory.createTitledBorder("Complete families"));
		cbDominant = new JCheckBox("Dominant variants");
		cbDominant.setSelected(true);
		cbRecessive = new JCheckBox("Recessive variants");
		cbCompHet = new JCheckBox("Compound heterozygous variants");
		panelCompleteFamily.add(cbDominant, "wrap");
		panelCompleteFamily.add(cbRecessive, "gaptop 2px, wrap");
		panelCompleteFamily.add(cbCompHet, "gaptop 2px, wrap");
		rbOr = new JRadioButton("At least one");
		rbOr.setSelected(true);
		rbAnd = new JRadioButton("All of them");
		ButtonGroup bgCarriers = new ButtonGroup();
		bgCarriers.add(rbOr);
		bgCarriers.add(rbAnd);
		panelCompleteFamily.add(new JLabel("Carriers among the 'green' individuals:"), "gaptop 8px, wrap");
		panelCompleteFamily.add(rbOr, "gaptop 2px, wrap");
		panelCompleteFamily.add(rbAnd, "gaptop 2px, wrap");
//		cbQuality = new JCheckBox("All selected individuals must pass quality");
//		panelCompleteFamily.add(cbQuality, "gaptop 8px, wrap");
		
		
		panelInheritance = new JPanel(new MigLayout("fill, ins 0, gap 0 0", "[pref!][]", ""));
		panelInheritance.setBorder(BorderFactory.createTitledBorder("Inheritance"));
		listMendelianacity = new JComboBox<String>();
		listMendelianacity.addActionListener(this);
		listMendelianacity.addItem("any");
		listMendelianacity.addItem("de novo");
		listMendelianacity.addItem("non-mendelian");
		listMendelianacity.addItem("strictly mendelian");
		panelInheritance.add(new JLabel("Mendelianicity: "), "");
		panelInheritance.add(listMendelianacity, "grow, wrap");
		
		listInheritance = new JComboBox<String>();
		listInheritance.addActionListener(this);
		listInheritance.addItem("any");
		listInheritance.addItem("none");
		listInheritance.addItem("father and mother");
		listInheritance.addItem("father");
		listInheritance.addItem("father only");
		listInheritance.addItem("mother");
		listInheritance.addItem("mother only");
		listInheritance.addItem("unknown");
		panelInheritance.add(new JLabel("Inheritance: "), "");
		panelInheritance.add(listInheritance, "grow, wrap");
		
		
		panelComparison = new JPanel(new MigLayout("fill, ins 0, gap 0 0", "[]", ""));
		rbOnlyA = new JRadioButton("Only in sample A");
		rbOnlyB = new JRadioButton("Only in sample B");
		rbBoth = new JRadioButton("Cumulative");
		rbBoth.setSelected(true);
		rbBoth.setToolTipText("Ideal when A is germline and B is Somatic variants. This will show all types of variants and mae a visual distinction between both types.");
		ButtonGroup bgCompare = new ButtonGroup();
		bgCompare.add(rbOnlyA);
		bgCompare.add(rbOnlyB);
		bgCompare.add(rbBoth);
		panelComparison.add(new JLabel("Display variants that are:"), "gaptop 4px, wrap");
		panelComparison.add(rbOnlyA, "gaptop 2px, wrap");
		panelComparison.add(rbOnlyB, "gaptop 2px, wrap");
		panelComparison.add(rbBoth, "gaptop 2px, wrap");
		
		
		listImpacts = new CustomizedList("Impacts");
		listImpacts.addSelectAllButton();
		
		ActionListener impactHighListener = new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				listImpacts.select( ServicesUtil.variantDatabase.getImpactsOfSeverity("HIGH") );
			}
		};
		listImpacts.addButton("high", "select all the high severity impacts", impactHighListener);
		
		ActionListener impactMedListener = new ActionListener() {	
			@Override
			public void actionPerformed(ActionEvent e) {
				List<String> l = ServicesUtil.variantDatabase.getImpactsOfSeverity("HIGH");
				l.addAll( ServicesUtil.variantDatabase.getImpactsOfSeverity("MED") );
				listImpacts.select( l );
			}
		};
		listImpacts.addButton("med+", "select all the medium and high severity impacts", impactMedListener);
		listImpacts.setMinimumSize(new Dimension(listImpacts.getMinimumSize().width, 150));
		panelFiltering.add(listImpacts, "span, growx, wrap");
		
		JPanel panelButtons2= new JPanel();
		panelButtons2.setLayout(new MigLayout("fill, ins 0, gap 0 0", "", ""));
		
		panelButtons2.add(new JPanel(), "growx, push");
		
		JButton bFilterFrequency = new JButton("Frequency");
		TextIcon tiFilter = new TextIcon(bFilterFrequency, IconManager.ICON_FILTER);
		tiFilter.setFont( ServicesUtil.iconManager.getIconFont(16.0f) );
		bFilterFrequency.setIcon(tiFilter);
		bFilterFrequency.setToolTipText("Add a filter on the frequency data.");
		bFilterFrequency.setAlignmentX(LEFT_ALIGNMENT);
		bFilterFrequency.setActionCommand("filter frequency");
		bFilterFrequency.addActionListener(this);
		panelButtons2.add(bFilterFrequency);
		
		JButton bFilterCNV = new JButton("CNV");
		bFilterCNV.setIcon(tiFilter);
		bFilterCNV.setToolTipText("Add a filter specific to CNVs.");
		bFilterCNV.setAlignmentX(LEFT_ALIGNMENT);
		bFilterCNV.setActionCommand("filter cnv");
		bFilterCNV.addActionListener(this);
		panelButtons2.add(bFilterCNV);
		
		JButton bFilterPatho = new JButton("Pathogenecity");
		bFilterPatho.setIcon(tiFilter);
		bFilterPatho.setToolTipText("Add a filter on the pathogenecity (CADD, Polyphen, SIFT...).");
		bFilterPatho.setAlignmentX(LEFT_ALIGNMENT);
		bFilterPatho.setActionCommand("filter patho");
		bFilterPatho.addActionListener(this);
		panelButtons2.add(bFilterPatho);
		
		JButton bFilterOther = new JButton("Other");
		bFilterOther.setIcon(tiFilter);
		bFilterOther.setToolTipText("Add a filter on the other data.");
		bFilterOther.setAlignmentX(LEFT_ALIGNMENT);
		bFilterOther.setActionCommand("filter other");
		bFilterOther.addActionListener(this);
		panelButtons2.add(bFilterOther);
		
		
//		buttonAddFilter = new JButton("Variants");
//		TextIcon tiFilter = new TextIcon(buttonAddFilter, IconManager.ICON_FILTER);
//		tiFilter.setFont( ServicesUtil.iconManager.getIconFont(16.0f) );
//		buttonAddFilter.setIcon(tiFilter);
//		buttonAddFilter.setToolTipText("Add a new filter for variant's global properties (cadd, aaf, etc.)");
//		buttonAddFilter.setAlignmentX(LEFT_ALIGNMENT);
//		buttonAddFilter.setActionCommand("add new filter");
//		buttonAddFilter.addActionListener(this);
//		panelButtons2.add(buttonAddFilter);
//		
//		buttonAddFilter2 = new JButton("Genotypes");
//		TextIcon tiFilter2 = new TextIcon(buttonAddFilter2, IconManager.ICON_FILTER);
//		tiFilter2.setFont( ServicesUtil.iconManager.getIconFont(16.0f) );
//		buttonAddFilter2.setIcon(tiFilter2);
//		buttonAddFilter2.setToolTipText("Add a new filter for variant's -patient specific- properties (GT, DP, GQ, etc.)");
//		buttonAddFilter2.setAlignmentX(LEFT_ALIGNMENT);
//		buttonAddFilter2.setActionCommand("add new gt filter");
//		buttonAddFilter2.addActionListener(this);
//		panelButtons2.add(buttonAddFilter2);
		
		panelButtons2.add(new JPanel(), "growx, push");
		
		panelFiltering.add(panelButtons2, "growx, span, wrap");
		
		listPanelFilters = new ArrayList<FilterComponent<? extends Filter>>();
		
		panelFilters = new JPanel();
		panelFilters.setLayout(new MigLayout("fill, ins 0", "", "[min!]"));
		JScrollPane scrollFilters = new JScrollPane(panelFilters);
		panelFiltering.add(scrollFilters, "span, w 350, growy, push, top, left, wrap");

		TextIcon ti = new TextIcon(new JLabel(), IconManager.ICON_FILTER);
		ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
		tabbedPane.addTab("Filters", ti, panelFiltering);

		refreshFilterTab();
		updateImpactList();
	}
	
	/**
	 * Returns true if a logical AND is applied between all the people for which we wants to keep the variants. 
	 * It means that all of them must be carrying a variant for it to be kept. 
	 * Otherwise, it returns false and will mean that at least one must carry the variant for it to be kept.
	 * @return true if all "keeper" must carry the variant, false if one "keeper" is enough
	 */
	public boolean keepAllVariants4CompleteFamilies(){
		return rbAnd.isSelected();
	}
	
	/**
	 * Returns true if we are considering homozygous autosomal dominqnt variants (mutations that must be inherited from both parents); false otherwise.
	 * 
	 * @return true if we are considering HOM autosomal dominant variants; false otherwise
	 */
	public boolean considerDominant(){
		return cbDominant.isSelected();
	}
	
	/**
	 * Returns true if we are considering homozygous autosomal recessive variants (mutations that must be inherited from both parents); false otherwise.
	 * 
	 * @return true if we are considering HOM autosomal recessive variants; false otherwise
	 */
	public boolean considerHomAutosomalRecessive(){
		return cbRecessive.isSelected();
	}
	
	/**
	 * Returns true if we are considering heterozygous compound variants (2 different rare variants, not necessarily at the same position, one inherited from each parents); false otherwise.
	 * 
	 * @return true if we are considering HET compound variants; false otherwise
	 */
	public boolean considerHetCompound(){
		return cbCompHet.isSelected();
	}
	
	private void initQualityTab(){
		tabbedPane.remove(panelQuality);
		
		panelQuality = new JPanel();
		panelQuality.setLayout(new MigLayout("fill, ins 0, gap 0 5", "[][pref!]", "[min!]"));
		
		
		
		JPanel panelButtons= new JPanel();
		panelButtons.setLayout(new MigLayout("fill, ins 0, gap 0 0", "", ""));
		
		panelButtons.add(new JPanel(), "growx, push");
		buttonSaveQualities = new JButton("Save");
		TextIcon tiSave = new TextIcon(buttonSaveQualities, IconManager.ICON_ARROW_RIGHT+IconManager.ICON_SAVE);
		tiSave.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		buttonSaveQualities.setIcon(tiSave);
		buttonSaveQualities.setToolTipText("Save as default values");
		buttonSaveQualities.setAlignmentX(LEFT_ALIGNMENT);
		buttonSaveQualities.setActionCommand("save defaults qualities");
		buttonSaveQualities.addActionListener(this);
		panelButtons.add(buttonSaveQualities);
		
		buttonLoadQualities = new JButton("Load");
		TextIcon tiLoad = new TextIcon(buttonLoadQualities, IconManager.ICON_SAVE+IconManager.ICON_ARROW_RIGHT);
		tiLoad.setFont( ServicesUtil.iconManager.getIconFont(14.0f) );
		buttonLoadQualities.setIcon(tiLoad);
		buttonLoadQualities.setToolTipText("Load default values");
		buttonLoadQualities.setAlignmentX(LEFT_ALIGNMENT);
		buttonLoadQualities.setActionCommand("load defaults qualities");
		buttonLoadQualities.addActionListener(this);
		panelButtons.add(buttonLoadQualities);
		
		panelButtons.add(new JPanel(), "growx, push");
		panelQuality.add(panelButtons, "growx, span, wrap");
		
		
		
		panelQuality.add( new JLabel("Minimum allele depth (DP): "), "" );
		tfQualityMinDP = new JTextField(4);
		panelQuality.add( tfQualityMinDP, "wrap" );
		
		JLabel labelQualGQ = new JLabel("Minimum genotype quality (GQ): ");
		panelQuality.add( labelQualGQ, "" );
		tfQualityMinGQ = new JTextField(4);
		panelQuality.add( tfQualityMinGQ, "wrap" );
		
		JPanel panelRange = new JPanel(new MigLayout("fill, ins 0, gap 0 0", "[][pref!]", "[min!]"));
		panelRange.setBorder( BorderFactory.createTitledBorder("Alleles ratio range for heterozygosity") );
		String tooltipText = "<html>Compute the ratio <b>AD/DP</b> to know the percentage of<br>"
				+"<b>alternative alleles</b> out of all the allele sequenced.<br>"
				+"If this ratio is within this range, then we will call the<br>"
				+"variant <b>heterozigous</b>. If this ratio is below the lower bound,<br>"
				+"then we will call <b>homozygous for the reference allele</b>,<br>"
				+"and finally if the ratio is above the upper bound, we will call<br>"
				+"<b>homozygous for the alternative allele</b>.</html>";
		
		JLabel rangeSliderValue1 = new JLabel();
	    JLabel rangeSliderValue2 = new JLabel();
 
        BoundedRangeModel brm = new DefaultBoundedRangeModel(13, 74, 0, 100);
        rangeSlider = new JRangeSlider(brm, SwingConstants.HORIZONTAL);
        rangeSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JRangeSlider slider = (JRangeSlider) e.getSource();
                rangeSliderValue1.setText(String.valueOf(slider.getLowValue())+" %");
                rangeSliderValue2.setText(String.valueOf(slider.getHighValue())+" %");
            }
        });

        panelRange.setToolTipText( tooltipText  );
		rangeSlider.setToolTipText( tooltipText );
		rangeSliderValue1.setToolTipText( tooltipText );
		rangeSliderValue2.setToolTipText( tooltipText );
        
        rangeSliderValue1.setText(String.valueOf(rangeSlider.getLowValue())+" %");
        rangeSliderValue2.setText(String.valueOf(rangeSlider.getHighValue())+" %");
        
//        panelQuality.add( new JLabel("Allele ratio range for heterozygosity:"), "spanx, growx,wrap" );
        panelRange.add( rangeSlider, "span,growx,wrap" );
        panelRange.add( rangeSliderValue1, "growx" );
        panelRange.add( rangeSliderValue2, "align right, wrap" );
		
        panelQuality.add(panelRange, "gap top 5px, spanx, growx, wrap");
        
        checkQualityParents = new Checkbox("Apply quality filters to the parents");
        panelQuality.add( checkQualityParents, "span, wrap");
        
        checkOverrideGT = new Checkbox("infer GT from the AD");
        checkOverrideGT.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if( checkOverrideGT.getState() ){
					tfQualityMinGQ.setEnabled(false);
					panelRange.setEnabled(false);
					checkQualityParents.setEnabled(false);
					labelQualGQ.setEnabled(false);
				}else{
					tfQualityMinGQ.setEnabled(true);
					panelRange.setEnabled(true);
					checkQualityParents.setEnabled(true);
					labelQualGQ.setEnabled(true);
				}
			}
		});
        panelQuality.add( checkOverrideGT, "span, wrap" );
        
        tfMinAD = new JTextField(5);
        tfMinAD.setText("2");
        panelQuality.add( new JLabel("Minimum AD: "), "");
        panelQuality.add( tfMinAD, "wrap" );
        
        panelQuality.add(new JPanel(), "growy, push");
        
		TextIcon ti = new TextIcon(new JLabel(), IconManager.ICON_CHECK_CIRCLE);
		ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
		tabbedPane.addTab("Quality", ti, panelQuality);
		
		loadQualityProperties();
	}
	
	private void initInfoTab(){
		tabbedPane.remove(panelInfo);
		
		panelInfo = new JPanel();
		panelInfo.setLayout(new MigLayout("fill, ins 0, gap 0 5", "[pref!]", "[min!]"));
		
		JButton bLoadAnnot = new JButton("Load gene annotation file");
		bLoadAnnot.addActionListener(this);
		bLoadAnnot.setActionCommand("load annotation");
		panelInfo.add(bLoadAnnot, "wrap");
		
		JLabel ta = new JLabel();
		ta.setText(ServicesUtil.variantDatabase.getInfos());
		panelInfo.add(ta, "grow,wrap");
		
		TextIcon ti = new TextIcon(new JLabel(), IconManager.ICON_INFO_CIRCLE);
		ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
		tabbedPane.addTab("Infos", ti, panelInfo);
	}
	
	private void saveQualityProperties(){
		ServicesUtil.qualitiesProperties.getProperties().clear();
		ServicesUtil.qualitiesProperties.getProperties().put("Gravity.qualities.minDP", tfQualityMinDP.getText());
		ServicesUtil.qualitiesProperties.getProperties().put("Gravity.qualities.minGQ", tfQualityMinGQ.getText());
		ServicesUtil.qualitiesProperties.getProperties().put("Gravity.qualities.minARhet", rangeSlider.getLowValue()+"");
		ServicesUtil.qualitiesProperties.getProperties().put("Gravity.qualities.maxARhet", rangeSlider.getHighValue()+"");
		ServicesUtil.qualitiesProperties.getProperties().put("Gravity.qualities.apply2parents", checkQualityParents.getState() ? "1" : "0" );
	}
	
	private void loadQualityProperties(){
		for( Object okey:ServicesUtil.qualitiesProperties.getProperties().keySet() ){
			String key = (String)okey;
			switch(key){
				case "Gravity.qualities.minDP":
					tfQualityMinDP.setText( ServicesUtil.qualitiesProperties.getProperties().getProperty(key) );
					break;
				case "Gravity.qualities.minGQ":
					tfQualityMinGQ.setText( ServicesUtil.qualitiesProperties.getProperties().getProperty(key) );
					break;
				case "Gravity.qualities.minARhet":
					rangeSlider.setLowValue( Integer.parseInt( ServicesUtil.qualitiesProperties.getProperties().getProperty(key) ) );
					break;
				case "Gravity.qualities.maxARhet":
					rangeSlider.setHighValue( Integer.parseInt( ServicesUtil.qualitiesProperties.getProperties().getProperty(key) ) );
					break;
				case "Gravity.qualities.apply2parents":
					if( ServicesUtil.qualitiesProperties.getProperties().getProperty(key).equals("1") )
						checkQualityParents.setState(true);
					else
						checkQualityParents.setState(false);
					break;
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getComponent()
	 */
	@Override
	public Component getComponent() {
		return this;
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getCytoPanelName()
	 */
	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.WEST;
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getIcon()
	 */
	@Override
	public Icon getIcon() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getTitle()
	 */
	@Override
	public String getTitle() {
		return getName();
	}
	
	public Patient getSelectedPatient(){
		return pedigreePanel.getPatient();
	}
	
	public Patient getSelectedFather(){
		if( pedigreePanel.getFather().getName().equals("[none]") )
			return null;
		return pedigreePanel.getFather();
	}
	
	public Patient getSelectedMother(){
		if( pedigreePanel.getMother().getName().equals("[none]") )
			return null;
		return pedigreePanel.getMother();
	}
	
	public Patient getSelectedPatientA(){
		return comparisonPanel.getPatientA();
	}
	
	public Patient getSelectedPatientB(){
		return comparisonPanel.getPatientB();
	}
	
	public int getFilterComparison(){
		if( rbOnlyA.isSelected() )
			return 1;
		if( rbOnlyB.isSelected() )
			return 2;
		return 3;
	}
	
	public String getSelectedFamily(){
		if( isCompleteFamily() )
			return familyPedigreePanel.getSelectedFamily();
		else
			return getSelectedPatient().getFamily();
	}
	
	public List<Patient> getSelectedAffected(){
		return familyPedigreePanel.getSelectedAffected();
	}
	
	public List<Patient> getSelectedUnaffected(){
		return familyPedigreePanel.getSelectedUnaffected();
	}
	
	public void updateParentsLists(){
		listAnalysisMode = new JComboBox<String>();
		listAnalysisMode.addItem("Family trio");
		listAnalysisMode.addItem("Complete family");
		listAnalysisMode.addItem("Comparison");
		listAnalysisMode.addActionListener(this);
		
		JPanel panelListAM = new JPanel(new MigLayout("fill, ins 0, gap 0 0", "[pref!][]", ""));
		panelListAM.add(new JLabel("Analysis mode: "));
		panelListAM.add(listAnalysisMode, "growx");
		
		pedigreePanel = new PedigreeSelectionPanel();
		pedigreePanel.setPatientList( ServicesUtil.variantDatabase.getPatients() );
		
		familyPedigreePanel = new FamilyPedigreePanel();
		
		comparisonPanel = new ComparisonPanel();
		comparisonPanel.setPatientList( ServicesUtil.variantDatabase.getPatients() );
		
		initNetworkTab();
		initFilterTab();
		initQualityTab();
		initInfoTab();
		panelTopAutism.removeAll();
		panelTopAutism.add(panelListAM, "gapbottom 5px, span, growx, wrap");
		
		refreshParentsLists();
	}
	
	public void refreshParentsLists(){
		panelTopAutism.remove( pedigreePanel );
		panelTopAutism.remove( familyPedigreePanel );
		panelTopAutism.remove( comparisonPanel );
		panelTopAutism.remove( buttonOpenDataset );
		panelTopAutism.remove( buttonCreateNetwork );
		panelTopAutism.remove( buttonUpdateNetwork );
		
		if( listAnalysisMode.getSelectedItem().equals("Family trio") )
			panelTopAutism.add(pedigreePanel, "gapbottom 10px, span, growx, wrap");
		if( listAnalysisMode.getSelectedItem().equals("Complete family") )
			panelTopAutism.add(familyPedigreePanel, "gapbottom 10px, span, growx, wrap");
		if( listAnalysisMode.getSelectedItem().equals("Comparison") )
			panelTopAutism.add(comparisonPanel, "gapbottom 10px, span, growx, wrap");
		
		panelTopAutism.add(buttonOpenDataset, "gapbottom 10px,align center");
		panelTopAutism.add(buttonCreateNetwork, "gapbottom 10px,align center");
		panelTopAutism.add(buttonUpdateNetwork, "gapbottom 10px,align center, wrap");
		panelTopAutism.add(pQuickSearch, "gapbottom 10px, spanx, growx, wrap");
		buttonOpenDataset.setVisible(true);
		buttonCreateNetwork.setVisible(true);
		buttonUpdateNetwork.setVisible(true);
		buttonUpdateNetwork.setEnabled(false);
		revalidate();
		repaint();
	}
	
	public boolean isFamilyTrio(){
		if( listAnalysisMode.getSelectedItem().equals("Family trio") )
			return true;
		return false;
	}
	
	public boolean isCompleteFamily(){
		if( listAnalysisMode.getSelectedItem().equals("Complete family") )
			return true;
		return false;
	}
	
	public boolean isComparisonMode(){
		if( listAnalysisMode.getSelectedItem().equals("Comparison") )
			return true;
		return false;
	}
	
	public void updatePathwaysList(){
		List<String> selected = pathwayChoice.getSelection();
		pathwayChoice.setList( new ArrayList<String>(ServicesUtil.pathways.getPathwaysList()), true );
		pathwayChoice.select(selected);
	}
	
	public void updateImpactList(){
		if(ServicesUtil.variantDatabase!=null){
			listImpacts.setList( ServicesUtil.variantDatabase.listVariantImpacts(), false );
			loadFiltersProperties();
			if( listImpacts.getSelection().size()==0 )
				listImpacts.selectAll();
		}
	}
	
	public void updateFiltersList(){
		panelFilters.removeAll();
		revalidate();
		repaint();
		for(FilterComponent<? extends Filter> apf:listPanelFilters){
			apf.setAlignmentX(LEFT_ALIGNMENT);
			panelFilters.add(apf, "growx, pushx, wrap");
		}
		panelFilters.add(new JPanel(), "growy, pushy");
		revalidate();
		repaint();
	}
	
	public List<Filter> getFilters4Comparison(){
		ArrayList<Filter> fs = new ArrayList<Filter>();
		for(FilterComponent<? extends Filter> apf:listPanelFilters){
			if( apf.getVariantFilter() instanceof GenotypeFilter ){
				GenotypeFilter gf = (GenotypeFilter) apf.getVariantFilter();
				gf.setPatient( getSelectedPatientA() );
				GenotypeFilter gf2 = new GenotypeFilter(gf.getProperty(), gf.getOperator(), gf.getValue(), getSelectedPatientB());
				fs.add(gf);
				fs.add(gf2);
			}else
				fs.add(apf.getVariantFilter());
		}
		fs.add(new ImpactFilter(listImpacts.getSelection()));
		
		fs.addAll( getQualityFilters( getSelectedPatientA() ) );
		fs.addAll( getQualityFilters( getSelectedPatientB() ) );
		
		return fs;
	}
	
	public List<Filter> getFilters(){
		ArrayList<Filter> fs = new ArrayList<Filter>();
		for(FilterComponent<? extends Filter> apf:listPanelFilters){
			if( apf.getVariantFilter() instanceof GenotypeFilter ){
				GenotypeFilter gf = (GenotypeFilter) apf.getVariantFilter();
				gf.setPatient( getSelectedPatient() );
				fs.add(gf);
			}else{
				fs.add(apf.getVariantFilter());
			}
		}
		fs.add(new ImpactFilter(listImpacts.getSelection()));
		
		if( !listMendelianacity.getSelectedItem().equals("any") )
			fs.add( new InheritanceFilter((String)listMendelianacity.getSelectedItem(), getSelectedPatient(), getSelectedFather(), getSelectedMother()) );
		if( !listInheritance.getSelectedItem().equals("any") )
			fs.add( new InheritanceFilter((String)listInheritance.getSelectedItem(), getSelectedPatient(), getSelectedFather(), getSelectedMother()) );
		
		// create quality filters
		fs.addAll( getQualityFilters( getSelectedPatient() ) );
		
		if( checkQualityParents.getState() ){
			if( getSelectedFather() != null ){
				fs.addAll( getQualityFilters( getSelectedFather() ) );
			}
			if( getSelectedMother() != null ){
				fs.addAll( getQualityFilters( getSelectedMother() ) );
			}
		}
		
		return fs;
	}
	
	public List<Filter> getGenericFilters(){
		ArrayList<Filter> fs = new ArrayList<Filter>();
		for(FilterComponent<? extends Filter> apf:listPanelFilters){
			if( apf.getVariantFilter() instanceof GenotypeFilter ){
			
			}else{
				fs.add(apf.getVariantFilter());
			}
		}
		fs.add(new ImpactFilter(listImpacts.getSelection()));
		
		return fs;
	}
	
	public List<Filter> getInheritanceFilters(Patient p){
		ArrayList<Filter> fs = new ArrayList<Filter>();
		
		if( !listMendelianacity.getSelectedItem().equals("any") )
			fs.add( new InheritanceFilter((String)listMendelianacity.getSelectedItem(), p, p.getFather(), p.getMother()) );
		if( !listInheritance.getSelectedItem().equals("any") )
			fs.add( new InheritanceFilter((String)listInheritance.getSelectedItem(), p, p.getFather(), p.getMother()) );
		
		return fs;
	}
	
	public List<Filter> getFilters(Patient p){
		ArrayList<Filter> fs = new ArrayList<Filter>();
		for(FilterComponent<? extends Filter> apf:listPanelFilters){
			if( apf.getVariantFilter() instanceof GenotypeFilter ){
				GenotypeFilter gf = (GenotypeFilter) apf.getVariantFilter();
				gf.setPatient( p );
				fs.add(gf);
			}else{
				fs.add(apf.getVariantFilter());
			}
		}
		fs.add(new ImpactFilter(listImpacts.getSelection()));
		
		//NEW
		fs.addAll( getInheritanceFilters(p) );
		
//		if( !listMendelianacity.getSelectedItem().equals("any") )
//			fs.add( new InheritanceFilter((String)listMendelianacity.getSelectedItem(), p, getSelectedFather(), getSelectedMother()) );
//		if( !listInheritance.getSelectedItem().equals("any") )
//			fs.add( new InheritanceFilter((String)listInheritance.getSelectedItem(), p, getSelectedFather(), getSelectedMother()) );
		
		// create quality filters
		fs.addAll( getQualityFilters( p ) );
		
		//NEW
		if( checkQualityParents.getState() ){
			if( p.getFather() != null ){
				fs.addAll( getQualityFilters( p.getFather() ) );
			}
			if( p.getMother() != null ){
				fs.addAll( getQualityFilters( p.getMother() ) );
			}
		}
		
		return fs;
	}
	
	public List<GenotypeFilter> getQualityFilters(Patient p){
		ArrayList<GenotypeFilter> fs = new ArrayList<GenotypeFilter>();
		if( checkOverrideGT.getState() ){
			Genotype.overrideGT(true);
			Genotype.setMinAD( Integer.parseInt( tfMinAD.getText() ) );
		}else{
			Genotype.overrideGT(false);
			fs.add(new GenotypeFilter(GenotypeFilter.PROPERTY_GQ, ">=", tfQualityMinGQ.getText(), p ));
			fs.add(new GenotypeFilter(GenotypeFilter.PROPERTY_MIN_RATIO_HET, "", (rangeSlider.getLowValue()/100f)+"", p ));
			fs.add(new GenotypeFilter(GenotypeFilter.PROPERTY_MAX_RATIO_HET, "", (rangeSlider.getHighValue()/100f)+"", p ));
		}
		fs.add(new GenotypeFilter(GenotypeFilter.PROPERTY_DP, ">=", tfQualityMinDP.getText(), p ));
		return fs;
	}
	
	public List<GenotypeFilter> getQualityFilters(){
		ArrayList<GenotypeFilter> fs = new ArrayList<GenotypeFilter>();
		if( checkOverrideGT.getState() ){
			Genotype.overrideGT(true);
			Genotype.setMinAD( Integer.parseInt( tfMinAD.getText() ) );
		}else{
			Genotype.overrideGT(false);
			fs.add(new GenotypeFilter(GenotypeFilter.PROPERTY_GQ, ">=", tfQualityMinGQ.getText(), null ));
			fs.add(new GenotypeFilter(GenotypeFilter.PROPERTY_MIN_RATIO_HET, "", (rangeSlider.getLowValue()/100f)+"", null ));
			fs.add(new GenotypeFilter(GenotypeFilter.PROPERTY_MAX_RATIO_HET, "", (rangeSlider.getHighValue()/100f)+"", null ));
		}
		fs.add(new GenotypeFilter(GenotypeFilter.PROPERTY_DP, ">=", tfQualityMinDP.getText(), null ));
		return fs;
	}
	
	public boolean displayAllGenesOfInterest(){
		return checkDisplayInterestGenes.isSelected();
	}
	
	public List<String> getSelectedInteractionsTypes(){
		return interactionChoice.getSelection();
	}
	
	public List<String> getSelectedPathways(){
		return pathwayChoice.getSelection();
	}
	
	public int getNetworkExpansionLevel(){
		return networkExpansionPanel.getExpansionLevel();
	}
	
	public boolean runOnAllGenes(){
		return pathwayChoice.circumvent();
	}
	
	private boolean checkFeasability(){
		int nb_genes = 0;
		if( displayAllGenesOfInterest() ){
			//check with user... but BAD
			if( runOnAllGenes() ){
				return false;
			}
			
			for(String pathway:getSelectedPathways()){
				nb_genes += ServicesUtil.pathways.getGenes(pathway).size();
				if( nb_genes > 2000 )
					return false;
			}
		}else{
			if( runOnAllGenes() ){
				if( isFamilyTrio() ){
					for( Gene g:ServicesUtil.variantDatabase.getGenesCarryingVariant(getSelectedPatient(), getFilters()) ){
						nb_genes++;
						if( nb_genes > 2000 )
							return false;
					}
				}
				
			}else{
				if( isFamilyTrio() ){
					for(String pathway:getSelectedPathways()){
						nb_genes += ServicesUtil.pathways.getGenes(pathway).stream().filter(g -> ServicesUtil.variantDatabase.getGene(g, getFilters()).carryAFilteredVariant(getSelectedPatient())).count();
						if( nb_genes > 2000 )
							return false;
					}
				}
			}
		}
		
		return true;
	}
	
	private Set<String> stringToSet(String list){
		Set<String> l = new HashSet<String>();
		String split[] = list.split(",|;|\\s");
		
		for(String s:split)
			if(s.trim().length()>0)
				if( s.trim().endsWith("*") ){
					String prefix = s.trim().substring(0, s.trim().length()-1);
					System.out.println("Prefix: "+prefix);
					for(String g:ServicesUtil.variantDatabase.getAllGenesNames()){
						if(g.startsWith(prefix))
							l.add(g);
					}
				}else{
					l.add(s.trim());
				}
		return l;
	}
	
	public void updateToAnotherIndividual(String patient){
		
		Patient np = null;
		for(Patient p:ServicesUtil.variantDatabase.getPatients()){
			if(p.getName().equals(patient)){
				np = p;
				break;
			}
		}
		
		if( listAnalysisMode.getSelectedItem().equals("Family trio") ){
			pedigreePanel.setPatient(np);
			ServicesUtil.dialogTaskManager.execute(ServicesUtil.gravityTaskFactory.createTaskCreateNetwork(true, stringToSet(tfQuickSearch.getText())));
		}else{
			if( listAnalysisMode.getSelectedItem().equals("Complete family") ){
				familyPedigreePanel.setSelectedFamily(np.getFamily());
				ServicesUtil.dialogTaskManager.execute(ServicesUtil.gravityTaskFactory.createTaskCreateCompleteFamilyNetwork(true, stringToSet(tfQuickSearch.getText())));
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if( e.getActionCommand().equals("quick search") ){
			
			if( isFamilyTrio() ){
				ServicesUtil.dialogTaskManager.execute(ServicesUtil.gravityTaskFactory.createTaskCreateNetwork(false, stringToSet(tfQuickSearch.getText())));
			}else
				if( isCompleteFamily() ){
					ServicesUtil.dialogTaskManager.execute(ServicesUtil.gravityTaskFactory.createTaskCreateCompleteFamilyNetwork(false, stringToSet(tfQuickSearch.getText())));
				}else{
					if( isComparisonMode() ){
						ServicesUtil.dialogTaskManager.execute(ServicesUtil.gravityTaskFactory.createTaskCreateComparisonNetwork(false, stringToSet(tfQuickSearch.getText())));
					}
				}
			
		}
		
		if( e.getActionCommand().equals("delete quick search") ){
			tfQuickSearch.setText("");
		}
		
		if( e.getActionCommand().equals("load annotation") ){
			File f = new File("/Users/fcliquet/Databanks/pLI/ExACpLI_values.sorted.txt");
			ServicesUtil.dialogTaskManager.execute( ServicesUtil.gravityTaskFactory.createTaskImportAnnotations(f, "pLI", "Float", 0, 1) );
		}
		
		if( e.getActionCommand().equals("create network") || e.getActionCommand().equals("update network") ){
			buttonUpdateNetwork.setEnabled(true);		
			boolean update = false;
			if( e.getActionCommand().equals("update network") )
				update = true;
			
			if( isFamilyTrio() ){
				if( !checkFeasability() ){
					int n = JOptionPane.showConfirmDialog( ServicesUtil.swingApplication.getJFrame(),
						    "This network will contain over 2000 genes, it may takes a while to draw. Do you still want to proceed?",
						    "Do you want to draw more than 2000 genes?", JOptionPane.YES_NO_OPTION);
					if( JOptionPane.YES_OPTION != n ){
						return;
					}
				}
				ServicesUtil.dialogTaskManager.execute(ServicesUtil.gravityTaskFactory.createTaskCreateNetwork(update, null));
			}else
				if( isCompleteFamily() ){
					if( !checkFeasability() ){
						int n = JOptionPane.showConfirmDialog( ServicesUtil.swingApplication.getJFrame(),
							    "This network will contain over 2000 genes, it may takes a while to draw. Do you still want to proceed?",
							    "Do you want to draw more than 2000 genes?", JOptionPane.YES_NO_OPTION);
						if( JOptionPane.YES_OPTION != n ){
							return;
						}
					}
					ServicesUtil.dialogTaskManager.execute(ServicesUtil.gravityTaskFactory.createTaskCreateCompleteFamilyNetwork(update, null));
				}else{
					if( isComparisonMode() ){
						if( !checkFeasability() ){
							int n = JOptionPane.showConfirmDialog( ServicesUtil.swingApplication.getJFrame(),
								    "This network will contain over 2000 genes, it may takes a while to draw. Do you still want to proceed?",
								    "Do you want to draw more than 2000 genes?", JOptionPane.YES_NO_OPTION);
							if( JOptionPane.YES_OPTION != n ){
								return;
							}
						}
						ServicesUtil.dialogTaskManager.execute(ServicesUtil.gravityTaskFactory.createTaskCreateComparisonNetwork(update, null));
					}
				}
		}
		
		if( e.getActionCommand().equals("open new dataset") ){
			if( ServicesUtil.variantDatabase != null ){
				int n = JOptionPane.showConfirmDialog( ServicesUtil.swingApplication.getJFrame(),
					    "Are you sure that you want to close this dataset and load a new one?",
					    "Load a new dataset?", JOptionPane.YES_NO_OPTION);
				if( JOptionPane.YES_OPTION != n ){
					return;
				}
			}
			
			ServicesUtil.variantDatabase = null;
//			FileChooserFilter vcfFilter = new FileChooserFilter("Variant Call Format", "vcf");
			FileChooserFilter geminiFilter = new FileChooserFilter("Gemini database", "db");
			ArrayList<FileChooserFilter> filters = new ArrayList<FileChooserFilter>();
//			filters.add(vcfFilter);
			filters.add(geminiFilter);
			File file = ServicesUtil.fileUtil.getFile(ServicesUtil.swingApplication.getJFrame(), "choose file to open", FileUtil.LOAD, "/Users/fcliquet/Documents/Data/GHFC/", "Load", filters);
//			if( file.getName().toLowerCase().endsWith(".vcf") ){
//				ServicesUtil.variantDatabase = new VcfDatabase(file);
//				ServicesUtil.dialogTaskManager.execute(ServicesUtil.autismTaskFactory.createTaskLoadData(this));
//			}
			if( file.getName().toLowerCase().endsWith(".db") ){
				try {
					
					JPanel p = new JPanel(new MigLayout());
					p.add(new JLabel("Select and set the prefiltering values desired to improve loading time:"), "spanx,wrap");
					
					JCheckBox cbImpacts = new JCheckBox("impacts: ");
					cbImpacts.setSelected(true);
					p.add(cbImpacts);
					JPanel pImpacts = new JPanel();
					String impacts[] = {"HIGH (LGD)","MED (missense)","LOW (synonymous)"};
					JList<String> listImpacts = new JList<String>(impacts);
					int[] defaultImpacts = {0,1};
					listImpacts.setSelectedIndices(defaultImpacts);
					pImpacts.add( listImpacts );
					p.add(pImpacts, "spanx,wrap");
					
//					TODO
					JCheckBox cbPathways = new JCheckBox("pathways/genes: ");
					cbPathways.setSelected(false);
					p.add(cbPathways);
					JPanel pPathways = new JPanel();
					String pathways[] = loadPathwaysPropertiesBis();
					Arrays.sort(pathways);
					JList<String> listPathways = new JList<String>(pathways);
					int[] defaultPathways = {0};
					listPathways.setSelectedIndices(defaultPathways);
					pPathways.add( listPathways );
					p.add(pPathways, "spanx,wrap");
					
					JCheckBox cbMaf = new JCheckBox("maximum allele frequency <= ");
					cbMaf.setSelected(true);
					p.add(cbMaf);
					JPanel pMaf = new JPanel();
					JTextField tf_maf = new JTextField("0.01", 5);
					pMaf.add(tf_maf);
					p.add(pMaf, "spanx,wrap");
					
					JCheckBox cbCadd = new JCheckBox("CADD >= ");
					cbCadd.setSelected(true);
					p.add(cbCadd);
					JPanel pCadd = new JPanel();
					JTextField tf_cadd = new JTextField("15", 5);
					pCadd.add(tf_cadd);
					p.add(pCadd, "spanx,wrap");
					
					JCheckBox cbMultiallelic = new JCheckBox("ignore multiallelic variants");
					cbMultiallelic.setSelected(true);
					p.add(cbMultiallelic, "spanx,wrap");
					
//					JCheckBox cbCnvFields = new JCheckBox("CNV fields for arrays");
//					cbCnvFields.setSelected(false);
//					p.add(cbCnvFields, "spanx,wrap");
					
					JCheckBox cbRepeatMasker = new JCheckBox("remove CNVs with repeat masker");
					cbRepeatMasker.setSelected(false);
					p.add(cbRepeatMasker, "spanx,wrap");
					
//					JCheckBox cbTypes = new JCheckBox();
//					cbTypes.setSelected(true);
//					p.add(cbTypes);
//					JPanel pTypes = new JPanel();
//					pTypes.add( new JLabel("types of variants: ") );
//					String types[] = {"SNPs","INDELs","SVs"};
//					JList<String> listTypes = new JList<String>(types);
//					int[] defaultTypes = {0,1,2};
//					listTypes.setSelectedIndices(defaultTypes);
//					pTypes.add( listTypes );
//					p.add(pTypes, "spanx,wrap");
					
					
					int confirm = JOptionPane.showConfirmDialog(ServicesUtil.swingApplication.getJFrame(), p, "Data prefiltering", JOptionPane.OK_CANCEL_OPTION);
					
					String impactFilter = "";
					if( cbImpacts.isSelected() ){
						// having the 3 selected is the same as not filtering
						if( listImpacts.getSelectedValuesList().size()==3 ){
							impactFilter = "";
						}
						if( listImpacts.getSelectedValuesList().size()==2 ){
							if( !listImpacts.isSelectedIndex(0) ){
								impactFilter = "!= 'HIGH'";
							}
							if( !listImpacts.isSelectedIndex(1) ){
								impactFilter = "!= 'MED'";
							}
							if( !listImpacts.isSelectedIndex(2) ){
								impactFilter = "!= 'LOW'";
							}
						}
						if( listImpacts.getSelectedValuesList().size()==1 ){
							if( listImpacts.isSelectedIndex(0) ){
								impactFilter = "= 'HIGH'";
							}
							if( listImpacts.isSelectedIndex(1) ){
								impactFilter = "= 'MED'";
							}
							if( listImpacts.isSelectedIndex(2) ){
								impactFilter = "= 'LOW'";
							}
						}
					}
					
					String mafFilter = "1.0";
					if( cbMaf.isSelected() && tf_maf.getText().matches("[\\x00-\\x20]*[+-]?(((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*") ){
						mafFilter = tf_maf.getText();
					}
					String caddFilter = "0.0";
					if( cbCadd.isSelected() ) {
						caddFilter = tf_cadd.getText();
					}
					if( confirm == JOptionPane.OK_OPTION ){
						ServicesUtil.variantDatabase = new GeminiDatabase(file.getCanonicalPath(), mafFilter, caddFilter, impactFilter, cbMultiallelic.isSelected(), /*cbCnvFields.isSelected(),*/ cbRepeatMasker.isSelected(), cbPathways.isSelected(), ServicesUtil.pathways.getGenes(listPathways.getSelectedValuesList()));
						ServicesUtil.dialogTaskManager.execute(ServicesUtil.gravityTaskFactory.createTaskLoadData(this));
						ServicesUtil.resultCohortDetail.enableChildrenOnlyOption();
					}
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
		}
		
		if( e.getActionCommand().equals("filter frequency") ){
			listPanelFilters.add(new FilterComponent<VariantFilter>("Filter frequency", _nbFilter++, this, ServicesUtil.variantDatabase.listFrequencyAttributes(), false, new VariantFilter()));
			updateFiltersList();
		}
		
		if( e.getActionCommand().equals("filter cnv") ){
			listPanelFilters.add(new FilterComponent<VariantFilter>("Filter CNVs", _nbFilter++, this, ServicesUtil.variantDatabase.listCNVsAttributes(), true, new VariantFilter()));
			updateFiltersList();
		}
		
		if( e.getActionCommand().equals("filter patho") ){
			listPanelFilters.add(new FilterComponent<VariantFilter>("Filter pathogenecity", _nbFilter++, this, ServicesUtil.variantDatabase.listPathoAttributes(), true, new VariantFilter()));
			updateFiltersList();
		}
		
		if( e.getActionCommand().equals("filter other") ){
			listPanelFilters.add(new FilterComponent<VariantFilter>("Filter", _nbFilter++, this, ServicesUtil.variantDatabase.listOtherAttributes(), true, new VariantFilter()));
			updateFiltersList();
		}
		
//		if( e.getActionCommand().equals("add new filter") ){
//			listPanelFilters.add(new FilterComponent<VariantFilter>(FilterComponent.TYPE_TEXT, _nbFilter++, this, ServicesUtil.variantDatabase.listVariantAttributes(), new VariantFilter()));
//			updateFiltersList();
//		}
//		
//		if( e.getActionCommand().equals("add new gt filter") ){
//			listPanelFilters.add(new FilterComponent<GenotypeFilter>(FilterComponent.TYPE_TEXT, _nbFilter++, this, GenotypeFilter.getProperties(), new GenotypeFilter()));
//			updateFiltersList();
//		}
		
		if( e.getActionCommand().equals("remove filter") ){
			JButton b = (JButton)e.getSource();
			listPanelFilters.remove(b.getParent());
			updateFiltersList();
		}
		
		if( e.getActionCommand().equals("save defaults filters") ){
			saveFiltersProperties();
		}

		if( e.getActionCommand().equals("load defaults filters") ){
			loadFiltersProperties();
		}
		
		if( e.getActionCommand().equals("save defaults qualities") ){
			saveQualityProperties();
		}

		if( e.getActionCommand().equals("load defaults qualities") ){
			loadQualityProperties();
		}
		
		if( e.getSource() == listAnalysisMode ){
			if(e.getActionCommand().equals("comboBoxChanged")){
				refreshParentsLists();
				refreshFilterTab();
			}
		}
	}

}
