/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.cytoscape.util.swing.IconManager;

import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.TextIcon;
import net.miginfocom.swing.MigLayout;

/**
 * Component representing a patient. It is mainly used in the FamilyPedigreePanel.
 * 
 * <p>It consists mainly in a small button containing a circle or square depending on the sex. 
 * An icon in the middle define if we are in presence of an affecter or unaffected person. 
 * The name displayed is the short version.</p>
 * 
 * <p>For the interactions it accepts left and right clicks.<br> Left click will circle through "green selected", 
 * "red selected" and not selected. This allows for much finer control to the user than using strictly the 
 * affected / unaffected notion of the pedigree, which might be incomplete (If he is unaffected for ASD 
 * but has ID for instance, it is not the same as unaffected for ASD and healthy).<br>
 * Right click will change the affected / unaffected sign and switch inside the component 
 * (but will not change the original patient used for the component).</p>
 * 
 * @author Freddy Cliquet
 * @version 1.0
 */
public class PatientComponent extends JPanel {

	private static final long serialVersionUID = -5702916485686145999L;
	private Patient patient;
	private JLabel labelName;
	private int sex, phenotype;
	private int selected = 0;
	
	/**
	 * Not selected state for the component
	 */
	public static final int NOT_SELECTED = 0;
	
	/**
	 * "carrying a mutation" "Affected" state for the component.
	 * That is green selected.
	 */
	public static final int MUTATED = 1;
	
	/**
	 * "not carrying a mutation" "Unaffected" state for the component.
	 * That is red selected.
	 */
	public static final int NOT_MUTATED = 2;
	
	/**
	 * Function to get access to the Patient represented by this component.
	 * @return The Patient represented by the component
	 */
	public Patient getPatient(){
		return patient;
	}
	
	/**
	 * Function to get the selected state of the component
	 * @return selected state of the component
	 */
	public int isSelected(){
		return selected;
	}
	
	/**
	 * Function to access the phenotype register by the component, not the original one from the patient. meaning the one that can be changed by right clicking.
	 * @return true if the patient is affected
	 */
	public boolean isAffected(){
		return phenotype == Patient.AFFECTED;
	}
	
	/**
	 * Function to access the phenotype register by the component, not the original one from the patient. meaning the one that can be changed by right clicking.
	 * @return true if the patient is unaffected
	 */
	public boolean isUnaffected(){
		return phenotype == Patient.UNAFFECTED;
	}
	
	/**
	 * Constructor for a new Component based on the given patient
	 * @param p Patient
	 */
	public PatientComponent(Patient p) {
		patient = p;
		sex = p.getPatientSex();
		phenotype = p.getPatientPhenotype();
		
		if( p.getPatientPhenotype() == Patient.AFFECTED )
			selected = MUTATED;
		else
			selected = NOT_MUTATED;
		
		setBorder(BorderFactory.createEtchedBorder());
		setToolTip();
		
		setLayout(new MigLayout("fill, ins 0, gap 0 0", "[][min!]", ""));
		
		final Color color_not_selected= UIManager.getColor("Panel.background");
		final Color color_mutated = new Color(164, 205, 0);
		final Color color_not_mutated = new Color(225, 64, 0);
		
		if( selected == MUTATED )
			setBackground( color_mutated );
		if( selected == NOT_MUTATED )
			setBackground( color_not_mutated );
		if( selected == NOT_SELECTED )
			setBackground( color_not_selected );
		
		addMouseListener(new MouseListener(){
			@Override
			public void mouseClicked(MouseEvent e) {
				if( e.getButton() == MouseEvent.BUTTON1 ){
					selected = (selected+1) % 3;
					
					if( selected == MUTATED )
						setBackground( color_mutated );
					if( selected == NOT_MUTATED )
						setBackground( color_not_mutated );
					if( selected == NOT_SELECTED )
						setBackground( color_not_selected );
					
					setToolTip();
					repaint();
				}
				if( e.getButton() == MouseEvent.BUTTON3 ){
					if( phenotype == Patient.AFFECTED )
						phenotype = Patient.UNAFFECTED;
					else
						phenotype = Patient.AFFECTED;
					setToolTip();
					repaint();
				}
			}
			@Override
			public void mousePressed(MouseEvent e) {}
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseExited(MouseEvent e) {}
		});
		
		@SuppressWarnings("serial")
		JPanel pp = new JPanel(){
			@Override
			protected void paintComponent(Graphics g) {
				Graphics2D g2 = (Graphics2D)g.create();
				g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON );
				g2.setColor(Color.BLACK);
				g2.setFont( ServicesUtil.iconManager.getIconFont(28f) );
				if( sex == Patient.MALE ){
					g2.drawRect(0, 2, 22, 22);
					g2.drawRect(1, 3, 20, 20);
					g2.drawRect(2, 4, 18, 18);
//					g2.drawString(IconManager.ICON_SQUARE_O, 0, 24);
				}else{
//					g2.drawOval(1, 1, 23, 23);
//					g2.drawOval(2, 2, 21, 21);
					g2.drawString(IconManager.ICON_CIRCLE_O, 0, 24);
				}
				g2.setFont( ServicesUtil.iconManager.getIconFont(16f) );
				if( phenotype == Patient.AFFECTED )
					g2.drawString(IconManager.ICON_STETHOSCOPE, 6, 20);
//				else
//					g2.drawString("H", 6, 20);
			}
		};
		pp.setPreferredSize(new Dimension(25,28));
		add(pp, "alignx center, spanx, wrap");
		
		labelName = new JLabel( patient.getShortName() );
		add( labelName , "alignx center,spanx, wrap" );
		
		setMinimumSize(new Dimension( Math.max(pp.getPreferredSize().width, labelName.getPreferredSize().width)+7 , getPreferredSize().height));
	}
	
	private void setToolTip(){
		setToolTipText("<html>Patient: "+patient.getName()
		//+"<br>Family: "+patient.getFamily()
		+"<br>Father: "+patient.getFatherName()
		+"<br>Mother: "+patient.getMotherName()
		+"<br>Sex: "+(sex==Patient.MALE ? "Male" : "Female")
		+"<br>Phenotype: "+(phenotype==Patient.AFFECTED ? "Affected" : "Unaffected")
//		+"<br>State: "+( selected ? "Selected" : "Not selected" )
		+"<br><br><i>left-click to select / unselect"
		+"<br>right click to change phenotype");
	}
	
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		Patient p = new Patient("C0733-011-105-001", "C0733-011-105", "C0733-011-105-002", "C0733-011-105-003", 1,	2);
//		Patient p2 = new Patient("387834-LEUVEN","LEUVEN","393737-LEUVEN","393738-LEUVEN",2,2);
//		
//		PatientComponent pc = new PatientComponent(p);
//		PatientComponent pc2 = new PatientComponent(p2);
//		
//		JPanel panel = new JPanel( new MigLayout() );
//		panel.add(pc);
//		panel.add(pc2);
//		
//		JFrame f = new JFrame("pedigree");
//		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		f.getContentPane().add( panel );
//		f.validate();
//		f.pack();
//		f.setVisible(true);
//	}

}
