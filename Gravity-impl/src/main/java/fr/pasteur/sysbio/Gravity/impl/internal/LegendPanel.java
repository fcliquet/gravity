/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;

/**
 * @author Freddy Cliquet
 * @version 1.0
 *
 */
public class LegendPanel extends JPanel implements CytoPanelComponent {

	private static final long serialVersionUID = -1388543687330407765L;

	public LegendPanel() {
		JLabel label = new JLabel();
		ImageIcon ii = new ImageIcon(getClass().getResource("/images/legend.png"));
		label.setIcon(ii);
		add(label);
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getComponent()
	 */
	@Override
	public Component getComponent() {
		return this;
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getCytoPanelName()
	 */
	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.SOUTH;
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getIcon()
	 */
	@Override
	public Icon getIcon() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.application.swing.CytoPanelComponent#getTitle()
	 */
	@Override
	public String getTitle() {
		return getName();
	}

}
