/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTableUtil;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import fr.pasteur.sysbio.Gravity.API.Filter;
import fr.pasteur.sysbio.Gravity.API.GenotypeFilter;
import fr.pasteur.sysbio.Gravity.API.GenotypeUtils;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.API.VariantFilter;
import fr.pasteur.sysbio.Gravity.API.gemini.GeminiVariant;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class CsvExportTask extends AbstractTask {

	private File _file;
	private String _individuals;
	private String _format;
	
	public CsvExportTask(File file, String individuals, String format){
		_file = file;
		_individuals = individuals;
		_format = format;
		System.out.println("'"+_individuals+"' '"+_format+"'");
	}
	
	private static boolean checkQuality(List<Filter> filters, Variant v){
		for(Filter f:filters){
			if( !f.passFilter(v) )
				return false;
		}
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.work.AbstractTask#run(org.cytoscape.work.TaskMonitor)
	 */
	@Override
	public void run(TaskMonitor arg0) throws Exception {
		PrintWriter pw = new PrintWriter(_file);
		List<CyNode> nodes = CyTableUtil.getNodesInState(ServicesUtil.lastNetwork, CyNetwork.SELECTED, true);
		List<Patient> patients = null;
		
		switch(_individuals){
			case "all":
				patients = ServicesUtil.variantDatabase.getPatients();
				break;
			case "patients":
				patients = ServicesUtil.variantDatabase.getPatientsWithParents();
				break;
			case "selected family":
				patients = ServicesUtil.variantDatabase.getPatients( ServicesUtil.controlPanel.getSelectedFamily() );
				break;
			case "selected patient":
				patients = new ArrayList<Patient>();
				patients.add(ServicesUtil.controlPanel.getSelectedPatient());
				break;
		}
		
//		if(ServicesUtil.resultCohortDetail.isChildrenOnly()){
//			patients = ServicesUtil.variantDatabase.getPatientsWithParents();
//		}else{
//			patients = ServicesUtil.variantDatabase.getPatients();
//		}
		
		
		switch( _format ){
		case "compact":
			pw.println( "chromosome,position,reference,alternative,impact,aa_change,aa_length,max_aaf,cohort_aaf,gene,cadd,mpc,patient,family,father,mother,affected,control,GT,de novo,inheritance,DP,Alternative allele ratio,GQ,Phred2" );
			break;
		case "full":
			pw.println( "chromosome,position,reference,alternative,impact,aa_change,aa_length,max_aaf,cohort_aaf,gene,cadd,mpc,patient,family,father,mother,affected,control,GT,de novo,inheritance,DP,Alternative allele ratio,GQ,Phred2,aaf_esp_ea,aaf_esp_aa,aaf_esp_all,aaf_1kg_amr,aaf_1kg_eas,aaf_1kg_sas,aaf_1kg_afr,aaf_1kg_eur,aaf_1kg_all,aaf_exac_all,aaf_adj_exac_all,aaf_adj_exac_afr,aaf_adj_exac_amr,aaf_adj_exac_eas,aaf_adj_exac_fin,aaf_adj_exac_nfe,aaf_adj_exac_oth,aaf_adj_exac_sas,polyphen_pred,polyphen_score,sift_pred,sift_score,pathways" );
			break;
		case "cnv":
			pw.println( "chromosome,position,reference,alternative,impact,aa_change,aa_length,max_aaf,cohort_aaf,gene,cadd,mpc,patient,family,father,mother,affected,control,GT,de novo,inheritance,DP,Alternative allele ratio,GQ,Phred2,pathways,cnv_length,cnv_tool,vep_cia_refstart,vep_cia_refstop,vep_cia_concat,vep_cia_concatstart,vep_cia_concatstop,vep_cia_concatsize,vep_cia_concatoverlap,vep_cia_concatcn,vep_cia_concat#snp,vep_cia_concatdensity,vep_cnvision_overlap,vep_cnvision_cn,vep_cnvision_#snp,vep_cnvision_density,vep_cnvision_score,vep_cnvision_#algos,vep_cnvision_algos,vep_cnvision_%3algos,vep_cnvision_%2algos,vep_cnvision_%1algo,vep_cohortfreq%,vep_isunique,vep_#dgv_hits,vep_dgv_ref,vep_dgv_observedevents,vep_dgv_samplesize,vep_dgv_freq%" );
			break;
		}
		
		if( nodes.size() >= 1 ){
			for(CyNode node:nodes){
				String geneName = ServicesUtil.lastNetwork.getRow( node ).get(CyNetwork.NAME, String.class);
				for(Variant v:ServicesUtil.variantDatabase.getGene(geneName, ServicesUtil.controlPanel.getGenericFilters()).getFilteredVariants()){
					for(Patient p:patients){
						if( GenotypeUtils.isCarryingVariant( v.getPatientGenotype( p ) ) && checkQuality( ServicesUtil.controlPanel.getFilters(p), v ) ){
							pw.print( v.getChromosome() + "," );
							pw.print( v.getPosition() + "," );
							pw.print( v.getReference() + "," );
							pw.print( v.getAlternative() + "," );
							pw.print( v.impact() + "," );
							
							if( v instanceof GeminiVariant ){
								GeminiVariant gv = (GeminiVariant)v;
								try {
									pw.print( ((String)gv.getObject("aa_change")).split("/")[0]+((String)gv.getObject("aa_length")).split("/")[0]+((String)gv.getObject("aa_change")).split("/")[1] + "," );
									pw.print( ((String)gv.getObject("aa_length")) + "," );
								} catch (Exception e) {
									pw.print( "," );
									pw.print( "," );
								}
							}else{
								pw.print( "," );
								pw.print( "," );
							}
							
							pw.print( v.maxAlleleFrequency() + "," );
							pw.print( v.cohortFrequency() + "," );
							pw.print( v.getGene() + "," );
							pw.print( (v.cadd_scaled()!=null ? v.cadd_scaled() : "") + "," );
							if( v instanceof GeminiVariant ){
								GeminiVariant gv = (GeminiVariant)v;
								try {
									pw.print( (gv.getObject("MPC")!=null ? (gv.getObject("MPC")) : "") + "," );  
								} catch (Exception e) {
									pw.print( "," );
								}
							}else{
								pw.print( "," );
							}
							pw.print( p.getName() + "," );
							pw.print( p.getFamily() + "," );
							pw.print( p.getFatherName() + "," );
							pw.print( p.getMotherName() + "," );
							pw.print( p.getPatientPhenotype() + "," );
							pw.print( p.getPatientFamilyType() + "," );
							pw.print( v.getPatientGenotype(p) + "," );
							
							if( p.hasParents() ){
								if( !_individuals.equals("selected patient")  ){
									pw.print( GenotypeUtils.isDeNovo(v.getPatientGenotype(p), v.getPatientGenotype(p.getFather()), v.getPatientGenotype(p.getMother())) + "," );
									pw.print( GenotypeUtils.inheritance(v.getPatientGenotype(p), p.getPatientSex(), v.getChromosome(), v.getPatientGenotype(p.getFather()), v.getPatientGenotype(p.getMother())) + "," );
									
								}else{
									pw.print( GenotypeUtils.isDeNovo(v.getPatientGenotype(p), v.getPatientGenotype( ServicesUtil.controlPanel.getSelectedFather() ), v.getPatientGenotype( ServicesUtil.controlPanel.getSelectedMother() )) + "," );
									pw.print( GenotypeUtils.inheritance(v.getPatientGenotype(p), p.getPatientSex(), v.getChromosome(), v.getPatientGenotype(ServicesUtil.controlPanel.getSelectedFather()), v.getPatientGenotype(ServicesUtil.controlPanel.getSelectedMother())) + "," );
									
								}
								
							}else{
								pw.print( "," );
								pw.print( "," );
							}
							
							
							pw.print( GenotypeUtils.DP( v.getPatient(p) ) + "," );
							pw.print( GenotypeUtils.alternativeAlleleRatio( v.getPatient(p) ) + "," );
							pw.print( GenotypeUtils.GQ( v.getPatient(p) ) + "," );
							pw.print( GenotypeUtils.Phreds( v.getPatient(p) ) );
							
							switch( _format ){
							case "compact":
								break;
							case "full":
								pw.print( "," + v.getObject("aaf_esp_ea") );
								pw.print( "," + v.getObject("aaf_esp_aa") );
								pw.print( "," + v.getObject("aaf_esp_all") );
								pw.print( "," + v.getObject("aaf_1kg_amr") );
								pw.print( "," + v.getObject("aaf_1kg_eas") );
								pw.print( "," + v.getObject("aaf_1kg_sas") );
								pw.print( "," + v.getObject("aaf_1kg_afr") );
								pw.print( "," + v.getObject("aaf_1kg_eur") );
								pw.print( "," + v.getObject("aaf_1kg_all") );
								pw.print( "," + v.getObject("aaf_exac_all") );
								pw.print( "," + v.getObject("aaf_adj_exac_all") );
								pw.print( "," + v.getObject("aaf_adj_exac_afr") );
								pw.print( "," + v.getObject("aaf_adj_exac_amr") );
								pw.print( "," + v.getObject("aaf_adj_exac_eas") );
								pw.print( "," + v.getObject("aaf_adj_exac_fin") );
								pw.print( "," + v.getObject("aaf_adj_exac_nfe") );
								pw.print( "," + v.getObject("aaf_adj_exac_oth") );
								pw.print( "," + v.getObject("aaf_adj_exac_sas") );
								pw.print( "," + v.getObject("polyphen_pred") );
								pw.print( "," + v.getObject("polyphen_score") );
								pw.print( "," + v.getObject("sift_pred") );
								pw.print( "," + v.getObject("sift_score") );
								pw.print( "," + ServicesUtil.pathways.getPathwaysList(v.getGene()).toString().replaceAll(",", ";") );  //pathways list
								break;
							case "cnv":
								pw.print( "," + ServicesUtil.pathways.getPathwaysList(v.getGene()).toString().replaceAll(",", ";") );  //pathways list
								pw.print( "," + v.getObject("sv_length") );
								pw.print( "," + v.getObject("sv_tool") );
								
								//TODO
								pw.print( "," + v.getObject("vep_cia_refstart") );
								pw.print( "," + v.getObject("vep_cia_refstop") );
								pw.print( "," + v.getObject("vep_cia_concat") );
								pw.print( "," + v.getObject("vep_cia_concatstart") );	
								pw.print( "," + v.getObject("vep_cia_concatstop") );	
								pw.print( "," + v.getObject("vep_cia_concatsize") );	
								pw.print( "," + v.getObject("vep_cia_concatoverlap") );	
								pw.print( "," + v.getObject("vep_cia_concatcn") );	
								pw.print( "," + v.getObject("vep_cia_concat#snp") );	
								pw.print( "," + v.getObject("vep_cia_concatdensity") );	
								pw.print( "," + v.getObject("vep_cnvision_overlap") );	
								pw.print( "," + v.getObject("vep_cnvision_cn") );	
								pw.print( "," + v.getObject("vep_cnvision_#snp") );	
								pw.print( "," + v.getObject("vep_cnvision_density") );	
								pw.print( "," + v.getObject("vep_cnvision_score") );	
								pw.print( "," + v.getObject("vep_cnvision_#algos") );	
								pw.print( "," + v.getObject("vep_cnvision_algos") );	
								pw.print( "," + v.getObject("vep_cnvision_%3algos") );	
								pw.print( "," + v.getObject("vep_cnvision_%2algos") );	
								pw.print( "," + v.getObject("vep_cnvision_%1algo") );	
								pw.print( "," + v.getObject("vep_cohortfreq%") );	
								pw.print( "," + v.getObject("vep_isunique") );	
								pw.print( "," + v.getObject("vep_#dgv_hits") );	
								pw.print( "," + v.getObject("vep_dgv_ref") );	
								pw.print( "," + v.getObject("vep_dgv_observedevents") );	
								pw.print( "," + v.getObject("vep_dgv_samplesize") );	
								pw.print( "," + v.getObject("vep_dgv_freq%") );
							}
							
							pw.println("");
						}
					}
				}
			}	
		}
		
		pw.flush();
		pw.close();
	}

}
