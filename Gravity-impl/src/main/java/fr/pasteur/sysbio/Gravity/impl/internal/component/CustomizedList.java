/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import fr.pasteur.sysbio.Gravity.impl.internal.utils.TextIcon;
import net.miginfocom.swing.MigLayout;

/**
 * Class defining a component consisting of a panel with a list inside and small buttons in columns on its right side.
 * The number of those buttons is variable and configurable by the user.
 * Additionally it is possible to add a checkbox underneath with a custom message to allow to circumvent the list. 
 * 
 * <p>Buttons in the right column are drawn in the order they are added.</p>
 * 
 * @author Freddy Cliquet
 * @version 2.0
 */
public class CustomizedList extends JPanel implements ActionListener {

	private static final long serialVersionUID = -4003513795143709114L;
	private String title;
	
	private JList<String> list;
	private DefaultListModel<String> modelList;
	private JPanel panelControls, panelCircumvent;
	private JCheckBox checkCircumventList;
	
	/**
	 * Create a new CustomizedList panel with the given title. The title is written in a border around the panel.
	 * @param windowTitle Title of the panel
	 */
	public CustomizedList(String windowTitle) {
		title = windowTitle;
		initComponents();
	}
	
	private void initComponents(){
		setBorder(BorderFactory.createTitledBorder(title));
		setLayout(new MigLayout("fill, ins 0, gap 0 0", "[][min!]", ""));
		
		list = new JList<String>();
		modelList = new DefaultListModel<String>();
		list.setModel(modelList);
		list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		JScrollPane scrollList = new JScrollPane(list);
		scrollList.setAlignmentY(TOP_ALIGNMENT);
		add(scrollList, "grow, push");
		
		panelControls = new JPanel(new MigLayout("fill, ins 0, gap 0 0", "[min!]", "[min!]"));
		add(panelControls, "gapleft 2px,wrap");
		
		panelCircumvent = new JPanel(new MigLayout("fill, ins 0, gap 0 0"));
		add(panelCircumvent, "span, growx");
	}
	
	/**
	 * Function used to define the content of the list component.
	 * @param list List of element to display in our container
	 * @param sort true if the list must be sorted (natural string order), false else
	 */
	public void setList(List<String> list, boolean sort){
		modelList.clear();
		if(sort)
			Collections.sort(list);
		for(String s:list){
			modelList.addElement(s);
		}
	}
	
	/**
	 * Add the possibility to circumvent the list with a checkbox.
	 * @param message message of the circumvent option (checkbox underneath the list)
	 */
	public void addCircumventOption(String message){
		checkCircumventList = new JCheckBox(message);
		checkCircumventList.setActionCommand("check circumvent");
		checkCircumventList.addActionListener(this);
		panelCircumvent.removeAll();
		panelCircumvent.add(checkCircumventList, "gaptop 3px, gapbottom 1px, spanx");
	}
	
	/**
	 * Function creating a new button on the right side of the list. 
	 * This constructor is for a simple text label.
	 * 
	 * @param text text label of the button
	 * @param tooltip tooltip of the button
	 * @param listener listener for the button
	 */
	public void addButton(String text, String tooltip, ActionListener listener){
		addButton( new TextIcon(new JButton(), text), tooltip, listener );
	}
	
	/**
	 * Function creating a new button on the right side of the list. 
	 * This constructor is for an icon label.
	 * 
	 * @param icon label icon of the button
	 * @param tooltip tooltip of the button
	 * @param listener listener for the button
	 */
	public void addButton(Icon icon, String tooltip, ActionListener listener){
		JButton button = new JButton(icon);
		button.addActionListener(listener);
		if( tooltip != null )
			button.setToolTipText(tooltip);
		panelControls.add(button, "growx, wrap");
	}
	
	/**
	 * Function creating a new button on the right side of the list. 
	 * This constructor is for a simple text label.
	 * 
	 * @param text text label of the button
	 * @param listener listener for the button
	 */
	public void addButton(String text, ActionListener listener){
		addButton( new TextIcon(new JButton(), text), null, listener );
	}
	
	/**
	 * Function creating a new button on the right side of the list. 
	 * This constructor is for an icon label.
	 * 
	 * @param icon label icon of the button
	 * @param listener listener for the button
	 */
	public void addButton(Icon icon, ActionListener listener){
		addButton(icon,  null, listener);
	}
	
	/**
	 * add a vertical spacer between 2 buttons on the right column.
	 */
	public void addSpacer(){
		panelControls.add(new JPanel(), "hmax 25,wrap");
	}
	
	/**
	 * Add a predefined button in the right column that will select all elements in the list.
	 */
	public void addSelectAllButton(){
		JButton buttonSelectAll = new JButton( new TextIcon(new JButton(), "all") );
		buttonSelectAll.setActionCommand("select all");
		buttonSelectAll.addActionListener(this);
		buttonSelectAll.setFont(buttonSelectAll.getFont().deriveFont(10f));
		panelControls.add(buttonSelectAll, "growx, wrap");
	}
	
	/**
	 * Add a predefined button in the right column that will unselect everything in the list.
	 */
	public void addSelectNoneButton(){
		JButton buttonSelectAll = new JButton( new TextIcon(new JButton(), "none") );
		buttonSelectAll.setActionCommand("select none");
		buttonSelectAll.addActionListener(this);
		buttonSelectAll.setFont(buttonSelectAll.getFont().deriveFont(10f));
		panelControls.add(buttonSelectAll, "growx, wrap");
	}
	
	/**
	 * Function to select all elements from the list.
	 */
	public void selectAll(){
		list.setSelectionInterval(0, list.getModel().getSize()-1);
	}
	
	/**
	 * Select the given element in the list.
	 * @param s element to be selected
	 */
	public void select(String s){
		for( int i=0;i<list.getModel().getSize() ; i++ ){
			if( list.getModel().getElementAt(i).equals(s) ){
				list.setSelectedIndex( i );
				return;
			}
		}
	}
	
	/**
	 * Select the given elements from the list.
	 * @param entries list of elements to be selected
	 */
	public void select(List<String> entries){
		ArrayList<Integer> indices = new ArrayList<Integer>();
		for(String s:entries){
			for( int i=0;i<list.getModel().getSize() ; i++ ){
				if( list.getModel().getElementAt(i).equals(s) ){
					indices.add(i);
				}
			}
		}
		int indices2[] = new int[indices.size()];
		for(int i=0;i<indices2.length;i++)
			indices2[i]=indices.get(i);
		list.setSelectedIndices(indices2);
	}
	
	/**
	 * Function to get the List of the selected elements.
	 * @return List of selected elements
	 */
	public List<String> getSelection(){
		return list.getSelectedValuesList();
	}
	
	/**
	 * Allows to know if the user wants to circumvent the choices. 
	 * @return true when circumventing the list
	 */
	public boolean circumvent(){
		return checkCircumventList.isSelected();
	}
	
	/**
	 * Allows to force check or uncheck the circumvent option
	 * @param circumvent new state of the circumvent option
	 */
	public void setCircumvent(boolean circumvent){
		checkCircumventList.setSelected(circumvent);
		list.setEnabled( !checkCircumventList.isSelected() );
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if( e.getActionCommand().equals("select all") ){
			list.setSelectionInterval(0, list.getModel().getSize()-1);
		}
		if( e.getActionCommand().equals("select none") ){
			list.clearSelection();
		}
		if( e.getActionCommand().equals("check circumvent") ){
			list.setEnabled( !checkCircumventList.isSelected() );
		}
	}

//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		JFrame f = new JFrame();
//		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		CustomizedList c = new CustomizedList("Pathways/Genes");
//		c.addCircumventOption("Run on all genes");
//		ActionListener al = new ActionListener() {
//			
//			@Override
//			public void actionPerformed(ActionEvent e) {
//				ArrayList<String> l = new ArrayList<String>();
//				l.add("Class_I");
//				l.add("Class_II");
//				l.add("Class_III");
//				l.add("SFARI");
//				l.add("FMRP_target");
//				l.add("KEGG_CellAdhesion");
//				l.add("KEGG_Tryptophan");
//				l.add("KEGG_Cholinergic_Synapse");
//				l.add("KEGG_Dopaminergic_Synapse");
//				c.setList(l, true);
//			}
//		};
//		c.addButton("+", al);
//		c.addSpacer();
//		TextIcon ti = new TextIcon(new JButton(), "all");
//		c.addButton(ti, al);
//		c.addSpacer();
//		c.addSelectAllButton();
//		c.addSelectNoneButton();
//		f.getContentPane().add( c );
//		f.validate();
//		f.pack();
//		f.setVisible(true);
//	}

}
