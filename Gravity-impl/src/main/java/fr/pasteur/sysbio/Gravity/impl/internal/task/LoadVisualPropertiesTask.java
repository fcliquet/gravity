/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;

import java.util.Set;

import javax.swing.SwingUtilities;

import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;

/**
 * @author Freddy Cliquet
 * @version 1.0
 *
 */
public class LoadVisualPropertiesTask extends AbstractTask {

	private CyNetworkView _view;
	
	public LoadVisualPropertiesTask(CyNetworkView view){
		_view = view;
	}
	
	private boolean haveStyle(VisualMappingManager vmm, String style){
		for (VisualStyle vs: vmm.getAllVisualStyles()) {
            if (vs.getTitle().equals(style))
                return true;
        }
        return false;
	}
	
	private void getStyle(VisualMappingManager vmm, String style){
		Set<VisualStyle> vsSet = ServicesUtil.loadVizmapFileTaskFactory.loadStyles(getClass().getResourceAsStream("/styles.xml"));
		if( vsSet.size() > 0 ){
			ServicesUtil.visualStyle = vsSet.iterator().next();
			vmm.addVisualStyle(ServicesUtil.visualStyle);
		}
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.work.AbstractTask#run(org.cytoscape.work.TaskMonitor)
	 */
	@Override
	public void run(TaskMonitor arg0) throws Exception {
		arg0.setStatusMessage("create style & update task");
		String myStyle = "gravity";
		
		if( !haveStyle(ServicesUtil.visualMappingManager, myStyle) ){
			getStyle( ServicesUtil.visualMappingManager, myStyle );
		}
		ServicesUtil.visualMappingManager.setVisualStyle(ServicesUtil.visualStyle, _view);
	}

}
