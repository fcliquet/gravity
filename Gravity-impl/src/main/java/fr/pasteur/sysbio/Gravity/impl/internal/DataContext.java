package fr.pasteur.sysbio.Gravity.impl.internal;

import java.util.ArrayList;
import java.util.List;

import org.cytoscape.work.Tunable;
import org.cytoscape.work.util.ListSingleSelection;

import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.VariantDatabase;

/**
 * 
 * @author Freddy Cliquet
 * @version 1.0
 */
public class DataContext {
	// DATA LOADING
	
	@Tunable(description="Patient", groups="Data")
	public ListSingleSelection<Patient> patient;
	
	@Tunable(description="Father", groups="Data", listenForChange="Patient")
	public ListSingleSelection<Patient> father;
	
	@Tunable(description="Mother", groups="Data", listenForChange="Patient")
	public ListSingleSelection<Patient> mother;
	
	// FONCTIONS...
	
	public DataContext(){
		if(ServicesUtil.variantDatabase != null)
			updateParentsLists(ServicesUtil.variantDatabase);
	}
	
	public void updateParentsLists(VariantDatabase vcf){
		List<Patient> l = new ArrayList<Patient>(vcf.getPatients());
		List<Patient> l2 = new ArrayList<Patient>(vcf.getPatients());
		patient = new ListSingleSelection<Patient>(l2);
		father = new ListSingleSelection<Patient>(l);
		mother = new ListSingleSelection<Patient>(l);
	}
}
