/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal;

import org.cytoscape.work.Tunable;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class NetworkContext {
	
	// INTERACTIONS
	
	@Tunable(description="Binary", groups="Interactions")
	public boolean binary = true;
	
	@Tunable(description="BinarySingle", groups="Interactions")
	public boolean binarySingle = true;
	
	@Tunable(description="CoComplex", groups="Interactions")
	public boolean coComplex = true;
	
	@Tunable(description="Prediction", groups="Interactions")
	public boolean prediction = true;
	
}
