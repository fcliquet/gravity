/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.API.gemini.GeminiVariant;
import net.miginfocom.swing.MigLayout;

/**
 * @author Freddy Cliquet
 * @version 1.1
 */
public class VariantPedigreeComponent extends VariantComponent {

	private static final long serialVersionUID = -8596210746582287540L;
	private List<Patient> _patients;
	private List<PatientVariantComponent> _patientsComponents;
	private Variant variant;
	private int recessive;
	
	Map<Patient, Set<Patient>> mates;
	Map<String, Patient> patients;
	Set<Patient> treated;
	int padding = 10;
	JPanel panelTree;
	
	public final static int DENOVO = 0;
	public final static int RECESSIVE = 1;
	public final static int COMPOUND = 2;
	
	public VariantPedigreeComponent(List<Patient> lp, Variant v, int recessive) {
		super(v);
		_patients = lp;
		variant = v;
		_patientsComponents = new ArrayList<PatientVariantComponent>();
		this.recessive = recessive;
		initComponents();
		treeStructure();
	}
	
	private void initComponents(){
		setLayout(new MigLayout("fill, ins 0, gap 0 0"));
		setBorder(BorderFactory.createEtchedBorder());
			
//		_patients = ServicesUtil.variantDatabase.getPatients( ServicesUtil.autismPanel.getSelectedFamily() );
		
		String cadd = "";
		if( variant.cadd()==null )
			cadd = "no cadd ";
		else
			cadd = "cadd: "+variant.cadd();
		
		if( variant instanceof GeminiVariant ){
			GeminiVariant gv = (GeminiVariant)variant;
			try {
				add(new JLabel("p."+((String)gv.getObject("aa_change")).split("/")[0]+((String)gv.getObject("aa_length")).split("/")[0]+((String)gv.getObject("aa_change")).split("/")[1]), "wrap");
			} catch (Exception e) {
				add(new JLabel(variant.impact()+" ; "+cadd), "wrap");
			}
		}else{
			add(new JLabel(variant.impact()+" ; "+cadd), "wrap");
		}
		
		add(new JLabel("Freq: max "+String.format("%.2f", variant.maxAlleleFrequency())+" ; cohort "+String.format("%.2f", variant.cohortFrequency())), "wrap");
		add(new JLabel("CADD "+variant.cadd_scaled()), "wrap");
		
		if(recessive == RECESSIVE )
			add( new JLabel("Autosomal recessive"), "wrap" );
		if(recessive == COMPOUND )
			add( new JLabel("Compound heterozygous"), "wrap" );
		
		panelTree = new JPanel(new MigLayout("fill, ins 0, gap 0 0"));
		add(panelTree, "gaptop 3, gapbottom 3, alignx center, growx");
	}
	
	private void treeStructure(){
		panelTree.removeAll();
		if( _patients == null )
			return;
		
		mates = new HashMap<Patient, Set<Patient>>();
		patients = new HashMap<String, Patient>();
		for(Patient p:_patients){
			patients.put(p.getName(), p);
		}
//		System.out.println("Patients: "+patients);
		for(Patient p:_patients){
			if( p.hasParents() ){
				Set<Patient> s = mates.get( patients.get(p.getFatherName()) );
				if( s == null ){
					s = new HashSet<Patient>();
					mates.put( patients.get(p.getFatherName()), s );
				}
				s.add( patients.get(p.getMotherName()) );
				
				s = mates.get( patients.get(p.getMotherName()) );
				if( s == null ){
					s = new HashSet<Patient>();
					mates.put( patients.get(p.getMotherName()), s );
				}
				s.add( patients.get(p.getFatherName()) );
			}
		}
		Set<Patient> root = new HashSet<Patient>();
		for(Patient p:_patients){
			if( p.getFatherName()==null && p.getMotherName()==null )
				root.add(p);
		}
		Set<Patient> removeFromRoot = new HashSet<Patient>();
		do{
			removeFromRoot.clear();
			for( Patient p:root ){
				if( mates.get(p) != null ){
					for( Patient p2:mates.get(p) ){
						if( !root.contains(p2) )
							removeFromRoot.add(p);
					}
				}
			}
			root.removeAll(removeFromRoot);
		}while( removeFromRoot.size()>0 );
			
//		System.out.println("mates: "+mates);
//		System.out.println("roots: "+root);
		treated = new HashSet<Patient>();
		boolean first = true;
		for(Patient p:root){
			if( !treated.contains(p) ){
				if( first ){
					panelTree.add(drawTree(p), "alignx center,wrap");
					first = false;
				}else{
					panelTree.add(drawTree(p), "gaptop 20px,alignx center,wrap");
				}
			}
		}
	}
	
	private JPanel drawTree(Patient root){
		treated.add(root);
		JPanel panel = new JPanel(new MigLayout("fill, ins 0, gap 0 0, top", "[50%][min!][50%]", ""));
		
		if( mates.get(root) == null ){
			panel.add( getLabel(root), "alignx center, top, spanx, wrap" );
		}else
			if( mates.get(root).size() > 1 )
				System.out.println("problem, too many mates!");
			else{
				panel.add( getLabel(root), "align right" );
				panel.add(getCoupleSpacer(), "aligny bottom");
				panel.add(getLabel( mates.get(root).toArray(new Patient[0])[0] ), "wrap"  );
				treated.add( mates.get(root).toArray(new Patient[0])[0] );
			}
		
		JPanel panelChildren = new JPanel(new MigLayout("fill, ins 0, gap 0 0, top"));
		List<Integer> widths = new ArrayList<Integer>();
		List<Integer> positions = new ArrayList<Integer>();
		boolean first = true;
		for( Patient p:_patients ){
			if( (p.getFatherName()!= null && p.getFatherName().equals( root.getName() )) || (p.getMotherName()!= null && p.getMotherName().equals( root.getName() )) ){
				JPanel tmp = drawTree(p);
				widths.add( tmp.getPreferredSize().width );
				if( mates.get(p)== null)
					positions.add(0);
				else
					positions.add( mates.get(p).size() );
				if( first ){
					panelChildren.add(tmp, "alignx center, grow, push");
					first = false;
				}else{
					panelChildren.add(tmp, "gapleft "+padding+",alignx center, grow, push");
				}
			}
		}
		if(widths.size()>0){
			panel.add( getBranch(widths, positions), "alignx center, spanx, wrap" );
		}
		panel.add(panelChildren, "alignx center, spanx, grow, push");
		
		return panel;
	}
	
	private PatientVariantComponent getLabel(Patient p){
		PatientVariantComponent lab = new PatientVariantComponent(p, variant, recessive);
		_patientsComponents.add(lab);
		return lab;
	}
	
	private JPanel getBranch(List<Integer> widths, List<Integer> positions){
		int width = widths.stream().mapToInt(Integer::intValue).sum() + (widths.size()-1)*padding ;
		
		@SuppressWarnings("serial")
		JPanel p = new JPanel(){
			@Override
			protected void paintComponent(Graphics g) {
				if( widths.size() == 1 ){
					g.setColor(Color.BLACK);
					g.drawLine(width/2, 10, width/2, 0);
				}else{
					int winc = 0;
					int prev = 0;
					for(int i=0 ; i<widths.size() ; i++){
						int w = widths.get(i);
						g.setColor(Color.BLACK);
						int shift = positions.get(i)*(13+5);
						g.drawLine(w/2 + winc - shift, 5, w/2 + winc - shift, 10);
						prev = w/2 + winc - shift;
						winc+=w;
						winc+=padding;
					}
					g.drawLine( widths.get(0)/2 - positions.get(0)*(13+5) , 5, prev, 5);
					//g.drawLine( widths.get(0)/2 - positions.get(0)*(13+5) , 10, width - widths.get(widths.size()-1)/2 - 1, 10);
					g.drawLine(width/2, 5, width/2, 0);
				}
			}
		};
		p.setMinimumSize(new Dimension(width, 10));
		return p;
	}
	
	private JPanel getCoupleSpacer(){
		@SuppressWarnings("serial")
		JPanel p = new JPanel(){
			@Override
			protected void paintComponent(Graphics g) {
//				super.paintComponent(g);
				g.setColor(Color.BLACK);
				g.drawLine(0, 0, 10, 0);
				g.drawLine(5, 0, 5, 15);
			}
		};
		p.setMinimumSize(new Dimension( 10, 15 ));
		return p;
	}

	@Override
	public Variant getVariant() {
		return variant;
	}
	
//	public static void main(String[] args) {
//		List<Patient> patients = new ArrayList<Patient>();
//		patients.add(new Patient("C0733-011-113-001", "C0733-011-113", "C0733-011-113-002", "C0733-011-113-003", 1, 2));
//		patients.add(new Patient("C0733-011-113-002", "C0733-011-113", null, null, 1, 1));
//		patients.add(new Patient("C0733-011-113-003", "C0733-011-113", null, null, 2, 1));
//		patients.add(new Patient("C0733-011-113-004", "C0733-011-113", "C0733-011-113-002", "C0733-011-113-003", 1, 1));
//		
//		List<Patient> patients2 = new ArrayList<Patient>();
//		patients2.add(new Patient("C0733-011-105-001", "C0733-011-105", "C0733-011-105-002", "C0733-011-105-003", 1,	2));
//		patients2.add(new Patient("C0733-011-105-002", "C0733-011-105", null, null, 1,	1));
//		patients2.add(new Patient("C0733-011-105-003", "C0733-011-105", null, "C0733-011-105-006", 2,	1));
//		patients2.add(new Patient("C0733-011-105-004", "C0733-011-105", "C0733-011-105-002", "C0733-011-105-003", 1,	1));
//		patients2.add(new Patient("C0733-011-105-005", "C0733-011-105", "C0733-011-105-002", "C0733-011-105-003", 2,	1));
//		patients2.add(new Patient("C0733-011-105-006", "C0733-011-105", null, null, 2,	1));
//		patients2.add(new Patient("C0733-011-105-007", "C0733-011-105", "C0733-011-105-008", "C0733-011-105-009", 2,	2));
//		patients2.add(new Patient("C0733-011-105-008", "C0733-011-105", null, null, 1,	1));
//		patients2.add(new Patient("C0733-011-105-009", "C0733-011-105", null, "C0733-011-105-006", 2,	1));
//		patients2.add(new Patient("C0733-011-105-010", "C0733-011-105", null, "C0733-011-105-006", 2,	1));
//		
//		List<Patient> patients3 = new ArrayList<Patient>();
//		patients3.add(new Patient("369298-LEUVEN", "LEUVEN", null, "387834-LEUVEN", 2, 2));
//		patients3.add(new Patient("387834-LEUVEN", "LEUVEN", "393737-LEUVEN", "393738-LEUVEN", 2, 2));
//		patients3.add(new Patient("393737-LEUVEN", "LEUVEN", null, null, 1, 1));
//		patients3.add(new Patient("393738-LEUVEN", "LEUVEN", null, null, 2, 1));
//		
//		
//		VariantPedigreeComponent fpp = new VariantPedigreeComponent(patients2);
//		JFrame f = new JFrame("pedigree");
//		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		f.getContentPane().add( fpp );
//		f.validate();
//		f.pack();
//		f.setVisible(true);
//	}

}
