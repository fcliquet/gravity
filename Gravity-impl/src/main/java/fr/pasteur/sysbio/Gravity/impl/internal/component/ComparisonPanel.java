/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cytoscape.util.swing.IconManager;

import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.IconListCellRenderer;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.TextIcon;
import net.miginfocom.swing.MigLayout;

/**
 * @author F. Cliquet
 * @version 1.0
 *
 */
public class ComparisonPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 3565867461347725888L;
	private JComboBox<Patient> _listA, _listB;
	private List<Patient> patients;

	public ComparisonPanel(){
		initComponents();
	}
	
	private void initComponents(){
		setBorder(BorderFactory.createEtchedBorder());
		setLayout(new MigLayout("fill, ins 1, gap 1 1", "[min!][]", "[min!]"));
		
		add(new JLabel("Sample A:"), "");
		_listA = new JComboBox<Patient>();
		add(_listA,"growx,wrap");
		
		add(new JLabel("Sample B:"), "");
		_listB = new JComboBox<Patient>();
		add(_listB,"growx,wrap");
	}
	
	public void setPatientList(List<Patient> lp){
		patients = lp;
		reloadPatientLists();
	}
	
	private void reloadPatientLists(){
		_listA.removeAllItems();
		_listB.removeAllItems();
		
		Patient pnull = new Patient("[none]", "[none]", "[none]", "[none]", Patient.UNKNOWN, Patient.UNKNOWN);
		
		Collections.sort(patients);
		
		Map<Object,Icon> icons = new HashMap<Object,Icon>();
		for(Patient p:patients){
			String icon = "";
			Color c = Color.BLACK;
			if( p.getPatientSex()==Patient.MALE ){
				icon += IconManager.ICON_MARS;
				c = Color.BLUE;
				if( p.getPatientPhenotype()==Patient.AFFECTED )
					icon += " "+IconManager.ICON_STETHOSCOPE;
			}else{
				if( p.getPatientSex()==Patient.FEMALE ){
					icon += IconManager.ICON_VENUS;
					c = new Color( 255, 0, 255 );
					if( p.getPatientPhenotype()==Patient.AFFECTED )
						icon += " "+IconManager.ICON_STETHOSCOPE;
				}else{
					if( p.getPatientPhenotype()==Patient.AFFECTED )
						icon += IconManager.ICON_STETHOSCOPE;
				}
			}
			TextIcon ti = new TextIcon(new JLabel(), icon);
			ti.setFont(ServicesUtil.iconManager.getIconFont(14f));
			ti.setForeground(c);
			icons.put(p, ti);
		}
		_listA.setRenderer(new IconListCellRenderer(icons));
		_listB.setRenderer(new IconListCellRenderer(icons));
		
		for(Patient s:patients){
			_listA.addItem(s);
			_listB.addItem(s);
		}
		revalidate();
		repaint();
	}
	
	public Patient getPatientA(){
		return (Patient)_listA.getSelectedItem();
	}
	
	public Patient getPatientB(){
		return (Patient)_listB.getSelectedItem();
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ComparisonPanel c = new ComparisonPanel();
		
		List<Patient> lp = new ArrayList<Patient>();
		lp.add(new Patient("01-patient", "01", "01-father", "01-mother", 1, 2));
		lp.add(new Patient("01-father", "01", "0", "0", 1, 1));
		lp.add(new Patient("01-mother", "01", "0", "0", 2, 1));
		lp.add(new Patient("02-patient", "02", "father", "mother", 2, 2));
		lp.add(new Patient("father", "02", "0", "0", 1, 1));
		lp.add(new Patient("mother", "02", "0", "0", 2, 1));
		c.setPatientList(lp);
		
		f.getContentPane().add( c );
		f.validate();
		f.pack();
		f.setVisible(true);
	}

}
