/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;

import org.cytoscape.work.Tunable;
import org.cytoscape.work.util.ListMultipleSelection;

/**
 * @author Freddy Cliquet
 * @version 1.1
 *
 */
public class PathwayContext {
	// PATHWAY / GENES

	@Tunable(description="", groups="Pathway/Genes")
	public ListMultipleSelection<String> pathways;
	
	// FONCTIONS
	
	public PathwayContext(Set<String> pathwayNames){
		if(pathwayNames !=null){
			ArrayList<String> ls = new ArrayList<String>(pathwayNames);
			Collections.sort(ls);
			pathways = new ListMultipleSelection<String>(ls);
			System.out.println("pathways in memory "+ls.size());
		}
	}

}
