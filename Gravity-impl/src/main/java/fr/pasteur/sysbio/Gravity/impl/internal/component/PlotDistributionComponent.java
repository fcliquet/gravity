/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.DoubleStream;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.apache.poi.util.ArrayUtil;
import org.cytoscape.util.swing.IconManager;

import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.TextIcon;
import net.miginfocom.swing.MigLayout;

/**
 * Component displaying smoothed distributions. It is designed to be simple and efficient, to avoid using complex java plotting libraries.
 * 
 * <p>This component must always display at least one serie, but more can be added to it.</p>
 * 
 * @author Freddy Cliquet
 * @version 1.1
 */
public class PlotDistributionComponent extends JPanel {

	private static final long serialVersionUID = 3532909954506424971L;
	private final static double MIN_TICK_WIDTH = 25;
	private final static double MIN_HALF_TICK_WIDTH = 5;
	
	String x_label = "nb variants";
	int tick_length = 3;
	int half_tick_length = 1;
	boolean full_y_axis = false;
	private List<Double[]> xx;
	private List<Double[]> yy;
	private List<String> legends;
	private List<Color> colors;
	private Double marker = null;
	
	/**
	 * Create a new component with the given serie of data to display.
	 * @param xData X values of the data serie
	 * @param yData Y values of the data serie
	 * @param legend value displayed as a legend for the given serie
	 * @param color color used for this serie
	 * @param xlabel label of the x axis
	 */
	public PlotDistributionComponent(Double xData[], Double yData[], String legend, Color color, String xlabel){
		setPreferredSize(new Dimension(80, 50));
		x_label = xlabel;
		
		xx = new ArrayList<Double[]>();
		yy = new ArrayList<Double[]>();
		legends = new ArrayList<String>();
		colors = new ArrayList<Color>();
		xx.add(xData);
		yy.add(yData);
		legends.add(legend);
		colors.add(color);
	}
	
	/**
	 * add a new serie to the data to plot with the necessaries informations such as legend and plotting color.
	 * @param xData X values of the data serie
	 * @param yData Y values of the data serie
	 * @param legend value displayed as a legend for the given serie
	 * @param color color used for this serie
	 */
	public void addSerie(Double xData[], Double yData[], String legend, Color color){
		xx.add(xData);
		yy.add(yData);
		legends.add(legend);
		colors.add(color);
	}
	
	/**
	 * Define if the y axis is drawn or not.
	 * @param drawIt true to draw the y axis
	 */
	public void setDrawYAxis(boolean drawIt){
		full_y_axis = drawIt;
	}
	
	/**
	 * Define the position of the markers.
	 * @param position marker position
	 */
	public void setMarker(double position){
		marker = position;
	}
	
	/**
	 * Build and return a panel containing the legend for the plot. It is built using the legends and colors given for each series.
	 * @return The legend panel
	 */
	public JPanel getLegend(){
		JPanel panel = new JPanel(new MigLayout("fill, ins 0, gap 0 0", "[min!]", "[min!]"));
		
		for(int i=0 ; i<legends.size() ; i++){
			TextIcon tik = new TextIcon(new JLabel(), IconManager.ICON_SQUARE);
			tik.setFont( ServicesUtil.iconManager.getIconFont(14f) );
			tik.setForeground( colors.get(i) );
			panel.add(new JLabel(tik));
			
			panel.add(new JLabel(""+legends.get(i)+"  "), "gapleft 1px");
		}
		
		return panel;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		g2d.setFont(g2d.getFont().deriveFont(10f));
		g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		double y_max = 0;
		double x_min = Double.MAX_VALUE;
		double x_max = -Double.MAX_VALUE;
		for( Double[] d : yy ){
			y_max = Math.max( y_max, Arrays.stream(d).max(Double::compare).get() );
		}
		for( Double[] d : xx ){
			x_min = Math.min(x_min, d[0]);
			x_max = Math.max(x_max, d[d.length-1]);
		}
		
//		double y_max = DoubleStream.of(y).max().getAsDouble();
		g2d.setBackground(Color.BLACK);
		int width = getPreferredSize().width;
		int height = getPreferredSize().height;
		int axisBottom = tick_length+2*g2d.getFontMetrics().getHeight();
		int axisLeft = g2d.getFontMetrics().stringWidth(""+(int)x_min)/2;
		if( full_y_axis ) axisLeft = tick_length+g2d.getFontMetrics().stringWidth(""+(int)y_max)+1;
		int axisRight = g2d.getFontMetrics().stringWidth(""+(int)x_max)/2+5;
		int axisTop = g2d.getFontMetrics().getAscent()/2;
		int plotWidth = width - axisLeft - axisRight;
		int plotHeight = height - axisBottom - axisTop;
		g2d.setColor(new Color(127, 127, 127, 127));
		g2d.translate(axisLeft, axisTop);
//		g2d.fillRect(0, 0, plotWidth, plotHeight);		
		g2d.setColor(Color.GREEN);
		
		double x_length = x_max - x_min;
		double x_step = x_length / (double)plotWidth;
		
		for( int ii=0 ; ii<xx.size() ; ii++ ){
			g2d.setColor(colors.get(ii));
			double x[] = Arrays.stream(xx.get(ii)).mapToDouble(Double::doubleValue).toArray();
			double y[] = Arrays.stream(yy.get(ii)).mapToDouble(Double::doubleValue).toArray();
			
			if( x.length > 2 ){
				SplineInterpolator si = new SplineInterpolator();
				PolynomialSplineFunction psf = si.interpolate( x , y);
				double x_r_old =  0;
				double y_r_old = psf.value( x_min )/y_max*(plotHeight*.9);
				if( y_r_old < 0 ) y_r_old = 0;
				for( int i=1 ; i<plotWidth ; i++){
					double x_r_curr = x_step*i;
					double y_r_curr = psf.value( x_r_curr+x_min )/y_max*(plotHeight*.9);
					if( y_r_curr < 0 )
						y_r_curr = 0;
					g2d.drawLine((int) (x_r_old/x_step), (int) (plotHeight-y_r_old), (int) (x_r_curr/x_step), (int) (plotHeight-y_r_curr));
					x_r_old = x_r_curr;
					y_r_old = y_r_curr;
				}
			}else{
				g2d.drawLine(0, (int)(plotHeight-y[0]/y_max*(plotHeight*.9)), plotWidth, (int)(plotHeight-y[1]/y_max*(plotHeight*.9)));
			}
		}
		
		
		g2d.setColor(Color.BLUE);
	
		if(marker != null){
			if( marker-x_min == 0 )
				g2d.drawLine((int)( (marker-x_min)/x_step)+1, plotHeight, (int)( (marker-x_min)/x_step)+1, 0);
			else
				g2d.drawLine((int)( (marker-x_min)/x_step), plotHeight, (int)( (marker-x_min)/x_step), 0);
		}
				
		// drawing axis
		g2d.setColor(Color.BLACK);
		g2d.drawLine(0, 0, 0, plotHeight);
		g2d.drawLine(0, plotHeight, plotWidth, plotHeight);
		
		//x axis
		int x_width = (int) (x_max - x_min);
		double tick_width = plotWidth / x_width;
		int tick_count = (int) Math.ceil( 1/(tick_width / MIN_TICK_WIDTH) );
		int half_tick_count = (int) Math.ceil( 1/(tick_width / MIN_HALF_TICK_WIDTH) );
		for( int i = 0 ; i <= x_length ; i++ ){
			if( i%tick_count == 0 ){
				g2d.drawLine( (int)(i/x_step) , plotHeight, (int)(i/x_step), plotHeight+tick_length);
				g2d.drawString((int)(x_min+i)+"", (int)(i/x_step)-(g2d.getFontMetrics().stringWidth((int)(x_min+i)+"")/2), plotHeight+g2d.getFontMetrics().getAscent()+tick_length);
			}else{
				if( i%half_tick_count == 0 ){
					g2d.drawLine( (int)(i/x_step) , plotHeight, (int)(i/x_step), plotHeight+half_tick_length);
				}
			}
		}
		
		//x label
		g2d.drawString(x_label, plotWidth/2 - g2d.getFontMetrics().stringWidth(x_label)/2, plotHeight+g2d.getFontMetrics().getAscent()+g2d.getFontMetrics().getHeight()+tick_length);
		
		//y axis
		if( full_y_axis ){
			double tick_height = plotHeight / y_max;
			int tick_count_h = (int) Math.ceil( 1/(tick_height / MIN_TICK_WIDTH) );
			for( int i = 0 ; i <= y_max ; i++ ){
				if( i%tick_count_h == 0 ){
					g2d.drawLine( -tick_length , (int)(plotHeight-(i/y_max*plotHeight)), 0, (int)(plotHeight-(i/y_max*plotHeight)));
					g2d.drawString(i+"", -tick_length-g2d.getFontMetrics().stringWidth(""+i)-1, (int)(plotHeight-(i/y_max*plotHeight)+g2d.getFontMetrics().getAscent()/2));
				}
			}
		}
	}

}

class NumberComparator implements Comparator<Number> {
    public int compare(Number a, Number b){
        return new BigDecimal(a.toString()).compareTo(new BigDecimal(b.toString()));
    }
}
