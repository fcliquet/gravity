package fr.pasteur.sysbio.Gravity.impl.internal;

import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTableUtil;
import org.cytoscape.model.events.RowsSetEvent;
import org.cytoscape.model.events.RowsSetListener;

import de.erichseifert.gral.data.DataTable;
import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.API.gemini.HgncGene;
import fr.pasteur.sysbio.Gravity.API.utils.Maths;
import fr.pasteur.sysbio.Gravity.impl.internal.component.BoxPlotComponent;
import fr.pasteur.sysbio.Gravity.impl.internal.component.PlotDistributionComponent;
import fr.pasteur.sysbio.Gravity.impl.internal.component.VariantComponent;
import fr.pasteur.sysbio.Gravity.impl.internal.component.VariantPedigreeComponent;
import net.miginfocom.swing.MigLayout;

/**
 * 
 * @author Freddy Cliquet
 * @version 1.0
 */
public class ResultGeneDetailPanel extends JPanel implements CytoPanelComponent, RowsSetListener, ActionListener {

	private static final long serialVersionUID = -5427176589327800561L;
	private List<VariantComponent> _gsvDetails;
	private Gene _gene;
	JLabel labelGene, labelCarried, labelUncarried;
	JTextArea taGeneInfos;
	JSeparator separator;
	JPanel contentPane;
	JPanel panelPlot;
	JComboBox<String> listSort;
	JButton buttonBioGPS;
	JButton buttonOMIM;
	JButton buttonGTEX;
	
	public ResultGeneDetailPanel(){
		setPreferredSize(new Dimension(300, getMinimumSize().height));
		_gsvDetails = new ArrayList<VariantComponent>();
		separator = new JSeparator();
		setLayout(new MigLayout("fill, ins 0, gap 0 5", "[]", "[]"));
		labelGene = new JLabel("no gene selected");
		
		JPanel p = new JPanel(new MigLayout("fill, ins 0, gap 0 0"));
		buttonBioGPS = new JButton("BioGPS");
		buttonBioGPS.setActionCommand("open BioGPS");
		buttonBioGPS.addActionListener(this);
		buttonBioGPS.setEnabled(false);
		p.add(buttonBioGPS, "growx");
		buttonOMIM = new JButton("OMIM");
		buttonOMIM.addActionListener(this);
		buttonOMIM.setActionCommand("open OMIM");
		buttonOMIM.setEnabled(false);
		p.add(buttonOMIM, "growx");
		buttonGTEX = new JButton("GTEx");
		buttonGTEX.addActionListener(this);
		buttonGTEX.setActionCommand("open GTEX");
		buttonGTEX.setEnabled(false);
		p.add(buttonGTEX, "growx");
		
		labelCarried = new JLabel("Variant(s) carried:");
		labelUncarried = new JLabel("Variant(s) not carried:");
		taGeneInfos = new JTextArea("");
		taGeneInfos.setLineWrap(true);
		add(labelGene, "wrap");
		add(p, "growx, wrap");
		add(taGeneInfos, "h min!, growx ,wrap");
		
		listSort = new JComboBox<String>();
		listSort.addItemListener(new ItemListener(){

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED){
					updateGeneDetails();
				}
			}
			
		});
		add(listSort, "growx,wrap");
		
		contentPane = new JPanel();
		contentPane.setLayout(new MigLayout("fill, ins 0, gap 0 5", "", "[min!]"));
		JScrollPane scrollPane = new JScrollPane(contentPane);
		scrollPane.getVerticalScrollBar().setUnitIncrement(30);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		contentPane.setAutoscrolls(true);
		add(scrollPane, "span 2, grow, push, top, left, wrap");
		
		panelPlot = new JPanel(new MigLayout("fill, ins 0, gap 0 0"));
		add( panelPlot, "span 2, growx, h pref!, wrap" );
	}
	
	@Override
	public Component getComponent() {
		return this;
	}

	@Override
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.EAST;
	}

	@Override
	public Icon getIcon() {
		return null;
	}

	@Override
	public String getTitle() {
		return getName();
	}

	void displayMultiplySelectedGenes(int count){
		labelGene.setText(count+" genes selected");
		buttonBioGPS.setEnabled(false);
		buttonOMIM.setEnabled(false);
		buttonGTEX.setEnabled(false);
		taGeneInfos.setText("");
		clearGeneDetails();
	}
	
	void displayNoSelectedGene(){
		labelGene.setText("no gene selected");
		buttonBioGPS.setEnabled(false);
		buttonOMIM.setEnabled(false);
		buttonGTEX.setEnabled(false);
		taGeneInfos.setText("");
		clearGeneDetails();
		panelPlot.removeAll();
	}
	
	void displayGeneDetails(String gene){
		labelGene.setText(gene);
		buttonBioGPS.setEnabled(true);
		buttonOMIM.setEnabled(true);
		buttonGTEX.setEnabled(true);
		
		if( ServicesUtil.controlPanel.isFamilyTrio() ){
			_gene = ServicesUtil.variantDatabase.getGene(gene, ServicesUtil.controlPanel.getFilters());
		}else{
			if( ServicesUtil.controlPanel.isCompleteFamily() || ServicesUtil.controlPanel.isComparisonMode() ){
				_gene = ServicesUtil.variantDatabase.getGene(gene, ServicesUtil.controlPanel.getGenericFilters());
			}
		}
		String selected = (String)listSort.getSelectedItem();
		
		taGeneInfos.setText( _gene.getDetails()+"\nPathways: "+ServicesUtil.pathways.getPathwaysList(gene) );
		
		listSort.removeAllItems();
		List<String> ls = ServicesUtil.variantDatabase.listVariantAttributes();
		Collections.sort(ls);
		for(String s:ls)
			listSort.addItem(s);
		listSort.setSelectedItem(selected);
		
		if( (String)listSort.getSelectedItem() == "" || (String)listSort.getSelectedItem()==null ){
			listSort.setSelectedItem("cadd_scaled");
		}
		if( (String)listSort.getSelectedItem() == "" || (String)listSort.getSelectedItem()==null ){
			listSort.setSelectedItem("cadd_raw");
		}
		if( (String)listSort.getSelectedItem() == "" || (String)listSort.getSelectedItem()==null ){
			listSort.setSelectedItem("cadd");
		}
		if( (String)listSort.getSelectedItem() == "" || (String)listSort.getSelectedItem()==null ){
			listSort.setSelectedIndex(0);
		}
		
		updateGeneDetails();
	}
	
	private void updateGeneDetails(){
		clearGeneDetails();
		_gsvDetails.clear();
		
		_gene.sortBy((String)listSort.getSelectedItem());
		
		if( ServicesUtil.controlPanel.isFamilyTrio() ){
			for(Variant gsv:_gene.getFilteredVariants(ServicesUtil.controlPanel.getSelectedPatient()))
				_gsvDetails.add(new VariantComponent(gsv));
		}else{
			if( ServicesUtil.controlPanel.isCompleteFamily() ){
				for(Variant gsv:_gene.getFilteredVariants(ServicesUtil.controlPanel.getSelectedAffected(), ServicesUtil.controlPanel.getSelectedUnaffected(), ServicesUtil.controlPanel.getQualityFilters(), ServicesUtil.controlPanel.keepAllVariants4CompleteFamilies()))
					_gsvDetails.add(new VariantPedigreeComponent( ServicesUtil.variantDatabase.getPatients( ServicesUtil.controlPanel.getSelectedFamily() ) , gsv, VariantPedigreeComponent.DENOVO));
				if( ServicesUtil.controlPanel.considerHomAutosomalRecessive() )
					for(Variant gsv:_gene.getFilteredRecessiveVariants(ServicesUtil.controlPanel.getSelectedAffected(), ServicesUtil.controlPanel.getSelectedUnaffected(), ServicesUtil.controlPanel.getQualityFilters(), ServicesUtil.controlPanel.keepAllVariants4CompleteFamilies()))
						_gsvDetails.add(new VariantPedigreeComponent( ServicesUtil.variantDatabase.getPatients( ServicesUtil.controlPanel.getSelectedFamily() ) , gsv, VariantPedigreeComponent.RECESSIVE));
				if( ServicesUtil.controlPanel.considerHetCompound() )
					for(Variant gsv:_gene.getCompoundHomozygousVariants(ServicesUtil.controlPanel.getSelectedAffected(), ServicesUtil.controlPanel.getSelectedUnaffected(), ServicesUtil.controlPanel.getQualityFilters(), ServicesUtil.controlPanel.keepAllVariants4CompleteFamilies()))
						_gsvDetails.add(new VariantPedigreeComponent( ServicesUtil.variantDatabase.getPatients( ServicesUtil.controlPanel.getSelectedFamily() ) , gsv, VariantPedigreeComponent.COMPOUND));
				
			}else{
				if( ServicesUtil.controlPanel.isComparisonMode() ){
					for(Variant gsv:_gene.getFilteredVariants(ServicesUtil.controlPanel.getSelectedPatientA(), ServicesUtil.controlPanel.getSelectedPatientB(), ServicesUtil.controlPanel.getQualityFilters(), ServicesUtil.controlPanel.getFilterComparison()))
						_gsvDetails.add(new VariantComponent(gsv));
				}
			}
		}
		
		Collections.sort(_gsvDetails, new Comparator<VariantComponent>() {
			@Override
			public int compare(VariantComponent gsv1, VariantComponent gsv2) {
				Object o1 = gsv1.getVariant().getObject((String)listSort.getSelectedItem());
				Object o2 = gsv2.getVariant().getObject((String)listSort.getSelectedItem());
				if( o1 == null && o2 == null )
					return 0;
				if( o1 == null )
					return 1;
				if( o2 == null )
					return -1;
				if( o1 instanceof Double )
					return Maths.compare( (Double)o2, (Double)o1);
				if( o1 instanceof Float )
					return Maths.compare( (Float)o2, (Float)o1);
				if( o1 instanceof Integer )
					return Maths.compare( (Integer)o2, (Integer)o1);
				if( o1 instanceof String )
					return ((String)o2).compareTo(((String)o1));
				return 0;
			}
		});
		
		contentPane.add(labelCarried, "wrap");
		for(VariantComponent gsvdp:_gsvDetails){
			gsvdp.setAlignmentX(LEFT_ALIGNMENT);
			gsvdp.setAlignmentY(TOP_ALIGNMENT);
			contentPane.add(gsvdp, "growx,wrap");
		}
		
		contentPane.add(new JPanel(), "push");
		
		contentPane.revalidate();
		contentPane.repaint();
	}
	
	void clearGeneDetails(){
		_gsvDetails.clear();
		contentPane.removeAll();	
		contentPane.revalidate();
		contentPane.repaint();
	}
	
	@SuppressWarnings("unchecked")
	void plot(List<String> genes){
		panelPlot.removeAll();
		
		if( ServicesUtil.controlPanel.isFamilyTrio() ){
			if( genes.size() > 100000 ){
				panelPlot.add(new JLabel("too many selected genes"), "align center center");
			}else{
				
				DataTable dtAff = new DataTable(Integer.class);
				DataTable dtUna = new DataTable(Integer.class);
				DataTable dtPar = new DataTable(Integer.class);
				DataTable dtCon = new DataTable(Integer.class);
				
				TreeMap<Integer, Integer> counts = new TreeMap<Integer,Integer>();
				int xp = 0;
				
				for( Patient p:ServicesUtil.variantDatabase.getPatients() ){
					int nbvar = 0;
					for( String gene:genes )
						nbvar += ServicesUtil.variantDatabase.getGene(gene, ServicesUtil.controlPanel.getFilters(p)).getFilteredVariants(p).size();
					
					if( p.getPatientFamilyType() == Patient.CONTROL )
						dtCon.add(nbvar);
					
					if( p.getPatientPhenotype() == Patient.AFFECTED )
						dtAff.add(nbvar);
					
					if( p.getPatientPhenotype() == Patient.UNAFFECTED && p.getPatientFamilyType() == Patient.ASD )
						if( p.getFather()!=null || p.getMother()!=null )
							dtUna.add(nbvar);
						else
							dtPar.add(nbvar);
					
					Integer nb = counts.get(nbvar);
					if( nb == null )
						counts.put(nbvar, 1);
					else
						counts.put(nbvar, nb+1);
					
					if( p.equals( ServicesUtil.controlPanel.getSelectedPatient() ) )
						xp = nbvar;
				}
				
				if( counts.size()>1 ){
					BoxPlotComponent bpc = new BoxPlotComponent(dtAff, dtUna, dtPar, dtCon);
					bpc.setPreferredSize(new Dimension(230, 200));
					panelPlot.add(bpc, "growx");
				}else{
					if( counts.firstKey() == 0 )
						panelPlot.add(new JLabel("no mutations for everyone"), "align center center");
					else
						panelPlot.add(new JLabel(counts.firstKey()+" mutations for everyone"), "align center center");
				}
			}
		}

	}
	
	@Override
	public void handleEvent(RowsSetEvent event) {
		// First see if this even has anything to do with selections
		if (!event.containsColumn(CyNetwork.SELECTED)) {
			// Nope, we're done
			return;
		}		
		
		// For each selected node, get the view in the current network
		// and change the shape
		//CyNetworkView currentNetworkView = ServicesUtil.applicationManager.getCurrentNetworkView();
		//CyNetwork currentNetwork = currentNetworkView.getModel();
		if (event.getSource() != ServicesUtil.lastNetwork.getDefaultNodeTable())
			return;
		
		List<CyNode> nodes = CyTableUtil.getNodesInState(ServicesUtil.lastNetwork, CyNetwork.SELECTED, true);
		if( nodes.size() > 1 ){
			displayMultiplySelectedGenes(nodes.size());
			List<String> genes = new ArrayList<String>();
			for(CyNode n:nodes)
				genes.add( ServicesUtil.lastNetwork.getRow( n ).get(CyNetwork.NAME, String.class) );
			plot( genes );
			return;
		}
		if( nodes.size() == 1 ){
			displayGeneDetails( ServicesUtil.lastNetwork.getRow( nodes.get(0) ).get(CyNetwork.NAME, String.class) );
			List<String> genes = new ArrayList<String>();
			genes.add( ServicesUtil.lastNetwork.getRow( nodes.get(0) ).get(CyNetwork.NAME, String.class) );
			plot( genes );
			return;
		}
		if( nodes.size()<=0 ){
			displayNoSelectedGene();
			return;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if( e.getActionCommand().equals("open BioGPS") ){
			try {
				if( _gene instanceof HgncGene ){
					HgncGene gg = (HgncGene)_gene;
					Desktop.getDesktop().browse(new URI("http://biogps.org/gene/"+gg.getEntrez_id()));
				}else
					Desktop.getDesktop().browse(new URI("http://biogps.org/search/?q="+_gene.getName()));
			} catch (IOException | URISyntaxException e1) {
			}
		}
		if( e.getActionCommand().equals("open OMIM") ){
			try {
				Desktop.getDesktop().browse(new URI(getOMIMurl(_gene.getName())));
			} catch (IOException | URISyntaxException e1) {
			}
		}
		if( e.getActionCommand().equals("open GTEX") ){
			try {
				Desktop.getDesktop().browse(new URI( "https://www.gtexportal.org/home/gene/"+_gene.getName() ));
			} catch (IOException | URISyntaxException e1) {
			}
		}
	}

	private static Map<String, String> gene2omim = null;
	private static String getOMIMurl(String gene){
		if( gene2omim == null){
			gene2omim = new HashMap<String, String>();
			BufferedReader br = new BufferedReader(new InputStreamReader(ResultGeneDetailPanel.class.getResourceAsStream("/mim2gene.txt")));
			try {
				String line = null;
				while( (line = br.readLine()) != null ){
					if( !line.startsWith("#") ){
						String[] l = line.split("\t");
						if( l.length >= 4 && l[1].equals("gene") ){
							gene2omim.put(l[3], l[0]);
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if( gene2omim.get(gene) != null )
			return "http://omim.org/entry/"+gene2omim.get(gene);
		else
			return "http://www.ncbi.nlm.nih.gov/omim?cmd=Search&term="+gene+"&doptcmdl=Synopsis";
	}
	
}
