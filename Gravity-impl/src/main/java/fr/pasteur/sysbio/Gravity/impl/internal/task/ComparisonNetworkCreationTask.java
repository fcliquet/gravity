/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.Interaction;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.API.VariantsUtils;
import fr.pasteur.sysbio.Gravity.API.utils.ConnectAllNodes;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import fr.pasteur.sysbio.Gravity.impl.internal.component.NetworkExpansionPanel;

/**
 * @author fcliquet
 *
 */
public class ComparisonNetworkCreationTask extends AbstractTask {

	boolean _update;
	Patient patientA, patientB;
	int _nbVariants = 0;
	Set<String> _geneList = null;
	
	public ComparisonNetworkCreationTask(boolean update, Set<String> geneList){
		_update = update;
		_geneList = geneList;
		patientA = ServicesUtil.controlPanel.getSelectedPatientA();
		patientB = ServicesUtil.controlPanel.getSelectedPatientB();
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.work.AbstractTask#run(org.cytoscape.work.TaskMonitor)
	 */
	@Override
	public void run(TaskMonitor arg0) throws Exception {
		arg0.setTitle("Updating view");
		arg0.setStatusMessage("retrieving genes");
		arg0.setProgress(0);
		
		Set<Interaction> interactions = ServicesUtil.ppi.getInteractions( ServicesUtil.controlPanel.getSelectedInteractionsTypes() );
		
		Set<String> genesNames;
		
		if( _geneList != null && _geneList.size()>0 ){
			genesNames = _geneList;
		}else{
			if( ServicesUtil.controlPanel.runOnAllGenes() ){
				genesNames = ServicesUtil.variantDatabase.getAllGenesNames();
				genesNames.addAll( ServicesUtil.ppi.getAllGenes() );
			}else{
				genesNames = ServicesUtil.pathways.getGenes( ServicesUtil.controlPanel.getSelectedPathways() );
				switch(ServicesUtil.controlPanel.getNetworkExpansionLevel()){
					case NetworkExpansionPanel.NO_EXPANSION:
						break;
					case NetworkExpansionPanel.SMART_EXPANSION:
						Set<String> genesNamesTmp = new HashSet<String>(genesNames);
						genesNamesTmp.addAll( ConnectAllNodes.connectAll(ServicesUtil.ppi, ServicesUtil.controlPanel.getSelectedInteractionsTypes(), genesNames) );
						genesNames = genesNamesTmp;
						break;
					case NetworkExpansionPanel.FIRST_NEIGHBOURS_EXPANSION:
					case NetworkExpansionPanel.SECOND_NEIGHBOURS_EXPANSION:
						for(int level=0; level<ServicesUtil.controlPanel.getNetworkExpansionLevel() ; level++){
							Set<String> genesNamesNew = new HashSet<String>(genesNames);
							for(Interaction i:interactions){
								if( genesNames.contains(i.symbol1) )
									genesNamesNew.add(i.symbol2);
								if( genesNames.contains(i.symbol2) )
									genesNamesNew.add(i.symbol1);
							}
							genesNames = genesNamesNew;
						}
						break;
				}
			}		
		}
		
		Map<String,CyNode> myNodes = new HashMap<String, CyNode>();
		
		CyNetwork myNet;
		if( _update && ServicesUtil.lastNetwork!=null){
			myNet = ServicesUtil.lastNetwork;
			myNet.getRow(myNet).set(CyNetwork.NAME, patientA+" - "+patientB+" network");
			myNet.removeEdges(myNet.getEdgeList());
			myNet.removeNodes(myNet.getNodeList());
			ServicesUtil.eventHelper.flushPayloadEvents();
		}else{
			myNet = ServicesUtil.cyNetworkFactory.createNetwork();
			ServicesUtil.lastNetwork = myNet;
			myNet.getRow(myNet).set(CyNetwork.NAME, patientA+" - "+patientB+" network");
			myNet.getDefaultNodeTable().createColumn("impact", String.class, false, "");
			myNet.getDefaultNodeTable().createColumn("severity", String.class, false, "");
//			myNet.getDefaultNodeTable().createColumn("de novo", Boolean.class, false, false);
//			myNet.getDefaultNodeTable().createColumn("CNV", Integer.class, false, 0);
			myNet.getDefaultNodeTable().createColumn("chrom", String.class, false, "");
			myNet.getDefaultNodeTable().createColumn("cadd A", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd B", Double.class, false, 0d);
//			myNet.getDefaultNodeTable().createColumn("cadd", Double.class, false, 0d);
//			myNet.getDefaultNodeTable().createColumn("cadd father", Double.class, false, 0d);
//			myNet.getDefaultNodeTable().createColumn("cadd mother", Double.class, false, 0d);
//			myNet.getDefaultNodeTable().createColumn("cadd de novo", Double.class, false, 0d);
//			myNet.getDefaultNodeTable().createColumn("cadd homalt", Double.class, false, 0d);
//			myNet.getDefaultNodeTable().createColumn("cadd unknown", Double.class, false, 0d);
			
			myNet.getDefaultEdgeTable().createColumn("confidence", Integer.class, false, 0);
		}
		
		if( ServicesUtil.controlPanel.displayAllGenesOfInterest() || ( _geneList != null && _geneList.size()>0 ) ){
			arg0.setStatusMessage("create nodes");
			
			for(String s:genesNames){
				CyNode node = myNet.addNode();
				myNet.getRow(node).set("impact", "");
				myNet.getRow(node).set("severity", "");
				myNet.getRow(node).set("chrom", "");
				myNet.getRow(node).set("cadd A", 0d);
				myNet.getRow(node).set("cadd B", 0d);
				myNet.getRow(node).set(CyNetwork.NAME, s);
				myNodes.putIfAbsent(s, node);
			}
		}
		
		arg0.setStatusMessage("map variants");
		
		List<Gene> genes = ServicesUtil.variantDatabase.getGenesCarryingVariant(ServicesUtil.controlPanel.getGenericFilters());
		
		for(Gene g:genes){
			if( g.carryAFilteredVariant( patientA, patientB, ServicesUtil.controlPanel.getQualityFilters(), ServicesUtil.controlPanel.getFilterComparison() ) ){
				CyNode node = myNodes.get(g.getName());
				if( node != null ){
					List<? extends Variant> vs = g.getFilteredVariants(patientA, patientB, ServicesUtil.controlPanel.getQualityFilters(), ServicesUtil.controlPanel.getFilterComparison());
					_nbVariants += vs.size();
					
					myNet.getRow(node).set("impact", VariantsUtils.getHighestImpact(vs).impact());
					myNet.getRow(node).set("severity", VariantsUtils.getHighestImpact(vs).impactSeverity());
					
					double caddA = g.maxcadd(patientA, ServicesUtil.controlPanel.getQualityFilters());
					if( ServicesUtil.controlPanel.getFilterComparison() == 2 )
						caddA = 0;
					myNet.getRow(node).set("cadd A", caddA);
					
					double caddB = g.maxcadd(patientB, ServicesUtil.controlPanel.getQualityFilters());
					if( ServicesUtil.controlPanel.getFilterComparison() == 1 )
						caddB = 0;
					myNet.getRow(node).set("cadd B", caddB);
					
//					System.out.println( "QF="+ServicesUtil.controlPanel.getFilterComparison()+" "+g.getName()+" "+caddA+" "+caddB );
					
					myNet.getRow(node).set("chrom", g.getChrom());
					myNet.getRow(node).set(CyNetwork.NAME, g.getName());
				}else{
					if( genesNames.contains(g.getName()) ){
						List<? extends Variant> vs = g.getFilteredVariants(patientA, patientB, ServicesUtil.controlPanel.getQualityFilters(), ServicesUtil.controlPanel.getFilterComparison());
						_nbVariants += vs.size();
						
						node = myNet.addNode();
						
						myNet.getRow(node).set("impact", VariantsUtils.getHighestImpact(vs).impact());
						myNet.getRow(node).set("severity", VariantsUtils.getHighestImpact(vs).impactSeverity());
						
						double caddA = g.maxcadd(patientA, ServicesUtil.controlPanel.getQualityFilters());
						if( ServicesUtil.controlPanel.getFilterComparison() == 2 )
							caddA = 0;
						myNet.getRow(node).set("cadd A", caddA);
						
						double caddB = g.maxcadd(patientB, ServicesUtil.controlPanel.getQualityFilters());
						if( ServicesUtil.controlPanel.getFilterComparison() == 1 )
							caddB = 0;
						myNet.getRow(node).set("cadd B", caddB);
						
//						System.out.println( "QF="+ServicesUtil.controlPanel.getFilterComparison()+" "+g.getName()+" "+caddA+" "+caddB );
						
						myNet.getRow(node).set("chrom", g.getChrom());
						myNet.getRow(node).set(CyNetwork.NAME, g.getName());
						
						myNodes.putIfAbsent(g.getName(), node);
					}else{
						// variant from a gene NOT considered of interest
					}
				}
			}
		}
		
		arg0.setStatusMessage("create edges");
		
		for( Interaction i:interactions ){
			CyNode node1 = myNodes.get(i.symbol1);
			CyNode node2 = myNodes.get(i.symbol2);
			if(node1!=null && node2!=null){
				CyEdge edge = myNet.addEdge(node1, node2, false);
				myNet.getRow(edge).set("confidence", ServicesUtil.ppi.getInteractionWeight(i, ServicesUtil.controlPanel.getSelectedInteractionsTypes()));
				myNet.getRow(edge).set(CyNetwork.NAME, i.symbol1+" - "+i.symbol2);
			}
		}
		
		if( !_update ){
			arg0.setTitle("add network");
			ServicesUtil.cyNetworkManager.addNetwork(myNet);
		}
		
		String working_pathway = "all genes";
		if( !ServicesUtil.controlPanel.runOnAllGenes() ){
			List<String> pathways = ServicesUtil.controlPanel.getSelectedPathways();
			if( pathways.size() > 2 )
				working_pathway = "multiple pathways";
			else{
				if( pathways.size()==1 ){
					working_pathway = pathways.get(0);
				}else{
					working_pathway = pathways.get(0)+" and "+pathways.get(1);
				}
			}
		}
		myNet.getRow(myNet).set(CyNetwork.NAME, patientA+" - "+patientB+" ("+myNet.getNodeCount()+" genes; "+_nbVariants+" variants) on "+working_pathway);
		
		TaskIterator ti = new TaskIterator(new NetworkViewTask(myNet));
		this.insertTasksAfterCurrentTask(ti);
		
	}

}
