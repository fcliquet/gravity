/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;


import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import fr.pasteur.sysbio.Gravity.API.events.DatabaseListener;
import fr.pasteur.sysbio.Gravity.impl.internal.ControlPanel;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class DataLoaderTask extends AbstractTask {

	ControlPanel _autismPanel;
	
	public DataLoaderTask(ControlPanel autismPanel){
		_autismPanel = autismPanel;
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.work.AbstractTask#run(org.cytoscape.work.TaskMonitor)
	 */
	@Override
	public void run(final TaskMonitor arg0) throws Exception {
		arg0.setTitle("Loading data");
		
		DatabaseListener dl = new DatabaseListener() {
			@Override
			public void progressChanged(String task, int current, int max) {
				if( current == 0 && max == 0 ){
					arg0.setStatusMessage(task);
				}
				if( current != 0 && max == 0 ){
					arg0.setStatusMessage(task+" : "+current);
				}
				if( current != 0 && max != 0 ){
					arg0.setProgress( (double)current / (double)max );
					arg0.setStatusMessage( task );
				}
			}
		};
		
		ServicesUtil.variantDatabase.addDatabaseListener(dl);
		
		ServicesUtil.variantDatabase.load();
		arg0.setStatusMessage("Updating UI");
		_autismPanel.updateParentsLists();
		_autismPanel.updateImpactList();
		
		ServicesUtil.variantDatabase.removeDatabaseListener(dl);
	}

}
