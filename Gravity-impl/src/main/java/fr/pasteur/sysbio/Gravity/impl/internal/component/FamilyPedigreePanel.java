/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import org.cytoscape.util.swing.IconManager;

import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import fr.pasteur.sysbio.Gravity.impl.internal.utils.TextIcon;
import net.miginfocom.swing.MigLayout;

/**
 * Component to display the list of families and the pedigree tree of the selected family.
 * 
 * <p>The pedigree tree will respect the standard for the shape (circle for women, 
 * square for men) and offer additional informations such as the phenotype 
 * (affected / unaffected through the icon used). The element of the tree are clickable
 * and go through 3 stages accept, reject or unselected. This is used to define the person
 * to use or not, and how to use them, in our following analysis.</p>
 * 
 * <p>The display as a tree is not yet working for every cases and need to be improved.
 * Families with more than 2 parents (divorce etc.) are not managed, neither are incestuous 
 * families. They pose problems during the creation of hte tree and should endup with no 
 * tree displayed at all.</p>
 * 
 * @author Freddy Cliquet
 * @version 1.1
 */
public class FamilyPedigreePanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 8458012116566910262L;
	private List<Patient> _patients;
	private List<PatientComponent> _patientsComponents;
	Map<Patient, Set<Patient>> mates;
	Map<String, Patient> patients;
	Set<Patient> treated;
	int padding = 10;
	JComboBox<String> comboFamilies;
	JPanel panelTree;
	
	/**
	 * Create and initialize a new FamilyPedigreePanel.
	 * It will load the list of families and draw the tree of the one selected by default.
	 */
	public FamilyPedigreePanel() {
		_patients = null;
		_patientsComponents = new ArrayList<PatientComponent>();
		initComponents();
		treeStructure();
	}
	
	private void initComponents(){
		setLayout(new MigLayout("fill, ins 0, gap 0 0"));
		setBorder(BorderFactory.createEtchedBorder());
		
		String fams[] = ServicesUtil.variantDatabase.getFamilies().toArray(new String[0]);
		Arrays.sort(fams);
		
		comboFamilies = new JComboBox<String>( fams );
		comboFamilies.addActionListener(this);
		add(comboFamilies, "gapbottom 5px, growx, wrap");
		
		_patients = ServicesUtil.variantDatabase.getPatients( (String)comboFamilies.getSelectedItem() );
		
		panelTree = new JPanel(new MigLayout("fill, ins 0, gap 0 0"));
		add(panelTree, "alignx center, growx, wrap");
		
		JPanel panelLegend = new JPanel(new MigLayout("fill, ins 0, gap 0 0", "[min!]", "[min!]"));
		panelLegend.add(new JLabel("Variants: "));
		
		final Color color_not_selected= UIManager.getColor("Panel.background");
		final Color color_mutated = new Color(164, 205, 0);
		final Color color_not_mutated = new Color(225, 64, 0);
		
		TextIcon tik = new TextIcon(new JLabel(), IconManager.ICON_SQUARE);
		tik.setFont( ServicesUtil.iconManager.getIconFont(14f) );
		tik.setForeground( color_mutated );
		panelLegend.add(new JLabel(tik));
		
		panelLegend.add(new JLabel(" keep   "));
		
		TextIcon tir = new TextIcon(new JLabel(), IconManager.ICON_SQUARE);
		tir.setFont( ServicesUtil.iconManager.getIconFont(14f) );
		tir.setForeground( color_not_mutated );
		panelLegend.add(new JLabel(tir));
		
		panelLegend.add(new JLabel(" reject   "));
		
		TextIcon tins = new TextIcon(new JLabel(), IconManager.ICON_SQUARE_O);
		tins.setFont( ServicesUtil.iconManager.getIconFont(14f) );
		tins.setForeground( Color.DARK_GRAY );
		panelLegend.add(new JLabel(tins));
		
		panelLegend.add(new JLabel(" not selected"));
		add(panelLegend, "gaptop 5, alignx center");
	}
	
	private void treeStructure(){
		panelTree.removeAll();
		if( _patients == null )
			return;
		
		mates = new HashMap<Patient, Set<Patient>>();
		patients = new HashMap<String, Patient>();
		for(Patient p:_patients){
			patients.put(p.getName(), p);
		}
//		System.out.println("Patients: "+patients);
		for(Patient p:_patients){
			if( p.hasParents() ){
				Set<Patient> s = mates.get( patients.get(p.getFatherName()) );
				if( s == null ){
					s = new HashSet<Patient>();
					mates.put( patients.get(p.getFatherName()), s );
				}
				s.add( patients.get(p.getMotherName()) );
				
				s = mates.get( patients.get(p.getMotherName()) );
				if( s == null ){
					s = new HashSet<Patient>();
					mates.put( patients.get(p.getMotherName()), s );
				}
				s.add( patients.get(p.getFatherName()) );
			}
		}
		Set<Patient> root = new HashSet<Patient>();
		for(Patient p:_patients){
			if( p.getFatherName()==null && p.getMotherName()==null )
				root.add(p);
		}
		Set<Patient> removeFromRoot = new HashSet<Patient>();
		do{
			removeFromRoot.clear();
			for( Patient p:root ){
				if( mates.get(p) != null ){
					for( Patient p2:mates.get(p) ){
						if( !root.contains(p2) )
							removeFromRoot.add(p);
					}
				}
			}
			root.removeAll(removeFromRoot);
		}while( removeFromRoot.size()>0 );
			
//		System.out.println("mates: "+mates);
//		System.out.println("roots: "+root);
		treated = new HashSet<Patient>();
		boolean first = true;
		for(Patient p:root){
			if( !treated.contains(p) ){
				if( first ){
					panelTree.add(drawTree(p), "alignx center,wrap");
					first = false;
				}else{
					panelTree.add(drawTree(p), "gaptop 20px,alignx center,wrap");
				}
			}
		}
	}
	
	private JPanel drawTree(Patient root){
		treated.add(root);
		JPanel panel = new JPanel(new MigLayout("fill, ins 0, gap 0 0, top", "[50%][min!][50%]", ""));
		
		if( mates.get(root) == null ){
			panel.add( getLabel(root), "alignx center, top, spanx, wrap" );
		}else
			if( mates.get(root).size() > 1 )
				System.out.println("problem, too many mates!");
			else{
				panel.add( getLabel(root), "align right" );
				panel.add(getCoupleSpacer(), "aligny bottom");
				panel.add(getLabel( mates.get(root).toArray(new Patient[0])[0] ), "wrap"  );
				treated.add( mates.get(root).toArray(new Patient[0])[0] );
			}
		
		JPanel panelChildren = new JPanel(new MigLayout("fill, ins 0, gap 0 0, top"));
		List<Integer> widths = new ArrayList<Integer>();
		List<Integer> positions = new ArrayList<Integer>();
		boolean first = true;
		for( Patient p:_patients ){
			if( (p.getFatherName()!= null && p.getFatherName().equals( root.getName() )) || (p.getMotherName()!= null && p.getMotherName().equals( root.getName() )) ){
				JPanel tmp = drawTree(p);
				widths.add( tmp.getPreferredSize().width );
				if( mates.get(p)== null)
					positions.add(0);
				else
					positions.add( mates.get(p).size() );
				if( first ){
					panelChildren.add(tmp, "alignx center, grow, push");
					first = false;
				}else{
					panelChildren.add(tmp, "gapleft "+padding+",alignx center, grow, push");
				}
			}
		}
		if(widths.size()>0){
			panel.add( getBranch(widths, positions), "alignx center, spanx, wrap" );
		}
		panel.add(panelChildren, "alignx center, spanx, grow, push");
		
		return panel;
	}
	
	private PatientComponent getLabel(Patient p){
		PatientComponent lab = new PatientComponent(p);
		_patientsComponents.add(lab);
		return lab;
	}
	
	private JPanel getBranch(List<Integer> widths, List<Integer> positions){
		int width = widths.stream().mapToInt(Integer::intValue).sum() + (widths.size()-1)*padding ;
		
		@SuppressWarnings("serial")
		JPanel p = new JPanel(){
			@Override
			protected void paintComponent(Graphics g) {
				if( widths.size() == 1 ){
					g.setColor(Color.BLACK);
					g.drawLine(width/2, 20, width/2, 0);
				}else{
					int winc = 0;
					int prev = 0;
					for(int i=0 ; i<widths.size() ; i++){
						int w = widths.get(i);
						g.setColor(Color.BLACK);
						int shift = positions.get(i)*(13+5);
						g.drawLine(w/2 + winc - shift, 10, w/2 + winc - shift, 20);
						prev = w/2 + winc - shift;
						winc+=w;
						winc+=padding;
					}
					g.drawLine( widths.get(0)/2 - positions.get(0)*(13+5) , 10, prev, 10);
					//g.drawLine( widths.get(0)/2 - positions.get(0)*(13+5) , 10, width - widths.get(widths.size()-1)/2 - 1, 10);
					g.drawLine(width/2, 10, width/2, 0);
				}
			}
		};
		p.setMinimumSize(new Dimension(width, 20));
		return p;
	}
	
	private JPanel getCoupleSpacer(){
		@SuppressWarnings("serial")
		JPanel p = new JPanel(){
			@Override
			protected void paintComponent(Graphics g) {
//				super.paintComponent(g);
				g.setColor(Color.BLACK);
				g.drawLine(0, 0, 10, 0);
				g.drawLine(5, 0, 5, 20);
			}
		};
		p.setMinimumSize(new Dimension( 10, 20 ));
		return p;
	}
	
	/**
	 * Getter on the selected family
	 * @return the family currently selected
	 */
	public String getSelectedFamily(){
		return (String)comboFamilies.getSelectedItem();
	}
	
	/**
	 * Getter on the patients selected as affected (in green)
	 * @return list of selected affected patient
	 */
	public List<Patient> getSelectedAffected(){
		List<Patient> la = new ArrayList<Patient>();
		for(PatientComponent pc:_patientsComponents){
			if( pc.isSelected() == PatientComponent.MUTATED )
				la.add( pc.getPatient() );
		}
		return la;
	}
	
	/**
	 * Getter on the patients selected as unaffected (in green)
	 * @return list of selected unaffected patient
	 */
	public List<Patient> getSelectedUnaffected(){
		List<Patient> lu = new ArrayList<Patient>();
		for(PatientComponent pc:_patientsComponents){
			if( pc.isSelected() == PatientComponent.NOT_MUTATED )
				lu.add( pc.getPatient() );
		}
		return lu;
	}
	
	public void setSelectedFamily(String family){
		comboFamilies.setSelectedItem(family);
		_patientsComponents.clear();
		_patients = ServicesUtil.variantDatabase.getPatients( (String)comboFamilies.getSelectedItem() );
		treeStructure();
		revalidate();
		repaint();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if( e.getSource() == comboFamilies ){
			if(e.getActionCommand().equals("comboBoxChanged")){
				_patientsComponents.clear();
				_patients = ServicesUtil.variantDatabase.getPatients( (String)comboFamilies.getSelectedItem() );
				treeStructure();
				revalidate();
				repaint();
			}
		}
	}

}
