package fr.pasteur.sysbio.Gravity.impl.internal.utils;

import java.awt.BorderLayout;

import javax.swing.BoundedRangeModel;
import javax.swing.DefaultBoundedRangeModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;

public class RangeSliderDemo extends JPanel {
	private static final long serialVersionUID = 7371922736119715421L;
	private JLabel rangeSliderLabel1 = new JLabel();
    private JLabel rangeSliderValue1 = new JLabel();
    private JLabel rangeSliderLabel2 = new JLabel();
    private JLabel rangeSliderValue2 = new JLabel();
    private JRangeSlider rangeSlider;
    
    

    public RangeSliderDemo() {
    	setLayout(new MigLayout("fill", "[][]", ""));
        
        rangeSliderLabel1.setText("Lower value:");
        rangeSliderLabel2.setText("Upper value:");
        rangeSliderValue1.setHorizontalAlignment(JLabel.LEFT);
        rangeSliderValue2.setHorizontalAlignment(JLabel.LEFT);
 
        BoundedRangeModel brm = new DefaultBoundedRangeModel(13, 73, 0, 100);
        rangeSlider = new JRangeSlider(brm, SwingConstants.HORIZONTAL);
        rangeSlider.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JRangeSlider slider = (JRangeSlider) e.getSource();
                rangeSliderValue1.setText(String.valueOf(slider.getLowValue())+" %");
                rangeSliderValue2.setText(String.valueOf(slider.getHighValue())+" %");
            }
        });

        add( new JLabel("Allele ratio range for heterozygosity:"), "spanx, growx,wrap" );
        add( rangeSlider, "span,growx,wrap" );
        add( rangeSliderValue1, "growx" );
        add( rangeSliderValue2, "align right, wrap" );
        
    }
    
    public void display() {
        // Initialize value display.
        rangeSliderValue1.setText(String.valueOf(rangeSlider.getLowValue())+" %");
        rangeSliderValue2.setText(String.valueOf(rangeSlider.getHighValue())+" %");
        
        // Create window frame.
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Range Slider Demo");
        
        // Set window content and validate.
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add(this, BorderLayout.CENTER);
        frame.pack();
        
        // Set window location and display.
        frame.setVisible(true);
    }
    
    /**
     * Main application method.
     * @param args String[]
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                RangeSliderDemo demo = new RangeSliderDemo();
                demo.display();
            }
        });
    }

}
