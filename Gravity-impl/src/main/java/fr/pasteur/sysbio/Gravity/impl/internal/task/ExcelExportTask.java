/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTableUtil;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import fr.pasteur.sysbio.Gravity.API.GenotypeUtils;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class ExcelExportTask extends AbstractTask {

	private File _file;
	
	public ExcelExportTask(File file){
		_file = file;
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.work.AbstractTask#run(org.cytoscape.work.TaskMonitor)
	 */
	@Override
	public void run(TaskMonitor arg0) throws Exception {
		List<CyNode> nodes = CyTableUtil.getNodesInState(ServicesUtil.lastNetwork, CyNetwork.SELECTED, true);
		List<Patient> patients;
		if(ServicesUtil.resultCohortDetail.isChildrenOnly()){
			patients = ServicesUtil.variantDatabase.getPatientsWithParents();
		}else{
			patients = ServicesUtil.variantDatabase.getPatients();
		}
		
		Workbook wb = new HSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
	    Sheet sheet = wb.createSheet("variants");
	    Row row = sheet.createRow((short)0);
	    row.createCell(0).setCellValue( createHelper.createRichTextString("chromosome") );
	    row.createCell(1).setCellValue( createHelper.createRichTextString("position") );
	    row.createCell(2).setCellValue( createHelper.createRichTextString("reference") );
	    row.createCell(3).setCellValue( createHelper.createRichTextString("alternative") );
	    row.createCell(4).setCellValue( createHelper.createRichTextString("impact") );
	    row.createCell(5).setCellValue( createHelper.createRichTextString("cadd") );
	    row.createCell(6).setCellValue( createHelper.createRichTextString("gene") );
	    int cpt=6;
	    for(Patient p:patients)
	    	row.createCell(cpt++).setCellValue( createHelper.createRichTextString(p.getName()) );
	    sheet.createFreezePane( 0, 1, 0, 1 );
	    
		if( nodes.size() >= 1 ){
			int rowCpt = 1;
			for(CyNode node:nodes){
				String geneName = ServicesUtil.lastNetwork.getRow( node ).get(CyNetwork.NAME, String.class);
				for(Variant v:ServicesUtil.variantDatabase.getGene(geneName, ServicesUtil.controlPanel.getFilters()).getFilteredVariants()){
					boolean carried = false;
					for(Patient p:patients){
						if( GenotypeUtils.isCarryingVariant( v.getPatientGenotype( p ) ) ){
							carried = true;
						}
					}
//					System.out.println(v.getGene()+" "+v.getChromosome()+" "+v.getPosition()+" "+v.cadd()+" "+v.getReference()+" "+v.getAlternative());
					if(carried){
						Row rowC = sheet.createRow((short)rowCpt++);
						rowC.createCell(0).setCellValue( createHelper.createRichTextString(v.getChromosome()) );
					    rowC.createCell(1).setCellValue( v.getPosition() );
					    rowC.createCell(2).setCellValue( createHelper.createRichTextString(v.getReference()) );
					    rowC.createCell(3).setCellValue( createHelper.createRichTextString(v.getAlternative()) );
					    rowC.createCell(4).setCellValue( createHelper.createRichTextString(v.impact()) );
					    if( v.cadd()!=null )
					    	rowC.createCell(5).setCellValue( v.cadd() );
					    rowC.createCell(6).setCellValue( createHelper.createRichTextString(v.getGene()) );
					    int col=6;
					    for(Patient p:patients)
					    	rowC.createCell(col++).setCellValue( createHelper.createRichTextString(v.getPatient(p)) );
					}
				}
			}	
		}		
		for( int i = 0 ; i < cpt ; i++ )
			sheet.autoSizeColumn((short)i);
		FileOutputStream fileOut = new FileOutputStream(_file);
		wb.write(fileOut);
		fileOut.close();
		wb.close();
	}

}
