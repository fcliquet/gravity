/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;

import java.awt.Color;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.SwingUtilities;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.Interaction;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.utils.ConnectAllNodes;
import fr.pasteur.sysbio.Gravity.API.utils.Partitionner;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;
import fr.pasteur.sysbio.Gravity.impl.internal.component.NetworkExpansionPanel;

/**
 * @author Freddy Cliquet
 * @version 1.2
 */
public class NetworkCreationTask extends AbstractTask {

	boolean _update;
	Patient patient, father, mother;
	int _nbVariants = 0;
	Set<String> _geneList = null;
	
	public NetworkCreationTask(boolean update, Set<String> geneList) {
		_update = update;
		_geneList = geneList;
		patient = ServicesUtil.controlPanel.getSelectedPatient();
		father = ServicesUtil.controlPanel.getSelectedFather();
		mother = ServicesUtil.controlPanel.getSelectedMother();
	}
	
	
	
	/* (non-Javadoc)
	 * @see org.cytoscape.work.AbstractTask#run(org.cytoscape.work.TaskMonitor)
	 */
	@Override
	public void run(TaskMonitor arg0) throws Exception {
		arg0.setTitle("Updating view");
		arg0.setStatusMessage("retrieving genes");
		arg0.setProgress(0);
		
		Set<Interaction> interactions = ServicesUtil.ppi.getInteractions( ServicesUtil.controlPanel.getSelectedInteractionsTypes() );
		
		Set<String> genesNames;
		
		if( _geneList != null && _geneList.size()>0 ){
			genesNames = _geneList;
		}else{
			if( ServicesUtil.controlPanel.runOnAllGenes() ){
				genesNames = ServicesUtil.variantDatabase.getAllGenesNames();
				genesNames.addAll( ServicesUtil.ppi.getAllGenes() );
			}else{
				genesNames = ServicesUtil.pathways.getGenes( ServicesUtil.controlPanel.getSelectedPathways() );
				switch(ServicesUtil.controlPanel.getNetworkExpansionLevel()){
					case NetworkExpansionPanel.NO_EXPANSION:
						break;
					case NetworkExpansionPanel.SMART_EXPANSION:
						Set<String> genesNamesTmp = new HashSet<String>(genesNames);
//						Partitionner p = new Partitionner(ServicesUtil.ppi, ServicesUtil.autismPanel.getSelectedInteractionsTypes() );
//						p.buildPartitions( genesNames );
//						p.sort();
						genesNamesTmp.addAll( ConnectAllNodes.connectAll(ServicesUtil.ppi, ServicesUtil.controlPanel.getSelectedInteractionsTypes(), genesNames) );
						genesNames = genesNamesTmp;
						break;
					case NetworkExpansionPanel.FIRST_NEIGHBOURS_EXPANSION:
					case NetworkExpansionPanel.SECOND_NEIGHBOURS_EXPANSION:
						for(int level=0; level<ServicesUtil.controlPanel.getNetworkExpansionLevel() ; level++){
							Set<String> genesNamesNew = new HashSet<String>(genesNames);
							for(Interaction i:interactions){
								if( genesNames.contains(i.symbol1) )
									genesNamesNew.add(i.symbol2);
								if( genesNames.contains(i.symbol2) )
									genesNamesNew.add(i.symbol1);
							}
							genesNames = genesNamesNew;
						}
						break;
				}
			}		
		}
		
		Map<String,CyNode> myNodes = new HashMap<String, CyNode>();
		
		CyNetwork myNet;
		if( _update && ServicesUtil.lastNetwork!=null){
			myNet = ServicesUtil.lastNetwork;
			myNet.getRow(myNet).set(CyNetwork.NAME, patient+" network");
			myNet.removeEdges(myNet.getEdgeList());
			myNet.removeNodes(myNet.getNodeList());
			ServicesUtil.eventHelper.flushPayloadEvents();
		}else{
			myNet = ServicesUtil.cyNetworkFactory.createNetwork();
			ServicesUtil.lastNetwork = myNet;
			myNet.getRow(myNet).set(CyNetwork.NAME, patient+" network");
			myNet.getDefaultNodeTable().createColumn("impact", String.class, false, "");
			myNet.getDefaultNodeTable().createColumn("severity", String.class, false, "");
			myNet.getDefaultNodeTable().createColumn("de novo", Boolean.class, false, false);
			myNet.getDefaultNodeTable().createColumn("CNV", Integer.class, false, 0);
			myNet.getDefaultNodeTable().createColumn("chrom", String.class, false, "");
			myNet.getDefaultNodeTable().createColumn("cnv id", String.class, false, "");
			myNet.getDefaultNodeTable().createColumn("cadd", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd father", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd mother", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd de novo", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd homalt", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("cadd unknown", Double.class, false, 0d);
			myNet.getDefaultNodeTable().createColumn("-log(maf)", Double.class, false, 0d);
			
			myNet.getDefaultEdgeTable().createColumn("confidence", Integer.class, false, 0);
		}
		
		if( ServicesUtil.controlPanel.displayAllGenesOfInterest() || ( _geneList != null && _geneList.size()>0 ) ){
			arg0.setStatusMessage("create nodes");
			
			for(String s:genesNames){
				CyNode node = myNet.addNode();
				myNet.getRow(node).set("impact", "");
				myNet.getRow(node).set("severity", "");
				myNet.getRow(node).set("de novo", false);
				myNet.getRow(node).set("CNV", 0);
				myNet.getRow(node).set("chrom", "");
				myNet.getRow(node).set("cnv id", "");
				myNet.getRow(node).set("cadd", 0d);
				myNet.getRow(node).set("cadd father", 0d);
				myNet.getRow(node).set("cadd mother", 0d);
				myNet.getRow(node).set("cadd de novo", 0d);
				myNet.getRow(node).set("cadd homalt", 0d);
				myNet.getRow(node).set("cadd unknown", 0d);
				myNet.getRow(node).set("-log(maf)", 0d);
				myNet.getRow(node).set(CyNetwork.NAME, s);
				myNodes.putIfAbsent(s, node);
			}
		}
		
		arg0.setStatusMessage("map variants");
		
		List<Gene> genes = ServicesUtil.variantDatabase.getGenesCarryingVariant(ServicesUtil.controlPanel.getFilters());
		
		for(Gene g:genes){
			if( g.carryAFilteredVariant( patient ) ){
				CyNode node = myNodes.get(g.getName());
				if( node != null ){
					_nbVariants += g.getFilteredVariants(patient).size();
//					System.out.println(g.getName()+" : "+g.getFilteredVariants(patient).size());
					
					myNet.getRow(node).set("impact", g.highestImpact( patient ));
					myNet.getRow(node).set("severity", g.highestImpactVariant( patient ).impactSeverity());
					
					double cadd = g.maxcadd(patient);
					myNet.getRow(node).set("cadd", cadd);
					
					double caddF = g.maxCaddFather(patient, father, mother);
					myNet.getRow(node).set("cadd father", caddF);
					
					double caddM = g.maxCaddMother(patient, father, mother);
					myNet.getRow(node).set("cadd mother", caddM);
					
					double caddDN = g.maxCaddDeNovo(patient, father, mother);
					myNet.getRow(node).set("cadd de novo", caddDN);
					
					double caddHA = g.maxCaddHomalt(patient, father, mother);
					myNet.getRow(node).set("cadd homalt", caddHA);
					
					double caddU = g.maxCaddUnknown(patient, father, mother);
					myNet.getRow(node).set("cadd unknown", caddU);
					
					double maf = g.minMAF(patient, father, mother);
					if( maf<=0 )
						maf = 5.4d;
					else
						maf = -Math.log10(maf);
					myNet.getRow(node).set("-log(maf)", maf);
					
					
					myNet.getRow(node).set("de novo", g.hasDeNovoMutations(patient, father, mother ));
					myNet.getRow(node).set("CNV", g.hasCNV(patient));
					myNet.getRow(node).set("chrom", g.getChrom());
					myNet.getRow(node).set("cnv id", g.getCNVid(patient));
					myNet.getRow(node).set(CyNetwork.NAME, g.getName());
				}else{
					if( genesNames.contains(g.getName()) ){
						_nbVariants += g.getFilteredVariants(patient).size();
						
						node = myNet.addNode();
						
						myNet.getRow(node).set("impact", g.highestImpact( patient ));
						myNet.getRow(node).set("severity", g.highestImpactVariant( patient ).impactSeverity());
						
						double cadd = g.maxcadd(patient);
						myNet.getRow(node).set("cadd", cadd);
						
						double caddF = g.maxCaddFather(patient, father, mother);
						myNet.getRow(node).set("cadd father", caddF);
						
						double caddM = g.maxCaddMother(patient, father, mother);
						myNet.getRow(node).set("cadd mother", caddM);
						
						double caddDN = g.maxCaddDeNovo(patient, father, mother);
						myNet.getRow(node).set("cadd de novo", caddDN);
						
						double caddHA = g.maxCaddHomalt(patient, father, mother);
						myNet.getRow(node).set("cadd homalt", caddHA);
						
						double caddU = g.maxCaddUnknown(patient, father, mother);
						myNet.getRow(node).set("cadd unknown", caddU);
						
						double maf = g.minMAF(patient, father, mother);
						if( maf<=0 )
							maf = 5.4d;
						else
							maf = -Math.log10(maf);
						myNet.getRow(node).set("-log(maf)", maf);
						
						myNet.getRow(node).set("de novo", g.hasDeNovoMutations(patient, father, mother ));
						myNet.getRow(node).set("CNV", g.hasCNV(patient));
						myNet.getRow(node).set("chrom", g.getChrom());
						myNet.getRow(node).set("cnv id", g.getCNVid(patient));
						myNet.getRow(node).set(CyNetwork.NAME, g.getName());
						
						myNodes.putIfAbsent(g.getName(), node);
					}else{
						// variant from a gene NOT considered of interest
					}
				}
			}
		}
		
		arg0.setStatusMessage("create edges");
		
		for( Interaction i:interactions ){
			CyNode node1 = myNodes.get(i.symbol1);
			CyNode node2 = myNodes.get(i.symbol2);
			if(node1!=null && node2!=null){
				CyEdge edge = myNet.addEdge(node1, node2, false);
				myNet.getRow(edge).set("confidence", ServicesUtil.ppi.getInteractionWeight(i, ServicesUtil.controlPanel.getSelectedInteractionsTypes()));
				myNet.getRow(edge).set(CyNetwork.NAME, i.symbol1+" - "+i.symbol2);
			}
		}
		
		if( !_update ){
			arg0.setTitle("add network");
			ServicesUtil.cyNetworkManager.addNetwork(myNet);
		}
		
		String working_pathway = "all genes";
		if( !ServicesUtil.controlPanel.runOnAllGenes() ){
			List<String> pathways = ServicesUtil.controlPanel.getSelectedPathways();
			if( pathways.size() > 2 )
				working_pathway = "multiple pathways";
			else{
				if( pathways.size()==1 ){
					working_pathway = pathways.get(0);
				}else{
					working_pathway = pathways.get(0)+" and "+pathways.get(1);
				}
			}
		}
		myNet.getRow(myNet).set(CyNetwork.NAME, patient+" ("+myNet.getNodeCount()+" genes; "+_nbVariants+" variants) on "+working_pathway);
		
		TaskIterator ti = new TaskIterator(new NetworkViewTask(myNet));
		this.insertTasksAfterCurrentTask(ti);
		
	}

}
