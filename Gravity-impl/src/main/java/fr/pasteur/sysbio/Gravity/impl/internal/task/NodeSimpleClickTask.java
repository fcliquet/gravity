/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

/**
 * @author fcliquet
 *
 */
public class NodeSimpleClickTask extends AbstractTask {

	@Override
	public void run(TaskMonitor arg0) throws Exception {
		System.out.println("simple click on node");
	}

}
