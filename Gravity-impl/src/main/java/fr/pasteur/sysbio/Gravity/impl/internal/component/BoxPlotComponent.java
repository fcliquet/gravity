/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.component;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Stroke;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

import javax.swing.JFrame;

import org.apache.commons.math3.stat.inference.MannWhitneyUTest;

import de.erichseifert.gral.data.DataSource;
import de.erichseifert.gral.data.DataTable;
import de.erichseifert.gral.graphics.Insets2D;
import de.erichseifert.gral.io.plots.DrawableWriter;
import de.erichseifert.gral.io.plots.DrawableWriterFactory;
import de.erichseifert.gral.plots.BoxPlot;
import de.erichseifert.gral.plots.BoxPlot.BoxWhiskerRenderer;
import de.erichseifert.gral.plots.XYPlot.XYNavigationDirection;
import de.erichseifert.gral.plots.colors.IndexedColorMapper;
import de.erichseifert.gral.plots.colors.IndexedColors;
import de.erichseifert.gral.ui.InteractivePanel;
import de.erichseifert.gral.util.DataUtils;


/**
 * @author Freddy Cliquet
 * @version 1.0
 *
 */
public class BoxPlotComponent extends javax.swing.JPanel {

	private static final long serialVersionUID = 8884644823926164011L;
	
	public BoxPlotComponent(DataTable dtAff, DataTable dtUna, DataTable dtPar, DataTable dtCon){
		super(new BorderLayout());
		setPreferredSize(new Dimension(230, 200));
		setBackground(Color.WHITE);
		
		double aff[] = new double[dtAff.getRowCount()];
		for(int i=0;i<dtAff.getRowCount();i++) aff[i] = (double)((int)dtAff.get(0, i));
		
		double una[] = new double[dtUna.getRowCount()];
		for(int i=0;i<dtUna.getRowCount();i++) una[i] = (double)((int)dtUna.get(0, i));
		
		double par[] = new double[dtPar.getRowCount()];
		for(int i=0;i<dtPar.getRowCount();i++) par[i] = (double)((int)dtPar.get(0, i));
		
		double con[] = new double[dtCon.getRowCount()];
		for(int i=0;i<dtCon.getRowCount();i++) con[i] = (double)((int)dtCon.get(0, i));
		
//		System.out.println("aff vs con = "+mannWhitney(aff, con));
//		System.out.println("aff vs una = "+mannWhitney(aff, una));
//		System.out.println("aff vs par = "+mannWhitney(aff, par));
//		System.out.println("una vs par = "+mannWhitney(una, par));
		
		DataSource boxData_1 = BoxPlot.createBoxData(dtAff);
		DataSource boxData_2 = BoxPlot.createBoxData(dtUna);
		DataSource boxData_3 = BoxPlot.createBoxData(dtPar);
		DataSource boxData_4 = BoxPlot.createBoxData(dtCon);
		
		DataTable dt = new DataTable(boxData_1);
		dt.add(boxData_2.getRow(0));
		dt.set(0, 1, 2);
		dt.add(boxData_3.getRow(0));
		dt.set(0, 2, 3);
		dt.add(boxData_4.getRow(0));
		dt.set(0, 3, 4);
		
		BoxPlot plot = new BoxPlot(dt);
		
		// Format plot
		plot.setInsets(new Insets2D.Double(2.0, 40.0, 35.0, 2.0));

		// Format axes
		plot.getAxisRenderer(BoxPlot.AXIS_X).setCustomTicks(
			DataUtils.map(
					new Double[] {1.0, 2.0, 3.0, 4.0},
					new String[] {"Affected", "Unaffected\np="+mannWhitney(aff, una), "Parents\np="+mannWhitney(aff, par), "Controls\np="+mannWhitney(aff, con)}
			)
		);
		plot.getAxisRenderer(BoxPlot.AXIS_X).setTickFont( plot.getAxisRenderer(BoxPlot.AXIS_X).getTickFont().deriveFont(8f) );

		// Format boxes
		Stroke stroke = new BasicStroke(2f);
		IndexedColorMapper cols = new IndexedColors(Color.RED, Color.CYAN, Color.BLUE, Color.GREEN);
		
		BoxWhiskerRenderer pointRenderer =
				(BoxWhiskerRenderer) plot.getPointRenderers(dt).get(0);
		pointRenderer.setWhiskerStroke(stroke);
		pointRenderer.setBoxBorderStroke(stroke);
		pointRenderer.setBoxBackground(cols);
		pointRenderer.setBoxBorderColor(Color.DARK_GRAY);
		pointRenderer.setWhiskerColor(Color.GRAY);
		pointRenderer.setCenterBarColor(Color.BLACK);

		plot.getNavigator().setDirection(XYNavigationDirection.VERTICAL);
		
		// Add plot to Swing component
		InteractivePanel panel = new InteractivePanel(plot);
		
//		DrawableWriter writer = DrawableWriterFactory.getInstance().get("image/svg+xml");
//	    try {
//			writer.write(plot, new FileOutputStream("/Users/fcliquet/Data/temp/plot.svg"), 230, 200);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		panel.setToolTipText("The p-values are indicative for a Mann-Whitney U test between the affected and each of the other categories.");
		add(panel);
	}
	
	private static MannWhitneyUTest mwut = new MannWhitneyUTest();
	private String mannWhitney(double[] a, double[] b){
		if( a.length > 0 && b.length > 0){
			double p = mwut.mannWhitneyUTest(a, b);
			return formatPvalue(p);
		}else{
			return "NA";
		}
	}
	
	private String formatPvalue(double d){
		String s=d+"";
		String split1[] = s.split("\\.|E");
		if(split1.length == 3){
			int len = Math.min(split1[1].length(), 2);
			return split1[0]+"."+split1[1].substring(0, len)+"E"+split1[2];
		}else{
			int len = Math.min(split1[1].length(), 5);
			return split1[0]+"."+split1[1].substring(0, len);
		}
	}
	
	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setTitle("Boxplot");
		
		DataTable aff = new DataTable(Integer.class);
		DataTable una = new DataTable(Integer.class);
		DataTable par = new DataTable(Integer.class);
		DataTable con = new DataTable(Integer.class);
		
		for(int i=0;i<10;i++){
			aff.add(0);
			una.add(1);
			par.add(1);
			par.add(2);
			con.add(2);
		}
		for(int i=0;i<10;i++){
			aff.add(1);
			una.add(2);
			par.add(3);
			par.add(2);
			con.add(3);
		}
		for(int i=0;i<10;i++){
			aff.add(2);
			una.add(3);
			par.add(3);
			par.add(3);
			con.add(4);
		}
		for(int i=0;i<10;i++){
			aff.add(3);
			una.add(4);
			par.add(5);
			par.add(4);
			con.add(5);
		}
		
		BoxPlotComponent bpc = new BoxPlotComponent(aff, una, par, con);
		
		f.getContentPane().add( bpc, BorderLayout.CENTER );
		f.validate();
		f.pack();
		f.setVisible(true);
	}

}
