/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.utils;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class IconTableCellRenderer extends DefaultTableCellRenderer {

	private static final long serialVersionUID = 913525984469779733L;

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		TextIcon ti = new TextIcon(new JLabel(), value.toString());
		ti.rotate();
		setIcon(ti);
		setBorder(UIManager.getBorder("TableHeader.cellBorder"));
        setHorizontalAlignment(JLabel.CENTER);
		return this;
	}
	
}
