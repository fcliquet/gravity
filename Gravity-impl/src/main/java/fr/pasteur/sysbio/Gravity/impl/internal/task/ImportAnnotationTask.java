/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.GeneAnnotation;
import fr.pasteur.sysbio.Gravity.API.gemini.GeminiDatabase;
import fr.pasteur.sysbio.Gravity.API.gemini.HgncGene;
import fr.pasteur.sysbio.Gravity.impl.internal.ServicesUtil;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class ImportAnnotationTask extends AbstractTask {

	private File _file;
	private String _type;
	private int _symbol;
	private int _annot;
	private String _label;
	
	public ImportAnnotationTask(File file, String label, String type, int symbol, int annot) {
		_file = file;
		_type = type;
		_symbol = symbol;
		_annot = annot;
		_label = label;
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.work.AbstractTask#run(org.cytoscape.work.TaskMonitor)
	 */
	@Override
	public void run(TaskMonitor arg0) throws Exception {
		
		BufferedReader br = new BufferedReader(new FileReader(_file));
		try {
			
			if( ServicesUtil.variantDatabase instanceof GeminiDatabase ){
				GeminiDatabase gd = (GeminiDatabase)ServicesUtil.variantDatabase;
				Map<String,HgncGene> geneAliases = new HashMap<String,HgncGene>();
				for( Gene gg:gd.getAllGenes() ){
					HgncGene g = (HgncGene)gg;
					for(String a:g.getAliases()){
						geneAliases.put(a, g);
					}
				}
				
				String line = null;
				while( (line = br.readLine()) != null ){
					if(! line.startsWith("#")){
						String lineSplitted[] = line.split("\t");
						
						if( gd.getGene( lineSplitted[_symbol] ) != null ){
							if( _type.equals("String") )
								gd.getGene( lineSplitted[_symbol] ).addAnnotation(new GeneAnnotation<String>(_label, lineSplitted[_annot]));
							if( _type.equals("Float") )
								gd.getGene( lineSplitted[_symbol] ).addAnnotation(new GeneAnnotation<Float>(_label, Float.valueOf(lineSplitted[_annot])));
							if( _type.equals("Int") )
								gd.getGene( lineSplitted[_symbol] ).addAnnotation(new GeneAnnotation<Integer>(_label, Integer.valueOf(lineSplitted[_annot])));
							
						}else{
							if( geneAliases.get( lineSplitted[_symbol] ) != null ){
								if( _type.equals("String") )
									geneAliases.get( lineSplitted[_symbol] ).addAnnotation(new GeneAnnotation<String>(_label, lineSplitted[_annot]));
								if( _type.equals("Float") )
									geneAliases.get( lineSplitted[_symbol] ).addAnnotation(new GeneAnnotation<Float>(_label, Float.valueOf(lineSplitted[_annot])));
								if( _type.equals("Int") )
									geneAliases.get( lineSplitted[_symbol] ).addAnnotation(new GeneAnnotation<Integer>(_label, Integer.valueOf(lineSplitted[_annot])));
							}
						}
					}
				}
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
