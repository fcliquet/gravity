package fr.pasteur.sysbio.Gravity.impl.internal.component;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

public class TestLoadingFilter {

	public static void main(String[] args) {
		
		JFrame jf = new JFrame("pouet");
		
		JPanel p = new JPanel(new MigLayout());
		p.add(new JLabel("Select and set the prefiltering values:"), "spanx,wrap");
		
		JCheckBox cbImpacts = new JCheckBox();
		cbImpacts.setSelected(true);
		p.add(cbImpacts);
		JPanel pImpacts = new JPanel();
		pImpacts.add( new JLabel("impacts: ") );
		String impacts[] = {"HIGH (LGD)","MED (missense)","LOW (synonymous)"};
		JList<String> listImpacts = new JList<String>(impacts);
		int[] defaultImpacts = {0,1};
		listImpacts.setSelectedIndices(defaultImpacts);
		pImpacts.add( listImpacts );
		p.add(pImpacts, "spanx,wrap");
		
		JCheckBox cbMaf = new JCheckBox();
		cbMaf.setSelected(true);
		p.add(cbMaf);
		JPanel pMaf = new JPanel();
		pMaf.add( new JLabel("maximum allele frequency <= ") );
		JTextField tf_maf = new JTextField("0.1", 5);
		pMaf.add(tf_maf);
		p.add(pMaf, "spanx,wrap");
		
		JCheckBox cbTypes = new JCheckBox();
		cbTypes.setSelected(true);
		p.add(cbTypes);
		JPanel pTypes = new JPanel();
		pTypes.add( new JLabel("types of variants: ") );
		String types[] = {"SNPs","INDELs","SVs"};
		JList<String> listTypes = new JList<String>(types);
		int[] defaultTypes = {0,1,2};
		listTypes.setSelectedIndices(defaultTypes);
		pTypes.add( listTypes );
		p.add(pTypes, "spanx,wrap");
		
//		p.add(new JCheckBox());
//		p.add(new JLabel("families: "), "spanx,wrap");
//		
//		p.add(new JCheckBox());
//		p.add(new JLabel("pathways / genes lists: "), "spanx,wrap");
		
		JOptionPane.showConfirmDialog(jf, p, "Data prefiltering", JOptionPane.OK_CANCEL_OPTION);
		
		System.exit(0);
	}

}
