/**
 * 
 */
package fr.pasteur.sysbio.Gravity.impl.internal.task;


import java.io.File;
import java.util.Set;

import org.cytoscape.work.TaskFactory;
import org.cytoscape.work.TaskIterator;

import fr.pasteur.sysbio.Gravity.impl.internal.ControlPanel;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class GravityTaskFactory implements TaskFactory {

	public GravityTaskFactory(){
	}
	
	/* (non-Javadoc)
	 * @see org.cytoscape.work.TaskFactory#createTaskIterator()
	 */
	@Override
	public TaskIterator createTaskIterator() {
		return null;
	}
	
	public TaskIterator createTaskLoadData(ControlPanel autismPanel) {
		return new TaskIterator(new DataLoaderTask(autismPanel));
	}

	public TaskIterator createTaskCreateNetwork( boolean update, Set<String> geneList) {
		return new TaskIterator(new NetworkCreationTask(update, geneList));
	}
	
	public TaskIterator createTaskCreateComparisonNetwork( boolean update, Set<String> geneList) {
		return new TaskIterator(new ComparisonNetworkCreationTask(update, geneList));
	}	

	public TaskIterator createTaskCreateCompleteFamilyNetwork( boolean update, Set<String> geneList) {
		return new TaskIterator(new CompleteFamilyNetworkCreationTask(update, geneList));
	}
	
	public TaskIterator createTaskExcelExport(File file){
		return new TaskIterator(new ExcelExportTask(file));
	}
	
	public TaskIterator createTaskCsvExport(File file, String individuals, String format){
		return new TaskIterator(new CsvExportTask(file, individuals, format));
	}
	
	public TaskIterator createTaskImportAnnotations(File file, String label, String type, int symbol, int annot){
		return new TaskIterator(new ImportAnnotationTask(file, label, type, symbol, annot));
	}

	/* (non-Javadoc)
	 * @see org.cytoscape.work.TaskFactory#isReady()
	 */
	@Override
	public boolean isReady() {
		return true;
	}

}
