package fr.pasteur.sysbio.Gravity.impl.internal;

import java.util.Properties;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.event.CyEventHelper;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.property.CyProperty;
import org.cytoscape.task.read.LoadVizmapFileTaskFactory;
import org.cytoscape.task.visualize.ApplyVisualStyleTaskFactory;
import org.cytoscape.util.swing.FileUtil;
import org.cytoscape.util.swing.IconManager;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.work.SynchronousTaskManager;
import org.cytoscape.work.swing.DialogTaskManager;
import org.cytoscape.work.swing.PanelTaskManager;

import fr.pasteur.sysbio.Gravity.API.PPI;
import fr.pasteur.sysbio.Gravity.API.Pathways;
import fr.pasteur.sysbio.Gravity.API.VariantDatabase;
import fr.pasteur.sysbio.Gravity.impl.internal.task.GravityTaskFactory;
import fr.pasteur.sysbio.Gravity.impl.internal.task.SimpleClickNodeViewTaskFactory;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class ServicesUtil {

	public static CyNetworkFactory cyNetworkFactory;
	public static CyNetworkViewFactory cyNetworkViewFactory;
	public static LoadVizmapFileTaskFactory loadVizmapFileTaskFactory;
	public static ApplyVisualStyleTaskFactory applyVisualStyleTaskFactory;
	
	public static VisualMappingManager visualMappingManager;
	@SuppressWarnings("rawtypes")
	public static SynchronousTaskManager synchronousTaskManager;
	public static CyNetworkManager cyNetworkManager;
	public static CyNetworkViewManager cyNetworkViewManager;
	public static CyLayoutAlgorithmManager cyLayoutAlgorithmManager;
	public static DialogTaskManager dialogTaskManager;
	public static PanelTaskManager panelTaskManager;
	
	public static CyApplicationManager applicationManager; //currentNetwork, currentView
	public static CySwingApplication swingApplication;
	
	public static IconManager iconManager;
	
	public static CyEventHelper eventHelper;
	
	public static GravityTaskFactory gravityTaskFactory;
	public static SimpleClickNodeViewTaskFactory simpleClickNodeViewTaskFactory;
	
	public static CyNetwork lastNetwork = null;
	
	public static VisualStyle visualStyle = null;
	
	public static FileUtil fileUtil;
	
	public static ControlPanel controlPanel;
	public static ResultGeneDetailPanel resultGeneDetailPanel;
	public static ResultCohortDetail resultCohortDetail;
	public static LegendPanel legendPanel;
	
	public static PPI ppi;
	public static Pathways pathways;
	
	public static VariantDatabase variantDatabase;
	
	public static CyProperty<Properties> pathwaysProperties;
	public static CyProperty<Properties> qualitiesProperties;
	public static CyProperty<Properties> interactionsProperties;
	public static CyProperty<Properties> filtersProperties;
	public static CyProperty<Properties> gravityProperties;
	
}
