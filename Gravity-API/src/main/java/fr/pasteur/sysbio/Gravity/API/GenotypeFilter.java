/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

import java.util.ArrayList;
import java.util.List;

/**
 * Class defining a more specific filter on the genotype data. 
 * This is separated from the VariantFilter as it need patient's data. 
 * This class is mostly used for the Quality Control.
 * 
 * <p>The operator is important to test against the DP or GQ as we allow for tests other than "greater or equal than". </p>
 * @author Freddy Cliquet
 * @version 1.0
 */
public class GenotypeFilter implements Filter {

	String _property;
	String _operator;
	String _value;
	Patient _patient;
	private static List<String> _listProperties = null;
	
	/**
	 * To filter on the GT, the different values accepted would be HOMREF, HET or HOMALT.
	 */
	public final static String PROPERTY_GT = "GT (HOMREF, HET, HOMALT)";
	
	/**
	 * To filter on the DP, the value need to be an integer.
	 */
	public final static String PROPERTY_DP = "Allele Depth (DP)";
	
	/**
	 * To filter on the GQ, value need to be a Float.
	 */
	public final static String PROPERTY_GQ = "Quality (GQ)";
	
	/**
	 * lower bound to check for the HET ratio (if the ALT allele count is too low)
	 */
	public final static String PROPERTY_MIN_RATIO_HET = "Minimal allele ratio for HET";
	
	/**
	 * upper bound to check for the HET ratio (if the ALT allele count is too high, meaning REF is too low)
	 */
	public final static String PROPERTY_MAX_RATIO_HET = "Maximal allele ratio for HET";
	
	/**
	 * Default constructor, do nothing
	 */
	public GenotypeFilter(){
	}
	
	/**
	 * Constructor with the full data necessary. the patient can be null but need to be set using setPatient before testing.
	 * @param property The property to test among the constraint defined one in this class.
	 * @param operator The operator to use (when applicable)
	 * @param value The value to compare with (when applicable)
	 * @param patient The patient
	 */
	public GenotypeFilter(String property, String operator, String value, Patient patient) {
		_property = property;
		_operator = operator;
		_value = value;
		_patient = patient;
	}

	@Override
	public String toString(){
		return _property+" "+_operator+" "+_value;
	}
	
	/**
	 * Setter for the patient
	 * @param p the Patient
	 */
	public void setPatient(Patient p){
		_patient = p;
	}
	
	/**
	 * Function that can be used to define a combobox menu with the different filter options.
	 * @return the List of properties that can be treated using this filter
	 */
	public static List<String> getProperties(){
		if( _listProperties == null ){
			_listProperties = new ArrayList<String>();
			_listProperties.add(PROPERTY_GT);
			_listProperties.add(PROPERTY_DP);
			_listProperties.add(PROPERTY_GQ);
		}
		return _listProperties;
	}
	
	@Override
	public boolean passFilter(Variant gsv) {
//		System.out.println(gsv.getPatient(_patient));
		switch(_property){
		case PROPERTY_GT:
			switch(_value){
			case "HOMREF":
				return GenotypeUtils.isHomRef(gsv.getPatientGenotype(_patient));
			case "HET":
				return GenotypeUtils.isHet(gsv.getPatientGenotype(_patient));
			case "HOMALT":
				return GenotypeUtils.isHomAlt(gsv.getPatientGenotype(_patient));
			default:
				return true;
			}
		case PROPERTY_DP:
			if( gsv.isCNV() )
				return true;
			switch(_operator){
			case ">=":
				return GenotypeUtils.DP(gsv.getPatient(_patient)) >= Float.valueOf(_value);
			case "<=":
				return GenotypeUtils.DP(gsv.getPatient(_patient)) <= Float.valueOf(_value);
			case "=":
				return GenotypeUtils.DP(gsv.getPatient(_patient)) == Float.valueOf(_value);
			case "!=":
				return GenotypeUtils.DP(gsv.getPatient(_patient)) != Float.valueOf(_value);
			}
		case PROPERTY_GQ:
			if( gsv.isCNV() )
				return true;
			switch(_operator){
			case ">=":
				return GenotypeUtils.GQ(gsv.getPatient(_patient)) >= Float.valueOf(_value);
			case "<=":
				return GenotypeUtils.GQ(gsv.getPatient(_patient)) <= Float.valueOf(_value);
			case "=":
				return GenotypeUtils.GQ(gsv.getPatient(_patient)) == Float.valueOf(_value);
			case "!=":
				return GenotypeUtils.GQ(gsv.getPatient(_patient)) != Float.valueOf(_value);
			}
		case PROPERTY_MIN_RATIO_HET:
			if( gsv.isCNV() )
				return true;
			
			if( GenotypeUtils.isHomRef( gsv.getPatientGenotype(_patient) ) ){
				return GenotypeUtils.alternativeAlleleRatio( gsv.getPatient(_patient) ) <= Float.valueOf(_value);
			}
			if( GenotypeUtils.isHet( gsv.getPatientGenotype(_patient) ) ){
				return GenotypeUtils.alternativeAlleleRatio( gsv.getPatient(_patient) ) >= Float.valueOf(_value);
			}
			return true;
		case PROPERTY_MAX_RATIO_HET:
			if( gsv.isCNV() )
				return true;
			
			if( GenotypeUtils.isHomAlt( gsv.getPatientGenotype(_patient) ) ){
				return GenotypeUtils.alternativeAlleleRatio( gsv.getPatient(_patient) ) >= Float.valueOf(_value);
			}
			if( GenotypeUtils.isHet( gsv.getPatientGenotype(_patient) ) ){
				return GenotypeUtils.alternativeAlleleRatio( gsv.getPatient(_patient) ) <= Float.valueOf(_value);
			}
			return true;
		default:
			return false;
		}
	}

	@Override
	public void setProperty(String property) {
		_property = property;
	}

	@Override
	public void setOperator(String operator) {
		_operator = operator;
	}

	@Override
	public void setValue(String value) {
		_value = value;
	}

	@Override
	public String getProperty() {
		return _property;
	}

	@Override
	public String getOperator() {
		return _operator;
	}

	@Override
	public String getValue() {
		return _value;
	}
}
