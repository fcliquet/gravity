/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.gemini;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.event.EventListenerList;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.cytoscape.service.util.internal.utils.ServiceUtil;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteConfig.TempStore;
import org.sqlite.SQLiteDataSource;
import org.sqlite.SQLiteJDBCLoader;

import fr.pasteur.sysbio.Gravity.API.Filter;
import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.API.VariantDatabase;
import fr.pasteur.sysbio.Gravity.API.events.DatabaseListener;

/**
 * This class correspond to the implementation of the VariantDatabase made to interact with a Gemini database.
 * 
 * <p>In this version a full Gemini database is read and loaded in memory, thus it is important to look for memory issues. 
 * It is also very important to optimize the querying given the number of entries to retrieve from the database. </p>
 * 
 * @author Freddy Cliquet
 * @version 1.1
 */
public class GeminiDatabase implements VariantDatabase {

	private String geminiDatabase;
	private SQLiteDataSource dataSourceGemini;
	private List<Patient> _patients;
	private Map<String, Patient> _patients2;
	private Map<String,HgncGene> _genes;
	private List<GeminiVariant> _variants;
	private List<GeminiVariant> _cnvs;
	protected EventListenerList listenerList = new EventListenerList();

	private static String _filterMAF = "1";
	private static String _filterCADD = "0";
	private static String _filterImpact = "";
	private static boolean _filterMultiallelic = false;
	private static boolean _filterFreebayesShortcut = false;
	private static boolean _cnvAdditionalFields = false;
	private static boolean _CNVsRepeatMasker = false;
	private static boolean _filterPathways = false;
	private static Set<String> _listPathways = null;
	
	private static String QUERY(boolean techno, boolean MPC){
		
		String INNER_QUERY = "SELECT variant_id, gene, MIN( priority ) as priority, anno_id "
				+"FROM variant_impacts viIN, variants_priorities vpIN "
				+"WHERE viIN.impact = vpIN.impact AND viIN.gene IS NOT NULL " 
				+"GROUP BY variant_id, gene ";
		
		if( _filterPathways ){
			INNER_QUERY = "SELECT variant_id, gene, MIN( priority ) as priority, anno_id "
					+"FROM variant_impacts viIN, variants_priorities vpIN, tmp_filter_gene tfg "
					+"WHERE viIN.impact = vpIN.impact AND viIN.gene IS NOT NULL AND viIN.gene = tfg.geneS " 
					+"GROUP BY variant_id, gene ";
		}		
		
		//viIN.impact_severity != 'LOW' AND 
		
		String VEP_TECHNO = "";
		if(techno){
			System.out.println("Techno field is present.");
			VEP_TECHNO = ", vep_techno ";
		}
		
		String VEP_MPC = "";
		if(MPC){
			System.out.println("MPC field is present.");
			VEP_TECHNO = ", MPC ";
		}
		
		String GT_QUALS = ", gt_quals ";
		if(_filterFreebayesShortcut){
			System.out.println("Not loading the GQ because of a Freebayes format issue");
			GT_QUALS = "";
		}
		
		String CNV_ARRAYS_FIELDS = "";
		if( _cnvAdditionalFields ){
			System.out.println("CNV file selected, loading fields specific to arrays");
			CNV_ARRAYS_FIELDS = ",v.vep_CIA_RefSTART,v.vep_CIA_RefSTOP,v.vep_CIA_Concat,v.vep_CIA_ConcatSTART,v.vep_CIA_ConcatSTOP,v.vep_CIA_ConcatSIZE,v.vep_CIA_ConcatOVERLAP,v.vep_CIA_ConcatCN,v.[vep_CIA_Concat#SNP],v.vep_CIA_ConcatDENSITY,v.vep_CNVision_OVERLAP,v.vep_CNVision_CN,v.[vep_CNVision_#SNP],v.vep_CNVision_DENSITY,v.vep_CNVision_SCORE,v.[vep_CNVision_#ALGOS],v.vep_CNVision_ALGOS,v.[vep_CNVision_%3Algos],v.[vep_CNVision_%2Algos],v.[vep_CNVision_%1Algo],v.[vep_CohortFreq%],v.vep_isUnique,v.[vep_#DGV_Hits],v.vep_DGV_Ref,v.vep_DGV_ObservedEvents,v.vep_DGV_SampleSIze,v.[vep_DGV_Freq%] ";
			CNV_ARRAYS_FIELDS = CNV_ARRAYS_FIELDS.toLowerCase();
//			System.out.println(CNV_ARRAYS_FIELDS);
		}
		
		String CNV_REPEAT_MASKER = "";
		if( _CNVsRepeatMasker ){
			System.out.println("Filtering out repeat masked CNVs.");
			CNV_REPEAT_MASKER = "AND ((type='sv' AND rmsk is null) OR type!='sv') ";
		}
		
		String CLINVAR = ", clinvar_sig, clinvar_disease_name, clinvar_dbsource, clinvar_origin, clinvar_dsdb, clinvar_disease_acc, clinvar_in_locus_spec_db, clinvar_on_diag_assay, clinvar_causal_allele, clinvar_gene_phenotype ";
		
		return "SELECT vi.variant_id, vi.gene, vi.transcript, vi.is_exonic, vi.is_coding, vi.is_lof, vi.exon, "
				+"vi.codon_change, vi.aa_change, vi.aa_length, vi.biotype, vi.impact, vi.impact_severity, vi.polyphen_pred, "
				+"vi.polyphen_score, vi.sift_pred, vi.sift_score, chrom, start, end, ref, alt, qual, filter, type, sub_type, "
				+"gts, gt_depths, gt_ref_depths, gt_alt_depths"+GT_QUALS+", gt_phred_ll_homref, gt_phred_ll_het, gt_phred_ll_homalt, "
				+"rs_ids, sv_length, sv_tool, aaf_esp_all, aaf_1kg_all, cadd_raw, cadd_scaled, aaf_adj_exac_all, max_aaf_all, vp.priority, "
				+"aaf_esp_ea,aaf_esp_aa,aaf_1kg_amr,aaf_1kg_eas,aaf_1kg_sas,aaf_1kg_afr,aaf_1kg_eur,aaf_adj_exac_afr,aaf_adj_exac_amr, "
				+"aaf_adj_exac_eas,aaf_adj_exac_fin,aaf_adj_exac_nfe,aaf_adj_exac_oth,aaf_adj_exac_sas "+VEP_TECHNO+CNV_ARRAYS_FIELDS+CLINVAR
				+"FROM variants as v, variant_impacts as vi, ("+INNER_QUERY+") vp "
				+"WHERE v.variant_id = vi.variant_id "+CNV_REPEAT_MASKER
				+"AND vi.variant_id = vp.variant_id "
				+"AND vi.gene = vp.gene "
				+"AND vi.anno_id = vp.anno_id "+(_filterMultiallelic?"AND sub_type!='unknown' ":"")
				+"AND v.max_aaf_all <= "+_filterMAF+" AND (cadd_scaled >= "+_filterCADD+" OR cadd_scaled is NULL) "+(_filterImpact.equals("")?"":"AND vi.impact_severity "+_filterImpact+" ")
				+"AND ( vi.is_exonic='1' OR vi.is_coding='1' OR vi.impact='splice_acceptor_variant' or vi.impact='splice_donor_variant' or vi.impact='splice_region_variant' ) "
				+"ORDER BY vi.variant_id, vi.gene, vi.impact_severity ";
	}
	
	private static String QUERY_COUNT(){
		
		String INNER_QUERY = "SELECT variant_id, gene, MIN( priority ) as priority, anno_id "
				+"FROM variant_impacts viIN, variants_priorities vpIN "
				+"WHERE viIN.impact = vpIN.impact AND viIN.gene IS NOT NULL " 
				+"GROUP BY variant_id, gene ";
		
		return "SELECT COUNT( * ) as count "
				+"FROM variants as v, variant_impacts as vi, ("+INNER_QUERY+") vp "
				+"WHERE v.variant_id = vi.variant_id "
				+"AND vi.variant_id = vp.variant_id "
				+"AND vi.gene = vp.gene "
				+"AND vi.anno_id = vp.anno_id "
				+"AND v.max_aaf_all <= "+_filterMAF+" "+(_filterImpact.equals("")?"":"AND vi.impact_severity "+_filterImpact+" ")
//				+"AND vi.impact_severity != 'LOW' "
				+"AND ( vi.is_exonic='1' OR vi.is_coding='1' OR vi.impact='splice_acceptor_variant' or vi.impact='splice_donor_variant' or vi.impact='splice_region_variant' ); ";
	}
	
	/**
	 * Create a new GeminiDatabase on the specified Gemini file. This will also set some SQlite parameters 
	 * to allow for a faster loading of the data, which are made for better loading, in case of writing of 
	 * any data, the developer need to be careful about any commit needed for instance.
	 * 
	 * @param geminiDatabase full path to the gemini database file
	 */
	public GeminiDatabase(String geminiDatabase){
		this.geminiDatabase = geminiDatabase;
		_variants = new ArrayList<GeminiVariant>();
		_cnvs = new ArrayList<GeminiVariant>();
		
		try {
			SQLiteJDBCLoader.initialize();
			SQLiteConfig config = new SQLiteConfig();
			config.setReadOnly(true);
			config.setCacheSize(250000);
			config.setPageSize(4096);
			config.setTempStore(TempStore.MEMORY);
			dataSourceGemini = new SQLiteDataSource();
			dataSourceGemini.setUrl("jdbc:sqlite:"+this.geminiDatabase);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public GeminiDatabase(String geminiDatabase, String filterMAF, String filterCADD, String filterImpact, boolean noMultiallelic, /*boolean cnvFields,*/ boolean cnvRepeatMasker, boolean pathways, Set<String> listPathways){
		this.geminiDatabase = geminiDatabase;
		_variants = new ArrayList<GeminiVariant>();
		_cnvs = new ArrayList<GeminiVariant>();
		_filterMAF = filterMAF;
		_filterCADD = filterCADD;
		_filterImpact = filterImpact;
		_filterMultiallelic = noMultiallelic;
//		_cnvAdditionalFields = cnvFields;
		_CNVsRepeatMasker = cnvRepeatMasker;
		_filterPathways = pathways;
		_listPathways = listPathways;
		
		try {
			SQLiteJDBCLoader.initialize();
			SQLiteConfig config = new SQLiteConfig();
			config.setReadOnly(true);
			config.setCacheSize(250000);
			config.setPageSize(4096);
			config.setTempStore(TempStore.MEMORY);
			dataSourceGemini = new SQLiteDataSource();
			dataSourceGemini.setUrl("jdbc:sqlite:"+this.geminiDatabase);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void loadPatients(){
		List<Patient> patients = new ArrayList<Patient>();
		BeanListHandler<Patient> h = new BeanListHandler<Patient>(Patient.class);
		QueryRunner run = new QueryRunner(dataSourceGemini);
		
		try {
//			patients = run.query("SELECT name, family_id as family, paternal_id as father, maternal_id as mother, sex, phenotype FROM samples", h );
			patients = run.query("SELECT * FROM samples", h );
		} catch (SQLException e) {
			e.printStackTrace();
		}
		_patients = patients;
		_patients2 = _patients.stream().collect(Collectors.toMap(Patient::getName, p -> p));
		Patient.setCohort(_patients2);
	}
	
	private boolean withTechno(){
		try {
			Connection c = dataSourceGemini.getConnection();
			Statement s = c.createStatement();
			ResultSet rs = s.executeQuery( "PRAGMA table_info(variants);" );
			ResultSetMetaData meta = rs.getMetaData();
			
			while(rs.next()){
				if( rs.getString(2).equals("vep_techno") )
					return true;
			}
	
			s.close();
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private boolean withMPC(){
		try {
			Connection c = dataSourceGemini.getConnection();
			Statement s = c.createStatement();
			ResultSet rs = s.executeQuery( "PRAGMA table_info(variants);" );
			ResultSetMetaData meta = rs.getMetaData();
			
			while(rs.next()){
				if( rs.getString(2).equals("MPC") )
					return true;
			}
	
			s.close();
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
//	private void loadGenes(){
//		Map<String, GeminiGene> genes = new HashMap<String,GeminiGene>();
//		_genes = new HashMap<String,GeminiGene>();
//		BeanMapHandler<String,GeminiGene> h = new BeanMapHandler<String,GeminiGene>(GeminiGene.class, "gene");
//		QueryRunner run = new QueryRunner(dataSourceGemini);
//		
//		try {
//			genes = run.query("select chrom, gene, ensembl_gene_id, hgnc_id, entrez_id, synonym, rvis_pct, mam_phenotype_id "
//						+"from gene_detailed "
//						+"where hgnc_id is not null "
//						+"group by chrom, gene, rvis_pct, ensembl_gene_id, hgnc_id, entrez_id, synonym, mam_phenotype_id;", h );
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
//		System.out.println("genes loaded");
//		
//		for( GeminiGene gg:genes.values() ){
//			if( gg.getSynonym() != null ){
//				String splits[] = gg.getSynonym().split(",");
//				for(int i=0;i<splits.length;i++){
//					if( genes.get(splits[i])!=null  )
//					_genes.put(splits[i], gg);
//				}
//			}
//		}
//		
//		System.out.println("alias created");
//		
//		_genes.putAll(genes);
//	}
	
	private void loadHgncGenes(){
		Map<String, HgncGene> genes = new HashMap<String,HgncGene>();
		_genes = new HashMap<String,HgncGene>();
		BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/hgnc_complete_set.txt")));
		try {
			String line = br.readLine();
			while( (line = br.readLine()) != null ){
//				System.out.println(line);
				String[] l = line.split("\t");
				
				HgncGene g = new HgncGene();
				g.setGene( l[1] );
				
				if( l[2].equals("entry withdrawn") ){
					g.setDescription( l[2] );
				}else{
					g.setChrom( l[6].split("q")[0].split("p")[0] );
					g.setLocus( l[6] );
					g.setDescription( l[2] );
					if( l.length >= 19 )
						g.setEntrez_id( l[18] );
					if( l.length >= 20 )
						g.setEnsemble_gene_id( l[19] );
					g.setSynonym( l[8] );
					g.setPreviousNames( l[10] );
					g.setHgnc_id( l[0] );
				}
				
				genes.put(g.getName(), g);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		_genes.putAll(genes);
	}
	
	private void createGeneFilterTable(){
		try {
			Connection c = dataSourceGemini.getConnection();
			Statement s = c.createStatement();
			s.execute( "DROP TABLE IF EXISTS tmp_filter_gene;" );
			s.execute( "CREATE TABLE tmp_filter_gene (geneS STRING PRIMARY KEY);" );
			
			for(String ptw:_listPathways){
				s.execute( "INSERT INTO tmp_filter_gene (geneS) VALUES ('"+ptw+"');" );
//				System.out.println(ptw);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void loadVariantsPriorities(){
		BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/variants_priorities.csv")));
		try {
			String line = br.readLine();
			Connection c = dataSourceGemini.getConnection();
			Statement s = c.createStatement();
			s.execute( "DROP TABLE IF EXISTS variants_priorities;" );
			s.execute( "CREATE TABLE variants_priorities (impact STRING PRIMARY KEY, impact_severity STRING, priority INT);" );
			
			while( (line = br.readLine()) != null ){
//				System.out.println(line);
				String[] l = line.split(",");
				s.execute( "INSERT INTO variants_priorities (impact, impact_severity, priority) VALUES ('"+l[0]+"','"+l[1]+"',"+l[2]+");" );
			}
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean loadVariants(int start, int amount){
		List<GeminiVariant> variants;
		BeanListHandler<GeminiVariant> h = new BeanListHandler<GeminiVariant>(GeminiVariant.class, new VariantRowProcessor());
		QueryRunner run = new QueryRunner(dataSourceGemini);
		boolean moreToLoad = false;
		try {
			variants = run.query( QUERY(withTechno(), withMPC()) + " LIMIT ?, ?;", h, start, amount );
			moreToLoad = (variants.size()==amount);
			_variants.addAll(variants);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return moreToLoad;
	}
	
	private GeminiVariantMetadata populateVariantMetadata(){
		GeminiVariantMetadata metadata = new GeminiVariantMetadata();
		try {
			Connection c = dataSourceGemini.getConnection();
			Statement s = c.createStatement();
			ResultSet rs = s.executeQuery( QUERY(withTechno(), withMPC())+" LIMIT 1;" );
			ResultSetMetaData meta = rs.getMetaData();
			for(short i=1 ; i<=meta.getColumnCount() ; i++)
				metadata.addAttribute(meta.getColumnLabel(i), i, meta.getColumnTypeName(i));
			s.close();
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return metadata;
	}
//	
//	private int countVariants(){
//		MapListHandler h = new MapListHandler();
//		QueryRunner run = new QueryRunner(dataSourceGemini);
//		
//		try {
//			List<Map<String, Object>> result = run.query(QUERY_COUNT(), h);
//			System.out.println("count "+(Integer)result.get(0).get("count"));
//			return (Integer)result.get(0).get("count");
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		return 0;
//	}
//	
//	private void testDB(){
//		System.out.println("START TEST");
//		
//		try {
//			Connection c = dataSourceGemini.getConnection();
//			Statement s = c.createStatement();
//			ResultSet rs = s.executeQuery( QUERY()+"LIMIT 25" );
//			ResultSetMetaData meta = rs.getMetaData();
//			for(short i=1 ; i<=meta.getColumnCount() ; i++)
//				System.out.println(i+" : "+meta.getColumnLabel(i)+" : "+meta.getColumnTypeName(i));
//			
//			System.out.println(" ======= ");
//			
//			while(rs.next())
//				System.out.println(rs.getString(1)+" :: "+rs.getString(2)+" :: "+rs.getString(13)+" :: "+rs.getString(14)+" :: "+rs.getString(45));
//			s.close();
//			c.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
//		System.out.println("END TEST");
//	}
	
	/**
	 * affect all the variants to the corresponding gene.
	 * splicing variant genes are not in the database, so not linked.
	 */
	private void affectVariant2Genes(){
		
		System.out.println("Building aliases index");
		Map<String,HgncGene> geneAliases = new HashMap<String,HgncGene>();
		for( HgncGene g:_genes.values() ){
			for(String a:g.getAliases()){
				geneAliases.put(a, g);
			}
		}
		System.out.println("Aliases index built");
		
		for(GeminiVariant gv:_variants){
			if( _genes.get(gv.getGene()) != null ){
				_genes.get(gv.getGene()).addVariant(gv);
			}else{
				boolean added = false;
//				System.out.println("search");
				if( geneAliases.get(gv.getGene()) != null ){
					geneAliases.get(gv.getGene()).addVariant(gv);
					added = true;
				}
//				for( HgncGene g:_genes.values() ){
//					if( g.containsAlias(gv.getGene()) ){
//						g.addVariant(gv);
//						added = true;
//						break;
//					}
//				}
				if(!added){
//					System.out.println("add");
					HgncGene g = new HgncGene();
					g.setGene(gv.getGene());
					g.setDescription("created because not in HGNC file");
					_genes.put(g.getName(), g);
					g.addVariant(gv);
					
				}
			}
		}
	}
	
	private void buildCNVlist(){
		//creation of a list containing only the CNV types of variants
		for(GeminiVariant gv:_variants){
			if( gv.isCNV() ){
				_cnvs.add(gv);
			}
		}
		HgncGene.setCNVs(_cnvs);
		
		updateOverlappingCNVs(80, true);
	}
	
	
	private int _overlap = 0;
	private boolean _twoway = false;
	public void updateOverlappingCNVs(int overlap, boolean twoway){
		if( overlap==_overlap && twoway==_twoway ){
//			System.out.println("Quality parameters unchanged");
		}else{
			System.out.println("Updating overlapping genes lists...");
			_overlap = overlap;
			_twoway = twoway;
			for(GeminiVariant v:_cnvs){
				for(GeminiVariant v2:_cnvs){
					if(v!=v2 && (int)v.getObject("variant_id") != (int)v2.getObject("variant_id")){
						if( v.getChromosome().equals(v2.getChromosome()) && ((String)v.getObject("sub_type")).equals((String)v2.getObject("sub_type")) ){
							int size1 = (int)v.getObject("end") - (int)v.getObject("start");
							int size2 = (int)v2.getObject("end") - (int)v2.getObject("start");
							int overlap_size = Math.min((int)v.getObject("end"), (int)v2.getObject("end")) - Math.max((int)v.getObject("start"), (int)v2.getObject("start"));
							if( overlap_size > 0 && overlap_size/(float)size1 > (_overlap/100f) && (_twoway && overlap_size/(float)size2 > (_overlap/100f)) )
								v.addOverlappingCNV(v2);
						}
					}
				}
			}
		}
	}
	
	private void affectGenes(){
		for(HgncGene g:_genes.values()){
			for(Variant v:g.getVariants()){
				v.setGeneObject(g);
			}
		}
	}
	/**
	 * Main function is here only for testing purposes.
	 * 
	 * @param args command line parameters: none expected
	 * @throws InterruptedException Exception if the thread can't wait before loading the data (for debugging issues)
	 */
	public static void main(String[] args) throws InterruptedException {
//		Thread.currentThread().sleep(10000);
//		GeminiDatabase gd = new GeminiDatabase("/Users/fcliquet/Documents/Data/GHFC/Gemini/WES_NGXBio_Chr1.db"); //EXOMES_PARIS-204-RefSeq //EXOMES_PARIS-204.VEP.ped.db
//		GeminiDatabase gd = new GeminiDatabase("/Users/fcliquet/Documents/Data/GHFC/Gemini/All_feroe_exomes_freebayes_GATK.db"); //EXOMES_PARIS-204-RefSeq //EXOMES_PARIS-204.VEP.ped.db
		GeminiDatabase gd = new GeminiDatabase("/Users/fcliquet/Data/gemini/WGS-Paris.freebayes.exonic.20170303.db"); //EXOMES_PARIS-204-RefSeq //EXOMES_PARIS-204.VEP.ped.db
		
//		System.out.println("Techno: "+gd.withTechno());
		
		gd.load();
		
		Runtime runtime = Runtime.getRuntime();
		long memory = runtime.totalMemory() - runtime.freeMemory();
	    System.out.println("Used memory is Mb: " + memory/1024L/1024L);
	    runtime.gc();
	    System.out.println(gd.numberOfVariants());
	    memory = runtime.totalMemory() - runtime.freeMemory();
	    System.out.println("Used memory is Mb: " + memory/1024L/1024L);
	}

	@Override
	public void load() {
		long startTime = System.currentTimeMillis();
		
		loadVariantsPriorities();
		createGeneFilterTable();
		
//		testDB();
		
		fireDatabaseProgressChange("Loading patients (step 1/6)", 0, 0);
		loadPatients();
		GeminiVariant.setPatients(_patients);
		fireDatabaseProgressChange("Patients loaded (step 1/6)", 0, 0);

		fireDatabaseProgressChange("Loading genes (step 2/6)", 0, 0);
		loadHgncGenes();
		fireDatabaseProgressChange("Genes loaded (step 2/6)", 0, 0);
		
		GeminiVariant.setMetadata( populateVariantMetadata() );
		
//		int nb_variants = countVariants();
		//load variants by batches
		
		int step = 250000;
		int start = 0;
		fireDatabaseProgressChange("Loading variants (step 3/6)", 0, 0);
		while( loadVariants(start, step) ){
			start+=step;
			fireDatabaseProgressChange("Loading variants (step 3/6)", start, 250000);
		}
		fireDatabaseProgressChange("Variants loaded (step 3/6)", 0, 0);

		fireDatabaseProgressChange("Affecting variants to genes (step 4/6)", 0, 0);
		affectVariant2Genes();
		fireDatabaseProgressChange("Variants affected to genes (step 4/6)", 0, 0);
		
		fireDatabaseProgressChange("Building CNVs list (step 5/6)", 0, 0);
		buildCNVlist();
		fireDatabaseProgressChange("CNVs list built (step 5/6)", 0, 0);
		
		fireDatabaseProgressChange("Affecting genes to variants (step 6/6)", 0, 0);
		affectGenes();
		fireDatabaseProgressChange("Genes affected to variants (step 6/6)", 0, 0);
		
		
		for(int i=0 ; i<5 ; i++){
			System.runFinalization();
			System.gc();
		}
		
		double execTime = ((System.currentTimeMillis()-startTime)/1000f);
		System.out.println("execution time: "+ execTime +" seconds" );
	}

	@Override
	public List<Patient> getPatients() {
		return _patients;
	}

	@Override
	public List<Gene> getAllGenes() {
		return new ArrayList<Gene>(_genes.values());
	}

	@Override
	public List<Gene> getGenesCarryingVariant() {
		List<Gene> l = new ArrayList<Gene>();
		for(HgncGene gg:_genes.values()){
			if( gg.hasVariant() )
				l.add(gg);
		}
		return l;
	}

	@Override
	public List<Gene> getGenesCarryingVariant(List<Filter> filters) {
		List<Gene> l = getGenesCarryingVariant();
		ArrayList<Gene> toRemove = new ArrayList<Gene>();
		for(Gene g:l){
			g.setFilters(filters);
			if( !g.hasFilteredVariant() )
				toRemove.add(g);
		}
		l.removeAll(toRemove);
		return l;
	}

	@Override
	public List<Gene> getGenesCarryingVariant(Patient p) {
		List<Gene> l = getGenesCarryingVariant();
		List<Gene> newL = new ArrayList<Gene>(l);
		for(Gene g:l){
			if(!g.carryAVariant(p))
				newL.remove(g);
		}
		return newL;
	}

	@Override
	public List<Gene> getGenesCarryingVariant(Patient p, List<Filter> filters) {
		List<Gene> l = getGenesCarryingVariant(filters);
		List<Gene> newL = new ArrayList<Gene>(l);
		for(Gene g:l){
			if(!g.carryAFilteredVariant(p))
				newL.remove(g);
		}
		return newL;
	}

	@Override
	public Gene getGene(String geneName) {
		return _genes.get(geneName);
	}

	@Override
	public Gene getGene(String geneName, List<Filter> filters) {
		Gene g = _genes.get(geneName);
		if( g == null ){
			g = new HgncGene();
			((HgncGene)g).setGene(geneName);
		}
		g.setFilters(filters);
		return g;
	}

	@Override
	public int numberOfVariants() {
		return _variants.size();
	}

	@Override
	public Patient getFather(Patient p) {
		return getPatient(p.getFatherName());
	}

	@Override
	public Patient getMother(Patient p) {
		return getPatient(p.getMotherName());
	}
	
	private Patient getPatient(String name){
		for(Patient p:_patients)
			if(p.getName().equals(name))
				return p;
		return null;
	}

	@Override
	public List<String> listVariantAttributes() {
		return _variants.get(0).listAttributes();
	}
	
	@Override
	public List<String> listFrequencyAttributes() {
		List<String> allAtt = _variants.get(0).listAttributes();
		List<String> l = new ArrayList<String>();
		l.add("maximum allele frequency");
		l.add("cohort frequency");
		for(String a:allAtt){
			if( a.startsWith("aaf_") )
				l.add(a);
		}
		return l;
	}
	
	@Override
	public List<String> listCNVsAttributes() {
		List<String> allAtt = _variants.get(0).listAttributes();
		List<String> l = new ArrayList<String>();
		for(String a:allAtt){
			if( a.startsWith("sv_") || a.startsWith("vep_cnvision_") || a.startsWith("vep_cia_") || a.startsWith("vep_dgv_") || a.startsWith("vep_#dgv_") )
				l.add(a);
		}
		return l;
	}
	
	@Override
	public List<String> listPathoAttributes() {
		List<String> allAtt = _variants.get(0).listAttributes();
		List<String> l = new ArrayList<String>();
		for(String a:allAtt){
			if( a.contains("cadd") || a.contains("dann") || a.startsWith("clinvar_") || a.startsWith("polyphen_") || a.startsWith("sift_") )
				l.add(a);
		}
		return l;
	}
	
	@Override
	public List<String> listOtherAttributes() {
		List<String> allAtt = _variants.get(0).listAttributes();
		List<String> l = new ArrayList<String>(allAtt);
		l.removeAll(listFrequencyAttributes());
		l.removeAll(listCNVsAttributes());
		l.removeAll(listPathoAttributes());
		l.remove("cohort_frequency");
		l.remove("max_aaf_all");
		return l;
	}

	@Override
	public Set<String> getAllGenesNames() {
		return new HashSet<String>(_genes.keySet());
	}

	@Override
	public List<String> listVariantImpacts() {
		Set<String> s = new HashSet<String>();
		for(GeminiVariant v:_variants){
			s.add(v.impact());
		}
		List<String> l = new ArrayList<String>(s);
		Collections.sort(l, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Integer.compare(GeminiVariant.impactPriority(o1), GeminiVariant.impactPriority(o2));
			}
		});
		return l;
	}

	@Override
	public List<Patient> getPatientsWithParents() {
		List<Patient> l = new ArrayList<Patient>();
		for(Patient p:_patients){
			if( getFather(p)!=null && getMother(p)!=null )
				l.add(p);
		}
		return l;
	}

	@Override
	public boolean hasPedigree() {
		return true;
	}

	@Override
	public List<String> getImpactsOfSeverity(String severity) {
		Set<String> s = new HashSet<String>();
		for(GeminiVariant v:_variants){
			if( v.impactSeverity().equals(severity) )
				s.add(v.impact());
		}
		List<String> l = new ArrayList<String>(s);
		Collections.sort(l, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Integer.compare(GeminiVariant.impactPriority(o1), GeminiVariant.impactPriority(o2));
			}
		});
		return l;
	}

	@Override
	public void addDatabaseListener(DatabaseListener databaseListener) {
		listenerList.add(DatabaseListener.class, databaseListener);
	}

	@Override
	public void removeDatabaseListener(DatabaseListener databaseListener) {
		listenerList.remove(DatabaseListener.class, databaseListener);
	}

	@Override
	public void fireDatabaseProgressChange(String task, int current, int max) {
		Object[] listeners = listenerList.getListenerList();
	    for (int i = 0; i < listeners.length; i = i+2) {
	      if (listeners[i] == DatabaseListener.class) {
	        ((DatabaseListener) listeners[i+1]).progressChanged(task, current, max);;
	      }
	    }
	}

	@Override
	public Set<String> getFamilies() {
		Set<String> s = new HashSet<String>();
		for(Patient p:_patients)
			s.add( p.getFamily() );
		return s;
	}

	@Override
	public List<Patient> getPatients(String family) {
		List<Patient> l = new ArrayList<Patient>();
		for(Patient p:_patients){
			if( p.getFamily().equals(family) )
				l.add(p);
		}
		return l;
	}

	@Override
	public String getInfos() {
		String s = "<html>";
		
		String sp[] = geminiDatabase.split("/");
		
		s += "<b>File: </b>"+sp[sp.length-1]+"<br/>";
		s += "<hr/>";
		s += "<b>Nb genes: </b>"+getGenesCarryingVariant().size()+"<br/>";
		s += "<b>Nb variants: </b>"+numberOfVariants()+"<br/>";
		s += "<b>Nb patients: </b>"+getPatients().size()+"<br/>";
		return s;
	}

}
