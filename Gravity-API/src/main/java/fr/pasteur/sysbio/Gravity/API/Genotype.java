/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

import java.io.Serializable;

import org.eclipse.collections.api.bimap.MutableBiMap;
import org.eclipse.collections.impl.bimap.mutable.HashBiMap;


/**
 * Class representing a Genotype as a full genotype line from a VCF, meaning not only both alleles, but also data such as DP, AD, GQ or PL.
 * 
 * <p>This will always be fine for the Gemini version, but it might need to be modified depending on the VCF format used for instance.</p>
 * 
 * <p>Space occupied by this class is a major concern, thus it has been optimized and need to be kept in mind for further changes. 
 * (An instance will contain a really important number of genotypes)</p>
 * 
 * <p>So far most access are done through the String representation of the GT and the GenotypeUtils function to parse it. 
 * This was done like this to begin with for simplification in the compatibility between the different interface (Gemini, VCF...) but could evolve.</p>
 * 
 * <p>The override mode is present specifically for cancer users that need not to consider the genotype from the vcf, but only the AD (alternative allele count). 
 * So when override is activated, the genotype will be inferred from the AD by using the minAD parameter. 
 * Thus 0/0 will be used for AD<minAD ; 0/1 for minAD<=AD<DP-minAD ; 1/1 for AD>=DP-minAD
 * 
 * @author Freddy Cliquet
 * @version 2.0
 * @see GenotypeUtils
 */
public class Genotype implements Serializable {
	private static final long serialVersionUID = -2477278729079788677L;
	private static MutableBiMap<String, Integer> gts=null;
	private static MutableBiMap<Integer, String> gts_reversed=null;
	private int gt;
	private short depth;
	private short refDepth;
	private short altDepth;
	private short qual;
	private short phred_ll_homref;
	private short phred_ll_het;
	private short phred_ll_homalt;	
	
	private static boolean overrideGT = false;
	private static int minAD = 1;
	
	/**
	 * Create a new Genotype object containing the representation of the alleles, the differents depths and the phred likelihoods.
	 * @param genotype String representation of the genotype (GT:AD:DP:GQ:PL)
	 */
	public Genotype(String genotype) {
		if(gts==null)
			gts = new HashBiMap<String, Integer>();
		
		String s[] = genotype.split(":");	
		gt = gts.getIfAbsentPut(s[0], gts.size());

		String s2[] = s[1].split(",");
		refDepth = Short.parseShort(s2[0]);
		altDepth = Short.parseShort(s2[1]);
		depth = Short.parseShort(s[2]);
		qual = Short.parseShort(s[3].split("\\.")[0]);
		
		String s3[] = s[4].split(",");
		try {
			phred_ll_homref = Short.parseShort(s3[0]);
		} catch (NumberFormatException e) {
			phred_ll_homref = Short.MAX_VALUE;
		}
		try {
			phred_ll_het = Short.parseShort(s3[1]);
		} catch (NumberFormatException e) {
			phred_ll_homref = Short.MAX_VALUE;
		}
		try {
			phred_ll_homalt = Short.parseShort(s3[2]);
		} catch (NumberFormatException e) {
			phred_ll_homref = Short.MAX_VALUE;
		}
	}
	
	/**
	 * Constructor for an already parsed genotype
	 * @param gt Genotype as a String 'X/Y' with 'X' and 'Y' being the alleles.
	 * @param depth Coverage of the sequencing on that spot
	 * @param refDepths Number of reads for the reference allele
	 * @param altDepths Number of reads for the alternative allele
	 * @param qual GQ value of the genotype (second lowest PL value)
	 * @param phred_homref Phred likelihood for the homozygous reference case
	 * @param phred_het Phred likelihood for the heterozygous case
	 * @param phred_homalt Phred likelihood for the homozygous alternative case
	 */
	public Genotype(String gt, short depth, short refDepths, short altDepths, short qual, short phred_homref, short phred_het, short phred_homalt){
		this.depth = depth;
		this.refDepth = refDepths;
		this.altDepth = altDepths;
		this.qual = qual;
		this.phred_ll_homref = phred_homref;
		this.phred_ll_het = phred_het;
		this.phred_ll_homalt = phred_homalt;
		if(gts==null)
			gts = new HashBiMap<String, Integer>();
		this.gt = gts.getIfAbsentPut(gt, gts.size());
	}
	
	/**
	 * Define if the genotype class will override the genotype provided by one chosen directly from the coverage (AD). This is usefull for cancer data where they don't use the GT, but the AD with a minimal number of reads.
	 * @param override true to use the override mode
	 */
	public static void overrideGT(boolean override){
		overrideGT = override;
	}
	
	/**
	 * Define the minimal number of reads to use when the override mode is active.
	 * @param min minimal number of read to consider the variant present
	 */
	public static void setMinAD(int min){
		minAD = min;
	}
	
	/**
	 * Check if currently overriding the GT by and inferred GT from the AD value.
	 * @return true if overriding the GT
	 */
	public static boolean isOverridingGT(){
		return overrideGT;
	}
	
	/**
	 * Getter on the GT itself, it is important to use this function as we use an optimized way to store it.
	 * @return The GT of this genotype as a String representation
	 */
	public String getGT(){
		if( overrideGT ){
			if( altDepth < minAD )
				return "0/0";
			if( altDepth >= minAD && altDepth < (depth-minAD) )
				return "0/1";
			if( altDepth >= (depth-minAD) )
				return "1/1";
		}
		
		if( gts_reversed==null )
			gts_reversed = gts.flipUniqueValues();
		return gts_reversed.get(gt);
	}
	
	@Override
	public String toString() {
		return getGT()+":"+refDepth+","+altDepth+":"+depth+":"+qual+":"+phred_ll_homref+","+phred_ll_het+","+phred_ll_homalt;
	}
}
