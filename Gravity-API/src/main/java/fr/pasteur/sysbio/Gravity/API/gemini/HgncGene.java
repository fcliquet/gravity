/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.gemini;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.pasteur.sysbio.Gravity.API.Filter;
import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.GeneAnnotation;
import fr.pasteur.sysbio.Gravity.API.GenotypeFilter;
import fr.pasteur.sysbio.Gravity.API.GenotypeUtils;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;
import fr.pasteur.sysbio.Gravity.API.utils.Maths;

/**
 * @author Freddy Cliquet
 * @version 1.0
 *
 */
public class HgncGene implements Gene, Serializable {

private static final long serialVersionUID = -3700036034777406493L;
	
	private String chrom;
	private String gene;
	private String description;
	private String locus;
	private String ensemble_gene_id;
	private String transcript;
	private String biotype;
	private String transcript_status;
	private String cds_length;
	private String protein_length;
	private String transcript_start;
	private String transcript_end;
	private String strand;
	private String synonym;
	private String prevNames;
	private float rvis_pct;
	private String mam_phenotype_id;
	private String hgnc_id;
	private String entrez_id;

	List<GeminiVariant> _variants;
	List<GeminiVariant> _filteredVariants;
	List<Filter> _filters;
	List<GeneAnnotation<?>> _annotations;
	
	private static List<GeminiVariant> _cnvs;
	private static List<GeminiVariant> _filteredCNVs = null;
	
	public List<GeminiVariant> getCNVs(){
		return _cnvs;
	}
	
	public List<GeminiVariant> getFilteredCNVs(Patient patient){
		if( _filteredCNVs == null ){
			_filteredCNVs = new ArrayList<GeminiVariant>(_cnvs);
			
			for(Filter vf:_filters){
				_filteredCNVs.removeIf(v -> !vf.passFilter(v));
			}
		}
		List<GeminiVariant> l = new ArrayList<GeminiVariant>();
		for( GeminiVariant gv : _filteredCNVs ){
			if( GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(patient)) ){
				l.add(gv);
			}
		}
		return l;
	}
	
	public List<GeminiVariant> getCNVs(Patient patient){
		List<GeminiVariant> l = new ArrayList<GeminiVariant>();
		for( GeminiVariant gv : _cnvs ){
			if( GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(patient)) ){
				l.add(gv);
			}
		}
		return l;
	}
	
	public static void setCNVs(List<GeminiVariant> cnvs){
		_cnvs = cnvs;
	}

	public HgncGene(){
		_variants = new ArrayList<GeminiVariant>();
		_filteredVariants = new ArrayList<GeminiVariant>();
		_filters = new ArrayList<Filter>();
		_annotations = new ArrayList<GeneAnnotation<?>>();
	}
	
//	public static void main(String[] args) {
//		HgncGene g = new HgncGene();
//		g.setGene("ACKR1");
//		g.setSynonym("\"CCBP1|GPD|Dfy|CD234\"");
//		g.setPreviousNames("\"FY|DARC\"");
//		System.out.println(g.containsAlias("DARC"));
//	}
	
	public boolean containsAlias(String name){
		String ss[] = null;
		
		if( synonym != null ){
			ss = synonym.replace("\"", "").split("\\|");
			for(String s:ss){
				if( s.equals(name) )
					return true;
			}
		}
		
		if( prevNames != null ){
			ss = prevNames.replace("\"", "").split("\\|");
			for(String s:ss){
				if( s.equals(name) )
					return true;
			}
		}
		
		return false;
	}
	
	public Set<String> getAliases(){
		Set<String> aliases = new HashSet<String>();
		String ss[] = null;
		
		if( synonym != null ){
			ss = synonym.replace("\"", "").split("\\|");
			for(String s:ss)
				aliases.add(s);
		}
		
		if( prevNames != null ){
			ss = prevNames.replace("\"", "").split("\\|");
			for(String s:ss)
				aliases.add(s);
		}
		
		return aliases;
	}
	
	public String toString(){
		return gene+" - "+transcript;
	}
	
	public void setDescription(String desc){
		description = desc;
	}
	
	public String getDescription(){
		return description;
	}
	
	public void setLocus(String loc){
		locus = loc;
	}
	
	public String getLocus(){
		return locus;
	}
	
	public void setPreviousNames(String previousNames){
		prevNames = previousNames;
	}
	
	public String getPreviousNames(){
		return prevNames;
	}
	
	public void addVariant(GeminiVariant gv){
		_variants.add(gv);
	}
	
	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.vcf.Gene#hasVariant()
	 */
	@Override
	public boolean hasVariant() {
		return (_variants.size()>0);
	}

	@Override
	public int numberOfVariants() {
		return _variants.size();
	}

	@Override
	public boolean hasFilteredVariant() {
		return (_filteredVariants.size()>0);
	}

	@Override
	public int numberOfFilteredVariants() {
		return _filteredVariants.size();
	}

	@Override
	public void setFilters(List<Filter> filters) {
		_filters = filters;
		_filteredVariants = new ArrayList<GeminiVariant>(_variants);
		_filteredCNVs = null;
		
		for(Filter vf:filters){
//			System.out.println("filter: "+vf);
//			System.out.println("before filtering: "+_filteredVariants.size());
			_filteredVariants.removeIf(v -> !vf.passFilter(v));
//			System.out.println("after filtering: "+_filteredVariants.size());
		}
	}

	@Override
	public List<Filter> getFilters() {
		return _filters;
	}

	@Override
	public void clearFilters() {
		_filters.clear();
		_filteredVariants = new ArrayList<GeminiVariant>(_variants);
	}

	@Override
	public boolean hasFilters() {
		return _filters.size()>0;
	}

	@Override
	public List<? extends Variant> getVariants() {
		return _variants;
	}

	@Override
	public List<? extends Variant> getFilteredVariants() {
		return _filteredVariants;
	}

	@Override
	public void sortBy(String s) {
		Collections.sort(_filteredVariants, new Comparator<GeminiVariant>() {
			@Override
			public int compare(GeminiVariant gsv1, GeminiVariant gsv2) {
				Object o1 = gsv1.getObject(s);
				Object o2 = gsv2.getObject(s);
				if( o1 == null && o2 == null )
					return 0;
				if( o1 == null )
					return 1;
				if( o2 == null )
					return -1;
				if( o1 instanceof Double )
					return Maths.compare( (Double)o2, (Double)o1);
				if( o1 instanceof Float )
					return Maths.compare( (Float)o2, (Float)o1);
				if( o1 instanceof Integer )
					return Maths.compare( (Integer)o2, (Integer)o1);
				if( o1 instanceof String )
					return ((String)o2).compareTo(((String)o1));
				return 0;
			}
		});
	}

	@Override
	public String highestImpact(Patient p) {
		int mostDangerousGSVcarried = Integer.MAX_VALUE;
		String impact = "No impact";
		
		for(Variant gsv:_filteredVariants){
			if( GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(p)) ){
				mostDangerousGSVcarried = Math.min(mostDangerousGSVcarried, gsv.impactPriority());	
				if( gsv.impactPriority() <= mostDangerousGSVcarried ){
					mostDangerousGSVcarried = gsv.impactPriority();
					impact = gsv.impact();
				}
			}
		}
		return impact;
	}
	
	@Override
	public Variant highestImpactVariant(Patient p) {
		int mostDangerousGSVcarried = Integer.MAX_VALUE;
		Variant v = null;
		for(Variant gsv:_filteredVariants){
			if( GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(p)) ){
				mostDangerousGSVcarried = Math.min(mostDangerousGSVcarried, gsv.impactPriority());	
				if( gsv.impactPriority() <= mostDangerousGSVcarried ){
					mostDangerousGSVcarried = gsv.impactPriority();
					v = gsv;
				}
			}
		}
		return v;
	};

	@Override
	public boolean hasDeNovoMutations(Patient patient, Patient father, Patient mother) {
		boolean denovo = false;
		
		for(Variant gsv:_filteredVariants){
			if( gsv.isCNV()){
				denovo = GenotypeUtils.isDeNovo(gsv.getPatientGenotype(patient), gsv.getPatientGenotypeEnlarged4CNVsFiltered(father), gsv.getPatientGenotypeEnlarged4CNVsFiltered(mother));
				if(denovo)
					return denovo;
			}else{
				denovo = GenotypeUtils.isDeNovo(gsv.getPatientGenotype(patient), gsv.getPatientGenotype(father), gsv.getPatientGenotype(mother));
				if(denovo)
					return denovo;
			}
		}
		
		return denovo;
	}

	@Override
	public String getName() {
		return gene;
	}

	@Override
	public boolean carryAVariant(Patient patient) {
		return _variants.stream().anyMatch(gsv -> {return GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(patient)); });
	}

	@Override
	public boolean carryAFilteredVariant(Patient patient) {
		return _filteredVariants.stream().anyMatch(gsv -> {return GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(patient)); });
	}
	
	@Override
	public boolean carryAFilteredVariant(Patient patientA, Patient patientB, List<GenotypeFilter> filtersQuality, int filteringMode) {
		if(filteringMode == 1)
			return _filteredVariants.stream().anyMatch(gsv -> {return ( checkQuality(filtersQuality, gsv, patientA) && GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(patientA))) && ( !checkQuality(filtersQuality, gsv, patientB) || !GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(patientB))); });
		if(filteringMode == 2)
			return _filteredVariants.stream().anyMatch(gsv -> {return ( !checkQuality(filtersQuality, gsv, patientA) || !GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(patientA))) && ( checkQuality(filtersQuality, gsv, patientB) && GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(patientB))); });
		return _filteredVariants.stream().anyMatch(gsv -> {return ( checkQuality(filtersQuality, gsv, patientA) && GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(patientA))) || ( checkQuality(filtersQuality, gsv, patientB) && GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(patientB))); });
	}

	/**
	 * @return the chrom
	 */
	public String getChrom() {
		if( chrom != null && !chrom.equals("") )
			return chrom;
		else{
			if( _variants != null && _variants.size()>0 )
				return _variants.get(0).getChromosome().substring(3, _variants.get(0).getChromosome().length());
		}
		return "";
	}
	
	public String getCNVid(Patient patient){
		for(Variant gsv:_filteredVariants){
			if( gsv.isCNV() && GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(patient)) ){
				return gsv.getChromosome()+":"+gsv.getPosition()+"-"+(gsv.getPosition()+Integer.parseInt(gsv.getObject("sv_length").toString()));
			}                                                  
		}
		return "";		
	}

	/**
	 * @param chrom the chrom to set
	 */
	public void setChrom(String chrom) {
		this.chrom = chrom;
	}

	/**
	 * @return the gene
	 */
	public String getGene() {
		return gene;
	}

	/**
	 * @param gene the gene to set
	 */
	public void setGene(String gene) {
		this.gene = gene;
	}

	/**
	 * @return the ensemble_gene_id
	 */
	public String getEnsemble_gene_id() {
		return ensemble_gene_id;
	}

	/**
	 * @param ensemble_gene_id the ensemble_gene_id to set
	 */
	public void setEnsemble_gene_id(String ensemble_gene_id) {
		this.ensemble_gene_id = ensemble_gene_id;
	}

	/**
	 * @return the transcript
	 */
	public String getTranscript() {
		return transcript;
	}

	/**
	 * @param transcript the transcript to set
	 */
	public void setTranscript(String transcript) {
		this.transcript = transcript;
	}

	/**
	 * @return the biotype
	 */
	public String getBiotype() {
		return biotype;
	}

	/**
	 * @param biotype the biotype to set
	 */
	public void setBiotype(String biotype) {
		this.biotype = biotype;
	}

	/**
	 * @return the transcript_status
	 */
	public String getTranscript_status() {
		return transcript_status;
	}

	/**
	 * @param transcript_status the transcript_status to set
	 */
	public void setTranscript_status(String transcript_status) {
		this.transcript_status = transcript_status;
	}

	/**
	 * @return the cds_length
	 */
	public String getCds_length() {
		return cds_length;
	}

	/**
	 * @param cds_length the cds_length to set
	 */
	public void setCds_length(String cds_length) {
		this.cds_length = cds_length;
	}

	/**
	 * @return the protein_length
	 */
	public String getProtein_length() {
		return protein_length;
	}

	/**
	 * @param protein_length the protein_length to set
	 */
	public void setProtein_length(String protein_length) {
		this.protein_length = protein_length;
	}

	/**
	 * @return the transcript_start
	 */
	public String getTranscript_start() {
		return transcript_start;
	}

	/**
	 * @param transcript_start the transcript_start to set
	 */
	public void setTranscript_start(String transcript_start) {
		this.transcript_start = transcript_start;
	}

	/**
	 * @return the transcript_end
	 */
	public String getTranscript_end() {
		return transcript_end;
	}

	/**
	 * @param transcript_end the transcript_end to set
	 */
	public void setTranscript_end(String transcript_end) {
		this.transcript_end = transcript_end;
	}

	/**
	 * @return the strand
	 */
	public String getStrand() {
		return strand;
	}

	/**
	 * @param strand the strand to set
	 */
	public void setStrand(String strand) {
		this.strand = strand;
	}

	/**
	 * @return the synonym
	 */
	public String getSynonym() {
		return synonym;
	}

	/**
	 * @param synonym the synonym to set
	 */
	public void setSynonym(String synonym) {
		this.synonym = synonym;
	}

	/**
	 * @return the rvis_pct
	 */
	public float getRvis_pct() {
		return rvis_pct;
	}

	/**
	 * @param rvis_pct the rvis_pct to set
	 */
	public void setRvis_pct(float rvis_pct) {
		this.rvis_pct = rvis_pct;
	}

	/**
	 * @return the mam_phenotype_id
	 */
	public String getMam_phenotype_id() {
		return mam_phenotype_id;
	}

	/**
	 * @param mam_phenotype_id the mam_phenotype_id to set
	 */
	public void setMam_phenotype_id(String mam_phenotype_id) {
		this.mam_phenotype_id = mam_phenotype_id;
	}

	/**
	 * @return the hgnc_id
	 */
	public String getHgnc_id() {
		return hgnc_id;
	}

	/**
	 * @param hgnc_id the hgnc_id to set
	 */
	public void setHgnc_id(String hgnc_id) {
		this.hgnc_id = hgnc_id;
	}

	/**
	 * @return the entrez_id
	 */
	public String getEntrez_id() {
		return entrez_id;
	}

	/**
	 * @param entrez_id the entrez_id to set
	 */
	public void setEntrez_id(String entrez_id) {
		this.entrez_id = entrez_id;
	}

	@Override
	public Double maxcadd(Patient p) {
		Double cadd = 0d;
		for(Variant gsv:_filteredVariants){
			if( GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(p)) ){
				cadd = Maths.max( cadd, gsv.cadd_scaled() );
			}
		}
		return cadd;
	}
	
	@Override
	public Double maxcadd(Patient p, List<GenotypeFilter> filters) {
		Double cadd = 0d;
		for(Variant gsv:_filteredVariants){
			if( checkQuality(filters, gsv, p) && GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(p)) ){
				cadd = Maths.max( cadd, gsv.cadd_scaled() );
			}
		}
		return cadd;
	}
	
	/**
	 * get the maximum cadd for the specified type of inheritance
	 * @param p patient
	 * @param f father
	 * @param m mother
	 * @param inheritance 0 for none, 1 for the father, 2 for the mother, 3 for both, -1 for unknown
	 * @return
	 */
	private Double maxCadd(Patient p, Patient f, Patient m, int inheritance){
		Double cadd = -Double.MAX_VALUE;
		for(Variant gsv:_filteredVariants){
			if( GenotypeUtils.inheritance(gsv.getPatientGenotype(p), p.getPatientSex(), gsv.getChromosome(), gsv.getPatientGenotype(f), gsv.getPatientGenotype(m))==inheritance ){
				if( GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(p)) ){
					cadd = Maths.max( cadd, gsv.cadd_scaled() );
				}
			}
		}
		return cadd;
	}
	
	@Override
	public Double minMAF(Patient p, Patient f, Patient m) {
		Double maf = 1.0d;
		for(Variant gsv:_filteredVariants){
			if( GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(p)) ){
					maf = Maths.min( maf, gsv.maxAlleleFrequency() );
			}
		}
		return maf;
	}

	@Override
	public Double maxCaddFather(Patient p, Patient f, Patient m) {
		return maxCadd(p,f,m,1);
	}

	@Override
	public Double maxCaddMother(Patient p, Patient f, Patient m) {
		return maxCadd(p,f,m,2);
	}

	@Override
	public Double maxCaddDeNovo(Patient p, Patient f, Patient m) {
		Double cadd = -Double.MAX_VALUE;
		for(Variant gsv:_filteredVariants){
			if( GenotypeUtils.isDeNovo(gsv.getPatientGenotype(p), gsv.getPatientGenotype(f), gsv.getPatientGenotype(m)) ){
				if( GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(p)) ){
					cadd = Maths.max(cadd, gsv.cadd_scaled() );
				}
			}
		}
		return cadd;
	}

	@Override
	public Double maxCaddHomalt(Patient p, Patient f, Patient m) {
		return maxCadd(p,f,m,3);
	}

	@Override
	public Double maxCaddUnknown(Patient p, Patient f, Patient m) {
		return maxCadd(p,f,m,-1);
	}

	@Override
	public String getDetails() {
		String s = "chromosome: "+getChrom()+"\n";
		s += "synonyms: "+( getSynonym()==null ? "" : getSynonym() )+"\n";
		s += "name: "+getDescription();
		for(GeneAnnotation<?> ga:getAnnotations())
			s += "\n"+ga.toString();
		return s;
	}

	@Override
	public List<? extends Variant> getFilteredVariants(Patient p) {
		List<GeminiVariant> l = new ArrayList<GeminiVariant>();
		for( GeminiVariant gv : _filteredVariants ){
			if( GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
				l.add(gv);
			}
		}
		return l;
	}
	
	@Override
	public List<? extends Variant> getFilteredVariants(Patient a, Patient b, List<GenotypeFilter> quality, int filteringMode){
		List<GeminiVariant> l = new ArrayList<GeminiVariant>();
		for( GeminiVariant gv : _filteredVariants ){
			if( filteringMode == 1)
				if( (GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(a)) && checkQuality(quality, gv, a)) && (!GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(b)) || !checkQuality(quality, gv, b)) ){
					l.add(gv);
				}
			if( filteringMode == 2)
				if( (!GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(a)) || !checkQuality(quality, gv, a)) && (GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(b)) && checkQuality(quality, gv, b)) ){
					l.add(gv);
				}
			if( filteringMode == 3)	
				if( (GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(a)) && checkQuality(quality, gv, a)) || (GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(b)) && checkQuality(quality, gv, b)) ){
					l.add(gv);
				}
		}
		return l;
	}

	private static boolean checkQuality(List<GenotypeFilter> filters, Variant v, Patient p){
		for(GenotypeFilter f:filters){
			f.setPatient(p);
			if( !f.passFilter(v) )
				return false;
		}
		return true;
	}
	
	@Override
	public List<? extends Variant> getFilteredVariants(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected) {
		if( allAffected ){
			// VARIANTS IN ALL AFFECTED AND IN NO UNAFFECTED
			List<GeminiVariant> l = new ArrayList<GeminiVariant>();
			for( GeminiVariant gv : _filteredVariants ){
				boolean keep = true;
				boolean foundInAffected = false;
				boolean definedInUnaffected = false;
				for(Patient p:affected){
					if( checkQuality(quality, gv, p) && !GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
						keep = false;
						break;
					}
					if( checkQuality(quality, gv, p) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
						foundInAffected = true;
					}
				}
				if( keep && foundInAffected ){
					for(Patient p:unaffected){
						if( checkQuality(quality, gv, p) && !GenotypeUtils.isUndefined( gv.getPatientGenotype(p) ) ){
							definedInUnaffected = true;
						}
						if( checkQuality(quality, gv, p) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
							keep = false;
							break;
						}
					}
				}
				if( keep && foundInAffected && (definedInUnaffected || unaffected.isEmpty() ) )
					l.add(gv);
			}
			return l;
		}else{
			// VARIANTS IN AT LEAST ONE AFFECTED AND IN NO UNAFFECTED	
			List<GeminiVariant> l = new ArrayList<GeminiVariant>();
			for( GeminiVariant gv : _filteredVariants ){
				boolean keep = true;
				boolean foundInAffected = false;
				boolean definedInUnaffected = false;
				for(Patient p:affected){
					if( checkQuality(quality, gv, p) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
						foundInAffected = true;
						break;
					}
				}
				for(Patient p:unaffected){
					if( checkQuality(quality, gv, p) && !GenotypeUtils.isUndefined( gv.getPatientGenotype(p) ) ){
						definedInUnaffected = true;
					}
					if( checkQuality(quality, gv, p) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
						keep = false;
						break;
					}
				}
				
				if( keep && foundInAffected && (definedInUnaffected || unaffected.isEmpty() ) )
					l.add(gv);
			}
			return l;
		}
	}

	@Override
	public boolean carryAFilteredVariant(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected) {
		if( allAffected ){
			// VARIANTS IN ALL AFFECTED AND IN NO UNAFFECTED
			for( GeminiVariant gv : _filteredVariants ){
				boolean keep = true;
				boolean foundInAffected = false;
				boolean definedInUnaffected = false;
				for(Patient p:affected){
					if( checkQuality(quality, gv, p) && !GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
						keep = false;
						break;
					}
					if( checkQuality(quality, gv, p) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
						foundInAffected = true;
					}
				}
				if( keep && foundInAffected ){
					for(Patient p:unaffected){
						if( checkQuality(quality, gv, p) && !GenotypeUtils.isUndefined( gv.getPatientGenotype(p) ) ){
							definedInUnaffected = true;
						}
						if( checkQuality(quality, gv, p) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
							keep = false;
							break;
						}
					}
				}
				if( keep && foundInAffected && (definedInUnaffected || unaffected.isEmpty() ) )
					return true;
			}
			return false;
		}else{
			// VARIANTS IN AT LEAST ONE AFFECTED AND IN NO UNAFFECTED
			for( GeminiVariant gv : _filteredVariants ){
				boolean keep = true;
				boolean foundInAffected = false;
				boolean definedInUnaffected = false;
				for(Patient p:affected){
					if( checkQuality(quality, gv, p) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
						foundInAffected = true;
						break;
					}
				}
				for(Patient p:unaffected){
					if( checkQuality(quality, gv, p) && !GenotypeUtils.isUndefined( gv.getPatientGenotype(p) ) ){
						definedInUnaffected = true;
					}
					if( checkQuality(quality, gv, p) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) ){
						keep = false;
						break;
					}
				}
				if( keep && foundInAffected && (definedInUnaffected || unaffected.isEmpty() ) )
					return true;
			}
			return false;
		}
	}

//	@Override
//	public String highestImpact(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected, boolean recessive, boolean allRecessiveAffected) {
//		int mostDangerousGSVcarried = Integer.MAX_VALUE;
//		String impact = "No impact";
//		
//		for( Variant gsv:getFilteredVariants(affected, unaffected, quality, allAffected) ){
//			mostDangerousGSVcarried = Math.min(mostDangerousGSVcarried, gsv.impactPriority());	
//			if( gsv.impactPriority() <= mostDangerousGSVcarried ){
//				mostDangerousGSVcarried = gsv.impactPriority();
//				impact = gsv.impact();
//			}
//		}
//		
//		if(recessive){
//			for( Variant gsv:getFilteredRecessiveVariants(affected, unaffected, quality, allRecessiveAffected) ){
//				mostDangerousGSVcarried = Math.min(mostDangerousGSVcarried, gsv.impactPriority());	
//				if( gsv.impactPriority() <= mostDangerousGSVcarried ){
//					mostDangerousGSVcarried = gsv.impactPriority();
//					impact = gsv.impact();
//				}
//			}
//		}
//		
//		return impact;
//	}

//	@Override
//	public Variant highestImpactVariant(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected, boolean recessive, boolean allRecessiveAffected, boolean compound) {
//		int mostDangerousGSVcarried = Integer.MAX_VALUE;
//		Variant v = null;
//		
//		for( Variant gsv:getFilteredVariants(affected, unaffected, quality, allAffected) ){
//			mostDangerousGSVcarried = Math.min(mostDangerousGSVcarried, gsv.impactPriority());	
//			if( gsv.impactPriority() <= mostDangerousGSVcarried ){
//				mostDangerousGSVcarried = gsv.impactPriority();
//				v = gsv;
//			}
//		}
//		
//		if(recessive){
//			for( Variant gsv:getFilteredRecessiveVariants(affected, unaffected, quality, allRecessiveAffected) ){
//				mostDangerousGSVcarried = Math.min(mostDangerousGSVcarried, gsv.impactPriority());	
//				if( gsv.impactPriority() <= mostDangerousGSVcarried ){
//					mostDangerousGSVcarried = gsv.impactPriority();
//					v = gsv;
//				}
//			}
//		}
//		
//		if(compound){
//			for( Variant gsv:getCompoundHomozygousVariants(affected, unaffected, quality, allAffected) ){
//				mostDangerousGSVcarried = Math.min(mostDangerousGSVcarried, gsv.impactPriority());	
//				if( gsv.impactPriority() <= mostDangerousGSVcarried ){
//					mostDangerousGSVcarried = gsv.impactPriority();
//					v = gsv;
//				}
//			}
//		}
//		
//		return v;
//	}

	@Override
	public List<? extends Variant> getFilteredRecessiveVariants(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected) {
		if( allAffected ){
			// VARIANTS HOMALT IN ALL AFFECTED AND HET IN ALL UNAFFECTED
			List<GeminiVariant> l = new ArrayList<GeminiVariant>();
			for( GeminiVariant gv : _filteredVariants ){
				boolean keep = true;
				boolean foundInAffected = false;
				boolean definedInUnaffected = false;
				for(Patient p:affected){
					if( checkQuality(quality, gv, p) && !GenotypeUtils.isHomAlt(gv.getPatientGenotype(p)) ){
						keep = false;
						break;
					}
					if( checkQuality(quality, gv, p) && GenotypeUtils.isHomAlt(gv.getPatientGenotype(p)) )
						foundInAffected = true;
				}
				if( keep && foundInAffected ){
					for(Patient p:unaffected){
						if( checkQuality(quality, gv, p) && !GenotypeUtils.isUndefined( gv.getPatientGenotype(p) ) ){
							definedInUnaffected = true;
						}
						if( checkQuality(quality, gv, p) && GenotypeUtils.isHomAlt(gv.getPatientGenotype(p)) ){
							keep = false;
							break;
						}
					}
				}
				
				if( keep && foundInAffected && (definedInUnaffected || unaffected.isEmpty() ) )
					l.add(gv);
			}
			return l;
		}else{
			// VARIANTS HOMALT IN AT LEAST ONE AFFECTED AND HET IN ALL UNAFFECTED
			List<GeminiVariant> l = new ArrayList<GeminiVariant>();
			for( GeminiVariant gv : _filteredVariants ){
				boolean keep = true;
				boolean foundInAffected = false;
				boolean definedInUnaffected = false;
				for(Patient p:affected){
					if( checkQuality(quality, gv, p) && GenotypeUtils.isHomAlt(gv.getPatientGenotype(p)) ){
						foundInAffected = true;
						break;
					}
				}
				for(Patient p:unaffected){
					if( checkQuality(quality, gv, p) && !GenotypeUtils.isUndefined( gv.getPatientGenotype(p) ) ){
						definedInUnaffected = true;
					}
					if( checkQuality(quality, gv, p) && GenotypeUtils.isHomAlt( gv.getPatientGenotype(p) ) ){
						keep = false;
						break;
					}
				}
				if( keep && foundInAffected && (definedInUnaffected || unaffected.isEmpty() ) )
					l.add(gv);
			}
			return l;
		}
	}

	
	@Override
	public boolean carryAFilteredRecessiveVariant(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected) {
		if( allAffected ){
			// VARIANTS HOMALT IN ALL AFFECTED AND HET IN ALL UNAFFECTED
			for( GeminiVariant gv : _filteredVariants ){
				boolean keep = true;
				boolean foundInAffected = false;
				boolean definedInUnaffected = false;
				for(Patient p:affected){
					if( checkQuality(quality, gv, p) && !GenotypeUtils.isHomAlt(gv.getPatientGenotype(p)) ){
						keep = false;
						break;
					}
					if( checkQuality(quality, gv, p) && GenotypeUtils.isHomAlt(gv.getPatientGenotype(p)) ){
						foundInAffected = true;
					}
				}
				if( keep && foundInAffected ){
					for(Patient p:unaffected){
						if( checkQuality(quality, gv, p) && !GenotypeUtils.isUndefined( gv.getPatientGenotype(p) ) ){
							definedInUnaffected = true;
						}
						if( checkQuality(quality, gv, p) && GenotypeUtils.isHomAlt(gv.getPatientGenotype(p)) ){
							keep = false;
							break;
						}
					}
				}
				
				if( keep && foundInAffected && (definedInUnaffected || unaffected.isEmpty() ) )
					return true;
			}
			return false;
		}else{
			// VARIANTS HOMALT IN AT LEAST ONE AFFECTED AND HET IN ALL UNAFFECTED
			for( GeminiVariant gv : _filteredVariants ){
				boolean keep = true;
				boolean foundInAffected = false;
				boolean definedInUnaffected = false;
				for(Patient p:affected){
					if( checkQuality(quality, gv, p) && GenotypeUtils.isHomAlt(gv.getPatientGenotype(p)) ){
						foundInAffected = true;
						break;
					}
				}
				for(Patient p:unaffected){
					if( checkQuality(quality, gv, p) && !GenotypeUtils.isUndefined( gv.getPatientGenotype(p) ) ){
						definedInUnaffected = true;
					}
					if( checkQuality(quality, gv, p) && GenotypeUtils.isHomAlt(gv.getPatientGenotype(p)) ){
						keep = false;
						break;
					}
				}
				if( keep && foundInAffected && (definedInUnaffected || unaffected.isEmpty() ) )
					return true;
			}
			return false;
		}
	}

	public List<? extends Variant> getCompoundHomozygousVariants(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected){
		Set<GeminiVariant> vars = new HashSet<GeminiVariant>();
		for(Patient p:affected){
			Set<GeminiVariant> varFat = new HashSet<GeminiVariant>();
			Set<GeminiVariant> varMot = new HashSet<GeminiVariant>();
			for( GeminiVariant gv : _filteredVariants ){
				if( checkQuality(quality, gv, p) && p.getFather() != null && p.getMother() != null ){
					if( checkQuality(quality, gv, p.getFather()) && checkQuality(quality, gv, p.getMother()) ){
						if( GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p.getFather())) && !GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p.getMother())) ){
							varFat.add(gv);
						}
						if( GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) && !GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p.getFather())) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p.getMother())) ){
							varMot.add(gv);
						}
					}
				}
			}
			if(allAffected){
				vars.addAll(varFat);
				vars.addAll(varMot);
				if( varFat.isEmpty() || varMot.isEmpty() )
					return new ArrayList<GeminiVariant>();
			}else{
				if( !varFat.isEmpty() && !varMot.isEmpty() ){
					vars.addAll(varFat);
					vars.addAll(varMot);
				}
			}
		}
		return new ArrayList<GeminiVariant>(vars);
	}
	
	public boolean carryCompoundHomozygousVariants(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected){
		boolean ok = false;
		for(Patient p:affected){
			Set<GeminiVariant> varFat = new HashSet<GeminiVariant>();
			Set<GeminiVariant> varMot = new HashSet<GeminiVariant>();
			for( GeminiVariant gv : _filteredVariants ){
				if( checkQuality(quality, gv, p) && p.getFather() != null && p.getMother() != null ){
					if( checkQuality(quality, gv, p.getFather()) && checkQuality(quality, gv, p.getMother()) ){
						if( GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p.getFather())) && !GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p.getMother())) ){
							varFat.add(gv);
						}
						if( GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p)) && !GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p.getFather())) && GenotypeUtils.isCarryingVariant(gv.getPatientGenotype(p.getMother())) ){
							varMot.add(gv);
						}
					}
				}
			}
			if(allAffected){
				ok = true;
				if( varFat.isEmpty() || varMot.isEmpty() )
					return false;
			}else{
				if( !varFat.isEmpty() && !varMot.isEmpty() )
					ok = true;
			}
		}
		return ok;
	}

	@Override
	public int hasCNV(Patient patient) {
		int cnv = 0;
		for(Variant gsv:_filteredVariants){
			if( gsv.isCNV() && GenotypeUtils.isCarryingVariant(gsv.getPatientGenotype(patient)) ){
				if( gsv.getObject("sub_type").equals("DEL") )
					return -1;
				cnv = 1;
			}                                                  
		}
		return cnv;
	}

	@Override
	public void addAnnotation(GeneAnnotation<?> a) {
		_annotations.add(a);
	}

	@Override
	public List<GeneAnnotation<?>> getAnnotations() {
		return _annotations;
	}
	
}
