/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.gemini;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.event.EventListenerList;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.BeanMapHandler;
import org.apache.commons.dbutils.handlers.MapListHandler;
import org.sqlite.SQLiteConfig;
import org.sqlite.SQLiteDataSource;
import org.sqlite.SQLiteJDBCLoader;
import org.sqlite.SQLiteConfig.TempStore;

import fr.pasteur.sysbio.Gravity.API.Filter;
import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.VariantDatabase;
import fr.pasteur.sysbio.Gravity.API.events.DatabaseListener;

/**
 * @author Freddy Cliquet
 * @version 1.0
 *
 */
public class LowFootprintGeminiDatabase implements VariantDatabase {

	private String geminiDatabase;
	private SQLiteDataSource dataSourceGemini;
	private List<Patient> _patients;
	private Map<String,HgncGene> _genes;
	private List<GeminiVariant> _variants;
	
	protected EventListenerList listenerList = new EventListenerList();
	
	public LowFootprintGeminiDatabase(String geminiDatabase){
		this.geminiDatabase = geminiDatabase;
		_variants = new ArrayList<GeminiVariant>();
		
		try {
			SQLiteJDBCLoader.initialize();
			SQLiteConfig config = new SQLiteConfig();
			config.setReadOnly(true);
			config.setCacheSize(250000);
			config.setPageSize(4096);
			config.setTempStore(TempStore.MEMORY);
			dataSourceGemini = new SQLiteDataSource();
			dataSourceGemini.setUrl("jdbc:sqlite:"+this.geminiDatabase);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void loadPatients(){
		List<Patient> patients = new ArrayList<Patient>();
		BeanListHandler<Patient> h = new BeanListHandler<Patient>(Patient.class);
		QueryRunner run = new QueryRunner(dataSourceGemini);
		
		try {
//			patients = run.query("SELECT name, family_id as family, paternal_id as father, maternal_id as mother, sex, phenotype FROM samples", h );
			patients = run.query("SELECT * FROM samples", h );
		} catch (SQLException e) {
			e.printStackTrace();
		}
		_patients = patients;
	}
	
	private void loadGenes(){
		Map<String, HgncGene> genes = new HashMap<String,HgncGene>();
		BeanMapHandler<String,HgncGene> h = new BeanMapHandler<String,HgncGene>(HgncGene.class, "gene");
		QueryRunner run = new QueryRunner(dataSourceGemini);
		
		try {
			genes = run.query("select chrom, gene, ensembl_gene_id, hgnc_id, entrez_id, synonym, rvis_pct, mam_phenotype_id "
						+"from gene_detailed "
						+"where hgnc_id is not null "
						+"group by chrom, gene, rvis_pct, ensembl_gene_id, hgnc_id, entrez_id, synonym, mam_phenotype_id;", h );
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		_genes = genes;
	}
	
	private boolean loadVariants(int start, int amount){
		List<GeminiVariant> variants;
		BeanListHandler<GeminiVariant> h = new BeanListHandler<GeminiVariant>(GeminiVariant.class);
		QueryRunner run = new QueryRunner(dataSourceGemini);
		boolean moreToLoad = false;
		try {
			variants = run.query(
					"SELECT gts, chrom, start, end, variant_id, anno_id, ref, alt, qual, filter, type, sub_type, call_rate, rs_ids, sv_cipos_start_left, sv_cipos_end_left, sv_cipos_start_right, sv_cipos_end_right, "
					+"sv_length, sv_is_precise, sv_tool, sv_evidence_type, sv_event_id, sv_mate_id, sv_strand, in_omim, clinvar_sig, clinvar_disease_name, clinvar_dbsource, clinvar_dbsource_id, clinvar_origin, "
					+"clinvar_dsdb, clinvar_dsdbid, clinvar_disease_acc, clinvar_in_locus_spec_db, clinvar_on_diag_assay, clinvar_causal_allele, clinvar_gene_phenotype, geno2mp_hpo_ct, pfam_domain, cyto_band, "
					+"rmsk, in_cpg_island, in_segdup, is_conserved, gerp_bp_score, gerp_element_pval, num_hom_ref, num_het, num_hom_alt, num_unknown, aaf, hwe, inbreeding_coeff, pi, recomb_rate, gene, transcript, "
					+"is_exonic, is_coding, is_splicing, is_lof, exon, codon_change, aa_change, aa_length, biotype, impact, impact_severity, polyphen_pred, polyphen_score, sift_pred, sift_score, anc_allele, "
					+"rms_bq, cigar, depth, strand_bias, rms_map_qual, in_hom_run, num_mapq_zero, num_alleles, num_reads_w_dels, haplotype_score, qual_depth, allele_count, allele_bal, "
					+"somatic_score, in_esp, aaf_esp_ea, aaf_esp_aa, aaf_esp_all, exome_chip bool, aaf_1kg_amr, aaf_1kg_eas, aaf_1kg_sas, aaf_1kg_afr, aaf_1kg_eur, aaf_1kg_all, grc, gms_illumina, "
					+"gms_solid, gms_iontorrent, in_cse, encode_tfbs, encode_dnaseI_cell_count, encode_dnaseI_cell_list, encode_consensus_gm12878, encode_consensus_h1hesc, encode_consensus_helas3, "
					+"encode_consensus_hepg2, encode_consensus_huvec, encode_consensus_k562, vista_enhancers, cosmic_ids, cadd_raw, cadd_scaled, fitcons, aaf_exac_all, aaf_adj_exac_all, aaf_adj_exac_afr, "
					+"aaf_adj_exac_amr, aaf_adj_exac_eas, aaf_adj_exac_fin, aaf_adj_exac_nfe, aaf_adj_exac_oth, aaf_adj_exac_sas, exac_num_het, exac_num_hom_alt, exac_num_chroms, max_aaf_all "
					+"FROM variants "
					+"WHERE is_exonic='1' OR is_coding='1' OR is_splicing='1' LIMIT ?, ?;", h, start, amount );
			moreToLoad = (variants.size()==amount);
			_variants.addAll(variants);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return moreToLoad;
	}
	
	private int countVariants(){
		MapListHandler h = new MapListHandler();
		QueryRunner run = new QueryRunner(dataSourceGemini);
		
		try {
			List<Map<String, Object>> result = run.query("select count(*) as count from variants WHERE is_exonic='1' OR is_coding='1' OR is_splicing='1';", h);
			return (Integer)result.get(0).get("count");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
//	private boolean loadGenotypes(int start, int amount){
//		MapListHandler h = new MapListHandler();
//		QueryRunner run = new QueryRunner(dataSourceGemini);
//		boolean moreToLoad = false;
//		
//		try {
//			List<Map<String, Object>> result = run.query("select * "
//						+"from genotypes_bis ORDER BY variant_id ASC LIMIT ?, ?;", h, start, amount );
//			int i=start;
//			moreToLoad = (result.size()==amount);
//			for(Map<String, Object> m:result){
//				if( _variants.get(i).getVariant_id()==(Integer)m.get("variant_id") )
//					_variants.get(i++).setGenotypes(m, getPatients());
//				else
//					System.out.println("SHIFT PROBLEM");
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//		
//		//System.out.println("genotypes count "+genotypes.size());
//		return moreToLoad;
//	}
	
	/**
	 * affect all the variants to the corresponding gene.
	 * splicing variant genes are not in the database, so not linked.
	 */
	private void affectVariant2Genes(){
		for(GeminiVariant gv:_variants){
			if( _genes.get(gv.getGene()) != null ){
				_genes.get(gv.getGene()).addVariant(gv);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#load()
	 */
	@Override
	public void load() {
		long startTime = System.currentTimeMillis();
		fireDatabaseProgressChange("Loading patients (step 1/5)", 0, 0);
		loadPatients();
		fireDatabaseProgressChange("Patients loaded (step 1/5)", 0, 0);
		
		fireDatabaseProgressChange("Loading genes (step 2/5)", 0, 0);
		loadGenes();
		fireDatabaseProgressChange("Genes loaded (step 2/5)", 0, 0);
		
//		int nb_variants = countVariants();		
//		//load variants by batches
//		int step = 2500;
//		int start = 0;
//		while( loadVariants(start, step) ){
//			start+=step;
//			fireDatabaseProgressChange("Loading variants (step 3/5)", start, nb_variants);
//		}
//		fireDatabaseProgressChange("Variants loaded (step 3/5)", 0, 0);
//		
//		
//		Collections.sort(_variants, new Comparator<GeminiVariant>(){
//			@Override
//			public int compare(GeminiVariant o1, GeminiVariant o2) {
//				return Integer.compare(o1.getVariant_id(), o2.getVariant_id());
//			}});
//		
//		fireDatabaseProgressChange("Variants sorted (step 3/5)", 0, 0);
//		
//		int step2 = 2500;
//		int start2 = 0;		
//		while( loadGenotypes(start2, step2) ){
//			start2+=step2;
//			fireDatabaseProgressChange("Loading genotypes (step 4/5)", start2, nb_variants);
//		}
//		fireDatabaseProgressChange("Genotypes loaded (step 4/5)", 0, 0);
//		
//		fireDatabaseProgressChange("Affecting variants to genes (step 5/5)", 0, 0);
//		affectVariant2Genes();
//		fireDatabaseProgressChange("Variants affected to genes (step 5/5)", 0, 0);
		
		double execTime = ((System.currentTimeMillis()-startTime)/1000f);
		System.out.println("execution time: "+ execTime +" seconds" );
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getPatients()
	 */
	@Override
	public List<Patient> getPatients() {
		return _patients;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getPatientsWithParents()
	 */
	@Override
	public List<Patient> getPatientsWithParents() {
		List<Patient> l = new ArrayList<Patient>();
		for(Patient p:_patients){
			if( getFather(p)!=null && getMother(p)!=null )
				l.add(p);
		}
		return l;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getFamilies()
	 */
	@Override
	public Set<String> getFamilies() {
		Set<String> s = new HashSet<String>();
		for(Patient p:_patients)
			s.add( p.getFamily() );
		return s;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getPatients(java.lang.String)
	 */
	@Override
	public List<Patient> getPatients(String family) {
		List<Patient> l = new ArrayList<Patient>();
		for(Patient p:_patients){
			if( p.getFamily().equals(family) )
				l.add(p);
		}
		return l;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#hasPedigree()
	 */
	@Override
	public boolean hasPedigree() {
		return true;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getFather(fr.pasteur.sysbio.VCFviz.API.Patient)
	 */
	@Override
	public Patient getFather(Patient p) {
		return getPatient(p.getFatherName());
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getMother(fr.pasteur.sysbio.VCFviz.API.Patient)
	 */
	@Override
	public Patient getMother(Patient p) {
		return getPatient(p.getMotherName());
	}
	
	private Patient getPatient(String name){
		for(Patient p:_patients)
			if(p.getName().equals(name))
				return p;
		return null;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getAllGenes()
	 */
	@Override
	public List<Gene> getAllGenes() {
		return new ArrayList<Gene>(_genes.values());
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getGenesCarryingVariant()
	 */
	@Override
	public List<Gene> getGenesCarryingVariant() {
		List<Gene> l = new ArrayList<Gene>();
		for(HgncGene gg:_genes.values()){
			if( gg.hasVariant() )
				l.add(gg);
		}
		return l;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getGenesCarryingVariant(java.util.List)
	 */
	@Override
	public List<Gene> getGenesCarryingVariant(List<Filter> filters) {
		List<Gene> l = getGenesCarryingVariant();
		ArrayList<Gene> toRemove = new ArrayList<Gene>();
		for(Gene g:l){
			g.setFilters(filters);
			if( !g.hasFilteredVariant() )
				toRemove.add(g);
		}
		l.removeAll(toRemove);
		return l;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getGenesCarryingVariant(fr.pasteur.sysbio.VCFviz.API.Patient)
	 */
	@Override
	public List<Gene> getGenesCarryingVariant(Patient p) {
		List<Gene> l = getGenesCarryingVariant();
		List<Gene> newL = new ArrayList<Gene>(l);
		for(Gene g:l){
			if(!g.carryAVariant(p))
				newL.remove(g);
		}
		return newL;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getGenesCarryingVariant(fr.pasteur.sysbio.VCFviz.API.Patient, java.util.List)
	 */
	@Override
	public List<Gene> getGenesCarryingVariant(Patient p, List<Filter> filters) {
		List<Gene> l = getGenesCarryingVariant(filters);
		List<Gene> newL = new ArrayList<Gene>(l);
		for(Gene g:l){
			if(!g.carryAFilteredVariant(p))
				newL.remove(g);
		}
		return newL;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getGene(java.lang.String)
	 */
	@Override
	public Gene getGene(String geneName) {
		return _genes.get(geneName);
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getGene(java.lang.String, java.util.List)
	 */
	@Override
	public Gene getGene(String geneName, List<Filter> filters) {
		Gene g = _genes.get(geneName);
		g.setFilters(filters);
		return g;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#listVariantAttributes()
	 */
	@Override
	public List<String> listVariantAttributes() {
		return _variants.get(0).listAttributes();
	}
	
	@Override
	public List<String> listFrequencyAttributes() {
		List<String> allAtt = _variants.get(0).listAttributes();
		List<String> l = new ArrayList<String>();
		l.add("maximum allele frequency");
		l.add("cohort frequency");
		for(String a:allAtt){
			if( a.startsWith("aaf_") )
				l.add(a);
		}
		return l;
	}
	
	@Override
	public List<String> listCNVsAttributes() {
		List<String> allAtt = _variants.get(0).listAttributes();
		List<String> l = new ArrayList<String>();
		for(String a:allAtt){
			if( a.startsWith("sv_") || a.startsWith("vep_cnvision_") || a.startsWith("vep_cia_") || a.startsWith("vep_dgv_") || a.startsWith("vep_#dgv_") )
				l.add(a);
		}
		return l;
	}
	
	@Override
	public List<String> listPathoAttributes() {
		List<String> allAtt = _variants.get(0).listAttributes();
		List<String> l = new ArrayList<String>();
		for(String a:allAtt){
			if( a.contains("cadd") || a.contains("dann") || a.startsWith("clinvar_") || a.startsWith("polyphen_") || a.startsWith("sift_") )
				l.add(a);
		}
		return l;
	}
	
	@Override
	public List<String> listOtherAttributes() {
		List<String> allAtt = _variants.get(0).listAttributes();
		List<String> l = new ArrayList<String>(allAtt);
		l.removeAll(listFrequencyAttributes());
		l.removeAll(listCNVsAttributes());
		l.removeAll(listPathoAttributes());
		l.remove("cohort_frequency");
		l.remove("max_aaf_all");
		return l;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#listVariantImpacts()
	 */
	@Override
	public List<String> listVariantImpacts() {
		Set<String> s = new HashSet<String>();
		for(GeminiVariant v:_variants){
			s.add(v.impact());
		}
		List<String> l = new ArrayList<String>(s);
		Collections.sort(l, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Integer.compare(GeminiVariant.impactPriority(o1), GeminiVariant.impactPriority(o2));
			}
		});
		return l;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#numberOfVariants()
	 */
	@Override
	public int numberOfVariants() {
		return _variants.size();
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getAllGenesNames()
	 */
	@Override
	public Set<String> getAllGenesNames() {
		return new HashSet<String>(_genes.keySet());
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#getImpactsOfSeverity(java.lang.String)
	 */
	@Override
	public List<String> getImpactsOfSeverity(String severity) {
		Set<String> s = new HashSet<String>();
		for(GeminiVariant v:_variants){
			if( v.impactSeverity().equals(severity) )
				s.add(v.impact());
		}
		List<String> l = new ArrayList<String>(s);
		Collections.sort(l, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return Integer.compare(GeminiVariant.impactPriority(o1), GeminiVariant.impactPriority(o2));
			}
		});
		return l;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#addDatabaseListener(fr.pasteur.sysbio.VCFviz.API.events.DatabaseListener)
	 */
	@Override
	public void addDatabaseListener(DatabaseListener databaseListener) {
		listenerList.add(DatabaseListener.class, databaseListener);
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#removeDatabaseListener(fr.pasteur.sysbio.VCFviz.API.events.DatabaseListener)
	 */
	@Override
	public void removeDatabaseListener(DatabaseListener databaseListener) {
		listenerList.remove(DatabaseListener.class, databaseListener);
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.VariantDatabase#fireDatabaseProgressChange(java.lang.String, int, int)
	 */
	@Override
	public void fireDatabaseProgressChange(String task, int current, int max) {
		Object[] listeners = listenerList.getListenerList();
	    for (int i = 0; i < listeners.length; i = i+2) {
	      if (listeners[i] == DatabaseListener.class) {
	        ((DatabaseListener) listeners[i+1]).progressChanged(task, current, max);;
	      }
	    }
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//LowFootprintGeminiDatabase gd = new LowFootprintGeminiDatabase("/Users/fcliquet/Documents/Data/GHFC/EXOMES_PARIS-204-RefSeq.db"); //EXOMES_PARIS-204-RefSeq //EXOMES_PARIS-204.VEP.ped.db
		LowFootprintGeminiDatabase gd = new LowFootprintGeminiDatabase("/Users/fcliquet/Documents/Data/GHFC/WE_NGXBio_Chr1.db"); //EXOMES_PARIS-204-RefSeq //EXOMES_PARIS-204.VEP.ped.db
		gd.load();
		
		Runtime runtime = Runtime.getRuntime();
		long memory = runtime.totalMemory() - runtime.freeMemory();
	    System.out.println("Used memory is Mb: " + memory/1024L/1024L);
	    runtime.gc();
	    System.out.println(gd.numberOfVariants());
	    memory = runtime.totalMemory() - runtime.freeMemory();
	    System.out.println("Used memory is Mb: " + memory/1024L/1024L);
	}

	@Override
	public String getInfos() {
		String s = "";
		return s;
	}

}
