/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

import java.util.HashSet;
import java.util.Set;

/**
 * Class of static functions made to process genotypes. The functions usually use the String representation of the genotypes and not the objects.
 * @author Freddy Cliquet
 * @version 1.2
 */
public class GenotypeUtils {

	/**
	 * Function returning the GQ from a full string GT representation.
	 * @param fullGT String representation of the GT (complete, not just the alleles)
	 * @return GQ of the genotype
	 */
	public static float GQ(String fullGT){
		return Float.valueOf(fullGT.split(":")[3]);
	}
	
	/**
	 * Function returning the 3 phreds likelihood values from a full string GT representation.
	 * @param fullGT String representation of the GT (complete, not just the alleles)
	 * @return the 3 phreds likelihood values as a string
	 */
	public static String Phreds(String fullGT){
		return fullGT.split(":")[4].replaceAll(",", ";");
	}
	
	/**
	 * Function returning the DP from a full string GT representation.
	 * @param fullGT String representation of the GT (complete, not just the alleles)
	 * @return DP of the genotype
	 */
	public static int DP(String fullGT){
		return Integer.valueOf(fullGT.split(":")[2]);
	}
	
	/**
	 * Function returning the ratio of the alternative allele from a full string GT representation.
	 * @param fullGT String representation of the GT (complete, not just the alleles)
	 * @return the ratio of the alternative allele (alternative allele count / coverage)
	 */
	public static float alternativeAlleleRatio(String fullGT){
		return Float.parseFloat( fullGT.split(":")[1].split(",")[1] )/Float.parseFloat( fullGT.split(":")[2] );
	}
	
	/**
	 * Function checking if the alleles are defined or not.
	 * @param gt short representation of the genotype
	 * @return true if no alleles are defined, false else
	 */
	public static boolean isUndefined(String gt){
		return ( gt.equals("./.") || gt.equals(".|.") );
	}
	
	/**
	 * Check if we have an homozygous reference allele, meaning 0/0. Phasing does not matters.
	 * @param gt genotype
	 * @return true if homozygous reference allele (0/0)
	 */
	public static boolean isHomRef(String gt){
		if( gt==null )
			return false;
		return ( gt.equals("0/0") || gt.equals("0|0") );
	}
	
	/**
	 * Check if we have an homozygous variant allele, meaning 1/1. Phasing does not matters.
	 * @param gt genotype
	 * @return true if homozygous variant allele (1/1)
	 */
	public static boolean isHomAlt(String gt){
		if( gt==null )
			return false;
		return ( gt.equals("1/1") || gt.equals("1|1") );
	}
	
	/**
	 * Check if we have an heterozygous variant allele, meaning 0/1 or 1/0. Phasing does not matters.
	 * @param gt genotype
	 * @return true if heterozygous variant allele (1/0 or 0/1)
	 */
	public static boolean isHet(String gt){
		return ( gt.equals("0/1") || gt.equals("0|1") || gt.equals("1/0") || gt.equals("1|0") );
	}
	
	/**
	 * Function to check that a genotype carries the variant
	 * @param gt genotype
	 * @return true if at least one of the alleles is the alternative, false else.
	 */
	public static boolean isCarryingVariant(String gt){
		try{
			return ( gt.charAt(0)=='1' || gt.charAt(2)=='1' );
		}catch(Exception e){
			return false;
		}
	}
	
	/**
	 * Function to check if the phenotype is phased or not. The distinction is made in the encoding based on the '/' for not phased or '|' for phased.
	 * @param gt genotype
	 * @return true if the genotype is phased, false else
	 */
	public static boolean isPhased(String gt){
		return ( gt.substring(1, 2).equals("|") );
	}
	
	/**
	 * Function returning the paternal allele, or if unphased, the first one
	 * @param gt genotype
	 * @return return the paternal or the first of the 2 alleles
	 */
	public static int getPaternalAllele(String gt){
		if( isPhased(gt) ){
			return Integer.parseInt( gt.split("|")[0] );
		}else{
			return Integer.parseInt( gt.split("/")[0] );
		}
	}
	
	/**
	 * Function returning the maternal, or if unphased, the second allele.
	 * @param gt genotype
	 * @return return the maternal or the second of the 2 alleles
	 */
	public static int getMaternalAllele(String gt){
		if( isPhased(gt) ){
			return Integer.parseInt( gt.split("|")[1] );
		}else{
			return Integer.parseInt( gt.split("/")[1] );
		}
	}
	
	/**
	 * Function giving for a gt and parents gt the inheritance of the variant.
	 * This should be: 0 for none, 1 for the father, 2 for the mother, 3 for both, -1 for unknown
	 * @param gt the patient's genotype
	 * @param sex the sex of the patient
	 * @param chromosome the chromosome carrying the variant checked
	 * @param gtDad the father's genotype
	 * @param gtMum the mother's genotype
	 * @return inheritance of the variant
	 */
	public static int inheritance(String gt, int sex, String chromosome, String gtDad, String gtMum){
		
		if( sex == Patient.MALE ){
			// on a man the X is only inherited from the mother
			if( chromosome.equals("chrX") ){
				if( isCarryingVariant(gt) && isCarryingVariant(gtMum) )
					return 2;
				else{
					if( isDeNovo(gt, gtDad, gtMum) )
						return 0;
					else
						return -1;
				}
					
			}
			// on a man the Y is only inherited from the father
			if( chromosome.equals("chrY") ){
				if( isCarryingVariant(gt) && isCarryingVariant(gtDad) )
					return 1;
				else{
					if( isDeNovo(gt, gtDad, gtMum) )
						return 0;
					else
						return -1;
				}
			}
		}
		if( sex == Patient.FEMALE ){
			// that should not be happening...
			if( chromosome.equals("chrY") ){
				if( isCarryingVariant(gt) && isCarryingVariant(gtDad) )
					return 1;
				else{
					if( isDeNovo(gt, gtDad, gtMum) )
						return 0;
					else
						return -1;
				}
			}
		}
		if( chromosome.equals("chrMT") ){
			if( isCarryingVariant(gt) && isCarryingVariant(gtMum) )
				return 2;
			else{
				if( isDeNovo(gt, gtDad, gtMum) )
					return 0;
				else
					return -1;
			}
		}
		
		Set<Integer> possibilities = new HashSet<Integer>();
		Set<Integer> possibilities2 = new HashSet<Integer>();
		Set<Integer> observed = new HashSet<Integer>();
		int res = -1;
		
		if( isHomRef(gtDad) ) possibilities.add(0);
		if( isHomAlt(gtDad) ) possibilities.add(1);
		if( gtDad==null || isHet(gtDad) ){
			possibilities.add(0);
			possibilities.add(1);
		}
			
		if( isHomAlt(gtMum) ){
			for( Integer i:possibilities )
				possibilities2.add(i+=2);
			possibilities.clear();
		}
		if( gtMum==null || isHet(gtMum) )
			for( Integer i:possibilities )
				possibilities2.add(i+2);
		possibilities.addAll(possibilities2);
		
		if( isHomRef(gt) ) observed.add(0);
		if( isHomAlt(gt) ) observed.add(3);
		if( isHet(gt) ){
			observed.add(1);
			observed.add(2);
		}
		possibilities.retainAll(observed);
		if( possibilities.size()==1 ) res = possibilities.toArray(new Integer[0])[0];
		if( possibilities.size()>1 ) res = -1;
		if( isHomRef(gt) ) res = 0;
		if( isHomAlt(gt) ){
			if( isHomRef(gtDad) ) res = 2;
			if( isHomRef(gtMum) ) res = 1;
		}
		if( isHomRef(gtDad) && isHomRef(gtMum) ) res = 0;
		return res;
	}
	
	/**
	 * Function informing if the inheritance is Mendelian or not for a gt and parents gt.
	 * @param gt the patient's genotype
	 * @param sex the sex of the patient
	 * @param chromosome the chromosome carrying the variant checked
	 * @param gtDad the father's genotype
	 * @param gtMum the mother's genotype
	 * @return true in case of non-mendelian transmission
	 */
	public static boolean nonMendelian(String gt, int sex, String chromosome, String gtDad, String gtMum){
		
		if( sex == Patient.MALE ){
			// on a man the X is only inherited from the mother
			if( chromosome.equals("chrX") ){
				if( isCarryingVariant(gt) && isCarryingVariant(gtMum) )
					return false;
				else
					return true;
			}
			// on a man the Y is only inherited from the father
			if( chromosome.equals("chrY") ){
				if( isCarryingVariant(gt) && isCarryingVariant(gtDad) )
					return false;
				else
					return true;
			}
		}
		if( sex == Patient.FEMALE ){
			// that should not be happening...
			if( chromosome.equals("chrY") ){
				if( isCarryingVariant(gt) && isCarryingVariant(gtDad) )
					return false;
				else
					return true;
			}
		}
		if( chromosome.equals("chrMT") ){
			if( isCarryingVariant(gt) && isCarryingVariant(gtMum) )
				return false;
			else
				return true;
		}
		
		Set<Integer> possibilities = new HashSet<Integer>();
		Set<Integer> possibilities2 = new HashSet<Integer>();
		Set<Integer> observed = new HashSet<Integer>();
		boolean res = false;
		
		if( isHomRef(gtDad) ) possibilities.add(0);
		if( isHomAlt(gtDad) ) possibilities.add(1);
		if( gtDad==null || isHet(gtDad) ){
			possibilities.add(0);
			possibilities.add(1);
		}
			
		if( isHomAlt(gtMum) ){
			for( Integer i:possibilities )
				possibilities2.add(i+=2);
			possibilities.clear();
		}
		if( gtMum==null || isHet(gtMum) )
			for( Integer i:possibilities )
				possibilities2.add(i+2);
		possibilities.addAll(possibilities2);
		
		if( isHomRef(gt) ) observed.add(0);
		if( isHomAlt(gt) ) observed.add(3);
		if( isHet(gt) ){
			observed.add(1);
			observed.add(2);
		}
		possibilities.retainAll(observed);
		if( possibilities.size()==0 ) res = true;
		return res;
	}
	
	/**
	 * Function giving for a gt and parents gt the inheritance of the variant.
	 * This should be: 0 for none, 1 for the father, 2 for the mother, 3 for both, -1 for unknown
	 * @param gt the patient's genotype
	 * @param gtDad the father's genotype
	 * @param gtMum the mother's genotype
	 * @return inheritance of the variant
	 */
	public static int inheritanceOld(String gt, String gtDad, String gtMum){
		if( isHomRef(gt) )
			return 0;
		
		if( gtDad == null ){
			
		}
		
		if( isPhased(gt) ){
			if( isHomAlt(gt) ){
				if( isCarryingVariant(gtMum) && isCarryingVariant(gtDad) )
					return 3;
				if( isCarryingVariant(gtMum) )
					return 2;
				if( isCarryingVariant(gtDad) )
					return 1;
				return 0;
			}
			// we are in an heterozygous case
			if( getPaternalAllele(gt)==1 ){
				if( isCarryingVariant(gtDad) )
					return 1;
				return 0;
			}
			if( getMaternalAllele(gt)==1 ){
				if( isCarryingVariant(gtMum) )
					return 2;
				return 0;
			}
		}else{
			if( isHomAlt(gt) ){
				if( isCarryingVariant(gtMum) && isCarryingVariant(gtDad) )
					return 3;
				if( isCarryingVariant(gtMum) )
					return 2;
				if( isCarryingVariant(gtDad) )
					return 1;
				return 0;
			}
			// we are in an heterozygous case
			if( isCarryingVariant(gtDad) && isHomRef(gtMum) )
				return 1;
			if( isCarryingVariant(gtMum) && isHomRef(gtDad) )
				return 2;
			if( isHomRef(gtMum) && isHomRef(gtDad) )
				return 0;
			return -1;
		}
		return -1;
	}
	
	/**
	 * Check if we are in a <i>de novo</i> case or not
	 * @param gt patient genotype
	 * @param gtDad father genotype, null if not available
	 * @param gtMum mother genotype, null if not available
	 * @return true if <i>de novo</i>
	 */
	public static boolean isDeNovo(String gt, String gtDad, String gtMum){
		if( isCarryingVariant(gt) && isHomRef(gtMum) && isHomRef(gtDad) )
			return true;
		return false;
	}
	
}
