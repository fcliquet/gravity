/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.utils;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class Maths {

	/**
	 * Returns the greater of two Float values. That is, the result is the argument closer to positive infinity. 
	 * If the arguments have the same value, the result is that same value. If either value is NaN, then the result is NaN. 
	 * Unlike the numerical comparison operators, this method considers negative zero to be strictly smaller than positive zero. 
	 * If one argument is positive zero and the other negative zero, the result is positive zero.
	 * 
	 * If one of the arguments is null, then the other one is returned. If both are null, then null is returned.
	 * 
	 * @param a an argument
	 * @param b another argument
	 * @return the larger of a and b
	 */
	public static Float max(Float a, Float b){
		if( a == null )
			return b;
		if( b == null )
			return a;
		return Math.max(a, b);
	}
	
	/**
	 * Returns the smaller of two Float values. That is, the result is the argument closer to positive infinity. 
	 * If the arguments have the same value, the result is that same value. If either value is NaN, then the result is NaN. 
	 * Unlike the numerical comparison operators, this method considers negative zero to be strictly smaller than positive zero. 
	 * If one argument is positive zero and the other negative zero, the result is negative zero.
	 * 
	 * If one of the arguments is null, then the other one is returned. If both are null, then null is returned.
	 * 
	 * @param a an argument
	 * @param b another argument
	 * @return the larger of a and b
	 */
	public static Float min(Float a, Float b){
		if( a == null )
			return b;
		if( b == null )
			return a;
		return Math.min(a, b);
	}
	
	/**
	 * Returns the greater of two Double values. That is, the result is the argument closer to positive infinity. 
	 * If the arguments have the same value, the result is that same value. If either value is NaN, then the result is NaN. 
	 * Unlike the numerical comparison operators, this method considers negative zero to be strictly smaller than positive zero. 
	 * If one argument is positive zero and the other negative zero, the result is positive zero.
	 * 
	 * If one of the arguments is null, then the other one is returned. If both are null, then null is returned.
	 * 
	 * @param a an argument
	 * @param b another argument
	 * @return the larger of a and b
	 */
	public static Double max(Double a, Double b){
		if( a == null )
			return b;
		if( b == null )
			return a;
		return Math.max(a, b);
	}

	/**
	 * Returns the smaller of two Double values. That is, the result is the argument closer to positive infinity. 
	 * If the arguments have the same value, the result is that same value. If either value is NaN, then the result is NaN. 
	 * Unlike the numerical comparison operators, this method considers negative zero to be strictly smaller than positive zero. 
	 * If one argument is positive zero and the other negative zero, the result is negative zero.
	 * 
	 * If one of the arguments is null, then the other one is returned. If both are null, then null is returned.
	 * 
	 * @param a an argument
	 * @param b another argument
	 * @return the larger of a and b
	 */
	public static Double min(Double a, Double b){
		if( a == null )
			return b;
		if( b == null )
			return a;
		return Math.min(a, b);
	}
	
	/**
	 * Returns the greater of two Double values. That is, the result is the argument closer to positive infinity. 
	 * If the arguments have the same value, the result is that same value. If either value is NaN, then the result is NaN. 
	 * Unlike the numerical comparison operators, this method considers negative zero to be strictly smaller than positive zero. 
	 * If one argument is positive zero and the other negative zero, the result is positive zero.
	 * 
	 * If one of the arguments is null, then the other one is returned. If both are null, then null is returned.
	 * 
	 * @param a an argument
	 * @param b another argument
	 * @return the larger of a and b
	 */
	public static Double max(Float a, Double b){
		if( a == null && b == null )
			return null;
		if( a == null )
			return b;
		if( b == null )
			return new Double(a);
		return Math.max(a, b);
	}

	/**
	 * Returns the smaller of two Double values. That is, the result is the argument closer to positive infinity. 
	 * If the arguments have the same value, the result is that same value. If either value is NaN, then the result is NaN. 
	 * Unlike the numerical comparison operators, this method considers negative zero to be strictly smaller than positive zero. 
	 * If one argument is positive zero and the other negative zero, the result is negative zero.
	 * 
	 * If one of the arguments is null, then the other one is returned. If both are null, then null is returned.
	 * 
	 * @param a an argument
	 * @param b another argument
	 * @return the larger of a and b
	 */
	public static Double min(Float a, Double b){
		if( a == null && b == null )
			return null;
		if( a == null )
			return b;
		if( b == null )
			return new Double(a);
		return Math.min(a, b);
	}
	
	/**
	 * Returns the greater of two Double values. That is, the result is the argument closer to positive infinity. 
	 * If the arguments have the same value, the result is that same value. If either value is NaN, then the result is NaN. 
	 * Unlike the numerical comparison operators, this method considers negative zero to be strictly smaller than positive zero. 
	 * If one argument is positive zero and the other negative zero, the result is positive zero.
	 * 
	 * If one of the arguments is null, then the other one is returned. If both are null, then null is returned.
	 * 
	 * @param a an argument
	 * @param b another argument
	 * @return the larger of a and b
	 */
	public static Double max(Double a, Float b){
		if( a == null && b == null )
			return null;
		if( a == null )
			return new Double(b);
		if( b == null )
			return a;
		return Math.max(a, b);
	}

	/**
	 * Returns the smaller of two Double values. That is, the result is the argument closer to positive infinity. 
	 * If the arguments have the same value, the result is that same value. If either value is NaN, then the result is NaN. 
	 * Unlike the numerical comparison operators, this method considers negative zero to be strictly smaller than positive zero. 
	 * If one argument is positive zero and the other negative zero, the result is negative zero.
	 * 
	 * If one of the arguments is null, then the other one is returned. If both are null, then null is returned.
	 * 
	 * @param a an argument
	 * @param b another argument
	 * @return the larger of a and b
	 */
	public static Double min(Double a, Float b){
		if( a == null && b == null )
			return null;
		if( a == null )
			return new Double(b);
		if( b == null )
			return a;
		return Math.min(a, b);
	}
	
	/**
	 * Compares the two specified Double values. The sign of the integer value returned is the same as that of the integer that would be returned by the call:
	 * 	new Double(d1).compareTo(new Double(d2))
	 * This version is null-proof. Null being lower than any value. Comparing two null arguments results in 0.
	 * @param d1 an argument
	 * @param d2 another argument
	 * @return the value 0 if d1 is numerically equal to d2; a value less than 0 if d1 is numerically less than d2; and a value greater than 0 if d1 is numerically greater than d2.
	 */
	public static int compare(Double d1, Double d2){
		if( d1 == null && d2 == null )
			return 0;
		if( d1 == null )
			return -1;
		if( d2 == null )
			return 1;
		return Double.compare(d1, d2);
	}
	
	/**
	 * Compares the two specified Float values. The sign of the integer value returned is the same as that of the integer that would be returned by the call:
	 * 	new Double(d1).compareTo(new Double(d2))
	 * This version is null-proof. Null being lower than any value. Comparing two null arguments results in 0.
	 * @param d1 an argument
	 * @param d2 another argument
	 * @return the value 0 if d1 is numerically equal to d2; a value less than 0 if d1 is numerically less than d2; and a value greater than 0 if d1 is numerically greater than d2.
	 */
	public static int compare(Float d1, Float d2){
		if( d1 == null && d2 == null )
			return 0;
		if( d1 == null )
			return -1;
		if( d2 == null )
			return 1;
		return Float.compare(d1, d2);
	}
	
	/**
	 * Compares the two specified Integer values. The sign of the integer value returned is the same as that of the integer that would be returned by the call:
	 * 	new Double(d1).compareTo(new Double(d2))
	 * This version is null-proof. Null being lower than any value. Comparing two null arguments results in 0.
	 * @param d1 an argument
	 * @param d2 another argument
	 * @return the value 0 if d1 is numerically equal to d2; a value less than 0 if d1 is numerically less than d2; and a value greater than 0 if d1 is numerically greater than d2.
	 */
	public static int compare(Integer d1, Integer d2){
		if( d1 == null && d2 == null )
			return 0;
		if( d1 == null )
			return -1;
		if( d2 == null )
			return 1;
		return Integer.compare(d1, d2);
	}
}
