/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.events;

import java.util.EventListener;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public interface DatabaseListener extends EventListener {

	public void progressChanged(String task, int current, int max);
	
}
