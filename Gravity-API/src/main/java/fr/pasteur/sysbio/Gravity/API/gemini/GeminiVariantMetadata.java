/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.gemini;

import java.util.Set;

import org.eclipse.collections.impl.map.mutable.primitive.ObjectShortHashMap;
import org.eclipse.collections.impl.map.mutable.primitive.ShortShortHashMap;


/**
 * Class closely related to GeminiVariant. It is designed to manage the metadata, 
 * meaning in this context all the different informations about each variants. 
 * This class will store the name and type of each information along with an index.
 * When requesting a data from a variant by name, it will need to refer to the 
 * metadata to know the type and index to read it from an array.
 * Working this way saves a lot of space considering that the alternative would be 
 * a map associating for every variant the name of every piece of information to 
 * the said information (and additional casting as they do not all have the same type).
 * 
 * <p>This storage is optimized and directly uses the query indexes instead of 
 * attributes names to save a lot of time. Indeed retrieving the attribute name 
 * for the native driver is slow (twice the time).</p>
 * 
 * @author Freddy Cliquet
 * @version 1.0
 * @see GeminiVariant
 */
public final class GeminiVariantMetadata {

	private ObjectShortHashMap<String> name2queryIndex = new ObjectShortHashMap<String>();
	
	private ShortShortHashMap queryIndex2index = new ShortShortHashMap();
	private ShortShortHashMap queryIndex2type = new ShortShortHashMap();
	
	/**
	 * SQL text attribute type
	 */
	public final static short TYPE_TEXT = 1;
	
	/**
	 * SQL integer/int attribute type
	 */
	public final static short TYPE_INTEGER = 2;
	
	/**
	 * SQL float/decimal/real attribute type
	 */
	public final static short TYPE_FLOAT = 3;
	
	/**
	 * SQL bool attribute type
	 */
	public final static short TYPE_BOOLEAN = 4;
	
	private static ObjectShortHashMap<String> types = null;
	private ShortShortHashMap typesCounters = new ShortShortHashMap();
	
	private static void initializeTypes(){
		types = new ObjectShortHashMap<String>();
		types.put("TEXT", TYPE_TEXT);
		types.put("VARCHAR", TYPE_TEXT);
		types.put("INT", TYPE_INTEGER);
		types.put("INTEGER", TYPE_INTEGER);
		types.put("REAL", TYPE_FLOAT);
		types.put("FLOAT", TYPE_FLOAT);
		types.put("DECIMAL", TYPE_FLOAT);
		types.put("BOOL", TYPE_BOOLEAN);
		types.put("BOOLEAN", TYPE_BOOLEAN);
	}
	
	/**
	 * Function to add a new attribute to the metadata. 
	 * This will auto-affect an index and find a the appropriate type for it.
	 * @param name Attribute name
	 * @param queryIndex index of the attribute inside the query
	 * @param type Type of the attribute as found in the database
	 */
	public void addAttribute(String name, short queryIndex, String type){
		if( types == null )
			initializeTypes();
		
		short typeIndex = types.get(type);
		short attributeIndex = typesCounters.getIfAbsentPut(typeIndex, (short) 0);
		typesCounters.addToValue(typeIndex, (short) 1);
		
		name2queryIndex.put(name, queryIndex);
		queryIndex2index.put(queryIndex, attributeIndex);
		queryIndex2type.put(queryIndex, typeIndex);
	}
	
	/**
	 * Allows to retrieve the index uniquely assigned to the specified attribute for its specific type.
	 * @param name Name of the attribute
	 * @return index assigned to this attribute
	 */
	public short getIndex(String name){
		return queryIndex2index.get(name2queryIndex.get(name));
	}
	
	/**
	 * Allows to retrieve the index uniquely assigned to the specified attribute for its specific type.
	 * @param queryIndex Index of the element in the query
	 * @return index assigned to this attribute
	 */
	public short getIndex(short queryIndex){
		return queryIndex2index.get(queryIndex);
	}
	
	/**
	 * Allows to retrieve the type of the specified attribute. Types are defined as constant in this class (TYPE_TEXT, TYPE_INTEGER, TYPE_FLOAT, TYPE_BOOLEAN).
	 * @param name Name of the attribute
	 * @return type of the specified attribute
	 */
	public short getType(String name){
		return queryIndex2type.get(name2queryIndex.get(name));
	}
	
	/**
	 * Allows to retrieve the type of the specified attribute. Types are defined as constant in this class (TYPE_TEXT, TYPE_INTEGER, TYPE_FLOAT, TYPE_BOOLEAN).
	 * @param queryIndex Index of the element in the query
	 * @return type of the specified attribute
	 */
	public short getType(short queryIndex){
		return queryIndex2type.get(queryIndex);
	}
	
	/**
	 * Function giving the Set of all the attributes names.
	 * @return Set of attributes name
	 */
	public Set<String> getAttributes(){
		return name2queryIndex.keySet();
	}
	
	/**
	 * Function to know the number of elements already registered of the given type. 
	 * Important to know the index of the next element.
	 * @param type type of element
	 * @return number of element of this type
	 */
	public short getSize(short type){
		return typesCounters.getIfAbsent(type, (short) 0);
	}
}
