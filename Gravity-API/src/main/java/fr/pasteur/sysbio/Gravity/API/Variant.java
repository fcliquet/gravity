package fr.pasteur.sysbio.Gravity.API;

import java.util.List;
import java.util.Map;

/**
 * Interface defining what a variant is and how to access all of its attributes. 
 * This is really important due to the strong heterogeneity between direct data 
 * from VCF files, Gemini databases and not even talking about the different 
 * annotation tools existing.
 * 
 * @author Freddy Cliquet
 * @version 1.1
 */
public interface Variant {

	/**
	 * Method to retrieve directly the cadd of the variant. It is useful to have a 
	 * direct access to it as the cadd could be present under different names (cadd, 
	 * cadd_raw, etc.). It is to be noted that if the variant does not have any cadd
	 * it is supposed to return null (hence using a Float object instead of a primitive).
	 * 
	 * @return cadd of the variant, null if the information doesn't exist
	 */
	Float cadd();
	
	/**
	 * Method to retrieve directly the cadd of the variant. It is useful to have a 
	 * direct access to it as the cadd could be present under different names (cadd, 
	 * cadd_raw, etc.). It is to be noted that if the variant does not have any cadd
	 * it is supposed to return null (hence using a Float object instead of a primitive).
	 * 
	 * @return cadd of the variant, null if the information doesn't exist
	 */
	Float cadd_scaled();

	/**
	 * Method to compute and/or access the cohort frequency observed for this Variant. 
	 * 
	 * @return the cohort frequency for the variant
	 */
	Double cohortFrequency();
	
	/**
	 * Method to get the alternative allele of the Variant
	 * 
	 * @return The alternative allele
	 */
	String getAlternative();

	/**
	 * Method giving back the chromosome carrying the Variant.
	 * 
	 * @return The chromosome of the Variant
	 */
	String getChromosome();

	/**
	 * Method to access the filter field of the raw VCF for the Variant.
	 * 
	 * @return The filter value from the Variant
	 */
	String getFilter();

	/**
	 * Method accessing the gene carrying the variant.
	 * 
	 * @return The gene carrying the variant
	 */
	String getGene();

	/**
	 * Method returning a String formated in HTML to be used as a tooltip to represent 2 set of both affected and unaffected patients. This tooltip is mainly used for the complete family analysis.
	 * 
	 * @param affected List of the affected patients
	 * @param unaffected List of the unaffected patients
	 * @return An HTML tooltip describing the Variant for all the affected and unaffected patients
	 */
	String getHTMLtooltip(List<Patient> affected, List<Patient> unaffected);

	/**
	 * Method returning a String formated in HTML to be used as a tooltip to represent The given patient along with its parents. This tooltip is mainly used for the trio family analysis.
	 * 
	 * @param patient Patient
	 * @param father patient's father
	 * @param mother patient's mother
	 * @return An HTML tooltip describing the Variant for all the affected and unaffected patients
	 */
	String getHTMLtooltip(Patient patient, Patient father, Patient mother);

	String getHTMLtooltip(Patient patientA, Patient patientB);

	/**
	 * Method to access the number of patients.
	 * 
	 * @return Number of patients
	 */
	int getNumberOfPatients();

	/**
	 * Method used to retrieve a given property from all the one stored in the Variant.
	 * This needs to proceed in a retrieve by name way as we do not know prior to loading 
	 * the list of all attributes to expect. The return value is an Object and will need 
	 * to be casted to a specific type after it as we do not know the type of the requested 
	 * attributes beforehand either.
	 * 
	 * @param _property Property to retrieve
	 * @return Value corresponding to the requested property
	 */
	Object getObject(String _property);

	/**
	 * Method to retrieve the full genotype of the given patient.
	 * 
	 * @param i Patient
	 * @return full genotype of the patient (GT:DP:AD:GQ:PL)
	 */
	String getPatient(Patient i);
	
	/**
	 * Method to retrieve the GT of the given patient in a form encoded using a 0 for the reference allele and a 1 for the alternative allele.
	 * 
	 * @param patient Patient
	 * @return GT of the patient using 0 for the reference allele and 1 for the alternative allele
	 */
	String getPatientGenotype(Patient patient);

	/**
	 * Method to retrieve the GT of the given patient encoded in the 0/1 fashion. 
	 * This will consider that for CNVs it has to check for other similar variants (checked by overlapping). 
	 * This is needed as variants can have slightly different boundaries or could be stored in different entries for simplification.
	 * Filters are applied.
	 * @param patient Patient
	 * @return GT of the patient
	 */
	String getPatientGenotypeEnlarged4CNVsFiltered(Patient patient);
	
	/**
	 * Method to retrieve the GT of the given patient encoded in the 0/1 fashion. 
	 * This will consider that for CNVs it has to check for other similar variants (checked by overlapping). 
	 * This is needed as variants can have slightly different boundaries or could be stored in different entries for simplification.
	 * @param patient Patient
	 * @return GT of the patient
	 */
	String getPatientGenotypeEnlarged4CNVs(Patient patient);
	
	/**
	 * Method to retrieve a similar variant carried by the given patient in the case of CNVs. So it will return the first variant carried matching the overlapping criteria.
	 * @param patient Patient
	 * @return Variant similar to the current one carried by patient
	 */
	Variant getSimilarCNV(Patient patient);
	
	/**
	 * Method to access the position of the variant in the genome.
	 * 
	 * @return Position of the variant
	 */
	int getPosition();

	/**
	 * Method to access the quality field of the raw VCF.
	 * 
	 * @return quality value from the variant
	 */
	float getQuality();
	
	/**
	 * Method to get the reference allele of the Variant
	 * 
	 * @return The reference allele
	 */
	String getReference();
	
	/**
	 * Method to retrieve the impact of the variant.
	 * 
	 * @return impact of the variant
	 */
	String impact();

	/**
	 * Give the priority, meaning the "danger" of the impact of the mutation.
	 * The lower the value, the more dangerous the mutation is.
	 * 
	 * @return priority of the impact
	 */
	int impactPriority();

	/**
	 * Method to retrieve the severity of the impact (can be seen a s a category such as HIGH, MED, LOW for instance)
	 * 
	 * @return the severity of the impact
	 */
	String impactSeverity();

	
	/**
	 * Method to retrieve the list of all attributes existing for the variants, they can then be retrieved using the getObject(String) method.
	 * 
	 * @return List of all the existing properties in the variant
	 */
	List<String> listAttributes();

	
	/**
	 * method behaving as an alias to retrieve the maximum allele frequency. Depending on the cases it could also compute it if necessary.
	 * 
	 * <p>This could for instance retrieve the allele frequency in the 1000genome project, in exac and in esp to then return the max value.</p>
	 * 
	 * @return maximum allele frequency
	 */
	Double maxAlleleFrequency();

	/**
	 * Method to affect all the patients and their genotypes to the variant. This exists because the genotypes could be stored separately from the variants and could be loaded independently. 
	 * 
	 * @param genotypes map associating patient's names to genotypes
	 * @param patients list of patients
	 */
	void setGenotypes(Map<String, Object> genotypes, List<Patient> patients);

	@Override
	public String toString();
	
	/**
	 * Method to check if the variant is a CNV or not.
	 * 
	 * @return true if the variant is a CNV, false else
	 */
	boolean isCNV();
	
	/**
	 * Setter for the gene
	 * @param g the Gene to set
	 */
	public void setGeneObject(Gene g);
	
	/**
	 * Getter for the Gene
	 * @return
	 */
	public Gene getGeneObject();
}