package fr.pasteur.sysbio.Gravity.API;

/**
 * This interface describes the main part of Filters that could be applied on Variants.
 *  
 * @author Freddy Cliquet
 * @version 1.0
 * @see Variant
 */
public interface Filter {

	/**
	 * Assessor for the property of the variant
	 * @return the operator used by the filter
	 */
	String getOperator();
	
	/**
	 * Assessor for the property of the variant
	 * @return The property used by the filter
	 */
	String getProperty();
	
	/**
	 * Assessor for the value used as a filter criteria
	 * @return Value used by the filter as a criteria
	 */
	String getValue();
	
	/**
	 * Function assessing if the given Variant passes this filter with success or not.
	 * @param gsv The variant to test
	 * @return true if the variant passes the filter, else false
	 */
	boolean passFilter(Variant gsv);
	
	/**
	 * Function setting the operator associated to the value to filter the variant.
	 * @param operator the operator associated to the value used to filter the variant.
	 */
	void setOperator(String operator);
	
	/**
	 * Function setting the Property field to define on which variant information the filter we need to apply.
	 * @param property the variant info field that the filter need to use
	 */
	void setProperty(String property);
	
	/**
	 * Function used to define the value used for the filtering. Depending on the operator and the type of the property, 
	 * this could behave as a threshold, the equality of a type, the blacklist of a type etc.
	 * @param value The value to compare with for filtering purpose.
	 */
	void setValue(String value);
}