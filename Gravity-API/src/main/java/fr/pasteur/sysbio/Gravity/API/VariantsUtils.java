/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Freddy Cliquet
 * @version 1.0
 *
 */
public class VariantsUtils {

	public static Set<? extends Variant> mergeVariantsLists(List<? extends Variant>... lists){
		Set<Variant> set = new HashSet<Variant>();
		for(List<? extends Variant> l:lists){
			if( l != null )
				set.addAll(l);
		}
		return set;
	}
	
	public static Variant getHighestImpact(Collection<? extends Variant> set){
		int mostDangerousGSVcarried = Integer.MAX_VALUE;
		Variant v = null;
		
		if(set==null)
			return null;
		
		for( Variant gsv:set ){
			mostDangerousGSVcarried = Math.min(mostDangerousGSVcarried, gsv.impactPriority());	
			if( gsv.impactPriority() <= mostDangerousGSVcarried ){
				mostDangerousGSVcarried = gsv.impactPriority();
				v = gsv;
			}
		}
		return v;
	}
	
}
