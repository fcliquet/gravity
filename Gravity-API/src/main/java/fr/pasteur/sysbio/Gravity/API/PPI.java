/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class managing all the Interactions from a Protein-Protein Interaction (PPI) network.
 * This class will load interactions from a packaged file.
 * 
 * <p>Different types of interactions are managedto describe the way data are obtained, thus the confidence we have in them.</p>
 * 
 * @author Freddy Cliquet
 * @version 1.0
 */
public class PPI {

	List<Interaction> _interactions;
	List<String> _interactionsTypes;
	List<Integer> _interactionsWeights;
	Map<String,List<Interaction>> _fastInteractions = null;
	List<String> _fastInteractionTypes = null;
	
	/**
	 * Constructor of an empty PPI
	 */
	public PPI(){
		_interactions = new ArrayList<Interaction>();
		_interactionsTypes = new ArrayList<String>();
		_interactionsWeights = new ArrayList<Integer>();
	}
	
	/**
	 * Function in charge of loading the packaged interactions.
	 */
	public void load(){
		_interactions.clear();
		
		HashMap<String, HashMap<String, Interaction>> inter = new HashMap<String, HashMap<String, Interaction>>();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/PPI_String.sorted.tsv")));
		try {
			String line = br.readLine();
			String lineSplitted[] = line.split("\t");
			
			for( int i=0 ; i<lineSplitted.length - 2 ; i++){
				_interactionsTypes.add(lineSplitted[i+2].split("=")[0]);
				_interactionsWeights.add(new Integer(lineSplitted[i+2].split("=")[1]));
			}
			
//			_interactionsTypes.add(lineSplitted[4].split("=")[0]);
//			_interactionsWeights.add(new Integer(lineSplitted[4].split("=")[1]));
//			_interactionsTypes.add(lineSplitted[5].split("=")[0]);
//			_interactionsWeights.add(new Integer(lineSplitted[5].split("=")[1]));
//			_interactionsTypes.add(lineSplitted[6].split("=")[0]);
//			_interactionsWeights.add(new Integer(lineSplitted[6].split("=")[1]));
//			_interactionsTypes.add(lineSplitted[7].split("=")[0]);
//			_interactionsWeights.add(new Integer(lineSplitted[7].split("=")[1]));
			
			while( (line = br.readLine()) != null ){
				String s1, s2; 
				if( line.split("\t")[0].compareTo(line.split("\t")[1]) <= 0 ){
					s1 = line.split("\t")[0];
					s2 = line.split("\t")[1];
				}else{
					s1 = line.split("\t")[1];
					s2 = line.split("\t")[0];
				}
				if( inter.get( s1 ) != null ){
					Interaction i = inter.get( s1 ).get( s2 );
					if( i!= null )
						i.update(line);
					else
						inter.get( s1 ).put( s2, new Interaction(line, _interactionsTypes.size()) );
				}else{
					inter.put( s1, new HashMap<String, Interaction>());
					inter.get( s1 ).put( s2, new Interaction(line, _interactionsTypes.size()) );
				}
			}
			for(HashMap<String, Interaction> m:inter.values())
				_interactions.addAll(m.values());
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void prepareFastInteractions(List<String> interactionTypes){
		_fastInteractions = new HashMap<String, List<Interaction>>();
		for( Interaction i:getInteractions(interactionTypes) ){
			List<Interaction> li = _fastInteractions.get(i.symbol1);
			if( li == null )
				li = new ArrayList<Interaction>();
			li.add(i);
			_fastInteractions.put(i.symbol1, li);
			
			List<Interaction> li2 = _fastInteractions.get(i.symbol2);
			if( li2 == null )
				li2 = new ArrayList<Interaction>();
			li2.add(i);
			_fastInteractions.put(i.symbol2, li2);
		}
	}
	
	/**
	 * Function returning the complete list of interactions
	 * @return List of all the interactions
	 */
	public List<Interaction> getInteractions(){
		return _interactions;
	}
	
	/**
	 * Function to get the List of interaction of the given types linked to the given Gene.
	 * @param gene A Gene that need to be involved in those interactions.
	 * @param interactionTypes A List of Interactions types to be retrieved.
	 * @return List of interactions related to the given Gene.
	 */
	public List<Interaction> getInteractions(String gene, List<String> interactionTypes){
		if( _fastInteractions == null || _fastInteractionTypes != interactionTypes ){
			prepareFastInteractions(interactionTypes);
			_fastInteractionTypes = interactionTypes;
		}
		return _fastInteractions.get(gene);
	}
	
	/**
	 * Function to get the set of interaction of the given types.
	 * @param interactionsTypes A List of Interactions types to be retrieved.
	 * @return Set of interactions.
	 */
	public Set<Interaction> getInteractions(List<String> interactionsTypes){
		Set<Interaction> l = new HashSet<Interaction>();
		for(Interaction i:_interactions){
			for( String s:interactionsTypes ){
				if( i.types[ _interactionsTypes.indexOf(s) ] == true )
					l.add(i);
			}
		}
		return l;
	}
	
	/**
	 * Function to retrieve a Map that will associate each Gene to his Set of Interactions.
	 * @param interactionsTypes List of the Interactions types to retrieve
	 * @return Map associating the gene names to the set of interactions associated to them
	 */
	public Map<String, Set<Interaction>> getInteractionsMap(List<String> interactionsTypes){
		Map<String, Set<Interaction>> l = new HashMap<String, Set<Interaction>>();
		
		for(Interaction i:_interactions){
			for( String s:interactionsTypes ){
				if( i.types[ _interactionsTypes.indexOf(s) ] == true ){
					if( l.get(i.symbol1) == null )
						l.put(i.symbol1, new HashSet<Interaction>());
					l.get(i.symbol1).add(i);
				}
			}
		}
		
		return l;
	}
	
	/**
	 * Function to get the cumulated weight, meaning here the confidence, for one interactions of all the types specified.
	 * @param i The interaction
	 * @param interactionsTypes List of the interactions types considered
	 * @return the confidence in the interactions considered
	 */
	public int getInteractionWeight(Interaction i, List<String> interactionsTypes){
		int weight = 0;
		for( String s:interactionsTypes )
			if( i.types[ _interactionsTypes.indexOf(s) ] == true )
				weight+=_interactionsWeights.get( _interactionsTypes.indexOf(s) );
		return weight;
	}
	
	/**
	 * Function to list the different types of interactions existing in the PPI
	 * @return List of all the types of interactions existing
	 */
	public List<String> getInteractionTypes(){
		return _interactionsTypes;
	}
	
	/**
	 * Function to retrieve the Set of all Genes directly involved in the PPI fro any type of Interactions
	 * @return Set of Genes from the PPI
	 */
	public Set<String> getAllGenes(){
		Set<String> genes = new HashSet<String>();
		for(Interaction i:_interactions){
			genes.add(i.symbol1);
			genes.add(i.symbol2);
		}
		return genes;
	}
	
//	/**
//	 * @param args
//	 */
//	public static void main(String[] args) {
//		PPI ppi = new PPI();
//		ppi.load();
//		//ppi.getInteractions(true, false, false, false).forEach(System.out::println);
//	}

}