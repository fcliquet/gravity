/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

import java.util.List;
import java.util.Set;

import fr.pasteur.sysbio.Gravity.API.events.DatabaseListener;

/**
 * Interface defining the important functions necessaries to implement a "database" meaning the access to a data source to read the variants.
 * 
 * <p>This interface has way to manage a DatabaseListener important to monitor the progress during the loading (which usually takes time).</p>
 * 
 * @author Freddy Cliquet
 * @version 2.0
 */
public interface VariantDatabase {

	/**
	 * Add a new database listener to the VariantDatabase
	 * 
	 * @param databaseListener a database listener
	 */
	public void addDatabaseListener(DatabaseListener databaseListener);
	
	/**
	 * Method used to fire a progression change to all the Database listeners
	 * 
	 * @param task description of the task in progress
	 * @param current current progress in the task
	 * @param max progress value at which the task will be completed
	 */
	public void fireDatabaseProgressChange(String task, int current, int max);
	
	/**
	 * Method to get the full list of all the genes
	 * 
	 * @return List of all the genes
	 */
	public List<Gene> getAllGenes();
	
	/**
	 * method to get access to the list of all genes names
	 * 
	 * @return List of all gene names
	 */
	public Set<String> getAllGenesNames();
	
	/**
	 * method to get access to the Set of all the families from the current dataset
	 * 
	 * @return Set of all the families
	 */
	public Set<String> getFamilies();
	
	/**
	 * Method that gives the father of a given patient.
	 * 
	 * @param p Patient
	 * @return the patient's father
	 */
	public Patient getFather(Patient p);
	
	/**
	 * Method returning the gene corresponding to the name provided.
	 * 
	 * @param geneName name of a gene
	 * @return Gene matching the given name
	 */
	public Gene getGene(String geneName);
	
	/**
	 * Method returning the gene corresponding to the name provided with all the given filters applied.
	 * 
	 * @param geneName name of a gene
	 * @param filters list of various variant filters
	 * @return Gene matching the given name
	 */
	public Gene getGene(String geneName, List<Filter> filters);
	
	/**
	 * Method to access the list of all the genes that carry at least one variant in at least one patient.
	 * 
	 * @return List of all the genes with a variant
	 */
	public List<Gene> getGenesCarryingVariant();
	
	/**
	 * Method to access the list of all the genes that carry at least one variant in at least one patient AFTER application of the given filters.
	 * 
	 * @param filters list of various variant filters
	 * @return List of all the genes with a variant
	 */
	public List<Gene> getGenesCarryingVariant(List<Filter> filters);
	
	/**
	 * Method to access the list of all the genes that carry at least one variant in the given patient.
	 * 
	 * @param p Patient
	 * @return List of all the genes with a variant
	 */
	public List<Gene> getGenesCarryingVariant(Patient p);
	
	/**
	 * Method to access the list of all the genes that carry at least one variant in the given patient AFTER application of the given filters.
	 * 
	 * @param p Patient
	 * @param filters list of various variant filters
	 * @return List of all the genes with a variant
	 */
	public List<Gene> getGenesCarryingVariant(Patient p, List<Filter> filters);
	
	/**
	 * Method to retrieve all the possible impacts observed with the given severity
	 * 
	 * @param severity Severity of impacts
	 * @return List of all the various impacts of the given severity
	 */
	public List<String> getImpactsOfSeverity(String severity);
	
	/**
	 * Method that gives the mother of a given patient
	 * 
	 * @param p Patient
	 * @return the patient's mother
	 */
	public Patient getMother(Patient p);
	
	/**
	 * Method to get access to a list of all the patients in the dataset
	 * 
	 * @return List of all patients
	 */
	public List<Patient> getPatients();
	
	/**
	 * Method to get access to a list of all the patients from the given family
	 * 
	 * @param family Family of the patients to retrieve
	 * @return List of patients form the given family
	 */
	public List<Patient> getPatients(String family);
	
	/**
	 * Method that list and return all the patient that have 2 known parents with data on them.
	 * 
	 * @return List of all the patient for which we know and have data about both parents
	 */
	public List<Patient> getPatientsWithParents();
	
	/**
	 * Method to check if our data have pedigree information included or not.
	 * 
	 * @return true if the data contain pedigree data
	 */
	public boolean hasPedigree();
	
	/**
	 * Method to access the list of all the variant's attributes.
	 * 
	 * @return List of all the attributes found in  the variants
	 */
	public List<String> listVariantAttributes();
	
	public List<String> listFrequencyAttributes();
	public List<String> listCNVsAttributes();
	public List<String> listPathoAttributes();
	public List<String> listOtherAttributes();
	
	/**
	 * Method that return a list of all the possible impacts (for which we have at least one variant)
	 * 
	 * @return List of all the possible impacts
	 */
	public List<String> listVariantImpacts();
	
	/**
	 * Method that will load the data. Calling this is time consuming and should be done in a controlled environment (Thread etc.along with management of the event linked to the DatabaseListener)
	 */
	public void load();
	
	/**
	 * Method to retrieve the total number of variants in the dataset
	 * 
	 * @return the number of variants in the dataset
	 */
	public int numberOfVariants();
	
	/**
	 * Method use to remove a DatabaseListener from the VariantDatabase
	 * 
	 * @param databaseListener DatabaseListener to unregister from the VariantDatabase
	 */
	public void removeDatabaseListener(DatabaseListener databaseListener);
	
	/**
	 * Summary of the database and its content.
	 * @return String of structured information to display about the database
	 */
	public String getInfos();
}
