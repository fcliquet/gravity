/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.pasteur.sysbio.Gravity.API.Interaction;
import fr.pasteur.sysbio.Gravity.API.PPI;

/**
 * Class to partition a network and to try and connect all the different partitions.
 * 
 * <p>This class will need to be expanded to choose in a smarter way the candidate gene used to connect partitions 
 * (and not use the first found as it is). It will also need to work on a provided set of gene in priority 
 * instead of the whole PPI for it to work only on a subset of gene carrying variants for instance.</p>
 * 
 * @author Freddy Cliquet
 * @version 1.0
 */
public class Partitionner {

	private List< Set<String> > partitions;
	private List<String> interactionTypes;
	private PPI network;
	
	/**
	 * Constructor of a new partitioner.
	 * @param ppi Protein-protein interaction network
	 * @param typesInteraction list of the types of interactions accepted
	 */
	public Partitionner(PPI ppi, List<String> typesInteraction){
		partitions = new ArrayList< Set<String> >();
		network = ppi;
		interactionTypes = typesInteraction;
	}
	
	/**
	 * Function that build the list of partition for the given collection of genes
	 * @param lg collection of genes
	 */
	public void buildPartitions(Collection<String> lg){
		List<String> rest = new ArrayList<String>(lg);
		for(Interaction i:network.getInteractions(interactionTypes)){
			if( lg.contains(i.symbol1) && lg.contains( i.symbol2 ) ){
				Set<String> p = getPartition( i.symbol1 );
				if( p == null ){
					Set<String> p2 = getPartition( i.symbol2 );
					if( p2 == null ){
						p = new HashSet<String>();
						rest.remove( i.symbol1 );
						rest.remove( i.symbol2 );
						p.add( i.symbol1 );
						p.add( i.symbol2 );
						partitions.add( p );
					}else{
						p2.add( i.symbol1 );
						rest.remove( i.symbol1 );
					}
				}else{
					Set<String> p2 = getPartition( i.symbol2 );
					if( p2 == null ){
						p.add( i.symbol2 );
						rest.remove( i.symbol2 );
					}else{
						if( p != p2 ){
							p.addAll(p2);
							partitions.remove(p2);
						}
					}
				}
			}else{
				if( lg.contains(i.symbol1) ){
					Set<String> p = getPartition( i.symbol1 );
					if( p == null ){
						p = new HashSet<String>();
						p.add( i.symbol1 );
						rest.remove( i.symbol1 );
						partitions.add( p );
					}
				}
				if( lg.contains(i.symbol2) ){
					Set<String> p = getPartition( i.symbol2 );
					if( p == null ){
						p = new HashSet<String>();
						p.add( i.symbol2 );
						rest.remove( i.symbol2 );
						partitions.add( p );
					}
				}
			}
		}
		for( String g: rest ){
			Set<String> p = new HashSet<String>();
			p.add( g );
			partitions.add( p );
		}
	}
	
	/**
	 * Get the partition of the given gene
	 * @param gene  Gene for which to retrieve the partition
	 * @return partition containing the Gene gene
	 */
	private Set<String> getPartition(String gene){
		for( Set<String> p : partitions )
			if( p.contains( gene ) ){
				return p;
			}
		return null;
	}
	
	private Set<String> getPartition(String gene, List<Set<String>> myPartitions){
		for( Set<String> p : myPartitions )
			if( p.contains( gene ) ){
				return p;
			}
		return null;
	}
	
	/**
	 * Function returning the partition at a given index
	 * @param index index of the partition
	 * @return the partition as a set of genes
	 */
	public Set<String> getPartition(int index){
		return partitions.get(index);
	}
	
	/**
	 * Function giving the number of existing partitions
	 * @return the number of partitions
	 */
	public int numberOfPartitions(){
		return partitions.size();
	}
	
	/**
	 * Function that print all the partitions to the standard output.
	 */
	public void printAll(){
		for( Set<String> p : partitions ){
			System.out.println(p);
		}
	}
	
	/**
	 * Function that sort all the partition by number of genes in them
	 */
	public void sort(){
		Collections.sort(partitions, new Comparator<Set<String>>() {
			@Override
			public int compare(Set<String> o1, Set<String> o2) {
				return Integer.compare(o2.size(), o1.size());
			}
		});
	}
	
	/**
	 * Function computing the list of genes to add to connect as much as possible the different partitions.
	 * @return the set of genes to add in the network to connect as much as possible of the partitions
	 */
	public Set<String> connectAll(){
		List<Set<String>> newPartitions = new ArrayList<Set<String>>(partitions);
		Set<String> newGenes = new HashSet<String>();
		Set<String> addedGenes = connectTo(newPartitions.get(0), newPartitions);
		while(addedGenes != null){
			newPartitions.get(0).addAll(addedGenes);
			newGenes.addAll( addedGenes );
//			System.out.println("nb partitions: "+newPartitions.size()+" adding: "+addedGenes+" big part: "+newPartitions.get(0).size());
			addedGenes = connectTo(newPartitions.get(0), newPartitions);
		}
//		System.out.println("Adding genes: "+newGenes);
		return newGenes;
	}
	
	private Set<String> connectTo( Set<String> ref, List<Set<String>> myPartitions){
		Node<Set<String>> root = new Node<Set<String>>(ref);
		Node<Set<String>> current = root;
		Set<String> geneTreated = new HashSet<String>();
		geneTreated.addAll( root.getValue() );
		List<Node<Set<String>>> queue = new ArrayList<Node<Set<String>>>();
		List<Node<Set<String>>> queue_next = new ArrayList<Node<Set<String>>>();
		queue.add(root);
		Set<String> linkers = new HashSet<String>();
		
		while( queue.size() > 0 ){
			current = queue.remove(0);
			Set<String> s = processNode(current, geneTreated, myPartitions, queue, queue_next);
			if( s != null )
				linkers.addAll(s);
			if( queue.size() == 0 ){
				if( linkers.size() > 0 )
					return linkers;
				System.out.println("queue size before adding: "+queue.size());
				queue.addAll(queue_next);
				System.out.println("queue size after adding: "+queue.size());
				queue_next = new ArrayList<Node<Set<String>>>();
			}
		}
		return null;
	}
	
	private Set<String> processNode(Node<Set<String>> current, Set<String> processedGenes, List<Set<String>> myPartitions, List<Node<Set<String>>> queue, List<Node<Set<String>>> queue_next){
		for( String gene:current.getValue() ){
			for( Interaction i:network.getInteractions(gene, interactionTypes) ){
				if( current.getValue().contains( i.symbol1 ) ){
					if( !processedGenes.contains( i.symbol2 ) ){
						Set<String> p = getPartition( i.symbol2, myPartitions );
						if( p==null ){
							p = new HashSet<String>();
							p.add( i.symbol2 );
						}else{
							myPartitions.remove(p);
							getRoot(current).getValue().addAll( p );
							return backtrack(current);
						}
						processedGenes.addAll(p);
						queue_next.add( current.addChild(p) );
					}
				}else{
					if( current.getValue().contains( i.symbol2 ) ){
						if( !processedGenes.contains( i.symbol1 ) ){
							Set<String> p = getPartition( i.symbol1, myPartitions );
							if( p==null ){
								p = new HashSet<String>();
								p.add( i.symbol1 );
							}else{
								myPartitions.remove(p);
								getRoot(current).getValue().addAll( p );
								return backtrack(current);
							}
							processedGenes.addAll(p);
							queue_next.add( current.addChild(p) );
						}
					}
				}
			}
		}
		return null;
	}
	
	private Node<Set<String>> getRoot(Node<Set<String>> node){
		Node<Set<String>> n = node;
		while( n.getParent() != null )
			n = n.getParent();
		return n;
	}
	
	private Set<String> backtrack(Node<Set<String>> current){
		Node<Set<String>> n = current;
		Set<String> set = new HashSet<String>();
		while( n.getParent() != null ){
			set.addAll( n.getValue() );
			n = n.getParent();
		}
		return set;
	}

}

class Node<T>{
	Node<T> parent;
	T node;
	List<Node<T>> children;
	
	public Node(T n){
		node = n;
		parent = null;
		children = new ArrayList<Node<T>>();
	}
	
	public Node(T n, Node<T> p){
		node = n;
		parent = p;
		children = new ArrayList<Node<T>>();
	}
	
	public Node<T> getParent(){
		return parent;
	}
	
	public List<Node<T>> getChildren(){
		return children;
	}
	
	public T getValue(){
		return node;
	}
	
	public Node<T> addChild(T n){
		Node<T> tmp = new Node<T>(n, this);
		children.add( tmp );
		return tmp;
	}
}
