/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

/**
 * @author Freddy Cliquet
 * @version 1.0
 * @param <T>
 *
 */
public class GeneAnnotation<T> {

	private T _annotation;
	private String _label;
	
	public GeneAnnotation(String label, T a){
		_annotation = a;
		_label = label;
	}
	
	public T getAnnotation(){
		return _annotation;
	}
	
	@Override
	public String toString() {
		return _label+": "+_annotation;
	}
}
