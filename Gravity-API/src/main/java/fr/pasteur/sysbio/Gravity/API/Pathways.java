/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class containing all the pathways or gene list that can be used for an analysis. 
 * They consists in Set of Genes and are all named using a unique name.
 * 
 * <p>Trying to add a pathway with an already existing name will have for consequence 
 * to add all the genes from the new one to the previously existing pathway.</p>
 * 
 * @author Freddy Cliquet
 * @version 1.1
 */
public class Pathways {
	private HashMap<String, Set<String>> _pathways;
	
	/**
	 * Constructor of an empty set of Pathways / Group of genes
	 */
	public Pathways(){
		_pathways = new HashMap<String, Set<String>>();
	}
	
	/**
	 * Load the default pathways provided within the package
	 * 
	 * <p>It is not used anymore as the Pathway are stored in a Cytoscape property file provided in the package.</p>
	 */
	@Deprecated
	public void load(){
		BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/prepare_genes.tsv")));
		try {
			String line = br.readLine();
			String[] labels = line.split("\t");
			for(int i=2;i<labels.length;i++){
				if( _pathways.get(labels[i])==null ){
					_pathways.put(labels[i], new HashSet<String>());
				}
			}
			
			while( (line = br.readLine()) != null ){
				String[] l = line.split("\t");
				for(int i=2;i<l.length;i++){
					if( l[i].equals("1") ){
						_pathways.get(labels[i]).add(l[1]);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Add a new pathway to the set in memory
	 * @param name Name of the pathway
	 * @param genes Set of genes it contains
	 */
	public void addPathway(String name, Set<String> genes){
		if( _pathways.get(name)==null )
			_pathways.put(name, genes);
		else
			_pathways.get(name).addAll(genes);
	}
	
	/**
	 * Retrieve a Set of genes constituting a given list of pathways
	 * @param pathwayNames list of pathways to retrieve from memory
	 * @return set of all the genes part of the requested pathways
	 */
	public Set<String> getGenes(List<String> pathwayNames){
		HashSet<String> genes = new HashSet<String>();
		for(String pathway:pathwayNames){
			if(_pathways.get(pathway) != null )
				genes.addAll(_pathways.get(pathway));
		}
		return genes;
	}
	
	/**
	 * Retrieve a Set of genes constituting the given pathway
	 * @param pathwayName pathway to retrieve from memory
	 * @return set of all the genes part of the requested pathways
	 */
	public Set<String> getGenes(String pathwayName){
		HashSet<String> genes = new HashSet<String>();
		if( _pathways.get(pathwayName) != null )
			genes.addAll(_pathways.get(pathwayName));
		return genes;
	}
	
	/**
	 * Give the Set of all the pathways in memory
	 * @return set of pathway names
	 */
	public Set<String> getPathwaysList(){
		return _pathways.keySet();
	}
	
	/**
	 * Give the Set of all the pathways in memory that contain the given gene
	 * @param gene Gene name
	 * @return set of pathway names
	 */
	public Set<String> getPathwaysList(String gene){
		Set<String> s = new HashSet<String>();
		for(String p:_pathways.keySet())
			if( _pathways.get(p).contains(gene) )
				s.add(p);
		return s;
	}
	
	/**
	 * remove the given pathways
	 * @param pathwayNames list of the pathways to remove
	 */
	public void removePathways(List<String> pathwayNames){
		for(String pathway:pathwayNames){
			_pathways.remove(pathway);
		}
	}
	
	/**
	 * Print the list of pathways in memory along with the number of genes in each of them.
	 */
	@Override
	public String toString(){
		String s = "";
		
		for(String key:_pathways.keySet()){
			s+=key+": "+_pathways.get(key).size()+"\n";
		}
		
		return s;
	}
	
//	public static void main(String[] args) {
//		Pathways p = new Pathways();
//		p.load();
//		System.out.println(p);
//	}
}
