package fr.pasteur.sysbio.Gravity.API;

/**
 * Class defining an Interaction in a PPI. 
 * It consist in the identifiers of the 2 genes and of a boolean array to define 
 * if the interaction exist for every types of interactions considered by our PPI.
 * 
 * <p>This class is not made to be used directly but is more of necessity for out PPI.</p>
 * 
 * @author Freddy Cliquet
 * @version 1.0
 * @see PPI
 */
public class Interaction {
	
	/**
	 * Symbol identifier of the first gene
	 */
	public String symbol1;
	
	/**
	 * Symbol identifier of the second gene
	 */
	public String symbol2;
	
	/**
	 * Array containing true if the interaction is valid for the given type.
	 * @see PPI
	 */
	public boolean types[];
	
	/**
	 * Constructor for a new interaction. This will parse a single line of a tsv file full of interaction.
	 * @param line line from a tsv file to be parsed
	 * @param nbTypes number of interaction types to be found
	 */
	public Interaction(String line, int nbTypes){
		String[] s = line.split("\t");
		symbol1 = s[0];
		symbol2 = s[1];
		types = new boolean[nbTypes];
		for(int i = 2 ; i < 2+nbTypes ; i++){
			if( s[i].equals("1") )types[i-2] = true;
		}
	}
	
	/**
	 * Function to update an interaction. It will parse a line and update the interaction previously not seen as seen, 
	 * it will not turn previously seen interaction as unseen.
	 * @param line line from a tsv file to parse
	 */
	public void update(String line){
		String[] s = line.split("\t");
		for(int i = 2 ; i < 2+types.length ; i++){
			if( s[i].equals("1") )types[i-2] = true;
		}
	}
}