/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

import java.util.List;

/**
 * Interface defining the main functions expected in a Gene and necessary for the App to work properly. 
 * An implementing class need to be written for any type of data source added in the App.
 * 
 * A Gene is basically a collection of Variants with various methods to interact with them and some additional informations.
 * 
 * @author Freddy Cliquet
 * @version 2.0
 */
public interface Gene {

	public List<? extends Variant> getCNVs();
	
	public List<? extends Variant> getFilteredCNVs(Patient patient);
	
	public List<? extends Variant> getCNVs(Patient patient);
	
	/**
	 * check if the Gene carry a Variant <b>that passes the filters</b> for the given affected 
	 * patients that is not carried by the unaffected patients.
	 * 
	 * <p>Carrying here is defined in a recessive context, so it means that the Patient must carry both alleles (HOMALT) 
	 * and the parents must carry at the most one allele (usually both HET, but in case of non-mendelian inheritance, one or both could be HOMREF).</p>
	 * 
	 * <p>Because of the quality, we are asking for at least one affected and one unaffected meeting the criteria that passes the quality filters. 
	 * This means that, because of unaffected not passing the quality, "a lot" of false positive are to be expected.</p>
	 * 
	 * <p>There is a specific case when the unaffected list is empty. In this case, we are searching for all commons variants between all the affected patients.</p>
	 * 
	 * @param affected The list of all affected patients
	 * @param unaffected The list of all unaffected patients
	 * @param quality The list of quality filters to apply to the data
	 * @param allAffected A boolean at true if all the affected patients must carry the variant for it to be considered, if false, only one affected need to carry it.
	 * @return true if the Gene carry at least one Variant for the affected that is not in the unaffected.
	 */
	public boolean carryAFilteredRecessiveVariant(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected);
	
	/**
	 * check if the Gene carry a Variant <b>that passes the filters</b> for the given affected 
	 * patients that is not carried by the unaffected patients.
	 * 
	 * <p>Carrying here means having at least one allele of a Variant in the affected and no alleles at all in the unaffected.</p>
	 * 
	 * <p>Because of the quality, we are asking for at least one affected and one unaffected meeting the criteria that passes the quality filters. 
	 * This means that, because of unaffected not passing the quality, "a lot" of false positive are to be expected.</p>
	 * 
	 * <p>There is a specific case when the unaffected list is empty. In this case, we are searching for all commons variants between all the affected patients.</p>
	 * 
	 * @param affected The list of all affected patients
	 * @param unaffected The list of all unaffected patients
	 * @param quality The list of quality filters to apply to the data
	 * @param allAffected A boolean at true if all the affected patients must carry the variant for it to be considered, if false, only one affected need to carry it.
	 * @return true if the Gene carry at least one Variant for the affected that is not in the unaffected.
	 */
	public boolean carryAFilteredVariant(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected);
	
	/**
	 * check if the Gene carry a Variant <b>that passes the filters</b> for the given Patient.
	 * Carrying here means having at least one allele of a Variant.
	 * @param patient The patient
	 * @return true if the Gene carry at least one Variant for the Patient
	 */
	public boolean carryAFilteredVariant(Patient patient);
	
	public boolean carryAFilteredVariant(Patient patientA, Patient patientB, List<GenotypeFilter> filtersQuality, int filteringMode);
	
	/**
	 * check if the Gene carry a Variant for the given Patient.
	 * Carrying here means having at least one allele of a Variant.
	 * @param patient The patient
	 * @return true if the Gene carry at least one Variant for the Patient
	 */
	public boolean carryAVariant(Patient patient);
	
	public List<? extends Variant> getCompoundHomozygousVariants(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected);
	
	public boolean carryCompoundHomozygousVariants(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected);
	
	/**
	 * Function to remove all the filters applied to the Gene
	 */
	public void clearFilters();
	
	/**
	 * Gives a String of details about this gene.
	 * Formated as multiple line text (with carriage return).
	 * @return a String containing various info about the gene
	 */
	public String getDetails();
	
	/**
	 * Function to get the chromosome where the gene is located, the information might not always be accessible (non HGNC genes for instance).
	 * @return the chromosome of the gene when available
	 */
	public String getChrom();
	
	/**
	 * Get a CNV id if there is a CNV in the gene, else empty string. The CNV id is a string formed like this "chr2:12345678-12456789"
	 * @return the id of the CNV (if there is one, empty else)
	 */
	public String getCNVid(Patient patient);
	
	/**
	 * Give the list of Variants <b>that passes the filters</b> for the given affected 
	 * patients that is not carried by the unaffected patients.
	 * 
	 * <p>Carrying here is defined in a recessive context, so it means that the Patient must carry both alleles (HOMALT) 
	 * and the parents must carry at the most one allele (usually both HET, but in case of non-mendelian inheritance, one or both could be HOMREF).</p>
	 * 
	 * <p>Because of the quality, we are asking for at least one affected and one unaffected meeting the criteria that passes the quality filters. 
	 * This means that, because of unaffected not passing the quality, "a lot" of false positive are to be expected.</p>
	 * 
	 * <p>There is a specific case when the unaffected list is empty. In this case, we are searching for all commons variants between all the affected patients.</p>
	 * 
	 * @param affected The list of all affected patients
	 * @param unaffected The list of all unaffected patients
	 * @param quality The list of quality filters to apply to the data
	 * @param allAffected A boolean at true if all the affected patients must carry the variant for it to be considered, if false, only one affected need to carry it.
	 * @return List of all the Variant that are carried by the affected and not the unaffected.
	 */
	public List<? extends Variant> getFilteredRecessiveVariants(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected);
	
	/**
	 * Function to retrieve all the Variants from the Gene that passes the filters
	 * @return List of Variants from the Gene
	 */
	public List<? extends Variant> getFilteredVariants();
	
	/**
	 * Give the list of Variants <b>that passes the filters</b> for the given affected 
	 * patients that is not carried by the unaffected patients.
	 * 
	 * <p>Carrying here means having at least one allele of a Variant in the affected and no alleles at all in the unaffected.</p>
	 * 
	 * <p>Because of the quality, we are asking for at least one affected and one unaffected meeting the criteria that passes the quality filters. 
	 * This means that, because of unaffected not passing the quality, "a lot" of false positive are to be expected.</p>
	 * 
	 * <p>There is a specific case when the unaffected list is empty. In this case, we are searching for all commons variants between all the affected patients.</p>
	 * 
	 * @param affected The list of all affected patients
	 * @param unaffected The list of all unaffected patients
	 * @param quality The list of quality filters to apply to the data
	 * @param allAffected A boolean at true if all the affected patients must carry the variant for it to be considered, if false, only one affected need to carry it.
	 * @return List of all the Variant that are carried by the affected and not the unaffected.
	 */
	public List<? extends Variant> getFilteredVariants(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected);
	
	public List<? extends Variant> getFilteredVariants(Patient a, Patient b, List<GenotypeFilter> quality, int filteringMode);
	
	/**
	 * Function to retrieve all the Variants from the Gene that passes the filters that belongs to the given patient
	 * @param p The Patient carrying the Variants
	 * @return List of Variants from the Gene
	 */
	public List<? extends Variant> getFilteredVariants(Patient p);
	
	/**
	 * Function to retrieve the list of Filters currently applied to the Gene
	 * @return The List of filters currently applied to the Gene
	 */
	public List<Filter> getFilters();
	
	/**
	 * Gives the Gene name
	 * @return the gene name
	 */
	public String getName();
	/**
	 * Function to retrieve all the Variants from the Gene
	 * @return List of Variants from the Gene
	 */
	public List<? extends Variant> getVariants();
	
	/**
	 * Function to evaluate if a given patient carries a <i>de novo</i> mutation in this Gene or not.
	 * @param patient The Patient to control
	 * @param father The patient father
	 * @param mother The patien mother
	 * @return true if the Patient carries a <i>de novo</i> mutation in the Gene, false else
	 */
	public boolean hasDeNovoMutations(Patient patient, Patient father, Patient mother);
	
	/**
	 * Function to evaluate if a given patient carries a CNV in this Gene or not. 
	 * We make a difference between deletion and other kind of CNVs. 
	 * We put the priority on deletions in case of multiple CNVs.
	 * 
	 * @param patient The patient to control
	 * @return -1 if the patient carry a CNV deletion in the gene, +1 for another type of CNV , 0 else
	 */
	public int hasCNV(Patient patient);
	
	/**
	 * Allows to know if this Gene contains any Variant informations passing all the Filters that were added previously using setFilters or not.
	 * @return true if the Gene contains Variant, false else
	 */
	public boolean hasFilteredVariant();

	/**
	 * Function to test if some filters are currently applied to the Gene or not
	 * @return true if some filters are applied, false else.
	 */
	public boolean hasFilters();
	
	/**
	 * Allows to know if this Gene contains any Variant informations or not.
	 * @return true if the Gene contains Variant, false else
	 */
	public boolean hasVariant();
	
	/**
	 * Highest registered impact of the variants <b>that passes the filters</b> for the given affected 
	 * patients that is not carried by the unaffected patients.
	 * 
	 * <p>Carrying here means having at least one allele of a Variant in the affected and no alleles at all in the unaffected.</p>
	 * 
	 * <p>Because of the quality, we are asking for at least one affected and one unaffected meeting the criteria that passes the quality filters. 
	 * This means that, because of unaffected not passing the quality, "a lot" of false positive are to be expected (in this context, it could means higher impact than expected).</p>
	 * 
	 * <p>There is a specific case when the unaffected list is empty. In this case, we are searching for all commons variants between all the affected patients.</p>
	 * 
	 * @param affected The list of all affected patients
	 * @param unaffected The list of all unaffected patients
	 * @param quality The list of quality filters to apply to the data
	 * @param allAffected A boolean at true if all the affected patients must carry the variant for it to be considered, if false, only one affected need to carry it.
	 * @param recessive A boolean at true to also consider the recessive case
	 * @param allRecessiveAffected A boolean at true if all the affected patients must carry the variant "in a recessive way" for the recessive variants to be considered.
	 * @return most deleterious impact of all the Variant that are carried by the affected and not the unaffected.
	 */
//	public String highestImpact(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected, boolean recessive, boolean allRecessiveAffected);
	
	/**
	 * Get the impact of the most harmful GeneSequenceVariant carried by Patient p on this Gene <b>after filtering</b>.
	 * @param p Patient
	 * @return highest impact of a GeneSequenceVariant for this Gene
	 */
	public String highestImpact(Patient p);
	
	/**
	 * Most deleterious Variant of the variants <b>that passes the filters</b> for the given affected 
	 * patients that is not carried by the unaffected patients.
	 * 
	 * <p>Carrying here means having at least one allele of a Variant in the affected and no alleles at all in the unaffected.</p>
	 * 
	 * <p>Because of the quality, we are asking for at least one affected and one unaffected meeting the criteria that passes the quality filters. 
	 * This means that, because of unaffected not passing the quality, "a lot" of false positive are to be expected (in this context, it could means higher impact than expected).</p>
	 * 
	 * <p>There is a specific case when the unaffected list is empty. In this case, we are searching for all commons variants between all the affected patients.</p>
	 * 
	 * @param affected The list of all affected patients
	 * @param unaffected The list of all unaffected patients
	 * @param quality The list of quality filters to apply to the data
	 * @param allAffected A boolean at true if all the affected patients must carry the variant for it to be considered, if false, only one affected need to carry it.
	 * @param recessive A boolean at true to also consider the recessive case
	 * @param allRecessiveAffected A boolean at true if all the affected patients must carry the variant "in a recessive way" for the recessive variants to be considered.
	 * @param compound A boolean at true to also consider the compound heterozygous cases
	 * @return most deleterious Variant of all the Variant that are carried by the affected and not the unaffected.
	 */
//	public Variant highestImpactVariant(List<Patient> affected, List<Patient> unaffected, List<GenotypeFilter> quality, boolean allAffected, boolean recessive, boolean allRecessiveAffected, boolean compound);
	
	/**
	 * Get the Variant with the most harmful impact carried by Patient p on this Gene <b>after filtering</b>.
	 * @param p Patient
	 * @return Variant with the highest impact for this Gene
	 */
	public Variant highestImpactVariant(Patient p);

	/**
	 * Function to get the maximum observed cadd for a given patient among the filtered variants of this Gene.
	 * @param p Patient to control
	 * @return the maximum observed cadd for the patient
	 */
	public Double maxcadd(Patient p);
	
	public Double maxcadd(Patient p, List<GenotypeFilter> filters);
	
	/**
	 * Function to get the maximum observed cadd for a given patient among the filtered variants of this Gene not inherited (<i>de novo</i>).
	 * 
	 * <p>It is necessary to know both parents to avoid cases with doubts about the origin of the gene (like for HET in the 3 of them) or to check in this case if <i>de novo</i> or not.</p>
	 * @param p Patient to control
	 * @param f His father
	 * @param m  His mother
	 * @return the maximum observed cadd for the patient
	 */
	public Double maxCaddDeNovo(Patient p, Patient f, Patient m);

	/**
	 * Function to get the maximum observed cadd for a given patient among the filtered variants of this Gene inherited from <b>his father</b>.
	 * 
	 * <p>It is necessary to know both parents to avoid cases with doubts about the origin of the gene (like for HET in the 3 of them).</p>
	 * @param p Patient to control
	 * @param f His father
	 * @param m  His mother
	 * @return the maximum observed cadd for the patient
	 */
	public Double maxCaddFather(Patient p, Patient f, Patient m);
	
	/**
	 * Function to get the maximum observed cadd for a given patient among the filtered variants of this Gene inherited from <b>both parents</b>.
	 * 
	 * <p>It is necessary to know both parents to avoid cases with doubts about the origin of the gene (like for HET in the 3 of them).</p>
	 * @param p Patient to control
	 * @param f His father
	 * @param m  His mother
	 * @return the maximum observed cadd for the patient
	 */
	public Double maxCaddHomalt(Patient p, Patient f, Patient m);
	
	/**
	 * Function to get the maximum observed cadd for a given patient among the filtered variants of this Gene inherited from <b>his mother</b>.
	 * 
	 * <p>It is necessary to know both parents to avoid cases with doubts about the origin of the gene (like for HET in the 3 of them).</p>
	 * @param p Patient to control
	 * @param f His father
	 * @param m  His mother
	 * @return the maximum observed cadd for the patient
	 */
	public Double maxCaddMother(Patient p, Patient f, Patient m);
	
	/**
	 * Function to get the maximum observed cadd for a given patient among the filtered variants of this Gene with doubts about its inheritance (often meaning that the patient and both its parents are HET).
	 * 
	 * <p>It is necessary to know both parents to avoid cases with doubts about the origin of the gene (like for HET in the 3 of them).</p>
	 * @param p Patient to control
	 * @param f His father
	 * @param m  His mother
	 * @return the maximum observed cadd for the patient
	 */
	public Double maxCaddUnknown(Patient p, Patient f, Patient m);
	
	public Double minMAF(Patient p, Patient f, Patient m);
	
	/**
	 * Function giving the number of Variant belonging to this Gene after filtering.
	 * @return the number of Variants located in the Gene
	 */
	public int numberOfFilteredVariants();
	
	/**
	 * Function giving the number of Variant belonging to this Gene.
	 * @return the number of Variants located in the Gene
	 */
	public int numberOfVariants();
	
	/**
	 * Set a List of Filter to apply to the Variant of the Gene. 
	 * Calling this will have for effect to clear the previous filters and to apply the new ones.
	 * @param filters List of Filter to apply
	 */
	public void setFilters(List<Filter> filters);
	
	/**
	 * Sort the <b>filtered</b> GeneSequenceVariant List by the specified attribute.
	 * 
	 * @param s the attribute used to sort the List
	 */
	public void sortBy(String s);
	
	/**
	 * Function to add a simple annotation to a gene.
	 * @param a the gene annotation
	 */
	public void addAnnotation(GeneAnnotation<?> a);
	
	/**
	 * Get the list of all the annotations associated to this gene
	 * @return a list of all the annotations
	 */
	public List<GeneAnnotation<?>> getAnnotations();
}
