/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;


/**
 * Class defining a Filter specifically on a Variant property.
 * 
 * <p>It is able to treat Double, Float, Integer and String values (operator == and != only for the last one)</p>
 * 
 * <p>An important point to take in consideration: when a type or an operator is not defined, 
 * or that the value to compare with is not compatible, the passFilter method will always return true, not filtering out the variant.</p>
 * 
 * @author Freddy Cliquet
 * @version 1.0
 */
public class VariantFilter implements Filter {

	String _property;
	String _operator;
	String _value;
	
	/**
	 * Default constructor, do nothing
	 */
	public VariantFilter(){
	}
	
	/**
	 * Constructor for a Filter on a specific property.
	 * @param property The property from the Variant to filter on
	 * @param operator The operator to apply
	 * @param value The value used as a filter
	 */
	public VariantFilter(String property, String operator, String value){
		_property = property;
		_operator = operator;
		_value = value;
	}
	
	@Override
	public String toString(){
		return _property+" "+_operator+" "+_value;
	}
	
	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#passFilter(fr.pasteur.sysbio.VCFviz.API.Variant)
	 */
	@Override
	public boolean passFilter(Variant gsv){
		try{
			Object o = gsv.getObject(_property);
			if( o == null && _value.toLowerCase().equals("null") ){
				if( _operator.equals("=") )
					return true;
				if( _operator.equals("!=") )
					return false;
			}
			switch(_operator){
				case ">=":
					if( o instanceof Double )
						return ((Double)o) >= Double.parseDouble(_value);
					if( o instanceof Float )
						return ((Float)o) >= Double.parseDouble(_value);
					if( o instanceof Integer )
						return ((Integer)o) >= Double.parseDouble(_value);			
					return true;
				case "<=":
					if( o instanceof Double )
						return ((Double)o) <= Double.parseDouble(_value);
					if( o instanceof Float )
						return ((Float)o) <= Double.parseDouble(_value);
					if( o instanceof Integer )
						return ((Integer)o) <= Double.parseDouble(_value);			
					return true;
				case "=":
					if( o instanceof Double )
						return ((Double)o) == Double.parseDouble(_value);
					if( o instanceof Float )
						return ((Float)o) == Double.parseDouble(_value);
					if( o instanceof Integer )
						return ((Integer)o) == Double.parseDouble(_value);			
					if( o instanceof String ){
						return ((String)o).equals(_value);
					}
					return true;
				case "!=":
					if( o instanceof Double )
						return ((Double)o) != Double.parseDouble(_value);
					if( o instanceof Float )
						return ((Float)o) != Double.parseDouble(_value);
					if( o instanceof Integer )
						return ((Integer)o) != Double.parseDouble(_value);			
					if( o instanceof String )
						return !((String)o).equals(_value);			
					return true;
				case "contains":
					if( o instanceof String )
						return ((String)o).contains(_value);
			}
		}catch(NumberFormatException e){
			return true;
		}
		return false;
	}

	@Override
	public void setProperty(String property) {
		_property = property;
	}

	@Override
	public void setOperator(String operator) {
		_operator = operator;
	}

	@Override
	public void setValue(String value) {
		_value = value;
	}

	@Override
	public String getProperty() {
		return _property;
	}

	@Override
	public String getOperator() {
		return _operator;
	}

	@Override
	public String getValue() {
		return _value;
	}
}
