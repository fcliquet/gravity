/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.pasteur.sysbio.Gravity.API.PPI;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class ConnectAllNodes {

	public static Set<String> connectAll(PPI ppi, List<String> typesInteraction, Collection<String> geneList){
		
		FindAllShortestPaths fasp = new FindAllShortestPaths(ppi, typesInteraction);	
		fasp.buildPartitions(geneList);
        fasp.printPartitions();
        Set<String> parts = fasp.addPartitions2Graph();
		List<String> lparts = new ArrayList(parts);
        fasp = null;
        Set<String> toAdd = new HashSet<String>();
//        Set<String> geneL = new HashSet<String>(geneList);
        Set<String> retG = new HashSet<String>();
        if( parts.size() <= 1 )
        	return null;
        
        for(int i = 0 ; i < lparts.size()-1 ; i++){
        	for(int j = i+1 ; j < lparts.size() ; j++){
        		fasp = new FindAllShortestPaths(ppi, typesInteraction);
        		fasp.buildPartitions(geneList);
        		parts = fasp.addPartitions2Graph();
        		lparts = new ArrayList(parts);
        		retG = fasp.findAll(lparts.get(i), lparts.get(j));
//        		geneL.addAll( retG );
        		toAdd.addAll( retG );
        		System.out.println(i+":"+lparts.get(i)+"-"+j+":"+lparts.get(j) + " = "+retG);
        	}
        }
        
        
//        int i = 0;
//        boolean keepgoing = true;
//        while( keepgoing ){
////	        System.out.println("retG "+retG);
////	        System.out.println("keepg "+keepgoing);
////	        System.out.println("i "+i);
////	        System.out.println("parts,size() "+parts.size());
//        	while( i < parts.size()-1 ){
//	        	int j = 1;
//	        	
////	        	System.out.println("\t retG "+retG);
////		        System.out.println("\t keepg "+keepgoing);
////		        System.out.println("\t j "+j);
////		        System.out.println("\t parts,size() "+parts.size());
//	        	while( j < parts.size() ){
//	        		fasp = new FindAllShortestPaths(ppi, typesInteraction);
//	        		fasp.buildPartitions(geneL);
//	        		parts = fasp.addPartitions2Graph();
//	        		lparts = new ArrayList(parts);
////	        		System.out.println("\t \t nb partitions: "+parts.size() );
//	        		System.out.println("\t \t nb partitions: "+parts.size()+" ; parts: "+ lparts.get(i)+"-"+lparts.get(j) );
//	        		retG = fasp.findAll(lparts.get(i), lparts.get(j));
//	        		if( retG==null || retG.size()==0 ){
//	        			keepgoing = false;
//	        		}else{
//	        			System.out.println("\t \t added "+retG);
//	        			if( parts.size() > 2 )
//	        				keepgoing = true;
//	        			else
//	        				keepgoing = false;
//	        			geneL.addAll( retG );
//		        		toAdd.addAll( retG );
//		        		break;
//	        		}
//	        		j++;
//	        	}
//	        	if(keepgoing){
//	        		i=0;
//	        		break;
//	        	}
//	        	i++;
//	        }
//        }
//       
        return toAdd;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PPI ppi = new PPI();
        ppi.load();
        List<String> geneList = Arrays.asList("TRIO", "CUL3", "MIB1", "MBD5", "NAA15", "ILF2", "AKAP9", "WAC", "NINL");
		System.out.println(ConnectAllNodes.connectAll(ppi, ppi.getInteractionTypes(), geneList));
	}

}
