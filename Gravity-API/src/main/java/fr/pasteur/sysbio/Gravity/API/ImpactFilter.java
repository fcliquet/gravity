/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

import java.util.List;

/**
 * Class defining the filter on the impact of the variants.
 * 
 * <p>In theory it could be coded using the VariantFilter class, 
 * but for the common cases where we want to accept multiple impacts, 
 * it would force us to create as many filters. 
 * Instead here we provide a list of impacts accepted and test it once.</p>
 * 
 * @author Freddy Cliquet
 * @version 1.0
 */
public class ImpactFilter implements Filter {

	private List<String> _acceptedImpacts;
	
	/**
	 * Constructor of the ImpactFilter
	 * @param impacts List of the impacts accepted by the filter
	 */
	public ImpactFilter(List<String> impacts){
		_acceptedImpacts = impacts;
	}
	
	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#passFilter(fr.pasteur.sysbio.VCFviz.API.Variant)
	 */
	@Override
	public boolean passFilter(Variant gsv) {
		return _acceptedImpacts.contains(gsv.impact());
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#setProperty(java.lang.String)
	 */
	@Override
	public void setProperty(String property) {
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#setOperator(java.lang.String)
	 */
	@Override
	public void setOperator(String operator) {
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
	}

	@Override
	public String getProperty() {
		return null;
	}

	@Override
	public String getOperator() {
		return null;
	}

	@Override
	public String getValue() {
		return null;
	}

}
