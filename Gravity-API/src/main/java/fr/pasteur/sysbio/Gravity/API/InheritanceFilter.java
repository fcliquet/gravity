/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

/**
 * Class to define a filter based on the inheritance of the variant for the individual.
 * 
 * <p>Provided the patient and the parents, this class will evaluate various types of inheritance. 
 * It goes from the <i>de novo</i>, non-mendelian or mendelian aspect on one side, 
 * to the inheritance coming from the mother (strictly or not) or from the father (strictly or not).</p>
 * 
 * @author Freddy Cliquet
 * @version 1.0
 */
public class InheritanceFilter implements Filter {

	Patient _patient;
	Patient _mother;
	Patient _father;
	String _inheritance;
	
	/**
	 * Constructor for a Inheritance Filter. The inheritance tested itself is coded as a String.
	 * 
	 * <ul> 
	 * 	<li> de novo </li> 
	 * 	<li> non-mendelian </li> 
	 * 	<li> strictly mendelian </li> 
	 * 	<li> none </li> 
	 * 	<li> father and mother </li> 
	 * 	<li> father </li> 
	 * 	<li> father only </li> 
	 * 	<li> mother </li> 
	 * 	<li> mother only </li> 
	 * 	<li> unknown </li> 
	 * </ul> 
	 * 
	 * <p>This constructor accept <b>null</b> for the patients, but they will need to be set with the setPatients method before testing the filter.</p>
	 * 
	 * @param inheritance String representing the inheritance tested
	 * @param patient The patient
	 * @param father The patient's father
	 * @param mother The patient's mother
	 */
	public InheritanceFilter(String inheritance, Patient patient, Patient father, Patient mother) {
		_patient = patient;
		_father = father;
		_mother = mother;
		_inheritance = inheritance;
	}
	
	/**
	 * Define the patient and its parents. Useful when testing multiple patients quickly. 
	 * @param patient The patient
	 * @param father The patient's father
	 * @param mother The patient's mother
	 */
	public void setPatients(Patient patient, Patient father, Patient mother){
		_patient = patient;
		_father = father;
		_mother = mother;
	}
	
	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#passFilter(fr.pasteur.sysbio.VCFviz.API.Variant)
	 */
	@Override
	public boolean passFilter(Variant gsv) {
		
		switch( _inheritance ){
			case "de novo":
				return GenotypeUtils.isDeNovo(gsv.getPatientGenotype(_patient), gsv.getPatientGenotypeEnlarged4CNVs(_father), gsv.getPatientGenotypeEnlarged4CNVs(_mother));
			case "non-mendelian":
				return GenotypeUtils.nonMendelian(gsv.getPatientGenotype(_patient), _patient.getPatientSex(), gsv.getChromosome(), gsv.getPatientGenotypeEnlarged4CNVs(_father), gsv.getPatientGenotypeEnlarged4CNVs(_mother));
			case "strictly mendelian":
				return !GenotypeUtils.nonMendelian(gsv.getPatientGenotype(_patient), _patient.getPatientSex(), gsv.getChromosome(), gsv.getPatientGenotypeEnlarged4CNVs(_father), gsv.getPatientGenotypeEnlarged4CNVs(_mother));
			
			case "none":
				int r = GenotypeUtils.inheritance(gsv.getPatientGenotype(_patient), _patient.getPatientSex(), gsv.getChromosome(), gsv.getPatientGenotypeEnlarged4CNVs(_father), gsv.getPatientGenotypeEnlarged4CNVs(_mother));
				return r == 0;
			case "father and mother":
				int r2 = GenotypeUtils.inheritance(gsv.getPatientGenotype(_patient), _patient.getPatientSex(), gsv.getChromosome(), gsv.getPatientGenotypeEnlarged4CNVs(_father), gsv.getPatientGenotypeEnlarged4CNVs(_mother));
				return r2 == 3;
			case "father":
				int r3 = GenotypeUtils.inheritance(gsv.getPatientGenotype(_patient), _patient.getPatientSex(), gsv.getChromosome(), gsv.getPatientGenotypeEnlarged4CNVs(_father), gsv.getPatientGenotypeEnlarged4CNVs(_mother));
				return (r3 == 1 || r3 == 3);
			case "father only":
				int r4 = GenotypeUtils.inheritance(gsv.getPatientGenotype(_patient), _patient.getPatientSex(), gsv.getChromosome(), gsv.getPatientGenotypeEnlarged4CNVs(_father), gsv.getPatientGenotypeEnlarged4CNVs(_mother));
				return r4 == 1;
			case "mother":
				int r5 = GenotypeUtils.inheritance(gsv.getPatientGenotype(_patient), _patient.getPatientSex(), gsv.getChromosome(), gsv.getPatientGenotypeEnlarged4CNVs(_father), gsv.getPatientGenotypeEnlarged4CNVs(_mother));
				return (r5 == 2 || r5 == 3);
			case "mother only":
				int r6 = GenotypeUtils.inheritance(gsv.getPatientGenotype(_patient), _patient.getPatientSex(), gsv.getChromosome(), gsv.getPatientGenotypeEnlarged4CNVs(_father), gsv.getPatientGenotypeEnlarged4CNVs(_mother));
				return r6 == 2;
			case "unknown":
				int r7 = GenotypeUtils.inheritance(gsv.getPatientGenotype(_patient), _patient.getPatientSex(), gsv.getChromosome(), gsv.getPatientGenotypeEnlarged4CNVs(_father), gsv.getPatientGenotypeEnlarged4CNVs(_mother));
				return r7 == -1;
			
			default:
				return true;
		}
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#setProperty(java.lang.String)
	 */
	@Override
	public void setProperty(String property) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#setOperator(java.lang.String)
	 */
	@Override
	public void setOperator(String operator) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#getProperty()
	 */
	@Override
	public String getProperty() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#getOperator()
	 */
	@Override
	public String getOperator() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Filter#getValue()
	 */
	@Override
	public String getValue() {
		// TODO Auto-generated method stub
		return null;
	}

}
