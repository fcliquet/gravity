package fr.pasteur.sysbio.Gravity.API.windows;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.Variant;
import net.miginfocom.swing.MigLayout;

public class GeneDetailsFrame extends JFrame {

	private static final long serialVersionUID = 4358993264199136487L;
	private Gene _gene;

	public GeneDetailsFrame(Gene gene){
		_gene = gene;
		
		setTitle("Gene details: "+_gene.getName());
		
		JPanel panel = new JPanel();
		panel.setLayout(new MigLayout());
		
		// Gene details
		
		// List of buttons for external links ? 
		// (redundant with right click menu in Cytoscape)
		
		// List Variants
		// Allow to open a window for a single variant details
		
		// display the info field in a table
		AbstractTableModel infoModel = new AbstractTableModel(){

			private static final long serialVersionUID = 6566197362071277669L;

			@Override
			public int getRowCount() {
				// TODO Auto-generated method stub
				return _gene.getVariants().size();
			}

			@Override
			public int getColumnCount() {
				// TODO Auto-generated method stub
				return 9;
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				Variant gsv = _gene.getVariants().get(rowIndex);
				switch(columnIndex){
					case 0:
						return gsv.getChromosome();
					case 1:
						return gsv.getPosition();
					case 2:
						return gsv.getReference();
					case 3:
						return gsv.getAlternative();
					case 4:
						return gsv.getQuality();
					case 5:
						return gsv.getFilter();
					default:
						return "0";
				}
			}
			
		};
		JTable infoTable = new JTable(infoModel);
		
		ListSelectionModel infoLSM = infoTable.getSelectionModel();
		infoLSM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		infoTable.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if( e.getClickCount() == 2 ){
					System.out.println("double click "+ infoModel.getValueAt( infoTable.getSelectedRow() , 1 ) );
					new VariantDetailsFrame(_gene.getVariants().get(infoTable.getSelectedRow()));
				}
			};
		});
		infoTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		panel.add(new JScrollPane(infoTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED), "wrap" );		
		panel.add(new JButton("Close"));
		
		add(panel);
		pack();
		setVisible(true);
	}

	public static void main(String[] args) {
//		VcfGene g = new VcfGene();
//		new GeneDetailsFrame(g);
	}
	
}
