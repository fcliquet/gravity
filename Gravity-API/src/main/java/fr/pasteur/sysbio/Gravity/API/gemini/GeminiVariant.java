/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.gemini;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.collections.api.bimap.MutableBiMap;
import org.eclipse.collections.impl.bimap.mutable.HashBiMap;

import fr.pasteur.sysbio.Gravity.API.Gene;
import fr.pasteur.sysbio.Gravity.API.Genotype;
import fr.pasteur.sysbio.Gravity.API.GenotypeUtils;
import fr.pasteur.sysbio.Gravity.API.Patient;
import fr.pasteur.sysbio.Gravity.API.Variant;

/**
 * Implementation of the Variant interface for the Gemini database. This class is highly optimized to store the various attributes, and for this uses a GeminiVariantMetadata object.
 * 
 * @author Freddy Cliquet
 * @version 3.0
 * @see GeminiVariantMetadata
 */
public class GeminiVariant implements Variant {

	private static GeminiVariantMetadata metadata;
	private static List<Patient> patients;
	private int[] integersAttributes;
	private float[] floatsAttributes;
	private boolean[] booleansAttributes;
	private int[] textAttributes;
	private static MutableBiMap<String,Integer>[] textAttributesMap = null;
	private static MutableBiMap<Integer,String>[] textAttributesMapReversed = null;
	private Map<Patient,Genotype> genotypes;
	private static Map<String,Integer> impactPriorities = null;
	private double _cohort_frequency;
	private boolean _cohortFrequencyComputed = false;
	private Gene _gene;
	private List<GeminiVariant> _overlappingCNVs = new ArrayList<GeminiVariant>();
	
	public void addOverlappingCNV(GeminiVariant v){
		for( GeminiVariant gv:_overlappingCNVs )
			if( (int)gv.getObject("variant_id") == (int)v.getObject("variant_id") )
				return;
		_overlappingCNVs.add(v);
	}
	
	public List<GeminiVariant> getOverlappingCNVs(){
		return _overlappingCNVs;
	}
	
	/**
	 * Method to define the set of metadata that will need to be stored for the variants.
	 * 
	 * @param variantMetadata the metadata of the variants
	 */
	public static void setMetadata(GeminiVariantMetadata variantMetadata){
		metadata = variantMetadata;
	}
	
	/**
	 * Method to set the list of all patients. 
	 * 
	 * @param listPatients list of all the patients
	 */
	public static void setPatients(List<Patient> listPatients){
		patients = listPatients;
	}
	
	/**
	 * method to return the metadata containe
	 * 
	 * @return the metadata of the variants
	 */
	public static GeminiVariantMetadata getMetadata(){
		return metadata;
	}
	
	/**
	 * Method to retrieve the full list of patients
	 * 
	 * @return the list of the patients
	 */
	public static List<Patient> getPatients(){
		return patients;
	}
	
	/**
	 * Create a new empty Variant and initialize all the arrays used to store the attributes thanks to the metadata.
	 * 
	 * @throws Exception the GeminiVariantMetadata need to be set up first
	 */
	@SuppressWarnings("unchecked")
	public GeminiVariant() throws Exception {
		if( metadata == null )
			throw new Exception("You need to set the metadata first.");
		if( textAttributesMap == null ){
			textAttributesMap = new MutableBiMap[ metadata.getSize( GeminiVariantMetadata.TYPE_TEXT ) ];
			textAttributesMapReversed = new MutableBiMap[ metadata.getSize( GeminiVariantMetadata.TYPE_TEXT ) ];
			for(int i=0 ; i<textAttributesMap.length ; i++){
				textAttributesMap[i] = new HashBiMap<String,Integer>();
				textAttributesMapReversed[i] = null;
			}
		}
		
		integersAttributes = new int[ metadata.getSize( GeminiVariantMetadata.TYPE_INTEGER ) ];
		floatsAttributes = new float[ metadata.getSize( GeminiVariantMetadata.TYPE_FLOAT ) ];
		booleansAttributes = new boolean[ metadata.getSize( GeminiVariantMetadata.TYPE_BOOLEAN ) ];
		textAttributes = new int[ metadata.getSize( GeminiVariantMetadata.TYPE_TEXT ) ];
	}
	
	/**
	 * Setter for an Integer attribute. This is the method to use when we know the name of the attributes.
	 * 
	 * <p>NB: Using the name create some overhead to write the value. It is acceptable when done from time 
	 * to time, but not when it is done frequently such as when loading the data. In those conditions, it 
	 * is better to simply use the query index.</p>
	 * 
	 * @param name name of the attribute
	 * @param value value of the attribute
	 */
	public void setIntegerAttribute(String name, int value){
		integersAttributes[ metadata.getIndex(name) ] = value;
	}
	
	/**
	 * Setter for an Integer attribute. This is the method to use when we know the index of the attribute (this avoid the overhead of retrieving the index from the name).
	 * 
	 * @param queryIndex index of the attribute
	 * @param value value of the attribute
	 */
	public void setIntegerAttribute(short queryIndex, int value){
		integersAttributes[ metadata.getIndex(queryIndex) ] = value;
	}
	
	/**
	 * Setter for a Float attribute. This is the method to use when we know the name of the attributes.
	 * 
	 * <p>NB: Using the name create some overhead to write the value. It is acceptable when done from time 
	 * to time, but not when it is done frequently such as when loading the data. In those conditions, it 
	 * is better to simply use the query index.</p>
	 * 
	 * @param name name of the attribute
	 * @param value value of the attribute
	 */
	public void setFloatAttribute(String name, float value){
		floatsAttributes[ metadata.getIndex(name) ] = value;
	}
	
	/**
	 * Setter for a Float attribute. This is the method to use when we know the index of the attribute (this avoid the overhead of retrieving the index from the name).
	 * 
	 * @param queryIndex index of the attribute
	 * @param value value of the attribute
	 */
	public void setFloatAttribute(short queryIndex, float value){
		floatsAttributes[ metadata.getIndex(queryIndex) ] = value;
	}
	
	/**
	 * Setter for a Boolean attribute. This is the method to use when we know the name of the attributes.
	 * 
	 * <p>NB: Using the name create some overhead to write the value. It is acceptable when done from time 
	 * to time, but not when it is done frequently such as when loading the data. In those conditions, it 
	 * is better to simply use the query index.</p>
	 * 
	 * @param name name of the attribute
	 * @param value value of the attribute
	 */
	public void setBooleanAttribute(String name, boolean value){
		booleansAttributes[ metadata.getIndex(name) ] = value;
	}
	
	/**
	 * Setter for a Boolean attribute. This is the method to use when we know the index of the attribute (this avoid the overhead of retrieving the index from the name).
	 * 
	 * @param queryIndex index of the attribute
	 * @param value value of the attribute
	 */
	public void setBooleanAttribute(short queryIndex, boolean value){
		booleansAttributes[ metadata.getIndex(queryIndex) ] = value;
	}
	
	/**
	 * Setter for a String attribute. This is the method to use when we know the name of the attributes.
	 * 
	 * <p>NB: Using the name create some overhead to write the value. It is acceptable when done from time 
	 * to time, but not when it is done frequently such as when loading the data. In those conditions, it 
	 * is better to simply use the query index.</p>
	 * 
	 * @param name name of the attribute
	 * @param value value of the attribute
	 */
	public void setTextAttribute(String name, String value){
		int index = textAttributesMap[ metadata.getIndex(name) ].getIfAbsentPut(value, textAttributesMap[ metadata.getIndex(name) ].size());
		textAttributes[ metadata.getIndex(name) ] = index;
	}
	
	/**
	 * Setter for a Text attribute. This is the method to use when we know the index of the attribute (this avoid the overhead of retrieving the index from the name).
	 * 
	 * @param queryIndex index of the attribute
	 * @param value value of the attribute
	 */
	public void setTextAttribute(short queryIndex, String value){
		int index = textAttributesMap[ metadata.getIndex(queryIndex) ].getIfAbsentPut(value, textAttributesMap[ metadata.getIndex(queryIndex) ].size());
		textAttributes[ metadata.getIndex(queryIndex) ] = index;
	}
	
	/**
	 * Method to set at once all the genotypes of all the patients for the current variant.
	 * 
	 * @param gts map associating the patients and the genotypes
	 */
	public void setGenotypes(Map<Patient,Genotype> gts){
		genotypes = gts;
	}
	
	/**
	 * Method that retrieve for a given impact its priority, meaning it convert a full impact text (String) into an integer that represent its priority (smaller integer means stronger impact).
	 * 
	 * @param impact impact string
	 * @return priority of the impact
	 */
	static int impactPriority(String impact){
		if( impactPriorities==null ){
			impactPriorities = new HashMap<String,Integer>();
			BufferedReader br = new BufferedReader(new InputStreamReader(GeminiVariant.class.getClassLoader().getResourceAsStream("/impact_priorities.txt")));
			try {
				String line;
				int prior = 1;
				while( (line = br.readLine()) != null ){
					if( ! line.startsWith("#") ){
						impactPriorities.put(line, prior++);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return impactPriorities.get(impact);
	}
	
	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#setGenotypes(java.util.Map, java.util.List)
	 */
	@Override
	public void setGenotypes(Map<String, Object> genotypes, List<Patient> patients) {
		// TODO Auto-generated method stub
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getHTMLtooltip(fr.pasteur.sysbio.VCFviz.API.Patient, fr.pasteur.sysbio.VCFviz.API.Patient, fr.pasteur.sysbio.VCFviz.API.Patient)
	 */
	@Override
	public String getHTMLtooltip(Patient patient, Patient father, Patient mother) {
		
		String s = "<html>";
		s += "Chromosome: "+getChromosome()+"<br>";
		s += "Position: "+getPosition()+"<br>";
		if( isCNV() ){
			s += "SV (CN): "+getObject("sub_type")+" ("+getObject("vep_cnvision_cn")+")<br>";
			s += "SV length: "+getObject("sv_length")+"<br>";
			s += "CNVision #algos: "+getObject("vep_cnvision_#algos")+"<br>";
		}else{
			s += "Reference: "+getReference()+"<br>";
			s += "Alternative: "+getAlternative()+"<br>";
			s += "Quality: "+getQuality()+"<br>";
			s += "Filter: "+getFilter()+"<br>";
			s += "Type: "+getObject("type")+" - "+getObject("sub_type")+"<br>";
		}
		s += "<hr>";
		s += "Gene.refGene: "+getGene()+"<br>";
		s += "impact: "+impact()+"<br>";
		s += "AA Change: "+getObject("aa_change")+" @ "+getObject("aa_length")+" (exon: "+getObject("exon")+")<br>";
		s += "InbreedingCoeff: "+getObject("inbreeding_coeff")+"<br>";
		if( isCNV() )
			s += "cohort frequency: "+cohortFrequency()+"<br>";
		if( !isCNV() ){
			s += "cadd phred: "+cadd_scaled()+"<br>";
			s += "MPC: "+getObject("MPC")+"<br>";
			s += "Polyphen: "+getObject("polyphen_score")+" ; "+getObject("polyphen_pred")+"<br>";
			s += "SIFT: "+getObject("sift_score")+" ; "+getObject("sift_pred")+"<br>";
		}
		if( getObject("vep_techno") != null )
			s += "Caller: "+getObject("vep_techno")+"<br>";
		if( !isCNV() ){
			s += "<hr>";
			s += "<b>Frequencies</b><br>";
			s += "ExAC_ALL: "+getObject("aaf_adj_exac_all")+"<br>";
			s += "1000g_all: "+getObject("aaf_1kg_all")+"<br>";
			s += "esp_all: "+getObject("aaf_esp_all")+"<br>";
			s += "cohort: "+cohortFrequency()+"<br>";
		}
		s += "<hr>";
		s += "<b>Clinvar</b><br>";
		s += "Significance: "+getObject("clinvar_sig")+"<br>";
		s += "Disease: "+getObject("clinvar_disease_name")+"<br>";
//		s += "Source: "+getObject("clinvar_dbsource")+"<br>";
		s += "Origin: "+getObject("clinvar_origin")+"<br>";
//		s += "Dsdb: "+getObject("clinvar_dsdb")+"<br>";
//		s += "Disease acc: "+getObject("clinvar_disease_acc")+"<br>";
//		s += "In locus spec: "+getObject("clinvar_in_locus_spec_db")+"<br>";
		s += "On diag assay: "+getObject("clinvar_on_diag_assay")+"<br>";
		s += "Causal allele: "+getObject("clinvar_causal_allele")+"<br>";
		
		if( getObject("clinvar_gene_phenotype") != null){
			s += "Gene phenotype:<br>";
			String splitted[] = getObject("clinvar_gene_phenotype").toString().split("\\|");
			for(String ss:splitted)
				s += "&nbsp;&nbsp;&nbsp;&nbsp;"+ss+"<br>";
		}else{
			s += "Gene phenotype: "+getObject("clinvar_gene_phenotype")+"<br>";
		}
		
		s += "<hr>";
		s += "<b>Genotypes</b><br>";
		if( isCNV()){
			s += "Patient: "+getPatientGenotype(patient)+" "+getChromosome()+":"+getPosition()+"-"+(getPosition()+(int)getObject("sv_length"))+"<br>";
			Variant v = getSimilarCNV(father);
			if(v==null)
				s += "Father: "+getPatientGenotypeEnlarged4CNVsFiltered(father)+"<br>";
			else
				s += "Father: "+getPatientGenotypeEnlarged4CNVsFiltered(father)+" "+v.getChromosome()+":"+v.getPosition()+"-"+(v.getPosition()+(int)v.getObject("sv_length"))+"<br>";
			v = getSimilarCNV(mother);
			if(v==null)
				s += "Mother: "+getPatientGenotypeEnlarged4CNVsFiltered(mother)+"<br>";
			else
				s += "Mother: "+getPatientGenotypeEnlarged4CNVsFiltered(mother)+" "+v.getChromosome()+":"+v.getPosition()+"-"+(v.getPosition()+(int)v.getObject("sv_length"))+"<br>";
			
		}else{
			s += "Patient: "+genotypes.get(patient).toString().replace("<","&lt;").replace(">","&gt;")+"<br>";
			if(father != null)
				s += "Father: "+genotypes.get(father).toString().replace("<","&lt;").replace(">","&gt;")+"<br>";
			if(mother != null)
				s += "Mother: "+genotypes.get(mother).toString().replace("<","&lt;").replace(">","&gt;")+"<br>";
		}
		s += "</html>";
		
		return s;
	}
	
	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getHTMLtooltip(fr.pasteur.sysbio.VCFviz.API.Patient, fr.pasteur.sysbio.VCFviz.API.Patient, fr.pasteur.sysbio.VCFviz.API.Patient)
	 */
	@Override
	public String getHTMLtooltip(Patient patientA, Patient patientB) {
		
		String s = "<html>";
		s += "Chromosome: "+getChromosome()+"<br>";
		s += "Position: "+getPosition()+"<br>";
		if( isCNV() ){
			s += "SV (CN): "+getObject("sub_type")+" ("+getObject("vep_cnvision_cn")+")<br>";
			s += "SV length: "+getObject("sv_length")+"<br>";
			s += "CNVision #algos: "+getObject("vep_cnvision_#algos")+"<br>";
		}else{
			s += "Reference: "+getReference()+"<br>";
			s += "Alternative: "+getAlternative()+"<br>";
			s += "Quality: "+getQuality()+"<br>";
			s += "Filter: "+getFilter()+"<br>";
			s += "Type: "+getObject("type")+" - "+getObject("sub_type")+"<br>";
		}
		s += "<hr>";
		s += "Gene.refGene: "+getGene()+"<br>";
		s += "impact: "+impact()+"<br>";
		s += "AA Change: "+getObject("aa_change")+" @ "+getObject("aa_length")+" (exon: "+getObject("exon")+")<br>";
		s += "InbreedingCoeff: "+getObject("inbreeding_coeff")+"<br>";
		if( isCNV() )
			s += "cohort frequency: "+cohortFrequency()+"<br>";
		if( !isCNV() ){
			s += "cadd phred: "+cadd_scaled()+"<br>";
			s += "Polyphen: "+getObject("polyphen_score")+" ; "+getObject("polyphen_pred")+"<br>";
			s += "SIFT: "+getObject("sift_score")+" ; "+getObject("sift_pred")+"<br>";
		}
		if( getObject("vep_techno") != null )
			s += "Caller: "+getObject("vep_techno")+"<br>";
		if( !isCNV() ){
			s += "<hr>";
			s += "<b>Frequencies</b><br>";
			s += "ExAC_ALL: "+getObject("aaf_adj_exac_all")+"<br>";
			s += "1000g_all: "+getObject("aaf_1kg_all")+"<br>";
			s += "esp_all: "+getObject("aaf_esp_all")+"<br>";
			s += "cohort: "+cohortFrequency()+"<br>";
		}
		s += "<hr>";
		s += "<b>Clinvar</b><br>";
		s += "Significance: "+getObject("clinvar_sig")+"<br>";
		s += "Disease: "+getObject("clinvar_disease_name")+"<br>";
//		s += "Source: "+getObject("clinvar_dbsource")+"<br>";
		s += "Origin: "+getObject("clinvar_origin")+"<br>";
//		s += "Dsdb: "+getObject("clinvar_dsdb")+"<br>";
//		s += "Disease acc: "+getObject("clinvar_disease_acc")+"<br>";
//		s += "In locus spec: "+getObject("clinvar_in_locus_spec_db")+"<br>";
		s += "On diag assay: "+getObject("clinvar_on_diag_assay")+"<br>";
		s += "Causal allele: "+getObject("clinvar_causal_allele")+"<br>";
		
		if( getObject("clinvar_gene_phenotype") != null){
			s += "Gene phenotype:<br>";
			String splitted[] = getObject("clinvar_gene_phenotype").toString().split("\\|");
			for(String ss:splitted)
				s += "&nbsp;&nbsp;&nbsp;&nbsp;"+ss+"<br>";
		}else{
			s += "Gene phenotype: "+getObject("clinvar_gene_phenotype")+"<br>";
		}
		
		s += "<hr>";
		s += "<b>Genotypes</b><br>";
		if( isCNV()){
			s += "Patient A: "+getPatientGenotype(patientA)+" "+getChromosome()+":"+getPosition()+"-"+(getPosition()+(int)getObject("sv_length"))+"<br>";
			s += "Patient B: "+getPatientGenotype(patientB)+" "+getChromosome()+":"+getPosition()+"-"+(getPosition()+(int)getObject("sv_length"))+"<br>";
//			Variant v = getSimilarCNV(father);
//			if(v==null)
//				s += "Father: "+getPatientGenotypeEnlarged4CNVsFiltered(father)+"<br>";
//			else
//				s += "Father: "+getPatientGenotypeEnlarged4CNVsFiltered(father)+" "+v.getChromosome()+":"+v.getPosition()+"-"+(v.getPosition()+(int)v.getObject("sv_length"))+"<br>";
//			v = getSimilarCNV(mother);
//			if(v==null)
//				s += "Mother: "+getPatientGenotypeEnlarged4CNVsFiltered(mother)+"<br>";
//			else
//				s += "Mother: "+getPatientGenotypeEnlarged4CNVsFiltered(mother)+" "+v.getChromosome()+":"+v.getPosition()+"-"+(v.getPosition()+(int)v.getObject("sv_length"))+"<br>";
			
		}else{
			s += "Patient A: "+genotypes.get(patientA).toString().replace("<","&lt;").replace(">","&gt;")+"<br>";
			s += "Patient B: "+genotypes.get(patientB).toString().replace("<","&lt;").replace(">","&gt;")+"<br>";
		}
		s += "</html>";
		
		return s;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getHTMLtooltip(java.util.List, java.util.List)
	 */
	@Override
	public String getHTMLtooltip(List<Patient> affected, List<Patient> unaffected) {
		String s = "<html>";
		s += "Chromosome: "+getChromosome()+"<br>";
		s += "Position: "+getPosition()+"<br>";
		s += "Reference: "+getReference()+"<br>";
		s += "Alternative: "+getAlternative()+"<br>";
		s += "Quality: "+getQuality()+"<br>";
		s += "Filter: "+getFilter()+"<br>";
		s += "Type: "+getObject("type")+" - "+getObject("sub_type")+"<br>";
		s += "<hr>";
		s += "Gene.refGene: "+getGene()+"<br>";
		s += "impact: "+impact()+"<br>";
		s += "AA Change: "+getObject("aa_change")+" @ "+getObject("aa_length")+" (exon: "+getObject("exon")+")<br>";
		s += "InbreedingCoeff: "+getObject("inbreeding_coeff")+"<br>";
		s += "cadd phred: "+cadd_scaled()+"<br>";
		s += "Polyphen: "+getObject("polyphen_score")+" ; "+getObject("polyphen_pred")+"<br>";
		s += "SIFT: "+getObject("sift_score")+" ; "+getObject("sift_pred")+"<br>";
		s += "<hr>";
		s += "<b>Frequencies</b><br>";
		s += "ExAC_ALL: "+getObject("aaf_adj_exac_all")+"<br>";
		s += "1000g_all: "+getObject("aaf_1kg_all")+"<br>";
		s += "esp_all: "+getObject("aaf_esp_all")+"<br>";
		s += "cohort: "+cohortFrequency()+"<br>";
		s += "<hr>";
		s += "<b>Genotypes</b><br>";
		s += "<b>Affected:</b><br>";	
		for(Patient p:affected)
			s += p.getName()+": "+genotypes.get(p)+"<br>";
		s += "<b>Unaffected:</b><br>";	
		for(Patient p:unaffected)
			s += p.getName()+": "+genotypes.get(p)+"<br>";
		s += "</html>";
		
		return s;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getChromosome()
	 */
	@Override
	public String getChromosome() {
		if( textAttributesMapReversed[ metadata.getIndex( "chrom" ) ] == null  )
			textAttributesMapReversed[ metadata.getIndex( "chrom" ) ] = textAttributesMap[ metadata.getIndex( "chrom" ) ].inverse();
		return textAttributesMapReversed[ metadata.getIndex( "chrom" ) ].get( textAttributes[ metadata.getIndex( "chrom" ) ] );
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getQuality()
	 */
	@Override
	public float getQuality() {
		return floatsAttributes[ metadata.getIndex( "qual" ) ];
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getFilter()
	 */
	@Override
	public String getFilter() {
		if( textAttributesMapReversed[ metadata.getIndex( "filter" ) ] == null  )
			textAttributesMapReversed[ metadata.getIndex( "filter" ) ] = textAttributesMap[ metadata.getIndex( "filter" ) ].inverse();
		return textAttributesMapReversed[ metadata.getIndex( "filter" ) ].get( textAttributes[ metadata.getIndex( "filter" ) ] );
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getPosition()
	 */
	@Override
	public int getPosition() {
		return integersAttributes[ metadata.getIndex( "start" ) ]+1;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getReference()
	 */
	@Override
	public String getReference() {
		if( textAttributesMapReversed[ metadata.getIndex( "ref" ) ] == null  )
			textAttributesMapReversed[ metadata.getIndex( "ref" ) ] = textAttributesMap[ metadata.getIndex( "ref" ) ].inverse();
		return textAttributesMapReversed[ metadata.getIndex( "ref" ) ].get( textAttributes[ metadata.getIndex( "ref" ) ] );
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getAlternative()
	 */
	@Override
	public String getAlternative() {
		if( textAttributesMapReversed[ metadata.getIndex( "alt" ) ] == null  )
			textAttributesMapReversed[ metadata.getIndex( "alt" ) ] = textAttributesMap[ metadata.getIndex( "alt" ) ].inverse();
		return textAttributesMapReversed[ metadata.getIndex( "alt" ) ].get( textAttributes[ metadata.getIndex( "alt" ) ] );
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getGene()
	 */
	@Override
	public String getGene() {
		if( textAttributesMapReversed[ metadata.getIndex( "gene" ) ] == null  )
			textAttributesMapReversed[ metadata.getIndex( "gene" ) ] = textAttributesMap[ metadata.getIndex( "gene" ) ].inverse();
		return textAttributesMapReversed[ metadata.getIndex( "gene" ) ].get( textAttributes[ metadata.getIndex( "gene" ) ] );
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#impact()
	 */
	@Override
	public String impact() {
		if( textAttributesMapReversed[ metadata.getIndex( "impact" ) ] == null  )
			textAttributesMapReversed[ metadata.getIndex( "impact" ) ] = textAttributesMap[ metadata.getIndex( "impact" ) ].inverse();
		return textAttributesMapReversed[ metadata.getIndex( "impact" ) ].get( textAttributes[ metadata.getIndex( "impact" ) ] );
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#impactSeverity()
	 */
	@Override
	public String impactSeverity() {
		if( textAttributesMapReversed[ metadata.getIndex( "impact_severity" ) ] == null  )
			textAttributesMapReversed[ metadata.getIndex( "impact_severity" ) ] = textAttributesMap[ metadata.getIndex( "impact_severity" ) ].inverse();
		return textAttributesMapReversed[ metadata.getIndex( "impact_severity" ) ].get( textAttributes[ metadata.getIndex( "impact_severity" ) ] );
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#impactPriority()
	 */
	@Override
	public int impactPriority() {
		return impactPriority(impact());
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#cadd()
	 */
	@Override
	public Float cadd() {
		if( getObject("type").equals("sv") )
			return null;
		return floatsAttributes[ metadata.getIndex( "cadd_raw" ) ];
	}
	
	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#cadd()
	 */
	@Override
	public Float cadd_scaled() {
		if( getObject("type").equals("sv") )
			return null;
		return floatsAttributes[ metadata.getIndex( "cadd_scaled" ) ];
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#maxAlleleFrequency()
	 */
	@Override
	public Double maxAlleleFrequency() {
		return (double) floatsAttributes[ metadata.getIndex( "max_aaf_all" ) ];
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#cohortFrequency()
	 */
	@Override
	public Double cohortFrequency() {
		if( !_cohortFrequencyComputed ){
			int cases = 0;
			
			if( isCNV() ){
				cases = getOverlappingCNVs().size() + 1;
//				System.out.println(getGene()+" : "+cases+" : "+genotypes.keySet().size()+" : "+patients.size());
//				System.out.println(getGene()+" "+getChromosome()+" "+getPosition()+" "+getObject("sv_length")+" "+getObject("sub_type")+" "+getObject("variant_id"));
//				for(GeminiVariant v:getOverlappingCNVs()){
//					System.out.println(v.getGene()+" "+v.getChromosome()+" "+v.getPosition()+" "+v.getObject("sv_length")+" "+v.getObject("sub_type")+" "+v.getObject("variant_id"));
//				}
				_cohort_frequency =  (double) cases / (double)patients.size();
			}else{
				for(Patient p:genotypes.keySet()){
					if( GenotypeUtils.isCarryingVariant( getPatientGenotype(p) ) )
						cases++;
				}
				_cohort_frequency =  (double) cases / (double)genotypes.keySet().size();
			}
			
			_cohortFrequencyComputed = true;
		}
		return _cohort_frequency;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getNumberOfPatients()
	 */
	@Override
	public int getNumberOfPatients() {
		return genotypes.size();
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getPatient(fr.pasteur.sysbio.VCFviz.API.Patient)
	 */
	@Override
	public String getPatient(Patient i) {
		try{
			return genotypes.get(i).toString();
		}catch(Exception e){
			System.out.println("NULL patient: "+i);
			return "";
		}
		
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getPatientGenotype(fr.pasteur.sysbio.VCFviz.API.Patient)
	 */
	@Override
	public String getPatientGenotype(Patient patient) {
		if( genotypes.get(patient) == null )
			return null;
		String genotype = genotypes.get(patient).getGT();
		
		if( Genotype.isOverridingGT() ){
			return genotype;
		}else{
			String newGenotype = "";
			if( genotype.split("/").length>1 ){
				
				if( genotype.split("/")[0].equals(getReference()) ){
					newGenotype+="0";
				}else{
					if( genotype.split("/")[0].equals(".") )
						newGenotype+=".";
					else
						newGenotype+="1";
				}
				newGenotype+="/";
				if( genotype.split("/")[1].equals(getReference()) ){
					newGenotype+="0";
				}else{
					if( genotype.split("/")[1].equals(".") )
						newGenotype+=".";
					else
						newGenotype+="1";
				}
				
				return newGenotype;
			}else{
				if( genotype.split("|")[0].equals(getReference()) ){
					newGenotype+="0";
				}else{
					if( genotype.split("|")[0].equals(".") )
						newGenotype+=".";
					else
						newGenotype+="1";
				}
				newGenotype+="|";
				if( genotype.split("|")[1].equals(getReference()) ){
					newGenotype+="0";
				}else{
					if( genotype.split("|")[1].equals(".") )
						newGenotype+=".";
					else
						newGenotype+="1";
				}
				return newGenotype;
			}
		}
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#getObject(java.lang.String)
	 */
	@Override
	public Object getObject(String _property) {
		
		if( _property.equals("cohort_frequency") || _property.equals("cohort frequency") ){
			return cohortFrequency();
		}
		if( _property.equals("maximum allele frequency") ){
			return getObject("max_aaf_all");
		}
		if( _property.equals("cadd_raw") ){
			return cadd();
		}
		if( _property.equals("cadd_scaled") ){
			return cadd_scaled();
		}
		
//		if( _property.equals("vep_techno") ){
//			if( textAttributesMapReversed[ metadata.getIndex( _property ) ] == null  )
//				textAttributesMapReversed[ metadata.getIndex( _property ) ] = textAttributesMap[ metadata.getIndex( _property ) ].inverse();
//			System.out.println("techno: "+ textAttributesMapReversed[ metadata.getIndex( _property ) ].get( textAttributes[ metadata.getIndex( _property ) ] ) );
//		}
		
		switch( metadata.getType(_property) ){
			case GeminiVariantMetadata.TYPE_BOOLEAN:
				return booleansAttributes[ metadata.getIndex( _property ) ];
			case GeminiVariantMetadata.TYPE_INTEGER:
				return integersAttributes[ metadata.getIndex( _property ) ];
			case GeminiVariantMetadata.TYPE_FLOAT:
				return floatsAttributes[ metadata.getIndex( _property ) ];
			case GeminiVariantMetadata.TYPE_TEXT:
				if( textAttributesMapReversed[ metadata.getIndex( _property ) ] == null  )
					textAttributesMapReversed[ metadata.getIndex( _property ) ] = textAttributesMap[ metadata.getIndex( _property ) ].inverse();
				return textAttributesMapReversed[ metadata.getIndex( _property ) ].get( textAttributes[ metadata.getIndex( _property ) ] );
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see fr.pasteur.sysbio.VCFviz.API.Variant#listAttributes()
	 */
	@Override
	public List<String> listAttributes() {
		ArrayList<String> l = new ArrayList<String>(metadata.getAttributes());
		l.add("cohort_frequency");
		return l;
	}

	@Override
	public boolean isCNV() {
		return getObject("type").equals("sv");
	}

	@Override
	public String getPatientGenotypeEnlarged4CNVsFiltered(Patient patient) {
		if( isCNV() ){
			// TODO QUality filtering
			for(GeminiVariant v:getOverlappingCNVs()){
				if( GenotypeUtils.isCarryingVariant(v.getPatientGenotype(patient)) )
					return v.getPatientGenotype(patient);
			}
			if( patient == null )
				return null;
			return "0/0";
		}else{
			return getPatientGenotype(patient);
		}
	}
	
	@Override
	public String getPatientGenotypeEnlarged4CNVs(Patient patient) {
		if( isCNV() ){
			for(GeminiVariant v:getOverlappingCNVs()){
				if( GenotypeUtils.isCarryingVariant(v.getPatientGenotype(patient)) )
					return v.getPatientGenotype(patient);
			}
			if( patient == null )
				return null;
			return "0/0";
		}else{
			return getPatientGenotype(patient);
		}
	}

	@Override
	public void setGeneObject(Gene g) {
		_gene = g;
	}

	@Override
	public Gene getGeneObject() {
		return _gene;
	}

	@Override
	public Variant getSimilarCNV(Patient patient) {
		if( isCNV() ){
			
			for(GeminiVariant v:getOverlappingCNVs()){
				if( GenotypeUtils.isCarryingVariant(v.getPatientGenotype(patient)) )
					return v;
			}
			return null;
		}else{
			return this;
		}
	}


}
