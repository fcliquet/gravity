/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.gemini;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.Inflater;

import org.apache.commons.dbutils.BasicRowProcessor;

import fr.pasteur.sysbio.Gravity.API.Genotype;
import fr.pasteur.sysbio.Gravity.API.Patient;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class VariantRowProcessor extends BasicRowProcessor {
	
	@Override
	public <T> T toBean(ResultSet rs, Class<T> type) throws SQLException {
		
		ResultSetMetaData meta = rs.getMetaData();
		GeminiVariant gv = null;
		
		Object[] lGts = null, lGtDepths = null, lGtRefDepths = null, lGtAltDepths = null, lGtQuals = null, lGtPhredHomref = null, lGtPhredHet = null, lGtPhredHomalt = null;
		
		try {
			gv = new GeminiVariant();
			for (short i = 1; i <= meta.getColumnCount(); i++) {
				short typeIndex = GeminiVariant.getMetadata().getType(i);

				switch (typeIndex) {
				case GeminiVariantMetadata.TYPE_BOOLEAN:
					gv.setBooleanAttribute( i , rs.getBoolean(i));
					break;
				case GeminiVariantMetadata.TYPE_INTEGER:
					gv.setIntegerAttribute( i, rs.getInt(i));
					break;
				case GeminiVariantMetadata.TYPE_FLOAT:
					gv.setFloatAttribute( i, rs.getFloat(i));
					break;
				case GeminiVariantMetadata.TYPE_TEXT:
					gv.setTextAttribute( i, rs.getString(i));
					break;
				default:
					InputStream is = rs.getBinaryStream(i);
	            	ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
	            	byte[] buffer2 = new byte[1];
	            	while (is.read(buffer2) > 0) {
	            		bos2.write(buffer2);
	            	}
	            	byte[] data = bos2.toByteArray();
	            	
	            	// we decompress the data using ZLIB
	            	Inflater decompresser = new Inflater();
	            	decompresser.setInput(data);
	            	ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
	            	
	            	StringBuilder sb = new StringBuilder();
	            	for (byte b : data) {
	                    sb.append(String.format("%02X ", b));
	                }
	            	
	            	byte[] buffer = new byte[8192];
	            	while (!decompresser.finished()) {
	            	    int size = decompresser.inflate(buffer);
	            	    bos.write(buffer, 0, size);
	            	}
	            	byte[] unzippeddata = bos.toByteArray();
	            	decompresser.end();
	            	
					switch( meta.getColumnLabel(i) ){
						case "gts":
							lGts = ArrayUnpickler.unpickle(unzippeddata);
							break;
						case "gt_depths":
							lGtDepths = ArrayUnpickler.unpickle(unzippeddata);
							break;
						case "gt_ref_depths":
							lGtRefDepths = ArrayUnpickler.unpickle(unzippeddata);
							break;
						case "gt_alt_depths":
							lGtAltDepths = ArrayUnpickler.unpickle(unzippeddata);
							break;
						case "gt_quals":
							lGtQuals = ArrayUnpickler.unpickle(unzippeddata);
							break;
						case "gt_phred_ll_homref":
							try{
								lGtPhredHomref = ArrayUnpickler.unpickle(unzippeddata);
							}catch(Exception e){
								//System.out.println(rs.getString("gene")+" :: "+rs.getInt("variant_id")+" :: phred_ll_homref");
								lGtPhredHomref = null;
							}
							break;
						case "gt_phred_ll_het":
							try{
								lGtPhredHet = ArrayUnpickler.unpickle(unzippeddata);
							}catch(Exception e){
								//System.out.println(rs.getString("gene")+" :: "+rs.getInt("variant_id")+" :: phred_ll_het");
								lGtPhredHet = null;
							}
							break;
						case "gt_phred_ll_homalt":
							try{
								lGtPhredHomalt = ArrayUnpickler.unpickle(unzippeddata);
							}catch(Exception e){
								//System.out.println(rs.getString("gene")+" :: "+rs.getInt("variant_id")+" :: phred_ll_homalt");
								lGtPhredHomalt = null;
							}
							break;
						default:
							break;
					}
					break;
				}
			} 
			
			if( lGtPhredHomref == null || lGtPhredHomref.length==0 ){
				lGtPhredHomref = new Integer[lGts.length];
				Arrays.fill(lGtPhredHomref, new Integer(0));
			}
			if( lGtPhredHet == null || lGtPhredHet.length==0 ){
				lGtPhredHet = new Integer[lGts.length];
				Arrays.fill(lGtPhredHet, new Integer(0));
			}
			if( lGtPhredHomalt == null || lGtPhredHomalt.length==0 ){
				lGtPhredHomalt = new Integer[lGts.length];
				Arrays.fill(lGtPhredHomalt, new Integer(0));
			}
			
			HashMap<Patient, Genotype> gts = new HashMap<Patient, Genotype>();
			for(int i=0 ; i<lGts.length ; i++){
				Genotype g;
				if(lGtQuals == null){
					g = new Genotype( (String)lGts[i], ((Integer)lGtDepths[i]).shortValue(), 
							((Integer)lGtRefDepths[i]).shortValue(), ((Integer)lGtAltDepths[i]).shortValue(), 
							(short)66, ((Integer)lGtPhredHomref[i]).shortValue(), 
							((Integer)lGtPhredHet[i]).shortValue(), ((Integer)lGtPhredHomalt[i]).shortValue() );
				}else{
					g = new Genotype( (String)lGts[i], ((Integer)lGtDepths[i]).shortValue(), 
							((Integer)lGtRefDepths[i]).shortValue(), ((Integer)lGtAltDepths[i]).shortValue(), 
							((Float)lGtQuals[i]).shortValue(), ((Integer)lGtPhredHomref[i]).shortValue(), 
							((Integer)lGtPhredHet[i]).shortValue(), ((Integer)lGtPhredHomalt[i]).shortValue() );
				}
				
				gts.put( GeminiVariant.getPatients().get(i) , g);
			}
			
			gv.setGenotypes(gts);
		} catch (Exception e) {
			System.out.println(rs.getString("gene")+" :: "+rs.getInt("variant_id"));
//			System.out.println(lGts.length);
//			System.out.println(lGtDepths.length);
//			System.out.println(lGtRefDepths.length);
//			System.out.println(lGtAltDepths.length);
//			System.out.println(lGtQuals.length);
//			System.out.println(lGtPhredHomref.length);
//			System.out.println(lGtPhredHet.length);
//			System.out.println(lGtPhredHomalt.length);
			e.printStackTrace(System.out);
		}
		return (T) gv;
	}
	
	@Override
	public <T> List<T> toBeanList(ResultSet rs, Class<T> type) throws SQLException {
		try {
            List<T> newlist = new LinkedList<T>();
            while (rs.next()) {
                newlist.add(toBean(rs, type));
            }
            return newlist;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
	}
	
}
