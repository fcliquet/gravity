/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.gemini;

/**
 * Exception that can be thrown by the ArrayUnpickler.
 * @author Freddy Cliquet
 * @version 1.0
 * @see ArrayUnpickler
 */
public class UnpicklerException extends RuntimeException {

	private static final long serialVersionUID = -34223162447493853L;

	/**
	 * Create a new exception
	 * @param message message of the exception
	 */
	public UnpicklerException(String message) {
		super(message);
	}
	
	/**
	 * create a new exception
	 * @param message message of the exception
	 * @param cause cause
	 */
	public UnpicklerException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
