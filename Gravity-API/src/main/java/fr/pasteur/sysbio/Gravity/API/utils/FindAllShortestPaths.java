/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fr.pasteur.sysbio.Gravity.API.Interaction;
import fr.pasteur.sysbio.Gravity.API.PPI;

/**
 * @author Freddy Cliquet
 * @version 1.0
 */
public class FindAllShortestPaths {
	
	private List<String> interactionTypes;
	private PPI network;
	private Graph graph;
	private List< Set<String> > partitions = new ArrayList< Set<String> >();
	
	public FindAllShortestPaths(PPI ppi, List<String> typesInteraction){
		network = ppi;
		interactionTypes = typesInteraction;
		Node.reinitialize();
//		printData();
		graph = new Graph();
		for(Interaction i:network.getInteractions(interactionTypes)){
			graph.add(i.symbol1, i.symbol2);
		}
//		System.out.println(findAllShortestPaths(graph, "AKAP9", "ETFB"));
//		System.out.println(findAllShortestPaths(graph, "AKAP9", "IRF2BPL"));
//		System.out.println(findAllShortestPaths(graph, "WAC", "ETFB"));
//		System.out.println(findAllShortestPaths(graph, "WAC", "IRF2BPL"));
	}
	
	public Set<String> addPartitions2Graph(){
		int i=1;
		Set<String> myPartitions = new HashSet<String>();
		for(Set<String> p:partitions){
			for(String g:p){
				graph.add("partition_"+i, g);
				myPartitions.add( "partition_"+i );
			}
			i++;
		}
		return myPartitions;
	}
	
	public Set<String> findAll(String from, String to){
		List<List<String>> genesNeeded = findAllShortestPaths(graph, from, to);
		Set<String> genes2add = new HashSet<String>();
		for(List<String> lg:genesNeeded){
			for(int i = 2 ; i < lg.size()-2 ; i++){
				genes2add.add(lg.get(i));
			}
		}
		
//		System.out.println(genesNeeded);
//		System.out.println(genes2add);
		return genes2add;
	}
	
	public void buildPartitions(Collection<String> geneList){
		List<String> rest = new ArrayList<String>(geneList);
		for(Interaction i:network.getInteractions(interactionTypes)){
			if( geneList.contains(i.symbol1) && geneList.contains( i.symbol2 ) ){
				Set<String> p = getPartition( i.symbol1 );
				if( p == null ){
					Set<String> p2 = getPartition( i.symbol2 );
					if( p2 == null ){
						p = new HashSet<String>();
						rest.remove( i.symbol1 );
						rest.remove( i.symbol2 );
						p.add( i.symbol1 );
						p.add( i.symbol2 );
						partitions.add( p );
					}else{
						p2.add( i.symbol1 );
						rest.remove( i.symbol1 );
					}
				}else{
					Set<String> p2 = getPartition( i.symbol2 );
					if( p2 == null ){
						p.add( i.symbol2 );
						rest.remove( i.symbol2 );
					}else{
						if( p != p2 ){
							p.addAll(p2);
							partitions.remove(p2);
						}
					}
				}
			}else{
				if( geneList.contains(i.symbol1) ){
					Set<String> p = getPartition( i.symbol1 );
					if( p == null ){
						p = new HashSet<String>();
						p.add( i.symbol1 );
						rest.remove( i.symbol1 );
						partitions.add( p );
					}
				}
				if( geneList.contains(i.symbol2) ){
					Set<String> p = getPartition( i.symbol2 );
					if( p == null ){
						p = new HashSet<String>();
						p.add( i.symbol2 );
						rest.remove( i.symbol2 );
						partitions.add( p );
					}
				}
			}
		}
		for( String g: rest ){
			Set<String> p = new HashSet<String>();
			p.add( g );
			partitions.add( p );
		}
	}
	
	/**
	 * Get the partition of the given gene
	 * @param gene  Gene for which to retrieve the partition
	 * @return partition containing the Gene gene
	 */
	private Set<String> getPartition(String gene){
		for( Set<String> p : partitions )
			if( p.contains( gene ) ){
				return p;
			}
		return null;
	}
	
	public void printPartitions(){
		System.out.println("My partitions: ");
		for(Set<String> p:partitions){
			System.out.println(p);
		}
	}
	
	private static Object mock = new Object();
	
	public static List<List<String>> findAllShortestPaths(Graph graph, String from, String to) {
        LinkedHashMap<Node, Object> queue = new LinkedHashMap<>();
        Set<Node> visited = new HashSet<>();
        queue.put(new Node(from, 0), mock);
 
        Node nodeTo = null;
        while (queue.keySet().size() > 0) {
            Node next = queue.keySet().iterator().next();
 
            if (next.key.equals(to)) {
                // base case: we found the end node and processed all edges to it -> we are done
                nodeTo = next;
                break;
            }
 
            for (Node n : graph.adjacents(next.key)) {
                if (!visited.contains(n)) {
                    if (queue.get(n) == null) {
                        queue.put(n, mock);
                    }
                    n.addParent(next);
                }
            }
 
            // removing the node from queue
            queue.remove(next);
            visited.add(next);
        }
        if (nodeTo == null) {
            return Collections.emptyList();
        }
 
        // Now performing the dfs from target node to gather all paths
        List<List<String>> result = new ArrayList<>();
        dfs(nodeTo, result, new LinkedList<>());
 
        return result;
    }
 
    private static void dfs(Node n, List<List<String>> result, LinkedList<String> path) {
        path.addFirst(n.key);
        if (n.getParents().size() == 0) {
            // base case: we came to target vertex
            result.add(new ArrayList<>(path));
        }
        for (Node p : n.getParents()) {
            dfs(p, result, path);
        }
        // do not forget to remove the processed element from path
        path.removeFirst();
    }
 
    private static class Graph {
        Map<String, Set<Node>> adjacencyList = new HashMap<>();
 
        public void add(String from, String to) {
            addEdge(to, from);
            addEdge(from, to);
        }
 
        private void addEdge(String from, String to) {
            Set<Node> list = adjacencyList.get(from);
 
            if (list == null) {
                list = new LinkedHashSet<>();
                adjacencyList.put(from, list);
            }
            list.add(Node.get(to));
        }
 
        public Set<Node> adjacents(String v) {
            Set<Node> nodes = adjacencyList.get(v);
            return nodes == null ? Collections.emptySet() : nodes;
        }
    }
 
    private static class Node {
        List<Node> parents = new ArrayList<>();
        private static Map<String, Node> map = new HashMap<>();
        private final String key;
        int level = -1;
 
        public static void reinitialize(){
        	map = new HashMap<>();
        }
        
        public static Node get(String str) {
            // inner interning of nodes
            Node res = map.get(str);
            if (res == null) {
                res = new Node(str, -1);
                map.put(str, res);
            }
            return res;
        }
 
        private Node(String str, int level) {
            key = str;
            this.level = level;
        }
 
        public void addParent(Node n) {
            // forbidding the parent it its level is equal to ours
            if (n.level == level) {
                return;
            }
            parents.add(n);
 
            level = n.level + 1;
        }
 
        public List<Node> getParents() {
            return parents;
        }
 
        public boolean equals(Object n) {
            return key.equals(((Node) n).key);
        }
 
        public int hashCode() {
            return key.hashCode();
        }
 
        @Override
        public String toString() {
            return key;
        }
    }
 
//    public static void main(String[] args) {
////        Graph graph = new Graph();
//// 
////        graph.add("1", "2");
////        graph.add("1", "3");
////        graph.add("2", "4");
////        graph.add("2", "5");
////        graph.add("4", "7");
////        graph.add("5", "7");
////        graph.add("3", "6");
////        graph.add("3", "8");
////        graph.add("8", "6");
////        graph.add("6", "7");
////        graph.add("7", "5");
//// 
////        System.out.println(findAllShortestPaths(graph, "1", "7"));
//// 
////        graph = new Graph();
//// 
////        graph.add("A", "B");
////        graph.add("B", "D");
////        graph.add("B", "C");
////        graph.add("C", "E");
////        graph.add("E", "F");
////        graph.add("F", "D");
//// 
////        System.out.println(findAllShortestPaths(graph, "A", "E"));
//        
//        PPI ppi = new PPI();
//        ppi.load();
////        FindAllShortestPaths fasp = new FindAllShortestPaths(ppi, ppi.getInteractionTypes());
//        List<String> geneList = Arrays.asList("AKAP9", "WAC", "NAA15", "MBD5", "IRF2BPL", "ETFB", "ZNF559");
//        FindAllShortestPaths.connectAll(ppi, ppi.getInteractionTypes(), geneList);
////        fasp.buildPartitions(geneList);
////        fasp.printPartitions();
////        fasp.addPartitions2Graph();
////        fasp.findAll("partition_1", "partition_4");
////        fasp.findAll("partition_1", "partition_3");
////        fasp.findAll("partition_1", "partition_2");
////        fasp.findAll("partition_2", "partition_3");
////        fasp.findAll("partition_2", "partition_4");
////        fasp.findAll("partition_3", "partition_4");
//    }
}
