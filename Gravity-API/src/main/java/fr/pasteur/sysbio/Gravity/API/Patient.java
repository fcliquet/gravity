/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API;

import java.io.Serializable;
import java.util.Map;

/**
 * Class representing a patient as it appears inside a Pedigree file. 
 * All the main characteristics of each patients are defined in here, including the parents and phenotypes.
 * 
 * <p>This class evolved and could evolved further to treat multiple possible cases of encoding of the phenotypes and sexes.
 * For instance it did evolve so that loading those informations from Gemini is straight forward. 
 * Or to add if a family is control/case (with a distinction from affected/unaffected).</p>
 * 
 * <p>NB. The family type (control/case) is always included even if not present in the data. The default value will be ASD.</p>
 * 
 * <p>NB2. To be able to use the getter on the father and mother such as they return the Patients, it is needed to set the cohort using the static setCohort() method.</p>
 * 
 * @author Freddy Cliquet
 * @version 2.1
 */
public class Patient implements Serializable, Comparable<Patient> {
	
	private static final long serialVersionUID = -6092705801414096621L;
	
	/**
	 * Used to denote unknown information for the sex or the phenotype.
	 */
	public final static int UNKNOWN = 0;
	
	/**
	 * Sex of the patient is Male
	 */
	public final static int MALE = 1;
	
	/**
	 * Sex of the patient is Female
	 */
	public final static int FEMALE = 2;
	
	/**
	 * The patient is not carrying the phenotype studied
	 */
	public final static int UNAFFECTED = 1;
	
	/**
	 * The patient is carrying the phenotype studied
	 */
	public final static int AFFECTED = 2;
	
	/**
	 * The patient is part of the control (and thus should also be unaffected)
	 */
	public final static int CONTROL = 1;
	
	/**
	 * The patient is part of the families containing affected people. thus can't be considered as a control, even if unaffected.
	 */
	public final static int ASD = 2;
	
	private static Map<String, Patient> cohort;
	
	private String name;
	private String family = null;
	private String father = null;
	private String mother = null;
	private int sex = 0;
	private int phenotype = 0;
	private int familyType = 2;
	
	/**
	 * Constructor for an empty patient. 
	 * 
	 * <p>This is very important to have one for the "bean" behavior used in the Gemini database reading.</p>
	 */
	public Patient(){
		this.name = null;
	}
	
	/**
	 * Constructor for an empty patient, except for the name.
	 * 
	 * @param name Name of the patient
	 */
	public Patient(String name){
		this.name = name;
	}
	
	/**
	 * Constructor for a more complete Patient. The only field not considered here is the family type.
	 * 
	 * @param name Name of the patient
	 * @param family Family of the patient
	 * @param father Father of the patient
	 * @param mother Mother of the patient
	 * @param sex Sex of the patient among UNKNOWN, MALE, FEMALE
	 * @param phenotype Phenotype of the patient among UNKNOWN, AFFECTED, UNAFFECTED
	 */
	public Patient(String name, String family, String father, String mother, int sex, int phenotype){
		this(name);
		this.family = family;
		this.father = father;
		this.mother = mother;
		this.sex = sex;
		this.phenotype = phenotype;
	}
	
	/**
	 * Getter on the name of the patient
	 * @return the name of the patient
	 */
	public String getName() {
		return name;
	}

	/**
	 * Function giving a shorter version of the patient's name (by removing the "family" from the name).
	 * 
	 * <p>such shorter names are useful to save space in the context of displaying family related information, especially since names tend to be long.</p>
	 * 
	 * @return short version of the patient's name
	 */
	public String getShortName(){
		String s = new String(name);
		s = s.replaceAll(family, "");
		if( s.startsWith("-") )
			s = s.substring(1);
		if( s.endsWith("-") )
			s = s.substring(0, s.length()-1);
		return s;
	}
	
	/**
	 * Getter on the family of the patient.
	 * @return the family of the patient
	 */
	public String getFamily() {
		return family;
	}

	/**
	 * Getter on the name of the father of the patient.
	 * @return the father's name
	 */
	public String getFatherName() {
		if( father == null || father.equals("0") || father.equals(".") )
			return null;
		return father;
	}
	
	/**
	 * Getter on the father of the patient
	 * @return the father
	 */
	public Patient getFather(){
		if( cohort != null )
			return cohort.get(getFatherName());
		else
			return null;
	}

	/**
	 * Getter on the name of the mother of the patient.
	 * @return the mother's name
	 */
	public String getMotherName() {
		if( mother == null || mother.equals("0") || mother.equals(".") )
			return null;
		return mother;
	}
	
	/**
	 * Getter on the mother of the patient
	 * @return the mother
	 */
	public Patient getMother(){
		if( cohort != null )
			return cohort.get(getMotherName());
		else
			return null;
	}

	/**
	 * Getter on the patient's sex encoded using the constant defined in this class: UNKNOWN, MALE or FEMALE.
	 * @return the patient's sex (UNKNOWN, MALE or FEMALE)
	 */
	public int getPatientSex() {
		return sex;
	}

	/**
	 * Getter on the patient's phenotype encoded using the constant defined in this class: UNKNOWN, AFFECTED or UNAFFECTED.
	 * @return the patient's phenotype
	 */
	public int getPatientPhenotype() {
		return phenotype;
	}

	/**
	 * Allows to know if the family is a control or has ASD patients in it.
	 * @return the patient's family Type (CONTROL or ASD)
	 */
	public int getPatientFamilyType(){
		return familyType;
	}
	
	/**
	 * Setter for the patient's name
	 * @param name the name of the patient
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Setter on the patient's family
	 * 
	 * <p>if the method name is not respecting the java convention, it is because of the Gemini loading to allow for an automatic loading of the database objects (It is faster to implement this way).</p>
	 * 
	 * @param family the family of the patient
	 */
	public void setFamily_id(String family) {
		this.family = family;
	}

	/**
	 * Setter for the patient's father
	 * @param father the patient's father
	 */
	public void setFather(String father) {
		this.father = father;
	}
	
	/**
	 * Setter for the patient's father
	 * 
	 * <p>This one is defined for the database loading.</p>
	 * @param father the patient's father
	 */
	public void setPaternal_id(String father) {
		setFather(father);
	}

	/**
	 * Setter for the patient's mother
	 * @param mother the patient's mother
	 */
	public void setMother(String mother) {
		this.mother = mother;
	}
	
	/**
	 * Setter for the patient's mother
	 * 
	 * <p>This one is defined for the database loading.</p>
	 * @param mother the patient's mother
	 */
	public void setMaternal_id(String mother) {
		setMother(mother);
	}
	
	/**
	 * Setter for the patient's sex
	 * 
	 * <p>NB. this take a String as a parameter and will manage several possibilities. 
	 * Both '1' and 'M' works for MALE (as well as the in-class defined constant MALE), 
	 * both '2' and 'F' for FEMALE (and the FEMALE constant), 
	 * the default value for any other input would be UNKNOWN.</p>
	 * 
	 * @param sex the patient's sex
	 */
	public void setSex(String sex) {
		switch( sex ){
			case "M":
			case "1":
				this.sex = Patient.MALE;
				break;
			case "F":
			case "2":
				this.sex = Patient.FEMALE;
				break;
			default:
				this.sex = Patient.UNKNOWN;
		}
	}

	
	/**
	 * Setter for the patient's phenotype
	 * 
	 * <p>NB. this take a String as a parameter and will manage several possibilities. 
	 * Both '1' and 'Unaffected' works for UNAFFECTED (as well as the in-class defined constant UNAFFECTED), 
	 * both '2' and 'Affected' for AFFECTED (and the AFFECTED constant), 
	 * the default value for any other input would be UNKNOWN.</p>
	 * 
	 * @param phenot the patient's phenotype
	 */
	public void setPhenotype(String phenot) {
		switch( phenot ){
			case "Affected":
			case "2":
				this.phenotype = Patient.AFFECTED;
				break;
			case "Unaffected":
			case "1":
				this.phenotype = Patient.UNAFFECTED;
				break;
			default:
				this.phenotype = Patient.UNKNOWN;
		}
	}
	
	/**
	 * Setter for the family type. 
	 * Basically CONTROL for families in which all persons are UNAFFECTED, 
	 * ASD for the families with at least one ASD case. The default value is ASD.
	 * 
	 * @param family the type of family (ASD or Control) to set
	 */
	public void setFamily(String family) {
		switch( family ){
			case "ASD":
				this.familyType = Patient.ASD;
				break;
			case "Control":
				this.familyType = Patient.CONTROL;
				break;
			default:
				this.familyType = Patient.ASD;
		}
	}

	/**
	 * Function returning the phenotype's label.
	 * @return The phenotype label
	 */
	private String getPhenotypeLabel(){
		if(phenotype == AFFECTED)
			return "Affected";
		if(phenotype == UNAFFECTED)
			return "Unaffected";
		return "Unknown";
	}
	
	/**
	 * Function returning the sex's label.
	 * @return The sex label
	 */
	private String getSexLabel(){
		if(sex == MALE)
			return "Male";
		if(sex == FEMALE)
			return "Female";
		return "Unknown";
	}
	
	/**
	 * Function returning the label of the family type.
	 * @return The family type label
	 */
	private String getFamilyLabel(){
		if(familyType == ASD)
			return "ASD";
		if(familyType == CONTROL)
			return "Control";
		return "Unknown";
	}
	
	/**
	 * Function testing if the patient has 2 parents or not.
	 * @return true if the patient has 2 parents, false else
	 */
	public boolean hasParents(){
		return getFatherName() != null && getMotherName() != null;
	}
	
	@Override
	public String toString(){
		if( phenotype == UNKNOWN )
			return name;
		return name;
	}
	
	/**
	 * Function to get a String representation of this patient a lot more complete, including the parents, sex, phenotype and family type.
	 * @return a long String representing the patient
	 */
	public String longString(){
		return name+" "+father+" "+mother+" "+getSexLabel()+" "+getPhenotypeLabel()+" "+getFamilyLabel();
	}
	
	/**
	 * Set the global cohort of patient to be used to allow for a quick and easy extraction of the parents.
	 * @param coh the global cohort of patients
	 */
	public static void setCohort(Map<String, Patient> coh){
		cohort = coh;
	}
	
//	private String getInfo(){
//		String s = "";
//		s+="Patient:   "+name+"\n";
//		s+="Sex:       "+getSexLabel()+"\n";
//		s+="Phenotype: "+getPhenotypeLabel()+"\n";
//		s+="Family:    "+family+"\n";
//		s+="Father:    "+father+"\n";
//		s+="Mother:    "+mother+"\n";
//		return s;
//	}
//	
//	public static void main(String[] args) {
//		Patient a = new Patient("1004021437-NICE-AME");
//		Patient b = new Patient("1005201323-NICE-BER", "NICE-BER", "1011171403-NICE-BER", "1011171405-NICE-BER", 1, 2);
//		
//		System.out.println(a);
//		System.out.println(b);
//		
//		System.out.println("====");
//		
//		System.out.println(a.getInfo());
//		System.out.println(b.getInfo());
//	}

	@Override
	public int compareTo(Patient o) {
		return name.compareTo(o.name);
	}
}
