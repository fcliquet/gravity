/**
 * 
 */
package fr.pasteur.sysbio.Gravity.API.gemini;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/**
 * Class providing a static method used to unpickle python 1D arrays of integers, floats or strings. 
 * It is made for the unpickling of Gemini data, so it may not work in some specific cases.
 * @author Freddy Cliquet
 * @version 1.0
 */
public class ArrayUnpickler {
	
	final static byte[] pattern_dtype = {(byte)'d', (byte)'t', (byte)'y', (byte)'p', (byte)'e', (byte)0x0A, (byte)0x71, (byte)0x04, (byte)0x55};
	final static byte[] pattern_dtype_2 = {(byte)'d', (byte)'t', (byte)'y', (byte)'p', (byte)'e', (byte)0x0A, (byte)0x71, (byte)0x07, (byte)0x55};
	final static byte[] pattern_tb = {(byte)'t', (byte)'b'};
	final static byte[] pattern_tq = {(byte)'t', (byte)'q'};
	final static byte[] pattern_ndarray = {(byte)'n', (byte)'d', (byte)'a', (byte)'r', (byte)'r', (byte)'a', (byte)'y'};
	final static byte[] pattern_28 = {(byte)'('};
	final static byte[] pattern_cnumpy_core_multiarray = "cnumpy.core.multiarray".getBytes();
	final static byte[] pattern_4E4E4E = {(byte)0x4E, (byte)0x4E, (byte)0x4E};
	
	/**
	 * Function that unpickle the python array into a java array of objects.
	 * @param data array of bytes representing the full pickled object
	 * @return the java translated array of objects
	 * @throws UnpicklerException exception thrown when there is a mismatch in the array size or that we do not know how to manage the data.
	 */
	public static Object[] unpickle(byte[] data){
		byte[] head, dtype;
		List<byte[]> splitted;
		boolean type2 = false;
//		System.out.println(printBytes(data));
		
		
		if( data[0] != (byte)0x80 )
			throw new UnpicklerException("not a pickled object");
		if( data[1] != (byte)0x02 )
			throw new UnpicklerException("Wrong protocol used ("+data[1]+"), working only for protocol 2");
		if( !Arrays.equals(Arrays.copyOfRange(data, 2, pattern_cnumpy_core_multiarray.length+2), pattern_cnumpy_core_multiarray) ){
			throw new UnpicklerException("Pickled object is not a cnumpy.core.multiarray");
		}
		splitted = split(pattern_dtype, data);
		if( splitted.size()==1 ){
			splitted = split(pattern_dtype_2, data);
			type2 = true;
		}
			
		dtype = splitted.get(splitted.size()-1);
    	head = splitted.get(0);
    	
    	// we go to the ndarray part of the header, then locate the '('
    	splitted = split(pattern_ndarray, head);
    	head = splitted.get(splitted.size()-1);
    	head = Arrays.copyOfRange(head, 1, head.length-1);
    	splitted = split(pattern_28, head);
    	head = splitted.get(1);
    	
    	// we read the shape of the, for now only if it is 1 dimension
    	int dimension = 0, arraySize = 0, position = 0;
    	if( head[position] == (byte)'K' ){
    		position++;
    		dimension = bytes_to_integer( head, position++, 1 );
    		if( dimension == 1 ){
    			if( head[position] == (byte)'K' ){
            		position++;
    				arraySize = bytes_to_integer(head, position++, 1);
    			}else
	    			if( head[position] == (byte)'M' ){
	            		position++;
	    				arraySize = bytes_to_integer(head, position, 2);
	    				position += 2;
	    			}else
	    				if( head[position] == (byte)'J' ){
		            		position++;
		    				arraySize = bytes_to_integer(head, position, 4);
		    				position += 4;
		    			}else
		    				throw new UnpicklerException("problem decoding length of array");
    		}else
    			throw new UnpicklerException("array of dimension greater than 1");
    	}else
    		throw new UnpicklerException("array of dimension greater than 1");
    	
    	if( head[position] != (byte)0x85 )
    		throw new UnpicklerException("problem reading array shape");
    	
    	String type = new String(Arrays.copyOfRange(dtype, 1, bytes_to_integer(dtype, 0, 1)+1));
    	int stringLength = 0, stringLength2 = 0;
//    	System.out.println("Type :: "+type);
//    	System.out.println("DType :: "+printBytes(dtype));
    	switch(type){
    		case "i4":
    			break;
    		case "f4":
    			break;
    		default:
    			if(type.startsWith("S")){
    				try {
						stringLength = Integer.parseInt( type.substring(1) );
						
						dtype = split( pattern_4E4E4E, dtype ).get(1);
						if( dtype[0] == (byte)'K' ){
							stringLength2 = bytes_to_integer(dtype, 1, 1);
		    			}else
			    			if( dtype[0] == (byte)'M' ){
			    				stringLength2 = bytes_to_integer(dtype, 1, 2);
			    			}else
			    				if( dtype[0] == (byte)'J' ){
			    					stringLength2 = bytes_to_integer(dtype, 1, 4);
				    			}else
				    				throw new UnpicklerException("problem decoding length of string");
						
					} catch (NumberFormatException e) {
						throw new UnpicklerException("problem with the type of the data: "+type);
					}
    			}else
    				throw new UnpicklerException("unpickling the type "+type+" is not yet implemented.");
    			break;	
    	}
    	if( stringLength != stringLength2 )
    		throw new UnpicklerException("string length mismatch problem");
    	
    	if(type2)
    		splitted = split(pattern_tq, dtype);
    	else
    		splitted = split(pattern_tb, dtype);
    	
    	if( splitted.size() > 3 ){
    		
//    		System.out.println("if");
    		
    		int len = 0;
    		for(int i=1;i<splitted.size()-1;i++){
    			len += splitted.get(i).length;
    		}
    		dtype = new byte[len+((splitted.size()-2)*2)];
    		int cpt = 0;
    		for(int i=1;i<splitted.size()-1;i++){
    			for(byte b:splitted.get(i)){
    				dtype[cpt++]=b;
    			}
    			dtype[cpt++]=(byte)'t';
    			dtype[cpt++]=(byte)'q';
    		}
    		dtype = Arrays.copyOfRange(dtype, 8, dtype.length-4);
    	}else{

//    		System.out.println("else");
    		
    		dtype = splitted.get(splitted.size()-2);
    		if( dtype[0]==(byte)0x89 && dtype[1]==(byte)0x55 )
        		dtype = Arrays.copyOfRange(dtype, 3, dtype.length);
        	else
        		if( dtype[2]==(byte)0x89 && dtype[3]==(byte)0x55 )
        			dtype = Arrays.copyOfRange(dtype, 3, dtype.length);
        		else
        			dtype = Arrays.copyOfRange(dtype, 6, dtype.length);
    		if( type2 ){
        		byte resize[] = new byte[dtype.length-4];
            	for(int i=2;i<=dtype.length-3;i++){
            		resize[i-2] = dtype[i];
            	}
            	dtype = resize;
        	}
    	}	
    	
    	List<Object> lo = new ArrayList<Object>();
    	
//    	System.out.println( printBytes(dtype) );
//    	System.out.println("TYPE: "+type);
    	
    	ByteBuffer bb = ByteBuffer.wrap(dtype);
    	bb.order(ByteOrder.LITTLE_ENDIAN);
    	switch(type){
    		case "i4":
    			while( bb.hasRemaining() ){
    	    		lo.add( bb.getInt() );
//    	    		System.out.println(" "+lo.get(lo.size()-1));
    	    	}
    	    	if( lo.size() != arraySize )
    	    		throw new UnpicklerException("length of array does not match the expected ("+lo.size()+" vs. "+arraySize+")");
    			break;
    		case "f4":
    			while( bb.hasRemaining() ){
    	    		lo.add( bb.getFloat() );
//    	    		System.out.println(" "+lo.get(lo.size()-1));
    	    	}
    	    	if( lo.size() != arraySize )
    	    		throw new UnpicklerException("length of array does not match the expected");
    			break;
    		default:
    			for(int i=0 ; i<dtype.length ; i+=stringLength){
    				lo.add( new String( Arrays.copyOfRange(dtype, i, i+stringLength) ).trim() );
//    				System.out.println( new String( Arrays.copyOfRange(dtype, i, i+stringLength) ).trim() );
    			}
    	    	if( lo.size() != arraySize )
    	    		throw new UnpicklerException("length of array does not match the expected");
    			break;
    	}
    	return lo.toArray();
	}
	
	private static String printBytes(byte[] bs){
    	StringBuilder sb = new StringBuilder();
    	for (byte b : bs) {
            sb.append(String.format("%02X ", b));
        }
    	return sb.toString();
    }
    
    private static boolean isMatch(byte[] pattern, byte[] input, int pos) {
        for(int i=0; i< pattern.length; i++) {
            if( pos+pattern.length > input.length || pattern[i] != input[pos+i] ) {
                return false;
            }
        }
        return true;
    }

    private static List<byte[]> split(final byte[] pattern, byte[] input) {
        List<byte[]> l = new LinkedList<byte[]>();
        int blockStart = 0;
        for(int i=0; i<input.length; i++) {
           if(isMatch(pattern,input,i)) {
              l.add(Arrays.copyOfRange(input, blockStart, i));
              blockStart = i+pattern.length;
              i = blockStart;
           }
        }
        l.add(Arrays.copyOfRange(input, blockStart, input.length ));        
        return l;
    }

    private static int bytes_to_integer(byte[] bytes, int offset, int size) {
		// this operates on little-endian bytes
    	if (size==1) {
			// 1-bytes unsigned int
			int i = bytes[0+offset] & 0xff;
			return i;
		} else if (size==2) {
			// 2-bytes unsigned int
			int i = bytes[1+offset] & 0xff;
			i <<= 8;
			i |= bytes[0+offset] & 0xff;
			return i;
		} else if (size==4) {
			// 4-bytes signed int
			int i = bytes[3+offset];
			i <<= 8;
			i |= bytes[2+offset] & 0xff;
			i <<= 8;
			i |= bytes[1+offset] & 0xff;
			i <<= 8;
			i |= bytes[0+offset] & 0xff;
			return i;
		}
		return 0;
    }
    
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:" + "/Users/fcliquet/Data/HERCULES/gemini/M086_Hg19.db");
            Statement statement = connection.createStatement();
            
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Variants WHERE variant_id = 18607 ;");
            while(resultSet.next()){
            	InputStream is = resultSet.getBinaryStream("gt_quals");
            	System.out.println("variant id "+resultSet.getString("variant_id"));
            	ByteArrayOutputStream bos2 = new ByteArrayOutputStream();
            	byte[] buffer2 = new byte[1];
            	while (is.read(buffer2) > 0) {
            		bos2.write(buffer2);
            	}
            	byte[] data = bos2.toByteArray();
            	
            	// we decompress the data using ZLIB
            	Inflater decompresser = new Inflater();
            	decompresser.setInput(data);
            	ByteArrayOutputStream bos = new ByteArrayOutputStream(data.length);
            	byte[] buffer = new byte[8192];
            	while (!decompresser.finished()) {
            	    int size = decompresser.inflate(buffer);
            	    bos.write(buffer, 0, size);
            	}
            	byte[] unzippeddata = bos.toByteArray();
            	decompresser.end();
            	
            	//we unpickle
            	Object[] os = unpickle(unzippeddata);
            	if(os == null)
            		System.out.println("array is null");
            	else{
            		for(int i=0 ; i<os.length ; i++){
            			System.out.println(i+" :: "+os[i]);
            		}
            	}
            }
            
        } catch (ClassNotFoundException notFoundException) {
            notFoundException.printStackTrace();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } catch (IOException e) {
			e.printStackTrace();
		} catch (DataFormatException e) {
			e.printStackTrace();
		}
	}

}
