package fr.pasteur.sysbio.Gravity.API.windows;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import fr.pasteur.sysbio.Gravity.API.Variant;
import net.miginfocom.swing.MigLayout;

public class VariantDetailsFrame extends JFrame {

	private static final long serialVersionUID = 1778985862645126268L;
	private Variant _gsv;

	public VariantDetailsFrame(Variant gsv){
		_gsv = gsv;
		setTitle("Variant Detail");
		
		JPanel panelE = new JPanel();
		
		JPanel panelS = new JPanel();
		panelS.setLayout(new MigLayout());
		
		JPanel panelW = new JPanel();
		panelW.setLayout(new MigLayout());
		
		// display the first fields
		panelW.add( new JLabel("Chromosome: "+_gsv.getChromosome()), "wrap" );
		panelW.add( new JLabel("Position: "+_gsv.getPosition()), "wrap" );
		panelW.add( new JLabel("Reference Allele: "+_gsv.getReference()), "wrap" );
		panelW.add( new JLabel("Alternative Allele: "+_gsv.getAlternative()), "wrap" );
		panelW.add( new JLabel("Quality: "+_gsv.getQuality()), "wrap" );
		panelW.add( new JLabel("Filter: "+_gsv.getFilter()), "wrap" );
		
		// display the info field in a table
		AbstractTableModel infoModel = new AbstractTableModel(){

			private static final long serialVersionUID = 981574890149234364L;

			@Override
			public int getRowCount() {
				// TODO Auto-generated method stub
				return 100;
			}

			@Override
			public int getColumnCount() {
				// TODO Auto-generated method stub
				return 2;
			}

			@Override
			public Object getValueAt(int rowIndex, int columnIndex) {
				// TODO Auto-generated method stub
				return new Integer((rowIndex+1)*(columnIndex+1));
			}
			
		};
		JTable infoTable = new JTable(infoModel);
		
		ListSelectionModel infoLSM = infoTable.getSelectionModel();
		infoLSM.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		infoTable.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if( e.getClickCount() == 2 ){
					System.out.println("double click "+ infoModel.getValueAt( infoTable.getSelectedRow() , 1 ) );
				}
			};
		});
		
		panelE.add(new JScrollPane(infoTable) );
		
		// display the patients related info in another table
		
		// Buttons
		panelS.add(new JButton("Close"));
		
		
		setLayout(new BorderLayout());
		add(panelW, BorderLayout.WEST);
		add(panelE, BorderLayout.EAST);
		add(panelS, BorderLayout.SOUTH);
		
		pack();
		setVisible(true);
	}

}
