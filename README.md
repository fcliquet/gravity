# README #

Version 1.0

### Summary ###
GRAVITY (for “Gene inteRaction Analysis of Variants in Individuals: a Tool for You”) is a new open-source Cytoscape app that allows an efficient visualization and analysis of all the exonic variants stored in a database by mapping them on a protein-protein interaction (PPI).

### How do I get started? ###
You can check out the [GRAVITY website](http:/gavity.pasteur.fr) to get more informations on what it offers, and how to use it.

### License ###
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).
