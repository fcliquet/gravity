=======
Results
=======

Network
-------

.. image:: images/network_01.png

.. image:: images/legend.png

For more details about the CADD score, read `Kircher M et al. "A general framework for estimating the relative pathogenicity of human genetic variants." Nat Genet. 2014 Feb 2. <https://www.nature.com/ng/journal/v46/n3/full/ng.2892.html>`_

Gene details
------------

On the right side of the main screen, in the Cytoscape 'results panel', there is a Gene details tab. This tab contains various informations concerning the selected gene.

.. image:: images/gene_details_01.png

The top part shows the gene name along with 2 buttons for a quick access in your browser to the BioGPS or the OMIM page of this gene.

.. image:: images/gene_details_02.png

Then a small area describe basics informations such as the chromosome the gene is on, the synonyms for the gene name, and the pathways (from the user's Network tab) that contain this gene.

.. image:: images/gene_details_03.png
  :width: 54%
.. image:: images/gene_details_03_other.png
  :width: 45%

The small dropdown list allows the user to select the sorting criteria for the variant list that follows.

Then all the variants carried by the gene, and **passing** the filters, are listed. The variants are constituted of:
 * a logo describing the heritability (star for *de novo*, question mark for *unknown*, mars symbol for father inheritance, venus symbol for mother inheritance, a combination of mars and venus symbols for an inheritance from both parents), and also describing the impact through its color (red for Likely Gene Disruptive (LGD), orange for missense, green for synonymous)
 * the precise impact defined in the annotation, or the AA change when one occurs
 * the genotypes of the patient and both his parents (if available)
 * the cadd score when available (only SNPs have cadds)
 * the maximum allele frequency and the cohort frequency
In addition to this summary, a tooltip with a more complete report appears when your mouse hover the variant. A double click on a variant can open an external opening of the BAM to visualize the variant and validate it manually. This must be setup locally and can be configured in Gravity using in Cytoscape the Properties menu in Edit -> Preferences. In the properties select the Gravity.gravity in the dropdown, and then you can edit the Gravity.settings.BAMserver ad set it to your URL serving BAMs. In this URL, it is possible to put the following markers so that they get replaced by the needed information at executions.

+---------------+--------------------------------------------+
| Marker        | Replaced by...                             |
+===============+============================================+
| %CHR%         | chromosome                                 |
+---------------+--------------------------------------------+
| %POSITION%    | chromosome':'position-100'-'position+100   |
+---------------+--------------------------------------------+
| %START%       | position-100                               |
+---------------+--------------------------------------------+
| %END%         | position+100+length(alt)                   |
+---------------+--------------------------------------------+
| %LOCUS%       | chromosome':'position                      |
+---------------+--------------------------------------------+
| %INDIVIDUALS% | semicolon separated list of individual IDs |
+---------------+--------------------------------------------+
| %FAMILY%      | Family ID                                  |
+---------------+--------------------------------------------+

.. image:: images/gene_details_04.png

The bottom plot represents the burden in term of quantity of mutations that pass the selected filters, in the selected gene, across the cohort. There is a box plot per category, namely the affected individuals, the unaffected siblings, the parents and the controls (for controls to be represented, an additional column named 'control' need to be present in the pedigree with values 2 for controls and 1 for non controls).
Under the categories other than Affected is indicated a p-value showing the significance of the difference between the affected distribution and each of the other ones. This p-value is computed using a *Mann-Whitney U test* and is not corrected for multiple testing (because this correction depends on how many other genes or set of genes the user looked and will be looking at).

Cohort details
--------------

This panel gives some informations about the whole cohort, not specific to the selected family.

.. image:: images/cohort_details_01.png

The top part is a table showing for the whole cohort, the number of mutations carried in the selected genes. In addition to this it shows in the severity column the most deleterious impact found among those variants for the given patient (red for LGD, orange for missense, green for synonymous), the cadd column give the most deleterious cadd carried by a variant in the given patient (if a cadd is available for any of those variants). It is to be noted that the most severe impact and the cadd do not necessarily represent the same variant.

.. image:: images/cohort_details_02.png

At the bottom the user can find exporting capabilities. He can select if he wants to export all the patients (default) or only the selected family, or the selected patient), then he can select a format between compact and full, which will just change the number of fields exported in the resulting file (various allele frequency and predictions scores are added in the full version).
The export is done as a CSV format, with a single line per variant per patient.
