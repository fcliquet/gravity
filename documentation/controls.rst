========
Controls
========

The *Gravity* menu in the left side control panel will allow the user to load the data and define all the desired parameters for the visualization and the analysis.

.. |img1| image:: images/open_button.png
  :scale: 100%
  :align: middle
.. |img2| image:: images/new_button.png
  :scale: 100%
  :align: middle
.. |img3| image:: images/refresh_button.png
  :scale: 100%
  :align: middle

+--------+--------------------------------------------+
| Button | Description                                |
+========+============================================+
| |img1| | Discard the currently loaded data and load |
|        | a new Gemini database.                     |
+--------+--------------------------------------------+
| |img2| | Create a new network view with the         |
|        | requested PPI and variants plotted.        |
+--------+--------------------------------------------+
| |img3| | Refresh the current network view.          |
+--------+--------------------------------------------+


When opening a Gemini database, Gravity will offer the user the possibility to prefilter the data on several criteria (impact of variants, maximum allele frequency, pathways previously saved in Gravity...). This step is important to reduce the loading time and the memory consumption.

.. warning::
  You need to keep in mind the parameters you used at this step, as it means they will always be "applied" whatever the filters you apply later on.

.. image:: images/data_prefiltering.png

The **Analysis mode** will define the behavior of the App towards the data. Family trio will allows for the analysis of a single patient by adding the knowledge of his aprents to know about inheritance of mutations, when the complete family mode will look at an entire family, or the subset of a family and highlight genes that are different in affected versus unaffected people.

.. image:: images/analysis_mode_2.png

Family trio
^^^^^^^^^^^

.. image:: images/family_trio.png

The patient selection window contains 3 dropdown menus, one of the patient and two others to select his/her parents. When the data loaded contains the pedigree informations, the name in the menus is completed with 2 icons, the first one indicate the sex of the patient, the second one indicate if the person is affected or not.
In addition to this, when a patient is selected, the parents are automatically selected.

In the case where the parents informations is not known, the user can still chose the parents alone in the two dropdown menus.

The checkbox *patients with parents only* will restrain the content of the patient list to only the people with both parents known. The App can manage patient with only one or no parents known, but it is to be noted that you need to keep the checkbox unchecked if you want to select one of those.
Not having both parents will limit the information displayed drastically (losing the capacity to detect *de novo* mutations and even any inheritance information).

1. Network
----------

.. image:: images/network_menu_2.png

a. Pathways, Genes lists
""""""""""""""""""""""""

We provide several predefined pathways / genes lists (SFARI, Class-I, Class-II, Class-III, FMRP-target, etc.). In addition to this, the user can add by copy/pasting new list of genes. There is an option to run the visualization on all genes; “all” having different meanings depending on the cases. In the case of a VCF file, it means all genes appearing in the VCF or having a known interaction.

There is an option to expand the network to first neighbours genes (or up to second neighbours genes). It is to be noted that genes are highly interacting together, so adding the first neighbours to a gene list, will highly increase the number of genes to display if proper filtering is not applied (e.g. SFARI + 1st neighbours has 3864 genes and 39,539 interactions versus 378 genes and 158 interactions for SFARI alone). Thus, we created a "smarter" expand that will use shortest path to try and connect all the displayed genes.

b. Interactions
"""""""""""""""

Interaction used are provides by Thomas Rolland. We have 4 types of interactions registered with different level of confidence. It is possible to use only some of those interactions.

2. Filters
----------

Multiples level of filtering are possible.

.. image:: images/filters_menu.png

a. For visualization
""""""""""""""""""""

Possibility to display all the genes of the selected network. Useful if you want to see in a single pathway, how many genes are affected and how. But this is a dangerous option in some combinations, as some pathways / gene list can be long, and it can be problematic with “all genes” option.

A warning will popup if the user try to display more than 2000 genes at the same time. It is to be noted that Cytoscape itself start limiting the informations displayed around those numbers (not drawing every details from the node visualization) in order to save memory. Some of those parameters can be tuned in Cytoscape preferences.

b. On variants
""""""""""""""

Possibility to filter on most of the “info” fields from the VCF files. Those filters are all linked together by an “and” operand, no “or” is defined so far. Common and useful categories to filter on are *aaf* and *cadd*. There is also a list of all the different possible impacts in our data (impact = exonic function = what the mutation is doing). For instance, this allows the user to display only deleterious mutations (or at least not to display the benign ones).

There is a category of filters for inheritance of variants. It is possible to select variants based on whether they are mendelian or not or de novo. And we can also filter to display mutation coming only from the father, only from the mother, coming from both, etc.

c. On genotypes
"""""""""""""""

This is made to filter on the DP (Depth), GQ (Genotype Quality, i.e. second smallest phred likelihood) or on the type of genotype (HOMREF, HET or HOMALT). Those filters are not really useful anymore, they are redundant with the Quality filters and with the inheritance filters.

3. Quality
----------

.. image:: images/quality_menu.png

Some additional filters are available in the Quality tab. They are separated from the others are they are specifically related to data quality and they should not really change from one analysis to the next. They consist in:

* Minimum allele depth (DP)
* Minimum genotype quality (GQ)
* Range for the ratio of allele Alt/DP to determine HET, HOMREF or HOMALT (a variant not passing the test is thrown out, the call for HET / HOM is not corrected, the reason being that the data in the VCF might be incomplete to do so)
* Option to also apply those strictly to the parents
* Additionally, for the cancer community, it is possible to infer the genotype directly from the number of alternative reads observed (AD) and to define the threshold for this.

It is to be noted that it is still possible to create filters for some of those in the regular Filter tab. if multiple filters are created on the same property, they are all used, and thus a variant must pass them all to be accepted.

Complete family
^^^^^^^^^^^^^^^

.. warning::
  You should read the Family trio section above. This section will treat only about what is specific to the Complete family mode.

.. image:: images/complete_family.png

In this mode, there is a dropdown menu to select the family to analyze. The selected family is displayed underneath the list as a pedigree tree.
By clicking on the visual representation of a person, it will change type of selection:

+-------+-----------------------------------------------+
| GREEN | **keep** all the variants in those patients   |
+-------+-----------------------------------------------+
| RED   | **reject** all the variants in those patients |
+-------+-----------------------------------------------+
| GRAY  | **ignore** those patients                     |
+-------+-----------------------------------------------+

The idea is to find all the mutations from the person selected in green that can't be found in the people selected in red.

**Note:** There is still some known issues with the pedigree tree representation. It is not yet able to display complex families (multiple wives or husbands for one person, or incestuous relationship).

Network
-------

The network tab is the same as for the Family trio.

Filters
-------

A small part of the tab is specific to the Complete family mode.

.. image:: images/filters_menu_families.png

First, the user can select the types of variants he wants to visualize between dominant, recessive and compound heterozygous.

Finally, the user can select wether all of the affected individuals (in green) must carry such a variant, or if any of those individuals is enough.

Quality
-------

The quality tab is the same as for the Family trio, but there is one important thing to know: when we compare affected and unaffected (red and green) we request for at least 1 patient passing the QC for both affected and unaffected. The patients selected but not passing the QC are ignored, meaning some presented results are dubious.

Comparison of 2 samples
^^^^^^^^^^^^^^^^^^^^^^^

.. warning::
  You should read the Family trio section above. This section will treat only about what is specific to the Complete family mode.

.. image:: images/comparison_mode.png

In this mode you select 2 related samples (A and B) to compare them. The objective is to either display something specific to A or B, or to display both samples together (if they represent complementary information).

Filters
-------

A small part of the tab is specific to the Complete family mode, the rest is unchanged.

.. image:: images/filters_menu_comparison.png
