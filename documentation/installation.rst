============
Installation
============

------------
Requirements
------------

Software
^^^^^^^^

* `Java 8 <http://www.java.com/>`_
* `Cytoscape 3.3.0 or later <http://www.cytoscape.org/download.php>`_

Hardware
^^^^^^^^

* At least 8Gb of memory, more if you are planning to load 100+ individuals cohort files.

----------------
App installation
----------------

* Launch Cytoscape

If a previous version was installed, you need to remove it first:

* go to the menu *Apps*, *Apps Manager...* and then click on the *Currently installed* tab.
* Select the plugin and uninstall it

Install the new version:

* Download the App: `Gravity 1.0.0 <http://bit.ly/2rY05a4>`_
* In Cytoscape, go to *Apps*, *Apps Manager...* and then click on the *Install from file...* button, then chose the *Gravity-impl-1.0.0.jar* file you previously downloaded.

In Cytoscape *Control Panel* (menu on the left side), there is a tab called *Gravity*. This is where you will control this App.
