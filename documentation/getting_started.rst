===============
Getting started
===============

-----------
Input files
-----------

You can download our sample dataset used in the following Getting Started there:
`CEU Trio NA12878 (1.4 GB) <http://bit.ly/2rXURvc>`_

This is a `Gemini <http://gemini.readthedocs.io/en/latest/>`_ database containing the NA12878 individuals with her parents called using the GATK best practices. This trio is commonly used for testing genomics tools. For demo purpose we also added CNVs of NA12878 (not for the parents) extracted from `Mills et al. 2011 <https://www.nature.com/articles/nature09708>`_.

--------------
Load a dataset
--------------

On the left side, in the gravity menu, click the load button a select your Gemini file.

.. image:: images/open_database.png

To produce the network as shown below, the following prefilters were applied for loading our sample dataset:

* impacts: HIGH and MED only
* maximum allele frequency <= 0.01
* ignore multiallelic variants
* remove CNVs with repeat masker

.. image:: images/choose_prefilters.png

Loading the data can take a few minutes depending on your computer and the size of your dataset. The prefilters make the loading faster but can only be changed by reloading the data.

----------------------------
Visualise your first network
----------------------------

* Select your patient (and eventually his parents if your pedigree file is incomplete)

.. note::
  In our sample dataset, the patient is NA12878, her father is NA12891 and her mother is 12892.

* In the Network tab, check the "run on all genes instead" option below the Pathways/Genes list.

.. image:: images/getting-started_network.png

* In the Filter tab, you need 2 filters: cadd_scaled >= 15 (found in 'Pathogenecity') and sub_type != DUP (found in 'Other'). Those will greatly reduce the number of variants displayed in the first network.

.. image:: images/getting-started_filters.png

* Hit the *create a new network* button

.. image:: images/getting-started_create.png

----------------------------
Your first Gravity network!
----------------------------

.. image:: images/network_03.png
  :target: _images/network_03.png

.. tip::
  For the sample dataset, we made the BAM visualization available online. If you select a gene and double click a variant on the right panel, your web browser should open and show you the BAM section of this variant. This visualisation is performed using pileup.js:
  `Vanderkam, Dan, et al. "pileup. js: a JavaScript library for interactive and in-browser visualization of genomic data." Bioinformatics 32.15 (2016): 2378-2379. <https://academic.oup.com/bioinformatics/article/32/15/2378/1744036/pileup-js-a-JavaScript-library-for-interactive-and>`_

  .. figure:: images/pileup_02.png

    `Example of missense variant found in CLSTN1. <http://gravity.pasteur.fr/pileup/?chr=chr1&start=9795938&end=9796139&individuals=NA12878;NA12891;NA12892>`_

====================
Instructional video
====================

.. raw:: html

  <iframe width="560" height="315" src="https://www.youtube.com/embed/_TxX4THGtUQ" frameborder="0" allowfullscreen></iframe>
