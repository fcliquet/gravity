.. GRAVITY documentation master file, created by
   sphinx-quickstart on Mon May  2 15:49:19 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GRAVITY
=======

GRAVITY (for "Gene inteRaction Analysis of Variants in Individuals: a Tool for You") is a new open-source Cytoscape app that allows an efficient visualization and analysis of all the exonic variants stored in a database by mapping them on a protein-protein interaction (PPI).

  .. image:: images/screenshot_01.png

--------
Features
--------

* Visualise sequencing data from cohorts using a Gemini database:
    `Paila, Umadevi, et al. "GEMINI: integrative exploration of genetic variation and genome annotations." PLoS Comput Biol 9.7 (2013): e1003153. <http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003153>`_
* Uses the pedigree informations through Gemini. The pedigree can be enriched with additional informations (multiple phenotypes, simple clinical data).
* Integrate interactions between the genes using a PPI network. The PPI currently implemented can be found in:
    `Rolland, Thomas, et al. "A proteome-scale map of the human interactome network." Cell 159.5 (2014): 1212-1226. <http://www.cell.com/cell/abstract/S0092-8674(14)01422-6>`_
* A Simplex family to focus on the mutation of a single individual but integrating the knowledge of his/her parents when they are available.
* A Multiplex family mode focusing on finding similarities/differences between affected and unaffected inside a family (dominant, recessive, compound heterozygous mutations).
* Cohort panel, to quickly see if other individual in your cohort share mutations in the selected genes.
* Quick search for one or multiple genes of interest.

-------------
Next releases
-------------

We are working actively on this app and are planning to add new functionalities based on our needs and requests.

* Alternative PPI and/or other types of interaction networks such as coexpression or regulation.
* Add quality parameters specific to CNVs.
* Expand the usage to input other than Gemini.

-------
License
-------

This work is licensed under a `Creative Commons Attribution-NonCommercial 4.0 International License <http://creativecommons.org/licenses/by-nc/4.0/>`_.

---------
Contents:
---------

.. toctree::
   :maxdepth: 2

   installation
   getting_started
   controls
   results
   data_preparation


* :ref:`search`

---------------
Acknowledgments
---------------

We would like to thank Yang-Min Kim for the design of the logo. Logo font `Faune, Alice Savoie / Cnap <http://www.cnap.graphismeenfrance.fr/faune/>`_.
