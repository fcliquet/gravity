=====================
Todo in next releases
=====================

GRAVITY is actively developed, we are planing several improvements in upcoming releases, among those:

* adding new filters at loading time (such as on pathways or on families)
* more dynamic management of the data (instead of loading everything in memory)
* improvements with the CNVs (we are still early stage in the testing of this functionality)
* developing new analysis mode, and improving the current ones
* improve the customization of the interface to adapt more to different kind of research subjects

We are open to suggestions, you can email us at: fcliquet@pasteur.fr
